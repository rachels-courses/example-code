// Done - do not edit

#include <iostream>
using namespace std;

#include "UTILITIES/Menu.hpp"

#include "Tester.hpp"

int main()
{
    Menu::Header( "Running tests..." );

    Tester tester;
    tester.Start();
    tester.Close();

    return 0;
}
