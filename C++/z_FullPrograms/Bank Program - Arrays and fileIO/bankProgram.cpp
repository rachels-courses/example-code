#include <iostream>     // cin cout
#include <iomanip>      // setprecision
#include <string>       // string
#include <fstream>      // File input and output
using namespace std;

int main()
{
    bool done = false;

    const int MAX_ACCOUNTS = 5;
    float accounts[MAX_ACCOUNTS];
    int userAccounts = 0;

    // Reset each account to $0
    for ( int i = 0; i < MAX_ACCOUNTS; i++ )
    {
        accounts[i] = 0;
    }

    // TODO: Load a file to read account info

    ifstream loadfile( "bank.txt" );

    loadfile >> userAccounts;
    for ( int i = 0; i < userAccounts; i++ )
    {
        loadfile >> accounts[i];
    }

    loadfile.close();

    // Main program loop
    while ( !done )
    {
        cout << endl << "---------------------------------" << endl;
        // Display all accounts
        for ( int i = 0; i < userAccounts; i++ )
        {
            cout << "Account #" << (i+1) << " = $" << fixed << setprecision( 2 ) << accounts[i] << endl;
        }
        cout << "---------------------------------" << endl;

        cout << "1. Deposit" << endl;
        cout << "2. Withdraw" << endl;
        cout << "3. Open account" << endl;
        cout << "4. Close account" << endl;
        cout << "5. Exit" << endl;
        cout << ">> ";

        int choice;
        cin >> choice;

        switch ( choice )
        {
            case 1:         // Deposit
            {
                cout << endl << "DEPOSIT" << endl;
                cout << "Deposit to which account (Account 1 - " << userAccounts << ")? (Or 0 to go back)" << endl;
                cout << ">> ";
                cin >> choice;

                if ( choice == 0 )
                {
                    // We don't need to do anything!
                    cout << "CANCELLED TRANSACTION" << endl;
                }
                else if ( choice >= 1 && choice <= userAccounts )
                {
                    cout << "UPDATE ACCOUNT" << endl;
                    int index = choice - 1;

                    cout << "Deposit amount: ";
                    float deposit;
                    cin >> deposit;

                    if ( deposit > 0 )
                    {
                        // Update the account
                        accounts[index] += deposit;
                    }
                    else
                    {
                        cout << "INVALID DEPOSIT AMOUNT" << endl;
                    }
                }
                else
                {
                    cout << "INVALID ACCOUNT NUMBER" << endl;
                }
            }
            break;

            case 2:         // Withdraw
            {
                cout << endl << "WITHDRAW" << endl;
                cout << "Withdraw from which account (Account 1 - " << userAccounts << ")? (Or 0 to go back)" << endl;
                cout << ">> ";
                cin >> choice;

                if ( choice == 0 )
                {
                    // We don't need to do anything!
                    cout << "CANCELLED TRANSACTION" << endl;
                }
                else if ( choice >= 1 && choice <= userAccounts )
                {
                    cout << "UPDATE ACCOUNT" << endl;
                    int index = choice - 1;

                    cout << "Withdraw amount: ";
                    float withdraw;
                    cin >> withdraw;

                    if ( withdraw > 0 && withdraw <= accounts[index] )
                    {
                        // Update the account
                        accounts[index] -= withdraw;
                    }
                    else
                    {
                        cout << "INVALID WITHDRAW AMOUNT" << endl;
                    }
                }
                else
                {
                    cout << "INVALID ACCOUNT NUMBER" << endl;
                }
            }
            break;

            case 3:         // Open account
            {
                if ( userAccounts < MAX_ACCOUNTS )
                {
                    cout << "New account opened" << endl;
                    userAccounts++;
                }
                else
                {
                    cout << "ALREADY HAVE MAX ACCOUNTS" << endl;
                }
            }
            break;

            case 4:         // Close account
            {
                if ( userAccounts == 0 )
                {
                    cout << "YOU HAVE NO ACCOUNTS!" << endl;
                }
                else if ( userAccounts >= 2 )
                {
                    cout << "Removing account #" << userAccounts << endl;
                    int oldIndex = userAccounts - 1;

                    cout << "Current balance is $" << fixed << setprecision( 2 ) << accounts[ oldIndex ] << endl;
                    cout << "Where do you want to move money to? (Account 1 - " << userAccounts - 1 << "): ";

                    int newAccountNumber;
                    cin >> newAccountNumber;

                    int newIndex = newAccountNumber - 1;

                    cout << "Account #" << newAccountNumber << " balance $" << fixed << setprecision( 2 ) << accounts[ newIndex ] << endl;
                    cout << "Adding $" << fixed << setprecision( 2 ) << accounts[ oldIndex ] << endl;

                    accounts[ newIndex ] += accounts[ oldIndex ];

                    userAccounts--;
                }
                else  // userAccounts == 1
                {
                    if ( accounts[0] == 0 )
                    {
                        // Remove the account
                        userAccounts--;
                    }
                    else
                    {
                        cout << "CANNOT CLOSE ACCOUNT WITH BALANCE" << endl;
                    }
                }
            }
            break;

            case 5:         // Exit
                done = true;
            break;

            default:
                cout << endl << "Invalid menu selection!" << endl;
        }
    }

    // Save bank data
    ofstream savefile( "bank.txt" );

    savefile << userAccounts << endl;

    for ( int i = 0; i < userAccounts; i++ )
    {
        savefile << accounts[i] << endl;
    }

    savefile.close();

    cout << endl << "GOODBYE" << endl;

    return 0;
}
