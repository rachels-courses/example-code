#ifndef _PROGRAM
#define _PROGRAM

class Program
{
public:
	Program();
	void Run();

	void Setup();
	void ClearBoard();
	void DrawBoard();
	bool BoardIsFull();

	bool IsTaken(int row, int col);
	int GetNumber();
	bool CanPlace(int row, int col, int number);
	void PlaceNumber(int row, int col, int number);

	void ClearScreen();
	void HitEnterToContinue();

private:
	const int WIDTH;
	const int HEIGHT;
	int m_board[9][9];
	bool m_overwritable[9][9];
};

#endif