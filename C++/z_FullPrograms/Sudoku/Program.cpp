﻿#include "Program.h"

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

Program::Program()
	: WIDTH(9), HEIGHT(9)
{

}

void Program::Setup()
{
	// Clear out board
	ClearBoard();
	
	// Set up puzzle

	string filename = "easy.txt";
	cout << "SUDOKU" << endl << endl;
	cout << "1. Easy" << endl;
	cout << "2. Hard" << endl;
	cout << endl << "CHOOSE A BOARD:" << endl;
	int choice;
	cin >> choice;
	if (choice == 1) { filename = "easy.txt"; }
	else if (choice == 2) { filename = "hard.txt"; }
	else { cout << "Invalid choice, defaulting to easy." << endl; }

	ifstream input(filename);
	int r = 0, c = 0;
	while (input >> m_board[r][c])
	{
		m_overwritable[r][c] = (m_board[r][c] == 0);
		c++;
		if (c >= WIDTH) {
			r++;
			c = 0;
		}
	}
}

void Program::Run()
{
	Setup();
	while ( true )
	{
		ClearScreen();
		DrawBoard();

		if (BoardIsFull())
		{
			cout << endl << "YOU WIN!" << endl;
			break;
		}

		int row, col;
		cout << "Enter ROW COL to add number to: ";
		cin >> row >> col;

		while (IsTaken(row, col))
		{
			cout << endl << "THAT WAS A STARTER VALUE! CANNOT OVERWRITE!" << endl;
			cout << "Enter ROW COL to add number to: ";
			cin >> row >> col;
		}

		int number = GetNumber();

		if (CanPlace(row, col, number))
		{
			PlaceNumber(row, col, number);
			cout << endl << "ADDED " << number << "!" << endl;
		}

		HitEnterToContinue();
	}
}

bool Program::BoardIsFull()
{
	for (int r = 0; r < HEIGHT; r++)
	{
		for (int c = 0; c < WIDTH; c++)
		{
			if (m_board[r][c] == 0)
			{
				return false;
			}
		}
	}
	return true;
}

void Program::PlaceNumber(int row, int col, int number)
{
	m_board[row][col] = number;
}

bool Program::CanPlace(int row, int col, int number)
{
	// ROW: Is this number already in the row?
	for (int c = 0; c < WIDTH; c++)
	{
		if (m_board[row][c] == number)
		{
			cout << "* " << number << " IS ALREADY IN THAT ROW!" << endl;
			return false;
		}
	}

	// COLUMN: Is this number already in the column?
	for (int r = 0; r < HEIGHT; r++)
	{
		if (m_board[r][col] == number)
		{
			cout << "* " << number << " IS ALREADY IN THAT COLUMN!" << endl;
			return false;
		}
	}

	// BLOCK: Is this number already in the 3x3 block?
	for (int r : { -1, 1 })
	{
		for (int c : {-1, 1})
		{
			if (row + r >= 0 && row + r < HEIGHT && col + c >= 0 && col + c < WIDTH)
			{
				// This is a valid location on the board (not out of bounds)
				if (m_board[row + r][col + c] == number)
				{
					cout << "* " << number << " IS ALREADY IN THAT BLOCK!" << endl;
					return false;
				}
			}
		}
	}
	// Ugly way:
	///* above ...*/ if (row > 0 && m_board[row - 1][col] == number) { return false; }
	///* below ...*/ if (row < HEIGHT - 1 && m_board[row + 1][col] == number) { return false; }
	///* left ....*/ if (col > 0 && m_board[row][col-1] == number) { return false; }
	///* right ...*/ if (col < WIDTH - 1 && m_board[row][col + 1] == number) { return false; }
	///* corner ..*/ if (row > 0 && col > 0 && m_board[row - 1][col - 1] == number) { return false; }
	///* corner ..*/ if (row < HEIGHT - 1 && col > 0 && m_board[row + 1][col - 1] == number) { return false; }
	///* corner ..*/ if (row < HEIGHT - 1 && col < WIDTH - 1 && m_board[row + 1][col + 1] == number) { return false; }
	///* corner ..*/ if (row > 0 && col < WIDTH - 1 && m_board[row - 1][col + 1] == number) { return false; }


	return true;
}
/*
[rc] of board:

[00][01][02] | [03][04][05] | [06][07][08]
[10][11][12] | [13][14][15] | [16][17][18]
[20][21][22] | [23][24][25] | [26][27][28]
------------------------------------------
[30][31][32] | [33][34][35] | [36][37][38]
[40][41][42] | [43][44][45] | [46][47][48]
[50][51][52] | [53][54][55] | [56][57][58]
------------------------------------------
[70][61][62] | [63][64][65] | [66][67][68]
[80][71][72] | [73][74][75] | [76][77][78]
[90][81][82] | [83][84][85] | [86][87][88]
*/

int Program::GetNumber()
{
	int num;
	cout << endl << "WHAT NUMBER DO YOU WANT TO PUT THERE? ";
	cin >> num;
	
	while (num < 1 || num > 9)
	{
		cout << endl << "NUMBER MUST BE BETWEEN 1 AND 9!" << endl;
		cout << endl << "WHAT NUMBER DO YOU WANT TO PUT THERE? ";
	}

	return num;
}

void Program::ClearBoard()
{
	for (int r = 0; r < HEIGHT; r++)
	{
		for (int c = 0; c < WIDTH; c++)
		{
			m_board[r][c] = 0; // 0 for "empty".
			m_overwritable[r][c] = true; // Can overwrite
		}
	}
}

bool Program::IsTaken(int row, int col)
{
	return (!m_overwritable[row][col]);
}

void Program::DrawBoard()
{
	for (int r = 0; r < HEIGHT; r++)
	{
		if (r != 0 && r % 3 == 0) {
			cout << string(WIDTH * 6, char(219)) << endl;
		}
		else {
			cout << string(WIDTH * 6, '-') << endl;
		}
		for (int c = 0; c < WIDTH; c++)
		{
			if (c != 0 && c % 3 == 0) {
				cout << " " << char(219) << " ";
			}
			else {
				cout << " | ";
			}
			if ( m_board[r][c] == 0 )
			{
				cout << "   ";
			}
			else
			{
				if (m_overwritable[r][c])
				{
					cout << " \x1B[33m" << m_board[r][c] << " \033[0m";
				}
				else
				{
					cout << " \x1B[31m" << m_board[r][c] << " \033[0m";
				}
				//cout << " " << m_board[r][c] << " " ;
			}
		}
		cout << endl;
		if (r == HEIGHT - 1) {
			cout << string(WIDTH * 6, '-') << endl;
		}
	}
}

void Program::ClearScreen()
{
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		system("cls");
	#else
		system("clear");
	#endif
}

void Program::HitEnterToContinue()
{
	cout << endl << "HIT ENTER TO CONTINUE" << endl;
	string a;
	cin.ignore();
	getline(cin, a);
}

int main()
{
	Program program;
	program.Run();

	return 0;
}