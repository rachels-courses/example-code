#ifndef _PROGRAM
#define _PROGRAM

#include "Account.h"
#include <vector>
#include <string>
using namespace std;

class Program
{
    public:
    void Run();

    void Menu_Main();
    void Menu_AddAccount();
    void Menu_Transaction();

    private:
    vector<Account> m_accounts;
    string m_datafile;

    void LoadData();
    void SaveData();

    void DisplaySummary();
};

#endif
