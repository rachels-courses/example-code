#include "Program.h"

#include "Utilities/Helper.hpp"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;

void Program::Run()
{
    srand( time( NULL ) );
    m_datafile = "accounts.txt";
    LoadData();
    Menu_Main();
    SaveData();
}

void Program::Menu_Main()
{
    bool done = false;
    while ( !done )
    {
        Helper::Header( "MAIN MENU" );

        DisplaySummary();

        cout << endl << "MENU OPTIONS:" << endl;
        cout << "0. EXIT" << endl;
        cout << "1. ADD ACCOUNT" << endl;
        cout << "2. TRANSACTION" << endl;

        int choice = Helper::GetIntInput( 0, 2 );

        switch( choice )
        {
            case 0: done = true; break;
            case 1: Menu_AddAccount(); break;
            case 2: Menu_Transaction(); break;
        }

        cout << string( 60, '\n' );
    }
}

void Program::Menu_AddAccount()
{
    Helper::Header( "ADD ACCOUNT" );

    Account newAccount;
    newAccount.number = rand() % 1000 + 10000;
    newAccount.balance = 0;

    cout << "How much are you depositing in the new account? $";
    cin >> newAccount.balance;

    while ( newAccount.balance < 0 )
    {
        cout << "ERROR: Balance can't be less than 0!" << endl;
        cout << "How much are you depositing in the new account? $";
        cin >> newAccount.balance;
    }

    m_accounts.push_back( newAccount );

    cout << endl << "New account added." << endl;
}

void Program::Menu_Transaction()
{
    Helper::Header( "TRANSACTION" );

    DisplaySummary();

    cout << endl << "Which account do you want to withdraw from? (ID): ";
    int id = Helper::GetIntInput( 0, m_accounts.size()-1 );

    cout << endl << "1. WITHDRAW \t 2. DEPOSIT" << endl;
    cout << "Which type of transaction? " << endl;
    int action = Helper::GetIntInput( 1, 2 );

    float amount;
    cout << "Enter amount of money: $";
    cin >> amount;

    while ( amount < 0 )
    {
        cout << "ERROR: Amount cannot be less than 0!" << endl;
        cout << "Enter amount of money: $";
        cin >> amount;
    }

    if ( action == 1 )
    {
        m_accounts[id].balance -= amount;
    }
    else if ( action == 2 )
    {
        m_accounts[id].balance += amount;
    }

    cout << endl << "Account updated" << endl;
}

void Program::LoadData()
{
    ifstream input( m_datafile );
    if ( input.fail() )
    {
        cout << "COULD NOT FIND DATA FILE " << m_datafile << endl;
        return;
    }

    int accountNumber;
    float balance;

    while ( input >> accountNumber >> balance )
    {
        Account newAccount;
        newAccount.number = accountNumber;
        newAccount.balance = balance;
        m_accounts.push_back( newAccount );
    }

    cout << endl << m_accounts.size() << " account(s) loaded." << endl;
}

void Program::SaveData()
{
    ofstream output( m_datafile );
    for ( const auto& account : m_accounts )
    {
        output << account.number << endl;
        output << account.balance << endl;
    }

    cout << endl << "Account data saved to: " << m_datafile << endl;
}

void Program::DisplaySummary()
{
    cout << left << fixed << setprecision( 2 );

    cout << "ACCOUNTS:" << endl << endl;

    cout << setw( 5 ) << "ID"
         << setw( 20 ) << "ACCT #"
         << setw( 20 ) << "BALANCE"
         << setw( 20 ) << "STATUS"
         << endl << string( 60, '-' ) << endl;

    for ( size_t i = 0; i < m_accounts.size(); i++ )
    {
        cout << setw( 5 ) << i
             << setw( 20 ) << m_accounts[i].number
             << setw( 20 ) << m_accounts[i].balance;

        if ( m_accounts[i].balance < 0 )
        {
            cout << setw( 20 ) << "OVERDRAWN";
        }

        cout << endl;
    }
}

