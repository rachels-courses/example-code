#ifndef ROOM_HPP
#define ROOM_HPP

#include <iostream>
#include <string>
using namespace std;

#include "Utilities.hpp"

enum Direction { NORTH = 0, SOUTH = 1, EAST = 2, WEST = 3 };

string GetDirectionString( Direction direction )
{
    if      ( direction == NORTH ) { return "NORTH"; }
    else if ( direction == SOUTH ) { return "SOUTH"; }
    else if ( direction == EAST )  { return "EAST"; }
    else if ( direction == WEST )  { return "WEST"; }
    else { return "UNKNOWN"; }
}

struct Room
{
    Room();
    Room( string id, string name, string description );
    void SetNeighbors( string north, string south, string east, string west );
    void Setup( string id = "", string name = "", string description = "" );

    void OutputRoomInfo();
    void OutputNeighbors();
    bool CanGo( Direction direction );
    string GetNeighborId( Direction direction );
    string GetId();

    string id;
    string name;
    string description;
    string neighborIds[4];
};

Room::Room()
{
    Setup();
}

Room::Room( string id, string name, string description )
{
    Setup( id, name, description );
}

void Room::Setup( string id /* = "" */, string name /* = ""*/, string description /* = "" */ )
{
    this->id = id;
    this->name = name;
    this->description = description;

    for ( int i = 0; i < 4; i++ )
    {
        neighborIds[i] = "NULL";
    }
}

void Room::SetNeighbors( string north, string south, string east, string west )
{
    neighborIds[NORTH] = north;
    neighborIds[SOUTH] = south;
    neighborIds[EAST] = east;
    neighborIds[WEST] = west;
}

void Room::OutputRoomInfo()
{
    Menu::ClearScreen();
    Menu::Header( name );
    cout << "\t" << description << endl;
}

void Room::OutputNeighbors()
{
    cout << "\t You can go: ";

    for ( int i = 0; i < 4; i++ )
    {
        if ( neighborIds[i] != "NULL" )
        {
            cout << GetDirectionString( Direction( i ) ) << " ";
        }
    }

    cout << endl;
}

bool Room::CanGo( Direction direction )
{
    for ( int i = 0; i < 4; i++ )
    {
        if ( direction == Direction(i) && neighborIds[i] != "NULL" )
        {
            return true;
        }
    }

    return false;
}

string Room::GetNeighborId( Direction direction )
{
    return neighborIds[ direction ];
}

string Room::GetId()
{
    return id;
}

#endif
