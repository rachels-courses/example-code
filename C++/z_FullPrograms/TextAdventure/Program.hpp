#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <iostream>
#include <fstream>
#include <map>
#include <string>
using namespace std;

#include "Room.hpp"
#include "Logger.hpp"

class Program
{
    public:
    Program();
    ~Program();

    void Run();

    private:
    bool SetupRooms();
    void CreateRoom( string id, string name, string description, string north, string south, string east, string west );
    string HandleUserInput();
    Room* m_ptrCurrentRoom;
    map<string, Room*> m_rooms;
    bool m_done;
    string m_endRoomId;
};

Program::Program()
{
    Logger::Out( "Function start", "Program::Program" );
    m_ptrCurrentRoom = nullptr;
    m_done = false;

    bool success = SetupRooms();
    if ( !success )
    {
        m_done = true;
    }
}

void Program::Run()
{
    Logger::Out( "Function start", "Program::Run" );
    string status = "";

    if ( m_done ) { Logger::Out( "Program is done preemptively", "Program::Run" ); }

    while ( !m_done )
    {
        m_ptrCurrentRoom->OutputRoomInfo();

        if ( m_ptrCurrentRoom->GetId() == m_endRoomId )
        {
            m_done = true;
            continue;
        }

        m_ptrCurrentRoom->OutputNeighbors();

        cout << endl << endl;

        status = HandleUserInput();

        cout << "\t" << status << endl;
        Menu::Pause();
    }

    cout << endl << "           The end." << endl;
    Menu::Pause();
}

bool Program::SetupRooms()
{
    string filename = "../data/maps.txt";
    Logger::Out( "Function start", "Program::SetupRooms" );
    ifstream input( filename );

    if ( !input.good() )
    {
        Logger::Out( "Error opening file", "Program::SetupRooms" );
        return false;
    }
    string buffer;

    string id, name, description, northId, southId, eastId, westId;

    while ( input >> buffer )
    {
        if ( buffer == "ROOM_BEGIN" )
        {
            input >> id;
            input.ignore();
            getline( input, name );
            getline( input, description );

            input >> buffer; // NORTH
            input >> northId;

            input >> buffer; // SOUTH
            input >> southId;

            input >> buffer; // EAST
            input >> eastId;

            input >> buffer; // WEST
            input >> westId;

            CreateRoom( id, name, description, northId, southId, eastId, westId );
        }
        else if ( buffer == "STARTING_ROOM" )
        {
            input >> buffer;
            m_ptrCurrentRoom = m_rooms[ buffer ];

            Logger::Out( "Set STARTING ROOM to " + buffer, "Program::SetupRooms" );
        }
        else if ( buffer == "ENDING_ROOM" )
        {
            input >> m_endRoomId;

            Logger::Out( "Set ENDING ROOM to " + m_endRoomId, "Program::SetupRooms" );
        }
    }

    return true;
}

void Program::CreateRoom( string id, string name, string description, string north, string south, string east, string west )
{
    Logger::Out( "Function start", "Program::CreateRoom" );
    m_rooms[id] = new Room( id, name, description );
    m_rooms[id]->SetNeighbors( north, south, east, west );
}

Program::~Program()
{
    Logger::Out( "Function start", "Program::~Program" );
    for ( auto& ptrRoom : m_rooms )
    {
        if( ptrRoom.second != nullptr )
        {
            delete ptrRoom.second;
        }
    }
}

string Program::HandleUserInput()
{
    Logger::Out( "Function start", "Program::HandleUserInput" );
    string status = "";

    string userInput = Menu::GetStringLine( "Now what?" );

    Logger::Out( "User entered \"" + userInput + "\"", "Program::HandleUserInput" );

    bool userInputMovement = false;
    for ( int i = 0; i < 4; i++ )
    {
        Direction direction = Direction( i );

        string command1 = StringUtil::ToLower( GetDirectionString( direction ) );
        string command2 = command1.substr( 0, 1 );

        if ( userInput == command1 || userInput == command2 )
        {
            userInputMovement = true;
            Logger::Out( "Try to move " + command1, "Program::HandleUserInput" );
            if ( m_ptrCurrentRoom->CanGo( direction ) )
            {
                status = "You went " + GetDirectionString( direction );
                string key = m_ptrCurrentRoom->GetNeighborId( direction );
                Logger::Out( "Neighbor ID for the " + command1 + ": " + key, "Program::HandleUserInput" );
                m_ptrCurrentRoom = m_rooms[ key ];
            }
            else
            {
                status = "You cannot go " + GetDirectionString( direction ) + " here!";
                break;
            }
        }
    }

    if ( !userInputMovement )
    {
        status = "Unknown command";
    }

    Logger::Out( "Resulting status: \"" + status + "\"", "Program::HandleUserInput" );
    return status;
}

#endif
