#include <iostream> // cin and cout
#include <iomanip>  // setprecision
#include <string>   // string
using namespace std;

int main()
{
    const float BASE_PRICE      = 8.00;
    const float SAUCE_PRICE     = 0.53;
    const float CHEESE_PRICE    = 0.73;
    const float MEATS_PRICE     = 1.25;
    const float VEGGIES_PRICE   = 0.83;

    string pizza = "";
    float price = BASE_PRICE;

    int menuChoice;

    bool done = false;
    while ( !done )
    {
        cout << endl;
        cout << "-------------------------------------------------" << endl;
        cout << "PIZZA MAKER" << endl;
        cout << endl;

        cout << "Current pizza: [" << pizza << "]" << endl;
        cout << "Current price: $" << price << endl;
        cout << endl;

        cout << "MAIN MENU" << endl;
        cout << "1. Add cheese" << endl;
        cout << "2. Sauces" << endl;
        cout << "3. Meats" << endl;
        cout << "4. Add veggies" << endl;
        cout << "5. Reset pizza" << endl;
        cout << "6. Checkout" << endl;
        cout << ">> ";
        cin >> menuChoice;

        if ( menuChoice == 1 )          // Add cheese
        {
            price += CHEESE_PRICE;
            pizza += "Cheese ";
        }
        else if ( menuChoice == 2 )     // Sauces
        {
            cout << endl << "SAUCES MENU" << endl;
            cout << "1. Tomato" << endl;
            cout << "2. Buffalo" << endl;
            cout << "3. Alfredo" << endl;
            cout << ">> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )          // Tomato
            {
                pizza += "Tomato Sauce ";
                price += SAUCE_PRICE;
            }
            else if ( menuChoice == 2 )     // Buffalo
            {
                pizza += "Buffalo Sauce ";
                price += SAUCE_PRICE;
            }
            else if ( menuChoice == 3 )     // Alfredo
            {
                pizza += "Alfredo Sauce ";
                price += SAUCE_PRICE;
            }
            else
            {
                cout << "ERROR: Invalid choice" << endl;
            }
        }
        else if ( menuChoice == 3 )     // Meats
        {
            cout << endl << "MEATS MENU" << endl;
            cout << "1. Pepperoni" << endl;
            cout << "2. Sausage" << endl;
            cout << "3. Ham" << endl;
            cout << "4. Chicken" << endl;
            cout << "5. Bacon" << endl;
            cout << ">> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )
            {
                pizza += "Pepperoni ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 2 )
            {
                pizza += "Sausage ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 3 )
            {
                pizza += "Ham ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 4 )
            {
                pizza += "Chicken ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 5 )
            {
                pizza += "Bacon ";
                price += MEATS_PRICE;
            }
            else
            {
                cout << "ERROR: Invalid choice" << endl;
            }
        }
        else if ( menuChoice == 4 )     // Veggies
        {
            cout << endl << "VEGGIES MENU" << endl;
            cout << "1. Mushrooms" << endl;
            cout << "2. Black olives" << endl;
            cout << "3. Pineapple" << endl;
            cout << "4. Tomatoes" << endl;
            cin >> menuChoice;

            if ( menuChoice == 1 )
            {
                pizza += "Mushrooms ";
                price += VEGGIES_PRICE;
            }
            else if ( menuChoice == 2 )
            {
                pizza += "Black olives ";
                price += VEGGIES_PRICE;
            }
            else if ( menuChoice == 3 )
            {
                pizza += "Pineapple ";
                price += VEGGIES_PRICE;
            }
            else if ( menuChoice == 4 )
            {
                pizza += "Tomato ";
                price += VEGGIES_PRICE;
            }
            else
            {
                cout << "ERROR: Invalid choice" << endl;
            }
        }
        else if ( menuChoice == 5 )     // Reset
        {
            pizza = "";
            price = BASE_PRICE;
            cout << endl << "Pizza was reset" << endl;
        }
        else if ( menuChoice == 6 )     // Checkout
        {
            cout << endl << "CHECKOUT" << endl;
            cout << "PRICE: $" << fixed << setprecision( 2 ) << price << endl;
            cout << "PIZZA: " << pizza << endl;
            done = true;
        }
        else
        {
            cout << "ERROR: Invalid choice" << endl;
        }
    }

    cout << endl;
    cout << "GOODBYE" << endl;

    return 0;
}
