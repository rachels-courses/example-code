#include <iostream>
using namespace std;

#include "Functions.hpp"
#include "Objects.hpp"

int main()
{
    Map gameMap;

    CreateMap( gameMap );

    Entity player;
    player.Setup( 40, 10, '@' );
    Entity enemy;
    enemy.Setup( 10, 5, '*' );

    bool done = false;
    while ( !done )
    {
        ClearScreen();
        DrawMap( gameMap, player, enemy );

        cout << "W: UP, S: DOWN, A: LEFT, D: RIGHT" << endl;
        cout << "ENTER ACTION THEN ENTER: ";
        string action;
        cin >> action;

        // Make it upper case
        action = toupper( action[0] );

        int wantToGoX = player.x;
        int wantToGoY = player.y;

        if ( action == "W" )
        {
            wantToGoY--;
        }
        else if ( action == "S" )
        {
            wantToGoY++;
        }
        else if ( action == "A" )
        {
            wantToGoX--;
        }
        else if ( action == "D" )
        {
            wantToGoX++;
        }

        if ( gameMap.IsFloor( wantToGoX, wantToGoY ) )
        {
            // It's a floor, so we can go there!
            player.x = wantToGoX;
            player.y = wantToGoY;
        }
    }

    return 0;
}
