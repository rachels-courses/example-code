#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP

#include "Objects.hpp"

#include <cstdlib>

void CreateMap( Map& gameMap )
{
    for ( int y = 0; y < gameMap.HEIGHT; y++ )
    {
        for ( int x = 0; x < gameMap.WIDTH; x++ )
        {
            if ( y == 0 || y == gameMap.HEIGHT-1 || x == 0 || x == gameMap.WIDTH - 1 )
            {
                // If it's an edge, make it a wall
                gameMap.tiles[x][y] = gameMap.wall;
            }
            else
            {
                // Floor
                gameMap.tiles[x][y] = gameMap.floor;
            }
        }
    }
}


void DrawMap( const Map& gameMap, const Entity& player, const Entity& enemy )
{
    for ( int y = 0; y < gameMap.HEIGHT; y++ )
    {
        for ( int x = 0; x < gameMap.WIDTH; x++ )
        {
            // If player is here, draw that instead of map tile
            if ( player.x == x && player.y == y )
            {
                cout << player.symbol;
            }
            // If enemy is here, draw that instead of map tile
            else if ( enemy.x == x && enemy.y == y )
            {
                cout << enemy.symbol;
            }
            // Otherwise just draw the map tile
            else
            {
                cout << gameMap.tiles[x][y];
            }
        }
        cout << endl; // end of row
    }
}

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

#endif
