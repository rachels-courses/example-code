#include <iostream>
using namespace std;

template <typename T>
void OutputArray( T arr[], int size )
{
    for ( int i = 0; i < size; i++ )
    {
        cout << "arr[" << i << "] = " << arr[i] << endl;
    }
}

template <typename T>
T Sum( T arr[], int size )
{
    T sum = 0;
    for ( int i = 0; i < size; i++ )
    {
        sum += arr[i];
    }
    return sum;
}


int main()
{
    cout << "SUMMING AN INT ARRAY" << endl;
    int intArr[] = { 1, 2, 3, 4, 5 };
    int intSum = Sum( intArr, 5 );
    OutputArray( intArr, 5 );
    cout << "Sum is " << intSum << endl << endl;
    
    cout << "SUMMING A FLOAT ARRAY" << endl;
    float floatArr[] = { 0.25, 0.35, 0.47, 0.33 };
    float floatSum = Sum( floatArr, 4 );
    OutputArray( floatArr, 4 );
    cout << "Sum is " << floatSum << endl;
    
    
    return 0;
}
