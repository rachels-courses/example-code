#ifndef _TEMPLATED_ARRAY
#define _TEMPLATED_ARRAY

#include <iostream>
#include <stdexcept>
using namespace std;

template <typename T>
class TemplatedArray
{
    public:
    TemplatedArray();
    TemplatedArray( int size );
    ~TemplatedArray();

    void PushToBack( T item );
    void RemoveFromBack();

    bool IsFull();
    bool IsEmpty();

    void Display();
    int Size();

    private:
    void AllocateMemory( int size );
    void DeallocateMemory();

    int m_arraySize;
    int m_storedItems;
    T* m_array;
};


template <typename T>
TemplatedArray<T>::TemplatedArray()
{
    cout << endl << "Function: Default constructor" << endl;
    m_arraySize = 0;
    m_storedItems = 0;
    m_array = nullptr;
}

template <typename T>
TemplatedArray<T>::TemplatedArray( int size )
{
    cout << endl << "Function: Constructor with size " << size << endl;
    m_array = nullptr;
    AllocateMemory( size );
}

template <typename T>
TemplatedArray<T>::~TemplatedArray()
{
    cout << endl << "Function: Destructor" << endl;
    DeallocateMemory();
}

template <typename T>
void TemplatedArray<T>::PushToBack( T item )
{
    cout << endl << "Function: PushToBack item " << item << endl;
    if ( IsFull() )
    {
        throw runtime_error( "Array is full!" );
    }
    if ( m_array == nullptr )
    {
        AllocateMemory( 10 );
    }

    m_array[ m_storedItems ] = item;
    m_storedItems++;
}

template <typename T>
void TemplatedArray<T>::RemoveFromBack()
{
    cout << endl << "Function: RemoveFromBack" << endl;
    if ( IsEmpty() )
    {
        throw runtime_error( "Array is empty!" );
    }

    // Lazy deletion
    m_storedItems--;
}

template <typename T>
bool TemplatedArray<T>::IsFull()
{
    cout << endl << "Function: IsFull?" << endl;
    return ( m_arraySize == m_storedItems );
}

template <typename T>
bool TemplatedArray<T>::IsEmpty()
{
    cout << endl << "Function: IsEmpty?" << endl;
    return ( m_storedItems == 0 );
}

template <typename T>
void TemplatedArray<T>::Display()
{
    cout << endl << "Function: Display" << endl;
    for ( int i = 0; i < m_storedItems; i++ )
    {
        cout << i << ". " << m_array[i] << endl;
    }
}

template <typename T>
int TemplatedArray<T>::Size()
{
    cout << endl << "Function: Size" << endl;
    return m_storedItems;
}

template <typename T>
void TemplatedArray<T>::AllocateMemory( int size )
{
    cout << endl << "Function: AllocateMemory with size " << size << endl;
    // Clear out any memory currently stored
    DeallocateMemory();

    m_array = new T[ size ];
    m_arraySize = size;
    m_storedItems = 0;
}

template <typename T>
void TemplatedArray<T>::DeallocateMemory()
{
    cout << endl << "Function: DeallocateMemory" << endl;
    // Free the memory allocated
    if ( m_array != nullptr )
    {
        delete [] m_array;
        m_array = nullptr;
        m_arraySize = 0;
        m_storedItems = 0;
    }
}

#endif
