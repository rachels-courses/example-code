import java.util.Scanner;

public class DoWhile
{
    public static void main()
    {
        Scanner input = new Scanner( System.in );
        boolean done = true;

        // Run the internal code at least once.
        // If the criteria is TRUE, loop through again.
        do
        {
            System.out.print( "Done? (y/n): ");
            String choice;
            choice = input.next();

            if ( choice.equals( "y" ) )
            {
                done = true;
            }
            else
            {
                done = false;
            }

        } while ( !done );

        System.out.println( "Bye" );
    }

}
