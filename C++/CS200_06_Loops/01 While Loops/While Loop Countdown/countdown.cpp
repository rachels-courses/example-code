#include <iostream>
using namespace std;

int main() {
  int counter;

  cout << "Count down from what #? ";
  cin >> counter;

  while (counter > 0) {
    
    // the \t is a tab character.
    cout << "counter is now: " << counter << endl;
    counter--;
  }

  cout << endl << "Final counter value: " << counter << endl;

  return 0;
}
