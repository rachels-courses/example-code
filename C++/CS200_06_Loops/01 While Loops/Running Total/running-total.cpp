#include <iostream>
using namespace std;

int main() {
  cout << "RUNNING TOTAL" << endl;
  
  float total = 0;
  float input;
  char again = 'y';

  while ( again == 'y' )
    {
      cout << endl << "total is " << total << ", enter number to add: ";
      cin >> input;

      total += input;
      
      cout << "Add another number? (y/n): ";
      cin >> again;
      again = tolower(again);
    }

  cout << endl << "FINAL TOTAL: " << total << endl;
  
  return 0;
}