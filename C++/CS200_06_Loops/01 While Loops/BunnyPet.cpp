#include <iostream>
#include <string>
using namespace std;

int main()
{
  int health = 100;
  int hunger = 0;
  int happiness = 100;
  bool game_running = true;
  int time = 0;
  string name = "Bunny";
  cout << "What is your bunny's name? ";
  getline( cin, name );

  while ( game_running )
    {
      time++;
      cout << string( 80, '-' ) << endl;
      cout << string( 40, '\n' );
      cout << endl << endl << "Time: " << time << endl << endl;

      cout << "BUNNY: " << name << endl;
      cout << "* HEALTH: %" << health << endl;
      cout << "* HUNGER: %" << hunger << endl;
      cout << "* HAPPY:  %" << happiness << endl;
      cout << endl;

      // Draw bunny
      if ( health <= 0 )
      {
        cout << name << " has died!" << endl;
        cout << " ________"  << endl;
        cout << "/_( x.x)_\\"  << endl;
        cout << "O(\")(\")O"  << endl;
        // Game over
        game_running = false;
        continue;
      }
      else if ( health >= 50 && hunger <= 50 && happiness >= 50 )
      {
        cout << name << " is alright!" << endl;
        cout << "(\\/)"  << endl;
        cout << "( n.n)"  << endl;
        cout << "c(\")(\")"  << endl;
      }
      else if ( health < 50 )
      {
        cout << name << " is feeling sick!" << endl;
        cout << "(\\/)"  << endl;
        cout << "( T.T)"  << endl;
        cout << "c(\")(\")"  << endl;        
      }
      else if ( hunger > 50 )
      {
        cout << name << " is feeling hungry!" << endl;
        cout << "(\\/)"  << endl;
        cout << "( >.<)"  << endl;
        cout << "c(\")(\")"  << endl;        
      }
      else if ( happiness < 50 )
      {
        cout << name << " is feeling unhappy!" << endl;
        cout << "(\\/)"  << endl;
        cout << "( -.-)"  << endl;
        cout << "c(\")(\")"  << endl;        
      }

      // Main menu
      cout << endl;
      cout << "  0. [ABANDON] (quit game)" << endl;
      cout << "  1. [PLAY] Number guesser" << endl;
      cout << "  2. [FEED] Carrot" << endl;
      cout << "  3. [FEED] Bread" << endl;
      cout << "  4. [FEED] Cake" << endl;
      cout << "  5. [HEAL] Medicine" << endl;
      cout << endl;
      cout << "CHOICE: ";
      int choice;
      cin >> choice;

      cout << endl;
      cout << string( 40, '\n' );
      if ( choice == 0 ) // quit
      {
        game_running = false;
        continue;
      }
      else if ( choice == 1 ) // play
      {
        cout << "PLAY NUMBER GUESSER" << endl << endl;
        if ( health < 50 )
        {
          cout << name << " is too sick to play!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( T.T)"  << endl;
          cout << "c(\")(\")"  << endl;
        }
        else
        {
          cout << name << " had fun!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( n.n)"  << endl;
          cout << "c(\")(\")"  << endl;

          happiness += 10;
          hunger += 5;
        }
      }
      else if ( choice == 2 ) // carrot
      {
        cout << "EAT CARROT" << endl << endl;
        if ( hunger >= 100 )
        {
          cout << name << " is too full to eat!!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( >.<)"  << endl;
          cout << "c(\")(\")"  << endl;    
          happiness -= 5;
        }
        else
        {
          cout << name << " ate a carrot!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( '.')"  << endl;
          cout << "c(\")(\")"  << endl;
          
          hunger -= 5;
          health += 5;
        }
      }
      else if ( choice == 3 ) // bread
      {
        cout << "EAT BREAD" << endl << endl;
        if ( hunger >= 100 )
        {
          cout << name << " is too full to eat!!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( >.<)"  << endl;
          cout << "c(\")(\")"  << endl;    
          happiness -= 5;
        }
        else
        {
          cout << name << " didn't like the bread..." << endl;
          cout << "(\\/)"  << endl;
          cout << "( T.T)"  << endl;
          cout << "c(\")(\")"  << endl;
          
          hunger -= 5;
          happiness -= 2;
        }
      }
      else if ( choice == 4 ) // cake
      {
        cout << "EAT CAKE" << endl << endl;
        if ( hunger >= 100 )
        {
          cout << name << " is too full to eat!!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( >.<)"  << endl;
          cout << "c(\")(\")"  << endl;    
          happiness -= 5;
        }
        else
        {
          cout << name << " scarfed down the cake!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( n.n)"  << endl;
          cout << "c(\")(\")"  << endl;
          
          hunger -= 10;
          health -= 10;
          happiness += 10;
        }
      }
      else if ( choice == 5 ) // medicine
      {
        cout << "TAKE MEDICINE" << endl << endl;
        if ( health >= 50 )
        {
          cout << name << " is doesn't feel sick!!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( >.<)"  << endl;
          cout << "c(\")(\")"  << endl;    
          happiness -= 15;
        }
        else
        {
          cout << name << " took the medicine!" << endl;
          cout << "(\\/)"  << endl;
          cout << "( T.T)"  << endl;
          cout << "c(\")(\")"  << endl;
          
          health += 25;
          happiness -= 10;          
        }
      }

      // Don't let any numbers go above 100 or below 0
      if ( health < 0 ) { health = 0; }
      else if ( health > 100 ) { health = 100; }

      if ( hunger < 0 ) { hunger = 0; }
      else if ( hunger > 100 ) { hunger = 100; }

      if ( happiness < 0 ) { happiness = 0; }
      else if ( happiness > 100 ) { happiness = 100; }

      cout << endl;
      cout << "PRESS ENTER TO CONTINUE" << endl;
      string a;
      cin.ignore();
      getline( cin, a );
    }

  cout << string( 40, '\n' );
  cout << "(\\/)"  << endl;
  cout << "( T.T)"  << endl;
  cout << "c(\")(\")"  << endl;
  cout << endl << "How could you?!" << endl;
  return 0;
}
 
