#include <iostream>
using namespace std;

int main() {
  bool done = false;
  while ( !done )
    {
      cout << endl << "MAIN MENU" << endl;
      cout << "1. Favorite book" << endl;
      cout << "2. Favorite movie" << endl;
      cout << "3. Quit" << endl;
      
      cout << endl << "Choice: ";
      int choice;
      cin >> choice;

      if ( choice == 1 )
      {
        cout << "Favorite book: Masters of Doom" << endl;
      }
      else if ( choice == 2 )
      {
        cout << "Favorite movie: The Lost Skeleton of Cadavra" << endl;
      }
      else if ( choice == 3 )
      {
        cout << "Goodbye!" << endl;
        done = true;
      }
      else
      {
        cout << "Invalid selection!" << endl;
      }
    }

  return 0;
}