#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
	cout << setprecision( 2 ) << fixed;
	bool running = true;

	const float price_burrito = 1.59;
	const float price_taco = 0.79;
	const float price_quesa = 3.50;

	const float tax = 0.091;

	float total = 0;

	while ( running )
	{
		cout << endl << "-------------------------" << endl;
		cout << "MAIN MENU" << endl;
		cout << "1. Burrito     $" << price_burrito << endl;
		cout << "2. Taco        $" << price_taco << endl;
		cout << "3. Quesadilla  $" << price_quesa << endl;
		cout << "4. Checkout" << endl << endl;

		cout << "RUNNING TOTAL: $" << total << endl << endl;

		int choice;
		cout << "CHOICE: ";
		cin >> choice;

		if ( choice == 1 ) // burrito
		{
			total += price_burrito;
		}
		else if ( choice == 2 ) // taco
		{
			total += price_taco;
		}
		else if ( choice == 3 ) // quesadilla
		{
			total += price_quesa;
		}
		else if ( choice == 4 ) // quit
		{
			running = false;
		}
		else // unknown
		{
			cout << "UNKNOWN COMMAND" << endl;
		}
	}

	// RECEIPT
	total = total + ( total * tax );
	cout << endl << "TOTAL PRICE: $" << total << endl;
	cout << endl << "THANK YOU FOR SHOPPING AT TACOPLACE" << endl;

	return 0;
}
