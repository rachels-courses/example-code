#include <iostream>
#include <string>
using namespace std;

int main()
{
    int totalStudents;
    string studentNames = "";
    string name;

    cout << "How many students? ";
    cin >> totalStudents;

    for ( int i = 0; i < totalStudents; i++ )
    {
        cout << "Enter name of student " << i << ": ";
        cin >> name;

        studentNames += name + " ";
    }

    cout << endl;
    cout << "Students: " << studentNames << endl;

    return 0;
}
