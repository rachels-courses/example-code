#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Node.hpp"

void OneNode()
{
    cout << endl << "ONE NODE" << endl;
    cout << left
        << setw( 10 ) << "data"
        << setw( 20 ) << "prev"
        << setw( 20 ) << "next" << endl;
    cout << "----------------------------------------------" << endl;

    Node<string> node;
    node.data = "one node";

    node.Display();
}

void TwoNodes()
{
    cout << endl << "TWO NODES" << endl;
    cout << left
        << setw( 10 ) << "data"
        << setw( 20 ) << "prev"
        << setw( 20 ) << "next" << endl;
    cout << "----------------------------------------------" << endl;

    Node<string> node1, node2;
    node1.data = "first";
    node2.data = "second";

    node1.ptrNext = &node2;

    node2.ptrPrev = &node1;

    node1.Display();
    node2.Display();
}

void ThreeNodes()
{
    cout << endl << "THREE NODES" << endl;
    cout << left
        << setw( 10 ) << "data"
        << setw( 20 ) << "prev"
        << setw( 20 ) << "next" << endl;
    cout << "----------------------------------------------" << endl;

    Node<string> node1, node2, node3;
    node1.data = "first";
    node2.data = "second";
    node3.data = "third";

    node1.ptrNext = &node2;

    node2.ptrPrev = &node1;
    node2.ptrNext = &node3;

    node3.ptrPrev = &node2;

    node1.Display();
    node2.Display();
    node3.Display();
}

int main()
{
    OneNode();
    TwoNodes();
    ThreeNodes();

    return 0;
}
