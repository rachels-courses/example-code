#include <iostream> // Console I/O
#include <fstream> // File I/O
#include <string>
using namespace std;

int main()
{

	const int CART_SIZE = 10;
	int cartItems = 0;
	string cart[CART_SIZE];

	const int PRODUCT_SIZE = 5;
	string products[PRODUCT_SIZE];

	// Load products from store into this array
	ifstream input("products.txt");
	for (int i = 0; i < PRODUCT_SIZE; i++)
	{
		getline(input, products[i]);
	}

	cout << "Menu: " << endl;
	for (int i = 0; i < PRODUCT_SIZE; i++)
	{
		cout << i << ". " << products[i] << endl;
	}

	bool done = false;
	while (!done)
	{
		cout << "Enter # of item to add to cart, or -1 to stop: ";
		int index;
		cin >> index;

		if (index == -1)
		{
			done = true;
		}
		else if ( index >= 0 && index < PRODUCT_SIZE )
		{
			if (cartItems == CART_SIZE)
			{
				cout << "ERROR - Cart is full!" << endl << endl;
			}
			else
			{
				cart[cartItems] = products[index];
				cartItems++;
				cout << "Item added to cart." << endl << endl;
			}
		}
	}

	// Output items in cart to a text file, too.
	ofstream output("receipt.txt");

	cout << endl << "ITEMS IN YOUR CART:" << endl;
	for (int i = 0; i < CART_SIZE; i++)
	{
		// Display item to screen
		cout << i << ". " << cart[i] << endl;

		// Write item to text file
		output << i << ". " << cart[i] << endl;
	}

	output.close();

	cout << "receipt.txt created in your PROJECT DIRECTORY" << endl;

	return 0;
}