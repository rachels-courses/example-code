#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main() {
  cout << "TO DO LIST" << endl << endl;

  string item1, item2, item3;

  cout << "Enter first item on todo list: ";
  getline(cin, item1);

  cout << "Enter second item on todo list: ";
  getline(cin, item2);

  cout << "Enter third item on todo list: ";
  getline(cin, item3);

  // Write out to do list to file
  ofstream output("todo.txt");
  output << item1 << endl;
  output << item2 << endl;
  output << item3 << endl;

  cout << endl << "Saved to \"todo.txt\"." << endl;
  return 0;
}