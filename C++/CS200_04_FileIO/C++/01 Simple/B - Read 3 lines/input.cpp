#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main() {
  ifstream input( "data.txt" );

  string line1, line2, line3;
  
  // Just read the first 3 lines out of the file.
  getline(input, line1);
  getline(input, line2);
  getline(input, line3);

  // Display what we read out of the file
  cout << "line1: " << line1 << endl << endl;
  cout << "line2: " << line2 << endl << endl;
  cout << "line3: " << line3 << endl << endl;
  
  return 0;
}