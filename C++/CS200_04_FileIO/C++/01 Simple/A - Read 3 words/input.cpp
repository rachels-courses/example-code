#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main() {
  ifstream input( "data.txt" );

  string word1, word2, word3;
  
  // Just read the first 3 words out of the file.
  input >> word1;
  input >> word2;
  input >> word3;

  // Display what we read out of the file
  cout << "word1: " << word1 << endl;
  cout << "word2: " << word2 << endl;
  cout << "word3: " << word3 << endl;
  
  return 0;
}