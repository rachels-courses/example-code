#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main() {
  cout << "MY CLASSES" << endl << endl;

  string course1, course2, course3;
  
  // Read the student's classes out of the text file
  ifstream input("data.txt");

  getline(input, course1);
  getline(input, course2);
  getline(input, course3);

  // Done loading; close file
  input.close();

  cout << "Courses loaded: " << endl;
  cout << "* course1: " << course1 << endl;
  cout << "* course2: " << course2 << endl;
  cout << "* course3: " << course3 << endl;

  cout << endl << "Update courses: " << endl;
  
  cout << "* New value for course1: ";
  getline(cin, course1);
  
  cout << "* New value for course2: ";
  getline(cin, course2);
  
  cout << "* New value for course3: ";
  getline(cin, course3);

  // Write out new data
  ofstream output("data.txt");
  
  output << course1 << endl;
  output << course2 << endl;
  output << course3 << endl;

  output.close();

  cout << endl << "DATA SAVED." << endl;

  return 0;
}