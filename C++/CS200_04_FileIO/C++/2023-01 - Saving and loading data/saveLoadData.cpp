#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
	string heroName;
	int heroLevel;
	float heroMoney;

	ifstream input("savegame.txt");
	if (input.fail())
	{
		cout << "No game file found. Creating new one..." << endl;
		heroName = "UNNAMED";
		heroLevel = 1;
		heroMoney = 0;
	}
	else
	{
		getline(input, heroName);
		input >> heroLevel;
		input >> heroMoney;
	}

	// Done loading
	input.close();

	cout << endl << "SAVE GAME DATA" << endl;
	cout << "Hero name: " << heroName << endl;
	cout << "Level: " << heroLevel << endl;
	cout << "Money: " << heroMoney << " gil" << endl;
	cout << endl;

	cout << "1. Update name" << endl;
	cout << "2. +1 level" << endl;
	cout << "3. +100 money" << endl;
	int choice;
	cout << "CHOICE: ";
	cin >> choice;

	switch (choice)
	{
	case 1:
		cout << "ENTER NEW NAME: ";
		cin.ignore();
		getline(cin, heroName);
		break;

	case 2:
		heroLevel++;
		cout << "Level is now " << heroLevel << endl;
		break;

	case 3:
		heroMoney += 100;
		cout << "Money is now " << heroMoney << " gil" << endl;
		break;
	}

	// Save the game data
	ofstream output("savegame.txt");
	// Save data in the same order we loaded it
	output << heroName << endl;
	output << heroLevel << endl;
	output << heroMoney << endl;

	cout << endl << "savegame.txt updated" << endl;

	output.close();

	return 0;
}