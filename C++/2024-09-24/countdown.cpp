#include <iostream>
using namespace std;

int main()
{
    int daysLeft = 0;

    cout << "How many days left until Diwali? ";
    cin >> daysLeft;

    while ( daysLeft > 0 )
    {
        cout << "There are... " << daysLeft << " days left until Diwali!" << endl;
        daysLeft--;
    }

    cout << "HAPPY DIWALI!" << endl;

    return 0;
}