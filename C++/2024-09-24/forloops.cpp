#include <iostream>
using namespace std;

int main()
{
    int daysLeft = 0;

    cout << "How many days left until Diwali? ";

    //while ( daysLeft > 0 )
    for ( daysLeft = 100; daysLeft > 0; daysLeft-- )
    {
        cout << "There are... " << daysLeft << " days left until Diwali!" << endl;
        daysLeft--;
    }

    cout << "HAPPY DIWALI!" << endl;

    for ( int i = 0; i < 100; i++ )
    {
        cout << i << endl;
    }

    return 0;
}