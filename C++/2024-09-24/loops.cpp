#include <iostream>
using namespace std;

int main()
{
    bool running = true;
    float a, b;
    char op;

    while ( running )
    {
        cout << "Enter two numbers: ";
        cin >> a >> b;
        cout << "Enter a math operation, or x to quit: ";
        cin >> op;
        
        if ( op == 'x' )
        {
            running = false;
        }
        else if ( op == '+' )
        {
            cout << a + b << endl;
        }
    }

    return 0;
}