#include <iostream>
using namespace std;

int main()
{
    int start, end, sum = 0;

    cout << "Enter a start and end value: ";
    cin >> start >> end;

    int counter = start;

    while ( counter <= end )
    {
        sum += counter; // sum = sum + counter
        cout << "SUM is now: " << sum << endl;
        counter++;
    }

    cout << "LOOP OVER, SUM is " << sum << endl;

    return 0;
}