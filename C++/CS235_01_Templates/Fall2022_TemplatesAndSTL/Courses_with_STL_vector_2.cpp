#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Course
{
	Course() { }

	Course(string c, string t, int ch)
	{
		code = c;
		title = t;
		creditHours = ch;
	}

	string code;
	string title;
	int creditHours;
};

int main()
{
	vector<Course> courses;
	courses.push_back(Course("CS134", "Programming Fundamentals", 3));
	courses.push_back(Course("CS200", "Concepts of Programming", 4));
	courses.push_back(Course("CS235", "Object Oriented Programming", 4));
	courses.push_back(Course("CS250", "Data Structures", 4));

	for (size_t i = 0; i < courses.size(); i++)
	{
		cout << "Item #" << i << " is " 
			<< courses[i].code << ", "
			<< courses[i].creditHours << " credit hours, "
			<< courses[i].title << endl;
	}

	return 0;
}
