#include <iostream>
#include <string>
#include <map>
using namespace std;

struct Course
{
	Course() { }

	Course(string c, string t, int ch)
	{
		code = c;
		title = t;
		creditHours = ch;
	}

	string code;
	string title;
	int creditHours;
};

int main()
{
    /* MyVector class file not included. */
    MyVector<Course> courses;
	courses.PushBack(Course("CS134", "Programming Fundamentals", 3));
	courses.PushBack(Course("CS200", "Concepts of Programming", 4));
	courses.PushBack(Course("CS235", "Object Oriented Programming", 4));
	courses.PushBack(Course("CS250", "Data Structures", 4));

	for (size_t i = 0; i < courses.GetSize(); i++)
	{
		cout << "Item #" << i << " is "
			<< courses.GetAt(i).code << ", "
			<< courses.GetAt(i).creditHours << " credit hours, "
			<< courses.GetAt(i).title << endl;
	}
	return 0;
}
