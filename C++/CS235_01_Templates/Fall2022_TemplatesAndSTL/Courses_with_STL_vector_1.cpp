#include <iostream>
#include <string>
#include <vector>
using namespace std;


int main()
{
	vector<string> courseCodes;

	courseCodes.push_back("CS134");
	courseCodes.push_back("CS200");
	courseCodes.push_back("CS235");
	courseCodes.push_back("CS250");

	// Normal for loop
	for (size_t i = 0; i < courseCodes.size(); i++)
	{
		cout << "Item #" << i << " is " << courseCodes[i] << endl;
	}

	return 0;
}
