#include <iostream>
#include <string>
#include <map>
using namespace std;

struct Course
{
	Course() { }

	Course(string c, string t, int ch)
	{
		code = c;
		title = t;
		creditHours = ch;
	}

	string code;
	string title;
	int creditHours;
};

int main()
{
	map<string, Course> courses;
	courses["CS134"] = Course("CS134", "Programming Fundamentals", 3);
	courses["CS200"] = Course("CS200", "Concepts of Programming", 4);
	courses["CS235"] = Course("CS235", "Object Oriented Programming", 4);
	courses["CS250"] = Course("CS250", "Data Structures", 4);

	cout << "Enter a course key: ";
	string key;
	cin >> key;

	try
	{
		cout << courses.at(key).code << ", "
			<< courses.at(key).creditHours << " hours "
			<< courses.at(key).title << endl;
	}
	catch (const out_of_range& ex)
	{
		cout << "ERROR: " << ex.what() << endl;
	}

	return 0;
}
