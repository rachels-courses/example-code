#include <iostream>
#include <vector>
#include <string>
using namespace std;

void Swap( string& a, string& b )
{
  string c = a;
  a = b;
  b = c;
}

void SelectionSort( vector<string>& data )
{
  for ( size_t i = 0; i < data.size() - 1; i++ )
    {
      size_t min_index = i;
      for ( size_t j = i + 1; j < data.size(); j++ )
        {
          if ( data[j] < data[min_index] )
            {
              min_index = j;
            }
        }

      if ( min_index != i )
        {
          Swap( data[i], data[min_index] );
        }
    }
}

void InsertionSort( vector<string>& data )
{
  size_t i = 1;
  while ( i < data.size() )
    {
      int j = i;
      while ( j > 0 && data[j-1] > data[j] )
        {
          Swap( data[j], data[j-1] );
          j = j - 1;
        }
      i = i + 1;
    }
}

void BubbleSort( vector<string>& data )
{
  for ( size_t i = 0; i < data.size() - 1; i++ )
    {
      for ( size_t j = 0; j < data.size() - i - 1; j++ )
        {
          if ( data[j] > data[j+1] )
            {
              Swap( data[j], data[j+1] );
            }
        }
    }
}

int LinearSearch( const vector<string>& data, string find_me )
{
  for ( size_t i = 0; i < data.size(); i++ )
    {
      if ( data[i].find( find_me ) != string::npos )
        {
          return i;
        }
    }
  return -1;
}

void PrintVector( const vector<string>& data )
{
  for ( size_t i = 0; i < data.size(); i++ )
    {
      cout << i << ". " << data[i] << "\t";
    }
  cout << endl;
}

int main()
{
  vector<string> unsortedData = { "CS 200", "CHEM 100", "THEA 120", "ASL 120" };

  cout << "UNSORTED DATA:" << endl;
  PrintVector( unsortedData );

  vector<string> copy = unsortedData;
  SelectionSort( copy );
  cout << endl << "AFTER SELECTION SORT:" << endl;
  PrintVector( copy );

  copy = unsortedData;
  InsertionSort( copy );
  cout << endl << "AFTER INSERTION SORT:" << endl;
  PrintVector( copy );

  copy = unsortedData;
  BubbleSort( copy );
  cout << endl << "AFTER BUBBLE SORT:" << endl;
  PrintVector( copy );

  cout << endl << "SEARCH" << endl;
  cout << "Enter an item to look for: ";
  string find;
  getline( cin, find );
  int result = LinearSearch( unsortedData, find );

  cout << "Found at index " << result << " in the unsorted vector" << endl;

  return 0;
}
