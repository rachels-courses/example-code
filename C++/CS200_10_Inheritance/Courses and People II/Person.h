#ifndef _PERSON_H
#define _PERSON_H

#include <string>
using namespace std;

class Person    // Parent class
{
public:
    void Setup( string newFullName, string newPhoneNumber, int newId );
    void SetupWizard();
    void Display();

protected:
    string fullName;
    string phoneNumber;
    int id;
};

class Teacher : public Person       // Teacher inherits from Person
{
public:
    void Setup( string newFullName, string newPhoneNumber, int newId, float newSalary );
    void Display();

protected:
    float salary;
};

class Student : public Person       // Student inherits from Person
{
public:
    void Setup( string newFullName, string newPhoneNumber, int newId, float newGpa );
    void Display();

protected:
    float gpa;
};

#endif
