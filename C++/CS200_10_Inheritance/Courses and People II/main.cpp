#include <iostream>
#include <array>
using namespace std;

#include "Course.h"
#include "Person.h"

int main()
{
    array<Course, 3> courses;
    courses[0].Setup( "Concepts of Programming", "CS", 200, 11474, 378 );
    courses[1].Setup( "Object-Oriented Programming", "CS", 235, 11486, 350 );
    courses[2].Setup( "Basic Data Structures", "CS", 250, 11489, 377 );

    array<Teacher, 3> teachers;
    teachers[0].Setup( "Rai S",     "123-456-7899", 123, 50000 );
    teachers[1].Setup( "Anuj S",    "123-456-1000", 124, 63000 );
    teachers[2].Setup( "Rebekah G", "123-456-5000", 125, 75000 );

    array<Student, 5> students;
    students[0].Setup( "John Carmack",      "123-123-0040", 126, 4.0 );
    students[1].Setup( "Stephanie Shirley", "123-123-2722", 127, 3.9 );
    students[2].Setup( "Ron Gilbert",       "123-123-0152", 129, 3.5 );
    students[3].Setup( "Grace Hopper",      "123-123-7893", 130, 3.6 );
    students[4].Setup( "Ada Lovelace",      "123-123-4505", 160, 2.9 );

    courses[0].AssignTeacher( teachers[0] );
    courses[0].RegisterStudent( students[0] );
    courses[0].RegisterStudent( students[1] );
    courses[0].RegisterStudent( students[2] );

    courses[1].AssignTeacher( teachers[1] );
    courses[1].RegisterStudent( students[3] );
    courses[1].RegisterStudent( students[4] );
    courses[1].RegisterStudent( students[2] );

    courses[2].AssignTeacher( teachers[2] );
    courses[2].RegisterStudent( students[1] );
    courses[2].RegisterStudent( students[3] );
    courses[2].RegisterStudent( students[0] );

    bool done = false;
    while ( !done )
    {
        cout << string( 80, '-' ) << endl;

        for ( unsigned int i = 0; i < courses.size(); i++ )
        {
            cout << "COURSE #" << i << endl;
            courses[i].Display();
        }

        cout << endl;

        cout << "1. Add student to class" << endl;
        cout << "2. Assign teacher to class" << endl;
        cout << "3. Quit" << endl;

        int choice;
        cin >> choice;

        if ( choice == 1 )
        {
            Student newStudent;
            newStudent.SetupWizard();

            cout << "Add student to which course? ";
            int index;
            cin >> index;

            courses[index].RegisterStudent( newStudent );
        }
        else if ( choice == 2 )
        {
            // Display available teachers
            for ( unsigned int i = 0; i < teachers.size(); i++ )
            {
                cout << "TEACHER #" << i << "\t";
                teachers[i].Display();
            }

            cout << "Which teacher? ";
            int teacherIndex;
            cin >> teacherIndex;

            cout << "Which course? ";
            int courseIndex;
            cin >> courseIndex;

            courses[courseIndex].AssignTeacher( teachers[teacherIndex] );
        }
        else if ( choice == 3 )
        {
            done = true;
        }
    }

    return 0;
}
