#include "Person.h"

#include <iostream>
#include <string>
using namespace std;

void Person::Setup( string newFullName, string newPhoneNumber, int newId )
{
    fullName = newFullName;
    phoneNumber = newPhoneNumber;
    id = newId;
}

void Person::SetupWizard()
{
    cout << "Enter full name: ";
    cin.ignore();
    getline( cin, fullName );

    cout << "Enter phone #: ";
    cin >> phoneNumber;

    cout << "Enter ID: ";
    cin >> id;
}

void Person::Display()
{
    cout << id << " " << phoneNumber << "\t" << fullName;
}

void Teacher::Setup( string newFullName, string newPhoneNumber, int newId, float newSalary )
{
    Person::Setup( newFullName, newPhoneNumber, newId );
    salary = newSalary;
}

void Teacher::Display()
{
    Person::Display();
    cout << "\t Salary: $" << salary << endl;
}
void Setup( string newFullName, string newPhoneNumber, int newId, float newSalary );

void Student::Setup( string newFullName, string newPhoneNumber, int newId, float newGpa )
{
    Person::Setup( newFullName, newPhoneNumber, newId );
    gpa = newGpa;
}

void Student::Display()
{
    Person::Display();
    cout << "\t GPA: " << gpa << endl;
}
