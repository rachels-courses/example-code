#include <iostream>
#include <vector>
using namespace std;

#include "Person.h"

int main()
{
    Student student;
    student.SetName( "Rose" );
    student.SetGpa( 3.5 );

    Teacher teacher;
    teacher.SetName( "Rhonda" );
    teacher.SetDepartment( "History" );

    Person person;
    person.SetName( "Visitor" );


    student.Display();
    teacher.Display();


    /*
    studentA.SetupStudent( "Bob", 2.5 );
    studentA.Display();

    studentA.SetName( "NotBob" );
    studentA.SetGpa( 5.0 );
    studentA.Display();

    cout << studentA.GetName() << endl;
    cout << studentA.GetGpa() << endl;

    cout << "GPA is: ";
    studentA.DisplayGpa();
    cout << endl << endl;
    */

    return 0;
}