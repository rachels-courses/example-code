#ifndef _PERSON
#define _PERSON

#include <string>
using namespace std;

// Parent (Base) class
class Person
{
    public:
    Person(); // Default constructor

    // MUTATORS
    void SetName( string newName );

    // ACCESSORS
    string GetName() const;

    protected:
    string name;
};

// Student inherits from Person
class Student : public Person
{
    public:
    // CONSTRUCTOR
    Student();

    // FUNCTION DECLARATIONS
    void Setup( string newName, float newGpa );
    void Display();

    // Setter (Mutator) functions:
    void SetGpa( float newGpa );

    // Getter (Accessor) functions:
    float GetGpa() const; // READ ONLY

    // EXTRA
    void DisplayGpa() const;

    private:
    float gpa;
};

class Teacher : public Person
{
    public:
    // CONSTRUCTOR
    Teacher();

    // FUNCTION DECLARATIONS
    void Setup( string newName, string newDepartment );
    void Display();

    // Setter (Mutator) functions:
    void SetDepartment( string newDepartment );

    // Getter (Accessor) functions:
    string GetDepartment() const; // READ ONLY

    private:
    string department;
};

#endif