#include "Person.h"

#include <iostream>
#include <iomanip>
using namespace std;

Person::Person()
{
    SetName( "NOTSET" );
}

void Person::SetName( string newName )
{
    name = newName;
}

string Person::GetName() const
{
    return name;
}







// FUNCTION DEFINITIONS

Student::Student()
{
    SetName( "NOTSET STUDENT" );
    SetGpa( 0.0 );
}

void Student::Setup( string newName, float newGpa )
{
    SetName( newName );
    SetGpa( newGpa );
}

void Student::Display()
{
    cout << "STUDENT: \t" 
    << GetName() << "\t";
    DisplayGpa();
    cout << endl;
}

void Student::SetGpa( float newGpa )
{
    // ERROR CHECKING:
    if ( newGpa < 0 || newGpa > 4.0 )
    {
        // Invalid value!
        cout << "Error: Cannot set gpa to " << newGpa << endl;
        return;
    }

    gpa = newGpa;
}

float Student::GetGpa() const
{
    return gpa;
}

void Student::DisplayGpa() const
{
    cout << setprecision( 1 ) << fixed;
    cout << gpa;
}



Teacher::Teacher()
{
    SetName( "NOTSET TEACHER" );
    SetDepartment( "NOTSET DEPARTMENT" );
}

void Teacher::Setup( string newName, string newDepartment )
{
    SetName( newName );
    SetDepartment( newDepartment );
}

void Teacher::SetDepartment( string newDepartment )
{
    department = newDepartment;
}

string Teacher::GetDepartment() const 
{
    return department;
}

void Teacher::Display()
{
    cout << "TEACHER: \t" << GetName() << "\t" << GetDepartment() << endl;
}