#ifndef _ACCOUNT_HPP        // File guard
#define _ACCOUNT_HPP

#include <string>
using namespace std;

class Account
{
    public:
    // Setter function (Mutator)
    void SetAccountId( int accountId )
    {
        if ( accountId >= 0 )
        {
            m_accountId = accountId;
        }
    }

    void SetUsername( string username )
    {
        m_username = username;
    }

    void SetPassword( string password )
    {
        m_password = password;
    }

    void SetEmail( string email )
    {
        m_email = email;
    }

    // Getter functions
    int GetAccountId()
    {
        return m_accountId;
    }

    string GetUsername()
    {
        return m_username;
    }

    string GetPassword()
    {
        return m_password;
    }

    string GetEmail()
    {
        return m_email;
    }

    private:
    int m_accountId;
    string m_username;
    string m_password;
    string m_email;
};

#endif
