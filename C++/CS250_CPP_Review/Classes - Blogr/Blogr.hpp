#ifndef _BLOGR_HPP        // File guard
#define _BLOGR_HPP

#include <iostream>
#include <string>
#include <vector>           // dynamic array
using namespace std;

#include "Account.hpp"

class Blogr
{
    public:
    void RegisterAccount();
    void ShowAccounts();

    private:
    vector<Account> m_accounts;
};



void Blogr::RegisterAccount()
{
    cout << "Enter a username: ";
    string username;
    cin >> username;

    cout << "Enter a password: ";
    string password;
    cin >> password;

    cout << "Enter an email address: ";
    string email;
    cin >> email;

    Account newAccount;
    newAccount.SetUsername( username );
    newAccount.SetPassword( password );
    newAccount.SetEmail( email );
    newAccount.SetAccountId( m_accounts.size() );
    m_accounts.push_back( newAccount );
}

void Blogr::ShowAccounts()
{
    for ( unsigned int i = 0; i < m_accounts.size(); i++ )
    {
        cout << i << ". " << m_accounts[i].GetUsername() << endl;
    }
}

#endif
