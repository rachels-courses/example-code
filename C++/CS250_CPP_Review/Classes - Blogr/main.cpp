#include "Account.hpp"
#include "Blogr.hpp"

int main()
{
    Blogr app;

    app.RegisterAccount();
    app.RegisterAccount();
    app.RegisterAccount();

    app.ShowAccounts();

    return 0;
}
