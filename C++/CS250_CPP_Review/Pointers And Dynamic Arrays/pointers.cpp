#include <iostream>
#include <string>
using namespace std;

class SmartDynamicArray
{
    public:
    void Add( int a );
    void Remove( int index );
    void Update( int index, int b );
    int Get();

    private:
    int * m_arr;
};


int main()
{
    // Variable declaration and assignment
//    string productA, productB, productC;
//    productA = "highlighters";
//    productB = "notebooks";
//    productC = "glue";
//
//    cout << "productA " << productA << endl;
//
//    // Address of operator: &
//    cout << "&productA " << &productA << endl;
//
//    // Create a pointer to point to existing data
//    string * ptrCurrentProduct = &productA;
//
//    cout << "ptrCurrentProduct " << ptrCurrentProduct << endl;
//    cout << "&ptrCurrentProduct " << &ptrCurrentProduct << endl;
//
//    ptrCurrentProduct = &productB;



    // Pointer to point to allocated data
    string* products = nullptr;

    int productCount;
    cout << "Enter product count: ";
    cin >> productCount;

    cout << "Address A: " << products << endl;

    // Dynamically allocate memory for an array of that size
    products = new string[productCount];


    cout << "Address B: " << products << endl;



    delete [] products;
    products = nullptr;

    cout << "Address C: " << products << endl;



    return 0;
}
