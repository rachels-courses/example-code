#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include "utilities/StringUtil.hpp"

class Character
{
    public:
    void SetTexture( const sf::Texture& tx );
    void SetPosition( float x, float y );
    void Draw( sf::RenderWindow& win );

    float GetX();
    float GetY();

    void SetSpeed( float speed );

    protected:
    float       m_x, m_y;
    float       m_speed;
    sf::Sprite  m_sprite;
};

void Character::SetSpeed( float speed )
{
    m_speed = speed;
}

void Character::SetTexture( const sf::Texture& tx )
{
    m_sprite.setTexture( tx );
}

void Character::SetPosition( float x, float y )
{
    m_x = x;
    m_y = y;
    m_sprite.setPosition( m_x, m_y );
}

void Character::Draw( sf::RenderWindow& win )
{
    win.draw( m_sprite );
}


float Character::GetX()
{
    return m_x;
}

float Character::GetY()
{
    return m_y;
}


// Player inherits from Character
class Player : public Character
{
    public:
    void Move();
/*
    public:
    void SetTexture( const sf::Texture& tx );
    void SetPosition( float x, float y );
    void Draw( sf::RenderWindow& win );

    protected:
    float       m_x, m_y;
    sf::Sprite  m_sprite;
*/
};

void Player::Move()
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        m_x -= m_speed;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        m_x += m_speed;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        m_y -= m_speed;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        m_y += m_speed;
    }

    m_sprite.setPosition( m_x, m_y );
}


class NPC : public Character
{
    public:
    void SetGoal( float x, float y );
    void Move();

    private:
    float m_goalX, m_goalY;

/*
    public:
    void SetTexture( const sf::Texture& tx );
    void SetPosition( float x, float y );
    void Draw( sf::RenderWindow& win );

    protected:
    float       m_x, m_y;
    sf::Sprite  m_sprite;
*/
};

void NPC::SetGoal( float x, float y )
{
    m_goalX = x;
    m_goalY = y;
}

void NPC::Move()
{
    // Goal is to the left
    if ( m_goalX < m_x )
    {
        m_x -= m_speed;
    }
    // Goal is to the right
    else if ( m_goalX > m_x )
    {
        m_x += m_speed;
    }

    // Goal is above
    if ( m_goalY < m_y )
    {
        m_y -= m_speed;
    }
    // Goal is below
    else if ( m_goalY > m_y )
    {
        m_y += m_speed;
    }


    m_sprite.setPosition( m_x, m_y );
}





int main()
{
    // Initialize SFML
    const int SCREEN_WIDTH = 1280;
    const int SCREEN_HEIGHT = 720;
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "My Program");

    // Load in textures (image files, usually png)
    bool success;
    sf::Texture textureBackground;
    success = textureBackground.loadFromFile("../assets/graphics/background.png");
    if (!success)
    {
        std::cout << "Error loading image" << std::endl;
        return 1;
    }

    // Create sprites (multiple sprites can use the same texture, i.e., enemies or blocks.)
    sf::Sprite spriteBackground;
    spriteBackground.setTexture(textureBackground);
    spriteBackground.setPosition(0, 0);


    sf::Texture txPlayer1;
    txPlayer1.loadFromFile( "../assets/graphics/player1.png" );

    sf::Texture txPlayer2;
    txPlayer2.loadFromFile( "../assets/graphics/player2.png" );



    // Object (variable whose data type is a class)
    Player player;
    player.SetPosition( 100, 200 );
    player.SetTexture( txPlayer1 );
    player.SetSpeed( 1 );

    NPC enemy;
    enemy.SetPosition( 300, 400 );
    enemy.SetTexture( txPlayer2 );
    enemy.SetSpeed( 0.5 );



    // Load in a font
    sf::Font fontMain;
    success = fontMain.loadFromFile("../assets/fonts/RosaSans-Regular.ttf");
    if ( !success )
    {
        std::cout << "Error loading font" << std::endl;
        return 3;
    }

    while (window.isOpen())
    {
        // Check for inputs / events (keyboard, mouse)
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Check if user hits "x" or "alt+f4" or another quit command
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }


        enemy.SetGoal( player.GetX(), player.GetY() );

        player.Move();
        enemy.Move();

        // Mouse input
        if ( sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i position = sf::Mouse::getPosition(window);
//            enemy.SetGoal( position.x, position.y );
        }

        // Update the window (draw)
        window.clear();
        window.draw(spriteBackground);
        player.Draw( window );
        enemy.Draw( window );
        window.display();
    }

    return 0;
}
