#include <iostream>
#include <string>
using namespace std;

int main()
{
    string text = "helloworld";
    
    cout << "Original text: " << text << endl;
    
    int start;
    int length;
    string replaceWith;
    
    cout << "Enter string to replace with: ";
    getline( cin, replaceWith );
    
    cout << "Enter position to begin replacing: ";
    cin >> start;
    
    cout << "Enter length of text to replacing: ";
    cin >> length;
    
    text = text.replace( start, length, replaceWith );    
    cout << endl << "String is now: " << text << endl;
    
    return 0;
}
