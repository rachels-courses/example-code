#ifndef _STORAGE
#define _STORAGE

class Storage
{
public:
	Storage();
	void AddItem(float value);
	void Display();

private:
	int m_itemCount;
	const int ARRAY_SIZE;
	float m_array[5];
};

#endif
