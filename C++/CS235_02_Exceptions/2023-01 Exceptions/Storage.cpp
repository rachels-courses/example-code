#include "Storage.h"

#include <iostream>
#include <stdexcept>
using namespace std;

Storage::Storage()
	: ARRAY_SIZE(5)
{
	m_itemCount = 0;
}

void Storage::AddItem(float value)
{
	// Error check
	if (m_itemCount == ARRAY_SIZE)
	{
		throw runtime_error("ARRAY IS FULL!");
	}

	m_array[m_itemCount] = value;
	m_itemCount++;
}

void Storage::Display()
{
	for (int i = 0; i < m_itemCount; i++)
	{
		cout << i << ". " << m_array[i] << endl;
	}
}