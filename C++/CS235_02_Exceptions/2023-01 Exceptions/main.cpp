#include <iostream>
#include <string>
using namespace std;

#include "Storage.h"

int main()
{
	Storage store;

	bool done = false;
	while (!done)
	{
		cout << "Items stored:" << endl;
		store.Display();

		cout << endl << "Enter a FLOAT to store a new value, or STOP to quit: ";
		string input;
		cin >> input;

		if (input == "STOP")
		{
			done = true;
		}
		else
		{
			float value = stof(input);
			try
			{
				store.AddItem(value);
			}
			catch (const runtime_error& ex)
			{
				cout << "EXCEPTION CAUGHT!" << endl;
				cout << ex.what() << endl;
			}
		}
	}
	
	cout << endl << "FINAL STORAGE:" << endl;
	store.Display();

	cout << endl << "GOODBYE" << endl;

	return 0;
}