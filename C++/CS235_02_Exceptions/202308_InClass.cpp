#include <iostream>
#include <stdexcept>
#include <vector>
using namespace std;

float Divide( float num, float denom ) // Problematic function
{
	if (denom == 0)
	{
		//cout << "ERROR! " << DIV_BY_ZERO << endl;
		//return DIV_BY_ZERO;
		throw invalid_argument("Cannot divide by zero!");
	}
	return num / denom;
}

int main()
{
	// Example 1: The vector's at function could throw an exception,
	// so we should listen for it.
	// https://cplusplus.com/reference/vector/vector/at/
	vector<string> data = { "Kansas", "Missouri", "Nebraska" };

	cout << "Enter record #";
	int index;
	cin >> index;

	cout << "The data at that index is:" << endl;

	try
	{
		cout << data.at(index) << endl;
	}
	catch (const out_of_range& ex)
	{
		cout << "ERROR: " << ex.what() << endl;
		index = 0;
	}
	catch (...)
	{
		cout << "ERROR: Unknown exception occurred" << endl;
	}


	// Example 2: We wrote a function that could throw an exception.
	// When we call that function, we should listen for that exception.
	float n, d;
	cout << "Enter a numerator: ";
	cin >> n;
	cout << "Enter a denominator: ";
	cin >> d;

	float result;

	try
	{
		result = Divide(n, d);
		cout << "result: " << result << endl;
	}
	catch (const invalid_argument& ex)
	{
		cout << "ERROR: " << ex.what() << endl;
	}

	return 0;
}
