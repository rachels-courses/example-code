#ifndef _LOG
#define _LOG

#include <string>
#include <fstream>
#include <iostream>
#include <map>
using namespace std;

class Log
{
public:
	Log();
	~Log();

	template <typename T>
	void Out(string name, T value);

private:
	ofstream output;
};

Log::Log()
{
	output.open("log.txt");
}

Log::~Log()
{
	output.close();
}

template <typename T>
void Log::Out(string name, T value)
{
	output << name << ": " << value << endl;
}

#endif
