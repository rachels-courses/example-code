#ifndef _GAMEDATA
#define _GAMEDATA

#include <string>
#include <map>
#include <fstream>
using namespace std;

class GameData
{
public:
	void LoadGameFile(string filename);
	string GetData(string key);

private:
	map<string, string> game_data;
};

#endif
