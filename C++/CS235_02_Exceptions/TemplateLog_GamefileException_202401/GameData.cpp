#include "GameData.h"

void GameData::LoadGameFile(string filename)
{
	ifstream input(filename);

	if (input.fail())
	{
		// File not found
		throw runtime_error("File " + filename + " not found!");
	}

	string key, value;
	input >> key >> value;
	game_data[key] = value;
}

string GameData::GetData(string key)
{
	//return game_data[key];
	return game_data.at(key);
}
