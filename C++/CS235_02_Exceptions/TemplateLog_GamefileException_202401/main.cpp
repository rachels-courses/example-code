#include "Log.h"
#include "GameData.h"

#include <iostream>
#include <stdexcept>
#include <string>
using namespace std;

int main()
{	
	Log log;

	GameData data;

	try
	{
		data.LoadGameFile("save.txt");
	}
	catch (runtime_error ex)
	{
		log.Out("error!", ex.what());
	}

	try
	{
		string character_name = data.GetData("name");
	}
	catch (out_of_range ex)
	{
		log.Out("error!", ex.what());
	}

	//cout << "Character name: " << character_name << endl;



	return 0;
}
