#include <iostream>
#include <fstream>
#include <string>
using namespace std;

float Divide(float num, float denom)
{
	if (denom == 0)
	{
		// ERROR
		throw invalid_argument("Error: Cannot divide by 0!");
	}
	else
	{
		return num / denom;
	}
}

float LoadData(string filename)
{
	ifstream input(filename);

	if (input.fail())
	{
		throw runtime_error("Cannot find file \"" + filename + "\"!");
	}

	float balance;
	input >> balance;

	return balance;
}

int main()
{
	float n, d;

	cout << "Enter a numerator: ";
	cin >> n;

	cout << "Enter a denominator: ";
	cin >> d;

	try
	{
		float quotient = Divide(n, d);
		cout << "Quotient: " << quotient << endl;
	}
	catch (const invalid_argument& ex)
	{
		cout << ex.what() << endl;
		return 1;
	}


	// Load bank data
	string filename;
	cout << "Enter a filename: ";
	cin >> filename;

	float balance;
	try
	{
		balance = LoadData(filename);
	}
	catch (const runtime_error& ex)
	{
		cout << ex.what() << endl;
		return 2;
	}

	cout << "Current balance is: " << balance << endl;



	return 0;
}
