#include <iostream>
#include <string>
using namespace std;

int main() {
  cout << "ADDRESSES" << endl << endl;

  bool savedGame = true;
  int number = 10;
  float price = 7.99;
  string text = "hello!";

  cout << "bool savedGame" << endl;
  cout << "* value: " << savedGame << endl
       << "* address: " << &savedGame << endl
       << "* variable size: " << sizeof(savedGame) << " bytes" << endl << endl;

  cout << "int number" << endl;
  cout << "* value: " << number << endl
       << "* address: " << &number << endl
       << "* variable size: " << sizeof(number) << " bytes" << endl << endl;

  cout << "float price" << endl;
  cout << "* value: " << price << endl
       << "* address: " << &price << endl
       << "* variable size: " << sizeof(price) << " bytes" << endl << endl;

  cout << "string text" << endl;
  cout << "* value: " << text << endl
       << "* address: " << &text << endl
       << "* variable size: " << sizeof(text) << " bytes" << endl << endl;

  return 0;
}