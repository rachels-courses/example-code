#include <iostream>
#include <iomanip>
#include <memory>
#include <string>
using namespace std;

int main()
{
  size_t array_size;
  cout << "Enter # of items to store: ";
  cin >> array_size;
  cin.ignore();

//  unique_ptr<string[]> arr = unique_ptr<string[]>( new string[ array_size ] );
  auto arr = unique_ptr<string[]>( new string[ array_size ] );

  for ( size_t i = 0; i < array_size; i++ )
  {
    cout << "Enter value for #" << i << ": ";
    getline( cin, arr[i] );
  }

  cout << endl << endl;
  cout << left << setw( 5 ) << "#" << setw( 20 ) << "VALUE" << endl << string( 40, '-' ) << endl;
  for ( size_t i = 0; i < array_size; i++ )
  {
    cout << setw( 5 ) << i << setw( 20 ) << arr[i] << endl;
  }
}
