#include <iostream>
#include <iomanip>
#include <string>
#include <memory>
using namespace std;

struct City
{
  City( string value )
  {
    name = value;
  }

  string name;
  shared_ptr<City> west_neighbor;
  shared_ptr<City> east_neighbor;
  shared_ptr<City> south_neighbor;
  shared_ptr<City> north_neighbor;
};

int main()
{
  shared_ptr<City> overland_park  = shared_ptr<City>( new City( "Overland Park" ) );
  shared_ptr<City> lenexa         = shared_ptr<City>( new City( "Lenexa" ) );
  shared_ptr<City> olathe         = shared_ptr<City>( new City( "Olathe" ) );
  shared_ptr<City> shawnee        = shared_ptr<City>( new City( "Shawnee" ) );

  // Set up neighbors
  lenexa->east_neighbor         = overland_park;
  overland_park->west_neighbor  = lenexa;
  lenexa->south_neighbor        = olathe;
  olathe->north_neighbor        = lenexa;
  lenexa->north_neighbor        = shawnee;
  shawnee->north_neighbor       = lenexa;

  cout << left;
  cout << setw( 20 ) << "CITY" << setw( 20 ) << "ADDRESS" << setw( 20 ) << "USE COUNT" << endl;
  cout << string( 80, '-' ) << endl;
  cout << setw( 20 ) << overland_park->name << setw( 20 ) << overland_park  << setw( 20 ) << overland_park.use_count() << endl;
  cout << setw( 20 ) << lenexa->name        << setw( 20 ) << lenexa         << setw( 20 ) << lenexa.use_count() << endl;
  cout << setw( 20 ) << olathe->name        << setw( 20 ) << olathe         << setw( 20 ) << olathe.use_count() << endl;
  cout << setw( 20 ) << shawnee->name       << setw( 20 ) << shawnee        << setw( 20 ) << shawnee.use_count() << endl;


  return 0;
}
