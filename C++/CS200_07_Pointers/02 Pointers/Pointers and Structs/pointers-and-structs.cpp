#include <iostream>
#include <iomanip>
using namespace std;

#include "MenuItem.h"

int main() 
{
  cout << "RESTAURANT" << endl << endl;
  
  // Set up data
  MenuItem item1, item2, item3;

  item1.name = "Hamburger";
  item1.price = 5.99;

  item2.name = "Milkshake";
  item2.price = 4.99;

  item3.name = "Curly fries";
  item3.price = 2.99;

  cout << fixed << setprecision(2);
  
  cout << "Original menu:" << endl;
  cout << "item1 (" << &item1 << ") $" << item1.price << " - " << item1.name << endl;
  cout << "item2 (" << &item2 << ") $" << item2.price << " - " << item2.name <<  endl;
  cout << "item3 (" << &item3 << ") $" << item3.price << " - " << item3.name <<  endl;

  MenuItem* ptrItem = nullptr;
  cout << endl << "Point to which item? 1, 2, or 3: ";
  int choice;
  cin >> choice;

  if ( choice == 1 ) {
    ptrItem = &item1;
  }
  else if ( choice == 2 ) {
    ptrItem = &item2;
  }
  else if ( choice == 3 ) {
    ptrItem = &item3;
  }
  else {
    cout << "INVALID SELECTION!" << endl;
    return 1;
  }

  cout << fixed << setprecision(2);
  cout << endl;
  cout << "ptrItem is pointing to address: " << ptrItem << endl;
  cout << "Item's name:  " << ptrItem->name << endl;
  cout << "Item's price: $" << ptrItem->price << endl;
  cout << "New name:     ";
  cin.ignore();
  getline(cin, ptrItem->name);
  cout << "New price:    $";
  cin >> ptrItem->price;

  cout << endl;
  cout << "Updated menu:" << endl;
  cout << "item1 (" << &item1 << ") $" << item1.price << " - " << item1.name << endl;
  cout << "item2 (" << &item2 << ") $" << item2.price << " - " << item2.name <<  endl;
  cout << "item3 (" << &item3 << ") $" << item3.price << " - " << item3.name <<  endl;

  
  return 0;
}