#include <iostream>
#include <string>
using namespace std;

struct Product
{
	void SetPrice(float new_price)
	{
		price = new_price;
	}

	string name;
	float price;
};

void Menu()
{
	bool done = false;
	while (!done)
	{
		cout << "1. Thing1" << endl;
		cout << "2. Exit" << endl;
		int choice;
		cin >> choice;
	}

}

int main()
{
	cout << "A" << endl;



	char currency = '$';
	int number = 20;
	float dollars = 19.99;
	double more_precision = 123.456;

	int* ptrInt = nullptr;
	double* ptrDouble = nullptr;

	cout << "VALUES:" << endl;
	cout << "currency:       " << currency << endl;
	cout << "number:         " << number << endl;
	cout << "dollars:        " << dollars << endl;
	cout << "more_precision: " << more_precision << endl;

	cout << endl << "SIZES:" << endl;
	cout << "currency:       " << sizeof(currency) << endl;
	cout << "number:         " << sizeof(number) << endl;
	cout << "dollars:        " << sizeof(dollars) << endl;
	cout << "more_precision: " << sizeof(more_precision) << endl;

	cout << endl << "ADDRESSES:" << endl;
	cout << "currency:       " << &currency << endl; // Address-of operator
	cout << "number:         " << &number << endl;
	cout << "dollars:        " << &dollars << endl;
	cout << "more_precision: " << &more_precision << endl;

	cout << endl << "POINTERS:" << endl;
	cout << "ptrInt:         " << ptrInt << endl;
	cout << "ptrDouble:      " << ptrDouble << endl;

	ptrInt = &number;
	ptrDouble = &more_precision;

	cout << endl << "POINTERS:" << endl;
	cout << "ptrInt:          " << ptrInt << endl;			// Address it's pointing TO
	cout << "ptrInt value:    " << *ptrInt << endl;			// Value stored at that address
	cout << "ptrDouble:       " << ptrDouble << endl;
	cout << "ptrDouble value: " << *ptrDouble << endl;

	// Dereferencing
	*ptrInt = 2;
	*ptrDouble = 100.2792;

	/*
	struct Product
	{
		void SetPrice(float new_price)
		{
			price = new_price;
		}

		string name;
		float price;
	};
	*/

	Product product1;
	product1.name = "Hamburger";
	product1.price = 5.99;

	Product* ptrProduct = &product1;

	cout << (*ptrProduct).name << endl;
	cout << ptrProduct->name << endl;

	ptrProduct->SetPrice(2.99);


	return 0;
}