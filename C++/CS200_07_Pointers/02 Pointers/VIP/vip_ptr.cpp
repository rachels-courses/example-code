#include <iostream>
#include <string>
using namespace std;

int main()
{
	string studentA = "Hikaru";
	string studentB = "Umi";
	string studentC = "Fuu";
	
	string* ptrTopStudent = nullptr;
	
	bool done = false;
	
	while ( !done )
	{
		cout << "Students:" << endl
			<< "A. " << studentA << "\t" << &studentA << endl
			<< "B. " << studentB << "\t" << &studentB << endl
			<< "C. " << studentC << "\t" << &studentC << endl << endl;
			
		cout << "Top student address: " << ptrTopStudent << endl;
		if ( ptrTopStudent != nullptr )
		{
			cout << "Top student name: " << *ptrTopStudent << endl;
		}
		
		cout << endl << "Set top student to who? (A, B, C, or Q to quit): ";
		char choice;
		cin >> choice;
		
		switch( choice )
		{
			case 'A': ptrTopStudent = &studentA; break;
			case 'B': ptrTopStudent = &studentB; break;
			case 'C': ptrTopStudent = &studentC; break;
			case 'Q': done = true; break;
			default: cout << "Invalid selection" << endl;
		}
	}
	
	return 0;
}
