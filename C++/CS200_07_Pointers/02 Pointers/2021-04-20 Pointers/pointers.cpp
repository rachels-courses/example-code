#include <iostream>
#include <string>
using namespace std;

struct Student
{
    string name;
    float gpa;
};

int main()
{
    string studentA, studentB;
    studentA = "Tony";
    studentB = "Zach";

    // DISPLAY MEMORY ADDRESS OF VARIABLE,
    // Use the & (address-of) operator.
    cout << "studentA, value: " << studentA             // Value
         << ", address: "       << &studentA << endl;   // Address

    cout << "studentB, value: " << studentB             // Value
         << ", address: "       << &studentB << endl;   // Address

    // POINTERS store an address.
    // When a pointer is not in use, it should be initialized
    // to nullptr.
    string* ptrTopStudent = nullptr;
    cout << "ptrTopStudent, value: " << ptrTopStudent << endl;

    // Assign POINTER to an ADDRESS of a variable.
    ptrTopStudent = &studentB;
    cout << "ptrTopStudent, value: " << ptrTopStudent << endl;

    // DEREFERENCE the POINTER to get the value of the item being pointed to.
    cout << "TOP STUDENT IS: " << *ptrTopStudent << endl;

    // OVERWRITE the value of the item being pointed to.
    cout << "Update student name: ";
    getline( cin, *ptrTopStudent );

    // Display updated student list
    cout << endl;
    cout << "studentA, value: " << studentA             // Value
         << ", address: "       << &studentA << endl;   // Address

    cout << "studentB, value: " << studentB             // Value
         << ", address: "       << &studentB << endl;   // Address

//    struct Student
//    {
//        string name;
//        float gpa;
//    };

    Student studentC;
    studentC.name = "Harshmeet";
    studentC.gpa = 3.8;

    Student* ptrStudent = nullptr;
    ptrStudent = &studentC;

    cout << studentC.name << endl;

    // THESE TWO ARE THE SAME:
    // Read from member variable
    cout << (*ptrStudent).name << endl;
    cout << ptrStudent->name << endl;

    // Write to member variable
    getline( cin, ptrStudent->name );

    return 0;
}
