#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

#include "Employee.hpp"

void SetupEmployees( Employee employeeList[10] );

int main()
{
    int num;
    int * ptr;
    ptr = &num;


    Employee employeeList[10];
    SetupEmployees( employeeList );

    employeeList[0].SetBoss( &employeeList[3] );
    employeeList[2].SetBoss( &employeeList[6] );
    employeeList[3].SetBoss( &employeeList[3] );
    employeeList[5].SetBoss( &employeeList[2] );
    employeeList[7].SetBoss( &employeeList[1] );



    cout << left << setw( 4 ) << "#"
         << setw( 20 ) << "NAME"
         << setw( 20 ) << "BOSS" << endl;
    cout << "----------------------------------" << endl;
    for ( int i = 0; i < 10; i++ )
    {
        string employeeName = employeeList[i].GetName();
        string bossName = employeeList[i].GetBossName();

        cout << left << setw( 4 )
            << i
            << setw( 20 )
            << employeeName
            << setw( 20 )
            << bossName << endl;
    }

    return 0;
}






void SetupEmployees( Employee employeeList[10] )
{
    employeeList[0].SetName( "Yoko Kamio" );
    employeeList[1].SetName( "Rajkumar Rao" );
    employeeList[2].SetName( "Wang He Di" );
    employeeList[3].SetName( "Ron Gilbert" );
    employeeList[4].SetName( "Roberta Williams" );
    employeeList[5].SetName( "Ku Hye-sun" );
    employeeList[6].SetName( "John Carmack" );
    employeeList[7].SetName( "Mario Casillas" );
    employeeList[8].SetName( "Eduardo Palomo" );
    employeeList[9].SetName( "Stephanie Arnow" );
}
