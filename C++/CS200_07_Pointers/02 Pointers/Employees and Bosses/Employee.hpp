#ifndef _EMPLOYEE_HPP
#define _EMPLOYEE_HPP

#include <string>
using namespace std;

class Employee
{
    public:
    Employee();

    void SetName( string name );
    string GetName();

    void SetBoss( Employee* boss );
    string GetBossName();

    private:
    string m_name;
    Employee* m_ptrBoss;
};

// Constructor function runs automatically
// when we create an Employee variable
Employee::Employee()
{
    m_ptrBoss = nullptr;
}

void Employee::SetName( string name )
{
    m_name = name;
}

string Employee::GetName()
{
    return m_name;
}

void Employee::SetBoss( Employee* boss )
{
    m_ptrBoss = boss;
}

string Employee::GetBossName()
{
    // ERROR CHECK: If m_ptrBoss == nullptr,
    // then it's not pointing to an address
    if ( m_ptrBoss == nullptr )
    {
        return "none";
    }
    else // m_ptrBoss is pointing to an address
    {
//        return (*m_ptrBoss).GetName();
        return m_ptrBoss->GetName();
    }
}


#endif
