#include "Account.hpp"

void Account::SetUsername( string username )
{
    m_username = username;
}

string Account::GetUsername()
{
    return m_username;
}

void Account::SetEmail( string email )
{
    m_email = email;
}

string Account::GetEmail()
{
    return m_email;
}

void Account::SetPassword( string password )
{
    m_password = password;
}

string Account::GetPassword()
{
    return m_password;
}
