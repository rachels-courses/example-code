#ifndef _ACCOUNT_HPP
#define _ACCOUNT_HPP

#include <string>
using namespace std;

class Account
{
    public:
    void SetUsername( string username );
    string GetUsername();

    void SetEmail( string email );
    string GetEmail();

    void SetPassword( string password );
    string GetPassword();

    private:
    string m_username;
    string m_email;
    string m_password;
};

#endif
