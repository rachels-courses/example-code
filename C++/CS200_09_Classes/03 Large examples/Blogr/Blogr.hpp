#ifndef _BLOGR_HPP
#define _BLOGR_HPP

#include "Account.hpp"
#include "Post.hpp"

class BlogrApp
{
    public:
    BlogrApp();

    void Run();
    void LoginMenu();
    void MainMenu();

    void Login();
    void ViewPosts();
    void WritePost();

    void DisplayAccounts();

    private:
    const int MAX_ACCOUNTS;
    int m_totalAccounts;
    Account m_accounts[10];
    int m_currentAccount;

    const int MAX_POSTS;
    int m_totalPosts;
    Post m_posts[10];
};

#endif
