#include "Post.hpp"


void Post::SetText( string text )
{
    m_text = text;
}

string Post::GetText()
{
    return m_text;
}

void Post::SetAuthorId( int id )
{
    m_authorId = id;
}

int Post::GetAuthorId()
{
    return m_authorId;
}

