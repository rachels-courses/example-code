#ifndef _POST_HPP
#define _POST_HPP

#include <iostream>
#include <string>
using namespace std;

class Post
{
    public:
    void SetText( string text );
    string GetText();

    void SetAuthorId( int id );
    int GetAuthorId();

    private:
    string m_text;
    int m_authorId;
};

#endif
