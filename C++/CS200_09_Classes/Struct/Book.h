#ifndef _BOOK // file guards
#define _BOOK

#include <string>
using namespace std;

struct Book
{
	string title;
	string author;
	string isbn;
	float price;
	bool checked_out;
};

#endif
