#include "Book.h"

#include <iostream>
using namespace std;

int main()
{
	Book book1;
	book1.title = "The Very Hungry Caterpillar";
	book1.author = "Eric Carle";
	book1.isbn = "0399256733";
	book1.price = 27.18;
	book1.checked_out = false;
	
	Book book2;
	book2.title = "No, David!";
	book2.author = "David Shannon";
	book2.isbn = "1338299581";
	book2.price = 7.62;
	book2.checked_out = false;
	
	bool running = true;
	while ( running )
	{
		// Main menu
		cout << endl;
		cout << string( 80, '-' ) << endl;
		cout << "MAIN MENU" << endl;
		cout << "0. Quit" << endl;
		cout << "1. Check out book" << endl;
		cout << "2. Return book" << endl;
		
		int choice;
		cin >> choice;
		
		if ( choice == 0 )
		{
			running = false;
		}
		else if ( choice == 1 )
		{
			cout << string( 80, '-' ) << endl;
			cout << "CHECK OUT BOOK" << endl;
			bool done_with_checkout = false;
			while ( done_with_checkout == false )
			{
				cout << endl;
				cout << "Which book?" << endl;
				cout << "0. End checkout" << endl;
				cout << "1. " << book1.title << " (Checked out: " << book1.checked_out << ")" << endl;
				cout << "2. " << book2.title << " (Checked out: " << book2.checked_out << ")" << endl;
				
				cin >> choice;
				if ( choice == 0 )
				{
					done_with_checkout = true;
				}
				else if ( choice == 1 && book1.checked_out == false )
				{
					cout << "Checked out " << book1.title << endl;
					book1.checked_out = true;
				}
				else if ( choice == 2 && book2.checked_out == false )
				{
					cout << "Checked out " << book2.title << endl;
					book2.checked_out = true;
				}
				else
				{
					cout << "That book is not available!" << endl;
				}
			}
		}
		else if ( choice == 2 )
		{
			cout << string( 80, '-' ) << endl;
			cout << "RETURN BOOK" << endl;
			
			cout << endl;
			cout << "Which book?" << endl;
			cout << "1. " << book1.title << " (Checked out: " << book1.checked_out << ")" << endl;
			cout << "2. " << book2.title << " (Checked out: " << book2.checked_out << ")" << endl;
			
			cin >> choice;
			
			if 		( choice == 1 && book1.checked_out == true )
			{
				cout << "Returned book " << book1.title << endl;
				book1.checked_out = false;
			}
			else if ( choice == 2 && book2.checked_out == true )
			{
				cout << "Returned book " << book2.title << endl;
				book2.checked_out = false;
			}
			else
			{
				cout << "Invalid book for return" << endl;
			}
		}
		
		/*
		switch( choice )
		{
			case 0:
			running = false;
			break;
			
			case 1:
			cout << string( 80, '-' ) << endl;
			cout << "CHECK OUT BOOK" << endl;
			break;
			
			case 2:
			cout << string( 80, '-' ) << endl;
			cout << "RETURN BOOK" << endl;
			break;
		}
		*/
	}
	
	cout << endl;
	cout << "GOODBYE" << endl;
	
	return 0;
}
