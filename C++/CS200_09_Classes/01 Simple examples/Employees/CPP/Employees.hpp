#ifndef _EMPLOYEE_HPP
#define _EMPLOYEE_HPP

#include <string>
using namespace std;

class Employee
{
    public:
    // Public member functions (methods)
    void Setup( string newName, float newPay );
    void SetupWithPrompts();
    void Display();

    private:
    // Private member variables
    string name;
    float hourlyPay;
};

#endif
