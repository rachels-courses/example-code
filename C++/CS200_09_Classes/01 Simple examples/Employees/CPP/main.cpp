#include <iostream>
#include <string>
using namespace std;

#include "Employees.hpp"

int main()
{
    Employee emp1;
    emp1.Setup( "Bob", 7.25 );
    emp1.Display();


    Employee emp2;
    // Temporary variables
    string name;
    float pay;

    cout << "Enter a name: ";
    getline( cin, name );

    cout << "Enter the pay: ";
    cin >> pay;

    emp2.Setup( name, pay );

    emp2.Display();


    cin.ignore();


    Employee emp3;
    emp3.SetupWithPrompts();
    emp3.Display();

    return 0;
}
