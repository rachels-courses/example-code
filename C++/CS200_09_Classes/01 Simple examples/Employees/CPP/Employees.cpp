#include "Employees.hpp"

#include <iostream>
using namespace std;

void Employee::Setup( string newName, float newPay )
{
    name = newName;
    hourlyPay = newPay;
}

void Employee::SetupWithPrompts()
{
    cout << "Enter a name: ";
    getline( cin, name );

    cout << "Enter the pay: ";
    cin >> hourlyPay;
}

void Employee::Display()
{
    cout << name << "\t $" << hourlyPay << endl;
}
