#include <SFML/Graphics.hpp>

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "My window");

    sf::CircleShape circle;
    circle.setPosition( 100, 100 );
    circle.setRadius( 100 );
    circle.setFillColor( sf::Color::Red );

    sf::RectangleShape rectangle;
    rectangle.setPosition( 300, 300 );
    rectangle.setSize( sf::Vector2f( 200, 100 ) );
    rectangle.setFillColor( sf::Color::Blue );

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear(sf::Color::White);
        window.draw( circle );
        window.draw( rectangle );
        window.display();
    }

    return 0;
}
