#include "Library.h"

#include <iostream>
#include <string>
using namespace std;

int main()
{
    Library myLibrary;

    // Can't set variable directly:
    // myLibrary.name = "Central Resource";
    // Use a function instead:
    myLibrary.SetName( "Central Resource" );
    myLibrary.SetAddress( "9875 W 87th St \n Overland Park, KS 66212" );

    myLibrary.AddBook( "Six of Crows", "Leigh Bardugo", 37.39 );
    myLibrary.AddBook( "Your Very Own Robot", "R.A. Montgomery", 4.79 );
    myLibrary.AddBook( "Diary of a Wimpy Kid", "Jeff Kinney", 13.87 );

    myLibrary.Display();

    return 0;
}
