#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "Book.h"

#include <array>
#include <string>
using namespace std;

class Library
{
    public:
    Library();

    // Member functions
    void SetName( string newName );
    void SetAddress( string newAddress );
    void AddBook( Book newBook );
    void AddBook( string title, string author, float price );
    void Display();

    private:
    // Member variables
    string m_name;
    string m_address;
    array<Book,100> m_books;
    int m_itemCount;
};

#endif
