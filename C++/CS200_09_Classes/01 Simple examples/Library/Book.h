#ifndef _BOOK_H
#define _BOOK_H

#include <string>
using namespace std;

struct Book
{
    string title;
    string author;
    float listPrice;
};

#endif
