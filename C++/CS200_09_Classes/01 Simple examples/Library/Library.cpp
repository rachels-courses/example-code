#include "Library.h"

#include <iostream>
using namespace std;

Library::Library()
{
    m_itemCount = 0;
}

void Library::SetName( string newName )
{
    m_name = newName;
}

void Library::SetAddress( string newAddress )
{
    m_address = newAddress;
}

void Library::AddBook( Book newBook )
{
    m_books[ m_itemCount ] = newBook;
    m_itemCount++;
}

void Library::AddBook( string title, string author, float price )
{
    m_books[ m_itemCount ].title = title;
    m_books[ m_itemCount ].author = author;
    m_books[ m_itemCount ].listPrice = price;
    m_itemCount++;
}

void Library::Display()
{
    cout << m_name << endl;
    cout << m_address << endl;

    cout << endl << "BOOKS:" << endl;
    for ( unsigned int i = 0; i < m_itemCount; i++ )
    {
        cout << m_books[i].title
            << "\t"
            << m_books[i].author
            << "\t"
            << m_books[i].listPrice
            << endl;
    }
}
