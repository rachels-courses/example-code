#ifndef _COURSE_H
#define _COURSE_H

#include "Person.h"

#include <array>
#include <string>
using namespace std;

class Course
{
public:
    Course();

    void Setup( string newTitle, string newSubject, int newCourseNumber, int newCrn, int newSection );
    void SetupWizard();
    void Display();
    void RegisterStudent( Person newStudent );
    void AssignTeacher( Person newTeacher );

// These variables can only be accessed
// by the Course object itself; nothing outside.
private:
    string title;
    int crn;
    int courseNumber;
    int section;
    string subject;
    array<Person,18> students;
    int studentCount;
    Person teacher;
};

#endif
