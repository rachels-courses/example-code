#include "Course.h"

#include <iostream>
using namespace std;

// Constructor function
Course::Course()
{
    studentCount = 0;
}

void Course::SetupWizard()
{
    cout << endl;
    cin.ignore();
    cout << "Enter course title: ";
    getline( cin, title );

    cout << "Enter subject (e.g., CSIS): ";
    cin >> subject;

    cout << "Enter course number: ";
    cin >> courseNumber;

    cout << "Enter CRN: ";
    cin >> crn;

    cout << "Enter section #: ";
    cin >> section;
}

void Course::Setup( string newTitle, string newSubject, int newCourseNumber,
    int newCrn, int newSection )
{
    title = newTitle;
    subject = newSubject;
    courseNumber = newCourseNumber;
    crn = newCrn;
    section = newSection;
}

void Course::Display()
{
    cout << string( 40, '#' ) << endl;
    cout << subject << " " << courseNumber
        << "-" << section << " (" << crn << ")"
        << "\t" << title << endl;

    cout << endl << "STUDENTS:" << endl;
    for ( unsigned int i = 0; i < studentCount; i++ )
    {
        students[i].Display();
    }

    cout << endl << "TEACHER:" << endl;
    teacher.Display();

    cout << string( 40, '#' ) << endl;

    cout << endl;
}

void Course::RegisterStudent( Person newStudent )
{
    students[studentCount] = newStudent;
    studentCount++;
}


void Course::AssignTeacher( Person newTeacher )
{
    teacher = newTeacher;
}




