#include "Person.h"

#include <iostream>
#include <string>
using namespace std;

void Person::Setup( string newFullName, string newPhoneNumber, int newId )
{
    fullName = newFullName;
    phoneNumber = newPhoneNumber;
    id = newId;
}

void Person::SetupWizard()
{
    cout << "Enter full name: ";
    cin.ignore();
    getline( cin, fullName );

    cout << "Enter phone #: ";
    cin >> phoneNumber;

    cout << "Enter ID: ";
    cin >> id;
}

void Person::Display()
{
    cout << id << " " << phoneNumber << "\t" << fullName << endl;
}
