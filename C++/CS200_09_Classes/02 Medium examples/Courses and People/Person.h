#ifndef _PERSON_H
#define _PERSON_H

#include <string>
using namespace std;

class Person
{
public:
    void Setup( string newFullName, string newPhoneNumber, int newId );
    void SetupWizard();
    void Display();

private:
    string fullName;
    string phoneNumber;
    int id;
};

#endif
