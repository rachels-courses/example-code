#include <iostream>
#include <string>
using namespace std;

enum class StoplightState {
    STOP = 0,
    SLOW = 1,
    GO = 2
};

ostream& operator<<( ostream& out, const StoplightState& state )
{
    if      ( state == StoplightState::STOP )
        out << "STOP";
    else if ( state == StoplightState::SLOW )
        out << "SLOW";
    else if ( state == StoplightState::GO )
        out << "GO";
    return out;
}

int main()
{
    StoplightState light = StoplightState::STOP;

    for ( int i = 0; i < 10; i++ )
    {
        cout << i << ". Stop light state is " << static_cast<int>(light) << " (" << light << ")" << endl;

        // Green to yellow
        if      ( light == StoplightState::GO ) { light = StoplightState::SLOW; }

        // Yellow to red
        else if ( light == StoplightState::SLOW ) { light = StoplightState::STOP; }

        // Red to green
        else if ( light == StoplightState::STOP ) { light = StoplightState::GO; }
    }

    return 0;
}

