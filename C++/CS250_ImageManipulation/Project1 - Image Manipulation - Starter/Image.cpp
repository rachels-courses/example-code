#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
using namespace std;

void PpmImage::LoadImage( const string& filename )
{
    cout << "Loading image from \"" << filename << "\"...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage( const string& filename )
{
    cout << "Saving image to \"" << filename << "\"...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2...";

    throw runtime_error( "Method not implemented!" );

    cout << "SUCCESS" << endl;
}
