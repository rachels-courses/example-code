#ifndef _BUILDING
#define _BUILDING

#include <string>
using namespace std;

#include "Room.h"

struct Building
{
  string name;
  Room room1;
  Room room2;
  Room room3;
};

#endif