#include <iostream>
using namespace std;

#include "Building.h"

int main() {
  Building house;
  house.name = "House";

  house.room1.name = "Livingroom";
  house.room1.width = 15;
  house.room1.length = 20;
  
  house.room2.name = "Bedroom";
  house.room2.width = 12;
  house.room2.length = 16;

  house.room3.name = "Bathroom";
  house.room3.width = 6;
  house.room3.length = 10;
  
  cout << "HOUSE INFORMATION" << endl << endl;
  cout << "Building name: " << house.name << endl;

  cout << endl << "ROOM 1: " << house.room1.name << endl;
  cout << "* width:  " << house.room1.width << " ft" << endl;
  cout << "* length: " << house.room1.length << " ft" << endl;
  cout << "* area:   " << house.room1.width * house.room1.length << " sqft" << endl;
  
  cout << endl << "ROOM 2: " << house.room2.name << endl;
  cout << "* width:  " << house.room2.width << " ft" << endl;
  cout << "* length: " << house.room2.length << " ft" << endl;
  cout << "* area:   " << house.room2.width * house.room2.length << " sqft" << endl;
  
  cout << endl << "ROOM 3: " << house.room3.name << endl;
  cout << "* width:  " << house.room3.width << " ft" << endl;
  cout << "* length: " << house.room3.length << " ft" << endl;
  cout << "* area:   " << house.room3.width * house.room3.length << " sqft" << endl;
  
  return 0;
}