#ifndef _ROOM
#define _ROOM

#include <string>
using namespace std;

struct Room
{
  string name;
  float width;
  float length;
};

#endif