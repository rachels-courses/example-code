#ifndef _MENUITEM
#define _MENUITEM

#include <string>
using namespace std;

struct MenuItem
{
  string name;
  float price;
};

#endif