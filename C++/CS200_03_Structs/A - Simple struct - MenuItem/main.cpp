#include <iostream>
#include <iomanip>
using namespace std;

#include "MenuItem.h"

int main() {

  MenuItem item1, item2, item3;

  item1.name = "Burrito";
  item1.price = 1.29;
  
  item2.name = "Taco";
  item2.price = 1.19;
  
  item3.name = "Quesadilla";
  item3.price = 3.45;

  cout << fixed << setprecision(2);
  cout << "RESTAURANT" << endl << endl;
  cout << "1. $" << item1.price << "\t " << item1.name << endl;
  cout << "2. $" << item2.price << "\t " << item2.name << endl;
  cout << "3. $" << item3.price << "\t " << item3.name << endl;

  return 0;
}