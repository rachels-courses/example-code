#ifndef _ROOM_H
#define _ROOM_H

#include <string>
using namespace std;

// Class declaration

struct Room
{
    // Member variables
    string name;
    float width;
    float length;

    // Member function (method)
    float GetSqFt();
    void Display();
};

#endif
