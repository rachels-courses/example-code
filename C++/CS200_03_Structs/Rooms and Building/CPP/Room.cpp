#include "Room.h"

#include <iostream>
#include <iomanip>
using namespace std;

// Class' function definitions

float Room::GetSqFt()
{
    return width * length;
}

void Room::Display()
{
    cout << left
        << setw( 20 ) << name
        << "\t" << width
        << "x"
        << length
        << "\t" << GetSqFt()
        << " sqft"
        << endl;
}
