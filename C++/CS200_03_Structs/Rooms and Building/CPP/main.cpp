#include <iostream>
#include <array>
using namespace std;

#include "Room.h"

int main()
{
//    Room myRoom;
//    myRoom.name = "Office";
//    myRoom.width = 12;
//    myRoom.length = 15;
//    myRoom.Display();
//
//    Room yourRoom;
//    yourRoom.name = "Gaming room";
//    yourRoom.width = 20;
//    yourRoom.length = 15;
//    yourRoom.Display();


    array<Room,4> rooms;

    cout << "GET INFORMATION" << endl;
    for ( unsigned int i = 0; i < rooms.size(); i++ )
    {
        cout << endl;
        cout << "ROOM #" << (i+1) << "..." << endl;
        cout << "What is room width?  ";
        cin >> rooms[i].width;

        cout << "What is room length? ";
        cin >> rooms[i].length;

        cout << "What is room name?   ";
        cin.ignore();
        getline( cin, rooms[i].name );
    }

    cout << endl << endl;
    cout << "ROOM INFORMATION" << endl;

    float sum = 0;
    for ( unsigned int i = 0; i < rooms.size(); i++ )
    {
        rooms[i].Display();
        sum += rooms[i].GetSqFt();
    }

    cout << endl << sum << " total sqft" << endl;

    return 0;
}
