// main.cpp

#include <iostream>
using namespace std;

#include "Book.h"

int main()
{
    cout << "-- LIBRARY PROGRAM --" << endl;

    // Create Book variables
    Book book1, book2;

    // Set up member variable values
    book1.title = "The Very Hungry Caterpillar";
    book1.author = "Eric Carle";
    book1.isbn = "0399256733";
    book1.price = 27.18;

    book2.title = "No, David!";
    book2.author = "David Shannon";
    book2.isbn = "1338299581";
    book2.price = 7.62;

    int quantity;
    float totalPrice = 0;
    
    cout << "Book #1: "
	 << book1.title
	 << " $" << book1.price
	 << endl;
    cout << "How many do you want to buy? ";
    cin >> quantity;

    totalPrice += book1.price * quantity;
    
    
    cout << "Book #2: "
	 << book2.title
	 << " $" << book2.price
	 << endl;
    cout << "How many do you want to buy? ";
    cin >> quantity;

    totalPrice += book2.price * quantity;

    cout << endl << "Total price: $" << totalPrice << endl;
    

    
    return 0;
}
