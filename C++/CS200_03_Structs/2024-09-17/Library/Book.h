// Book.h

#include <string>
using namespace std;

#ifndef _BOOK
#define _BOOK

// STRUCT DECLARATION:
struct Book
{
    // MEMBER VARIABLES:
    string title;
    string author;
    float price;
    string isbn;
};

#endif
