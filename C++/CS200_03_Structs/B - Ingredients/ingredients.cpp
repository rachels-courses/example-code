#include <iostream>
using namespace std;

#include "Ingredient.h"

int main() {
  cout << "RECIPE" << endl << endl;

  // Setting up the data
  Ingredient ing1, ing2, ing3, ing4, ing5, ing6, ing7;

  ing1.name = "baking powder";
  ing1.quantity = 0.5;
  ing1.unitOfMeasurement = "tsp";

  ing2.name = "butter";
  ing2.quantity = 1;
  ing2.unitOfMeasurement = "cup(s)";
    
  ing3.name = "white sugar";
  ing3.quantity = 1.5;
  ing3.unitOfMeasurement = "cup(s)";

  ing4.name = "egg";
  ing4.quantity = 1;
  ing4.unitOfMeasurement = "whole";

  ing5.name = "all-purpose flour";
  ing5.quantity = 2.75;
  ing5.unitOfMeasurement = "cup(s)";

  ing6.name = "baking soda";
  ing6.quantity = 1;
  ing6.unitOfMeasurement = "tsp";

  ing7.name = "vanilla extract";
  ing7.quantity = 1;
  ing7.unitOfMeasurement = "tsp";


  // Display recipe
  cout << "Ingredients:" << endl;
  
  cout << "* " << ing1.quantity << " " << ing1.unitOfMeasurement << " of " << ing1.name << endl;
  cout << "* " << ing2.quantity << " " << ing2.unitOfMeasurement << " of " << ing2.name << endl;
  cout << "* " << ing3.quantity << " " << ing3.unitOfMeasurement << " of " << ing3.name << endl;
  cout << "* " << ing4.quantity << " " << ing4.unitOfMeasurement << " of " << ing4.name << endl;
  cout << "* " << ing5.quantity << " " << ing5.unitOfMeasurement << " of " << ing5.name << endl;
  cout << "* " << ing6.quantity << " " << ing6.unitOfMeasurement << " of " << ing6.name << endl;
  cout << "* " << ing7.quantity << " " << ing7.unitOfMeasurement << " of " << ing7.name << endl;

  return 0;
}