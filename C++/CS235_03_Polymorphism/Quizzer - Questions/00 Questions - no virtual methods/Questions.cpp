#include "Questions.hpp"

/* QUESTION *************************************************************************/
Question::Question()
{
    cout << "Question::Question()" << endl;
    m_question      = "(Q) What is 1/2?";
}

Question::~Question()
{
    cout << "Question::~Question()" << endl;
}

bool Question::AskQuestion()
{
    cout << "Question::AskQuestion()" << endl;
    return false;
}

void Question::DisplayQuestion()
{
    cout << "Question::DisplayQuestion()" << endl;
    cout << "* " << m_question << endl;
}

/* TRUE/FALSE QUESTION **************************************************************/
TrueFalseQuestion::TrueFalseQuestion()
{
    cout << "TrueFalseQuestion::TrueFalseQuestion()" << endl;
    m_question      = "(TF) The answer to 3-2 is 5.";
    m_answer        = false;
}

TrueFalseQuestion::~TrueFalseQuestion()
{
    cout << "TrueFalseQuestion::~TrueFalseQuestion()" << endl;
}

bool TrueFalseQuestion::AskQuestion()
{
    cout << "TrueFalseQuestion::AskQuestion()" << endl;
    DisplayQuestion();
    bool answer = true;
    return ( answer == m_answer );
}

/* MULTIPLE CHOICE QUESTION *********************************************************/
MultipleChoiceQuestion::MultipleChoiceQuestion()
{
    cout << "MultipleChoiceQuestion::MultipleChoiceQuestion()" << endl;
    m_question      = "(MC) What is 10x10?";
    m_options[0]    = "200";
    m_options[1]    = "20";
    m_options[2]    = "100";
    m_options[3]    = "10";
    m_correct = 2;
}

MultipleChoiceQuestion::~MultipleChoiceQuestion()
{
    cout << "MultipleChoiceQuestion::~MultipleChoiceQuestion()" << endl;
}

void MultipleChoiceQuestion::ListAllAnswers()
{
    cout << "MultipleChoiceQuestion::ListAllAnswers()" << endl;
    for ( int i = 0; i < 4; i++ )
    {
        cout << (i+1) << ". " << m_options[i] << endl;
    }
}

bool MultipleChoiceQuestion::AskQuestion()
{
    cout << "MultipleChoiceQuestion::AskQuestion()" << endl;
    DisplayQuestion();
    ListAllAnswers();
    int answer = 2;
    return ( answer == m_correct );
}

/* FILL-IN-THE-BLANK QUESTION *******************************************************/
FillInQuestion::FillInQuestion()
{
    cout << "FillInQuestion::FillInQuestion()" << endl;
    m_question  = "(FITB) What is 2+2?";
    m_answer    = "4";
}

FillInQuestion::~FillInQuestion()
{
    cout << "FillInQuestion::~FillInQuestion()" << endl;
}

bool FillInQuestion::AskQuestion()
{
    cout << "FillInQuestion::AskQuestion()" << endl;
    DisplayQuestion();
    string answer = "cheese";
    return ( answer == m_answer );
}
