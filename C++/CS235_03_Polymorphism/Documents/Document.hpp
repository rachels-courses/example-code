#ifndef _DOCUMENT_HPP
#define _DOCUMENT_HPP

#include <vector>
#include <string>
#include <iostream>
using namespace std;

class Document
{
    public:
    virtual ~Document() {}

    virtual void Save( string filename ) = 0;
    virtual void Add() = 0;
    virtual void View() = 0;
    virtual string GetType();

    protected:
    string m_type;
};

class TextDocument : public Document
{
    public:
    TextDocument();
    virtual ~TextDocument() {}

    virtual void Save( string filename );
    virtual void AddLine( string line );
    virtual void Add();
    virtual void View();

    protected:
    vector<string> m_lines;
};

class CsvDocument : public Document
{
    public:
    CsvDocument();
    ~CsvDocument() {}

    virtual void Save( string filename );
    virtual void SetHeader( int col, string header );
    virtual void AddCell( int row, int col, string line );
    virtual void Add();
    virtual void View();

    protected:
    int m_totalRows, m_totalColumns;
    string m_header[100];
    string m_cells[100][100];
};

class HtmlDocument : public Document
{
    public:
    HtmlDocument();
    ~HtmlDocument() {}

    virtual void Save( string filename );
    virtual void AddHtml( string line );
    virtual void Add();
    virtual void View();

    protected:
    vector<string> m_html;
};

#endif
