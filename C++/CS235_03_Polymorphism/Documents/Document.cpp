#include "Document.hpp"
#include "Menu.hpp"

#include <fstream>

string Document::GetType()
{
    return m_type;
}

/* TEXT DOCUMENT *********************************************/
TextDocument::TextDocument()
{
    m_type = "TextDocument";
}

void TextDocument::Save( string filename )
{
    filename += ".txt";
    ofstream output( filename );

    for ( auto& line : m_lines )
    {
        output << line << endl;
    }
}

void TextDocument::Add()
{
    cout << "Enter new line:" << endl;
    string line;
    getline( cin, line );
    AddLine( line );
}

void TextDocument::AddLine( string line )
{
    m_lines.push_back( line );
}

void TextDocument::View()
{
    for ( auto& line : m_lines )
    {
        cout << line << endl;
    }
}

/* CSV DOCUMENT *********************************************/
CsvDocument::CsvDocument()
{
    m_type = "CsvDocument";
    m_totalColumns = 0;
    m_totalRows = 0;
}

void CsvDocument::Save( string filename )
{
    filename += ".csv";
    ofstream output( filename );

    // Write out header
    for ( int col = 0; col < m_totalColumns; col++ )
    {
        output << m_header[col] << ",";
    }
    output << endl;

    // Write out cells
    for ( int row = 0; row < m_totalRows; row++ )
    {
        for ( int col = 0; col < m_totalColumns; col++ )
        {
            output << m_cells[row][col] << ",";
        }
        output << endl;
    }
}

void CsvDocument::Add()
{
    string choice = Menu::ShowStringMenuWithPrompt( { "Header", "Cell" } );

    if ( choice == "Header" )
    {
        int column = Menu::GetValidChoice( 0, 100, "Enter column index" );
        string text = Menu::GetStringLine( "Enter header" );
        SetHeader( column, text );
    }
    else if ( choice == "Cell" )
    {
        int column = Menu::GetValidChoice( 0, 100, "Enter column index" );
        cout << "Column is " << m_header[column] << endl;
        int row = Menu::GetValidChoice( 0, 100, "Enter row index" );
        string text = Menu::GetStringLine( "Enter cell contents" );
        AddCell( row, column, text );
    }
}

void CsvDocument::SetHeader( int col, string header )
{
    m_header[col] = header;

    if ( col > m_totalColumns )
    {
        m_totalColumns = col;
    }
}

void CsvDocument::AddCell( int row, int col, string line )
{
    m_cells[row][col] = line;

    if ( col > m_totalColumns )
    {
        m_totalColumns = col;
    }

    if ( row > m_totalRows )
    {
        m_totalRows = row;
    }
}

void CsvDocument::View()
{
    // Write out header
    for ( int col = 0; col < m_totalColumns; col++ )
    {
        cout << m_header[col] << ",";
    }
    cout << endl;

    // Write out cells
    for ( int row = 0; row < m_totalRows; row++ )
    {
        for ( int col = 0; col < m_totalColumns; col++ )
        {
            cout << m_cells[row][col] << ",";
        }
        cout << endl;
    }
}

/* HTML DOCUMENT *********************************************/
HtmlDocument::HtmlDocument()
{
    m_type = "HtmlDocument";
}

void HtmlDocument::Save( string filename )
{
    filename += ".html";
    ofstream output( filename );

    output << "<html><head><title>" + filename + "</title></head><body>" << endl;

    for ( auto& line : m_html )
    {
        output << line << endl;
    }

    output << "</body></html>" << endl;
}

void HtmlDocument::AddHtml( string line )
{
    m_html.push_back( line );
}

void HtmlDocument::Add()
{
    cout << "Enter new line:" << endl;
    cin.ignore();
    string line;
    getline( cin, line );
    AddHtml( line );
}

void HtmlDocument::View()
{
    for ( auto& line : m_html )
    {
        cout << line << endl;
    }
}
