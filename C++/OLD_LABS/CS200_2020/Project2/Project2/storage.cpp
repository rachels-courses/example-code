#include "storage.hpp"
#include "menus.hpp"
#include "utilities/Logger.hpp"

#include <iostream>
#include <fstream>
#include <limits>
using namespace std;

/**
@param      int         itemCount       The amount of items stored in the array
@param      const int   ARRAY_SIZE      The maximum array size
@return     Returns "true" if the array is full and "false" otherwise.
*/
bool IsFull         ( const int itemCount, const int ARRAY_SIZE )
{
    Logger::Out( "Function begin", "IsFull" ); // Output debug info (just ignore this)
    return false; // placeholder; remove me when implementing the function
}

/**
@param      const int   index           The index we're checking for validity
@param      const int   itemCount       The amount of items stored in the array
@param      const int   ARRAY_SIZE      The maximum array size
@return     Returns "true" if the index is valid and "false" otherwise.
*/
bool IsValidIndex   ( const int index, const int itemCount, const int ARRAY_SIZE )
{
    Logger::Out( "Function begin", "IsValidIndex" ); // Output debug info (just ignore this)
    return false; // placeholder; remove me when implementing the function
}

/**
@param      const string    item            The new item we want to put in the array
@param      string          arr[]           The array of data
@param      int &           itemCount       The amount of items stored in the array
@param      const int       ARRAY_SIZE      The maximum array size
@return     void                            Returns nothing
*/
void AddItem       ( const string item, string arr[], int & itemCount, const int ARRAY_SIZE )
{
    Logger::Out( "Function begin", "AddItem" ); // Output debug info (just ignore this)
    // Implement this function
}

/**
@param      const int       index           The index of the item we want to update
@param      const string    newItem         The new item we want to replace the old item with
@param      string          arr[]           The array of data
@param      const int       itemCount       The amount of items stored in the array
@param      const int       ARRAY_SIZE      The maximum array size
@return     void                            Returns nothing
*/
void UpdateItem    ( const int index, string newItem, string arr[], const int itemCount, const int ARRAY_SIZE )
{
    Logger::Out( "Function begin", "UpdateItem" ); // Output debug info (just ignore this)
    // Implement this function
}

/**
@param      string          arr[]           The array of data
@param      int &           itemCount       The amount of items stored in the array
@param      const int       ARRAY_SIZE      The maximum array size
@return     void                            Returns nothing
*/
void ClearAllItems ( string arr[], int & itemCount, const int ARRAY_SIZE )
{
    Logger::Out( "Function begin", "ClearAllItems" ); // Output debug info (just ignore this)
    // Implement this function
}

/**
@param      string          arr[]           The array of data
@param      int &           itemCount       The amount of items stored in the array
@return     void                            Returns nothing
*/
void ViewAllItems  ( const string arr[], const int itemCount )
{
    Logger::Out( "Function begin", "ViewAllItems" ); // Output debug info (just ignore this)
    // Implement this function
}

/**
@param      const string    filepath        The location and filename of the output file
@param      const string    arr[]           The array of data
@param      const int       itemCount       The amount of items stored in the array
@return     void                            Returns nothing
*/
void SaveItems     ( const string filepath, const string arr[], const int itemCount )
{
    Logger::Out( "Function begin", "SaveItems" ); // Output debug info (just ignore this)
    // Implement this function
}

/**
@param      const string    filepath        The location and filename of the input file
@param      string          arr[]           The array of data
@param      int &           itemCount       The amount of items stored in the array
@return     void                            Returns nothing
*/
void LoadItems     ( const string filepath, string arr[], int & itemCount )
{
    Logger::Out( "Function begin", "LoadItems" ); // Output debug info (just ignore this)
    // Implement this function
}
