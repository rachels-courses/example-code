\contentsline {chapter}{\numberline {1}Turn-in instructions}{2}
\contentsline {chapter}{\numberline {2}Reference - Project setup}{3}
\contentsline {paragraph}{Visual Studio:}{3}
\contentsline {paragraph}{Code::Blocks:}{3}
\contentsline {chapter}{\numberline {3}Variables lab 1: Recipe program}{4}
\contentsline {section}{\numberline {3.1}Step 1: Declare ingredient variables}{5}
\contentsline {section}{\numberline {3.2}Step 2: Display ingredients to the screen}{5}
\contentsline {paragraph}{Concept: User interface design -}{5}
\contentsline {section}{\numberline {3.3}Step 3: Create a variable for ``batchSize''}{6}
\contentsline {section}{\numberline {3.4}Step 4: Ask the user how many batches to make}{6}
\contentsline {paragraph}{Concept: User interface design -}{6}
\contentsline {section}{\numberline {3.5}Step 5: Recalculate ingredient amounts}{7}
\contentsline {section}{\numberline {3.6}Step 6: Display updated ingredient list}{7}
\contentsline {section}{\numberline {3.7}Example output}{7}
\contentsline {chapter}{\numberline {4}Variables lab 2: Student information}{9}
\contentsline {section}{\numberline {4.1}1. Declare and initialize variables}{10}
\contentsline {section}{\numberline {4.2}2. Display student information}{10}
\contentsline {section}{\numberline {4.3}3. Have user update info}{10}
\contentsline {section}{\numberline {4.4}4. Display updated info}{11}
\contentsline {section}{\numberline {4.5}Example output}{11}
\contentsline {chapter}{\numberline {5}Compile error help}{12}
