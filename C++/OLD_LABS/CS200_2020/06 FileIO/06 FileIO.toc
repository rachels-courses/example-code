\contentsline {chapter}{\numberline {1}Setup}{2}
\contentsline {section}{\numberline {1.1}Turn-in instructions}{2}
\contentsline {section}{\numberline {1.2}Setup}{2}
\contentsline {chapter}{\numberline {2}File I/O lab 1: To Do List}{3}
\contentsline {paragraph}{Program overview:}{3}
\contentsline {section}{\numberline {2.1}Specifications}{3}
\contentsline {subsection}{\numberline {2.1.1}Starter code}{3}
\contentsline {subsection}{\numberline {2.1.2}DisplayMainMenu function}{4}
\contentsline {subsection}{\numberline {2.1.3}GetChoice function}{4}
\contentsline {subsection}{\numberline {2.1.4}GetToDoItem}{5}
\contentsline {subsection}{\numberline {2.1.5}main()}{5}
\contentsline {section}{\numberline {2.2}Possible errors}{6}
\contentsline {subsection}{\numberline {2.2.1}The program is skipping my input!}{6}
\contentsline {section}{\numberline {2.3}Testing}{7}
\contentsline {chapter}{\numberline {3}File I/O lab 2: Voting}{9}
\contentsline {paragraph}{Program overview:}{9}
\contentsline {section}{\numberline {3.1}Specifications}{9}
\contentsline {section}{\numberline {3.2}Testing}{10}
\contentsline {chapter}{\numberline {4}File I/O lab 3: Saving and loading data}{11}
\contentsline {paragraph}{Program overview:}{11}
\contentsline {section}{\numberline {4.1}Specifications}{11}
\contentsline {subsection}{\numberline {4.1.1}Setup}{11}
\contentsline {subsection}{\numberline {4.1.2}SaveDataFile function}{12}
\contentsline {paragraph}{Parameters:}{12}
\contentsline {subsection}{\numberline {4.1.3}LoadDataFile function}{12}
\contentsline {paragraph}{Parameters:}{12}
\contentsline {section}{\numberline {4.2}Testing}{14}
\contentsline {chapter}{\numberline {5}Appendix}{15}
\contentsline {section}{\numberline {5.1}Bank starter code}{15}
