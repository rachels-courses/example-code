#ifndef _SEARCH_HPP
#define _SEARCH_HPP

#include <string>
#include <vector>
using namespace std;

#include "animal.hpp"

int LinearSearch( string findme, vector<Animal> arr );
int BinarySearch( string findme, vector<Animal> arr );

#endif
