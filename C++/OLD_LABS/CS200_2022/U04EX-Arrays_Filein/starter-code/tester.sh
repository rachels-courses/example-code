# -- Color codes from --
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# Variables for my test scripts
BuildingProgram="${Color_Off}-------------------------------------------------"
TestSpecs="\n ${Green}-------------------------------------------------"
ExpectedOutput="\n${Green}EXPECTED OUTPUT:"
ActualOutput="\n${Yellow}ACTUAL OUTPUT:"
FileOutput="\n${Purple}TEXT OUTPUT:"
TestCases="\n${Cyan}TEST CASES:"
ProgramCode="${Blue}-------------------------------------------------"
ProgramDone="\n\n ${White}------- PRESS A KEY TO CONTINUE -------------------------------------------------"
Header="\n\n${White}-------------------------------------------------\n"

clear

echo -e $BuildingProgram
echo -e "Build program 1..."
clang++ program1.cpp -o program1exe

echo -e $BuildingProgram
echo -e "Build program 2..."
clang++ program2.cpp -o program2exe

echo -e $BuildingProgram
echo -e "Build program 3..."
clang++ program3.cpp -o program3exe

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 1"

echo -e $TestSpecs
echo -e "TEST 1: ./program1exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program1exe filename"
echo -e $ActualOutput
./program1exe


echo -e $TestSpecs
echo -e "TEST 2: ./program1exe numbers1.txt"
echo -e $ExpectedOutput
echo -e "EXPECTED OUTPUT: 5 + 60 + 99 + 10 = 264"
echo -e $ActualOutput
./program1exe numbers1.txt


echo -e $TestSpecs
echo -e "TEST 3: ./program1exe ./program1exe numbers2.txt"
echo -e $ExpectedOutput
echo -e "EXPECTED OUTPUT: 5 + 4 + 3 + 2 = 14"
echo -e $ActualOutput
./program1exe numbers2.txt


# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 2"

echo -e $TestSpecs
echo -e "TEST 2: ./program2exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program2exe input_file"
echo -e $ActualOutput
./program2exe


echo -e $TestSpecs
echo -e "TEST 2: ./program2exe grades1.txt"
echo -e $ExpectedOutput
echo "EXPECTED OUTPUT:"
echo "COURSE     GRADE"
echo "---------------"
echo "CS134      4.0"
echo "CS200      3.5"
echo "CS210      4.0"
echo "ASL120     3.0"
echo ""
echo "GPA: 3.625"
echo -e $ActualOutput
./program2exe grades1.txt


echo -e $TestSpecs
echo -e "TEST 2: ./program2exe grades2.txt"
echo -e $ExpectedOutput
echo "EXPECTED OUTPUT:"
echo "COURSE     GRADE"
echo "---------------"
echo "CS134      4.0"
echo "CS200      3.5"
echo "CS210      4.0"
echo "ASL120     3.0"
echo ""
echo "GPA: 3.625"
echo -e $ActualOutput
./program2exe grades2.txt





# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 3"

echo -e $TestSpecs
echo -e "TEST 2: ./program3exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program3exe input_file"
echo -e $ActualOutput
./program3exe


echo -e $TestSpecs
echo -e "TEST 2: 50 25 | ./program3exe bank.txt"
echo -e $ExpectedOutput
echo "Valid transaction"
echo -e $FileOutput
echo "bank.txt before:"
cat bank.txt
echo -e $ActualOutput
echo 50 25 | ./program3exe bank.txt
echo -e $FileOutput
echo "bank.txt after:"
cat bank.txt


echo -e $TestSpecs
echo -e "TEST 3: -9 25 | ./program3exe bank.txt"
echo -e $ExpectedOutput
echo "Test invalid deposit"
echo -e $FileOutput
echo "bank.txt before:"
cat bank.txt
echo -e $ActualOutput
echo -9 25 | ./program3exe bank.txt
echo -e $FileOutput
echo "bank.txt after:"
cat bank.txt


echo -e $TestSpecs
echo -e "TEST 4: 20 10000 | ./program3exe bank.txt"
echo -e $ExpectedOutput
echo "Test invalid deposit - Deposit 20 and withdraw 10000"
echo -e $FileOutput
echo "bank.txt before:"
cat bank.txt
echo -e $ActualOutput
echo -9 25 | ./program3exe bank.txt
echo -e $FileOutput
echo "bank.txt after:"
cat bank.txt


echo -e $TestCases
cat program3_tests.txt
