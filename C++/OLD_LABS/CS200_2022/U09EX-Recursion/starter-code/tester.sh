# -- Color codes from --
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# Variables for my test scripts
BuildingProgram="${Color_Off}-------------------------------------------------"
TestSpecs="\n ${Green}-------------------------------------------------"
ExpectedOutput="\n${Green}EXPECTED OUTPUT:"
ExpectedOutput_Display="${Black}${On_Green}"
ActualOutput="\n${Yellow}ACTUAL OUTPUT:"
ActualOutput_Display="${Black}${On_Yellow}"
FileOutput="\n${Purple}TEXT OUTPUT:"
TestCases="\n${Cyan}TEST CASES:"
ProgramCode="${Blue}-------------------------------------------------"
ProgramDone="\n\n ${White}------- PRESS A KEY TO CONTINUE -------------------------------------------------"
Header="\n\n${White}-------------------------------------------------\n"

clear

echo -e $BuildingProgram
echo -e "Build program 1..."
clang++ program1.cpp -o program1exe

echo -e $BuildingProgram
echo -e "Build program 2..."
clang++ program2.cpp -o program2exe

echo -e $BuildingProgram
echo -e "Build program 3..."
clang++ program3.cpp -o program3exe

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 1"

echo -e $Color_Off
echo -e $TestSpecs
echo -e "TEST 1: ./program1exe"
echo -e $ExpectedOutput
echo -e $ExpectedOutput_Display
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program1exe START END"
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program1exe
echo -e $Color_Off

echo -e $Color_Off
echo -e $TestSpecs
echo -e "TEST 2: ./program1exe 1 5"
echo -e $ExpectedOutput
echo -e $ExpectedOutput_Display
echo "ITERATIVE: 1 2 3 4 5"
echo "RECURSIVE: 1 2 3 4 5"
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program1exe 1 5
echo -e $Color_Off

echo -e $Color_Off
echo -e $TestSpecs
echo -e "TEST 3: ./program1exe 5 10"
echo -e $ExpectedOutput
echo -e $ExpectedOutput_Display
echo "ITERATIVE: 5 6 7 8 9 10"
echo "RECURSIVE: 5 6 7 8 9 10"
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program1exe 5 10
echo -e $Color_Off


# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 2"

echo -e $Color_Off
echo -e $TestSpecs
echo -e "TEST 1: ./program2exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program2exe START END"
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program1exe
echo -e $Color_Off

echo -e $Color_Off
echo -e $TestSpecs
echo "TEST 2: ./program2exe 1 5"
echo -e $ExpectedOutput
echo -e $ExpectedOutput_Display
echo "ITERATIVE: 15"
echo "RECURSIVE: 15"
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program2exe 1 5
echo -e $Color_Off

echo -e $Color_Off
echo -e $TestSpecs
echo -e "TEST 3: ./program1exe 5 10"
echo -e $ExpectedOutput
echo -e $ExpectedOutput_Display
echo "ITERATIVE: 45"
echo "RECURSIVE: 45"
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program2exe 5 10
echo -e $Color_Off


# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 3"

echo -e $Color_Off
echo -e $TestSpecs
echo "TEST 1: ./program3exe"
echo -e $ExpectedOutput
echo -e $ExpectedOutput_Display
echo "ITERATIVE VERSIONS:"
echo ""
echo "List 1: "
echo "0  CS134     1  CS200     2  CS235     "
echo ""
echo "List 2: "
echo "0  Kabe      1  Luna      2  Pixel     3  Korra     "
echo ""
echo "RECURSIVE VERSIONS:"
echo ""
echo "List 1: "
echo "0  CS134     1  CS200     2  CS235     "
echo ""
echo "List 2: "
echo "0  Kabe      1  Luna      2  Pixel     3  Korra "    
echo ""
echo "GOODBYE"
echo ""
echo -e $Color_Off
echo -e $ActualOutput
echo -e $ActualOutput_Display
./program3exe
echo -e $Color_Off





#cat program3.cpp
