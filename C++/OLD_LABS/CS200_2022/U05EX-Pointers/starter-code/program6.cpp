#include <fstream>  /* use ifstream and ofstream */
#include <iomanip>  /* Use setprecision */
#include <iostream> /* Use cout */
#include <string>   /* Use strings */
using namespace std;

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 4) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0]
         << " input_file account_index action amount" << endl;
    cout << "action should be W for withdraw or D for deposit" << endl;
    cout << "amount is how much to withdraw/deposit to the account specified"
         << endl;
    cout << endl;
    cout << "Examples:" << endl;
    cout << argumentList[0] << " p5_bank1.txt 0 W 10" << endl;
    cout << argumentList[0] << " p5_bank2.txt 1 D 250" << endl;
    return 1; /* Exit with error code 1 */
  }

  // Add code here

  /* Quit program with code 0 (no errors) */
  return 0;
}