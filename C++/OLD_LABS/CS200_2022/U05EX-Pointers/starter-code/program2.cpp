#include <iomanip>  /* Use formatting */
#include <iostream> /* Use cout */
#include <string>   /* Use string */
using namespace std;

int main() {
  // Create basic variables
  string student1 = "UNASSIGNED";
  string student2 = "UNASSIGNED";
  string student3 = "UNASSIGNED";

  // Display table of information - each variable's MEMORY ADDRESS and VALUE.
  cout << left;
  cout << "ORIGINAL DATA:" << endl;
  cout << string(80, '-') << endl;
  cout << setw(15) << "Variable:  | " << setw(20) << "student1" << setw(20)
       << "student2" << setw(20) << "student3" << endl
       << string(80, '-') << endl;
  cout << setw(15) << "Address:   | " << setw(20) << &student1 << setw(20)
       << &student2 << setw(20) << &student3 << endl
       << string(80, '-') << endl;
  cout << setw(15) << "Value:     | " << setw(20) << student1 << setw(20)
       << student2 << setw(20) << student3 << endl
       << string(80, '-') << endl;
  cout << endl;

  // Add code here

  cout << left;
  cout << "MODIFIED DATA:" << endl;
  cout << string(80, '-') << endl;
  cout << setw(15) << "Variable:  | " << setw(20) << "student1" << setw(20)
       << "student2" << setw(20) << "student3" << endl
       << string(80, '-') << endl;
  cout << setw(15) << "Address:   | " << setw(20) << &student1 << setw(20)
       << &student2 << setw(20) << &student3 << endl
       << string(80, '-') << endl;
  cout << setw(15) << "Value:     | " << setw(20) << student1 << setw(20)
       << student2 << setw(20) << student3 << endl
       << string(80, '-') << endl;
  cout << endl;

  /* Quit program with code 0 (no errors) */
  cout << endl << "THE END" << endl;
  return 0;
}