#include <iomanip>  /* Use formatting */
#include <iostream> /* Use cout */
using namespace std;

int main() {
  // Create basic variables
  int number1 = 1337;
  int number2 = 1010;
  int number3 = 5;

  // Display table of information - each variable's MEMORY ADDRESS and VALUE.
  cout << left;
  cout << endl << string(80, '-') << endl;
  cout << setw(15) << "Variable:  | " << setw(20) << "number1" << setw(20)
       << "number2" << setw(20) << "number3" << endl
       << string(80, '-') << endl;
  cout << setw(15) << "Address:   | " << setw(20) << &number1 << setw(20)
       << &number2 << setw(20) << &number3 << endl
       << string(80, '-') << endl;
  cout << setw(15) << "Value:     | " << setw(20) << number1 << setw(20)
       << number2 << setw(20) << number3 << endl
       << string(80, '-') << endl;

  // Add code here

  /* Quit program with code 0 (no errors) */
  cout << endl << "THE END" << endl;
  return 0;
}