# -- Color codes from --
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# Variables for my test scripts
BuildingProgram="${Color_Off}-------------------------------------------------"
TestSpecs="\n ${Green}-------------------------------------------------"
ExpectedOutput="\n${Green}EXPECTED OUTPUT:"
ActualOutput="\n${Yellow}ACTUAL OUTPUT:"
FileOutput="\n${Purple}TEXT OUTPUT:"
TestCases="\n${Cyan}TEST CASES:"
ProgramCode="${Blue}-------------------------------------------------"
ProgramDone="\n\n ${White}------- PRESS A KEY TO CONTINUE -------------------------------------------------"
Header="\n\n${White}-------------------------------------------------\n"

clear

echo -e $BuildingProgram
echo -e "Build program 1..."
clang++ program1.cpp -o program1exe

echo -e $BuildingProgram
echo -e "Build program 2..."
clang++ program2.cpp -o program2exe

echo -e $BuildingProgram
echo -e "Build program 3..."
clang++ program3.cpp -o program3exe

echo -e $BuildingProgram
echo -e "Build program 4..."
clang++ program4.cpp -o program4exe

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 1"

echo -e $TestSpecs
echo -e "TEST 1: ./program1exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program1exe batches"
echo -e $ActualOutput
./program1exe

echo -e $TestSpecs
echo -e "TEST 2: ./program1exe 1"
echo -e $ExpectedOutput
echo -e "recipe with 1x ingredients"
echo -e $ActualOutput
./program1exe 1

echo -e $TestSpecs
echo -e "TEST 3: ./program1exe 2"
echo -e $ExpectedOutput
echo -e "recipe with 2x ingredients"
echo -e $ActualOutput
./program1exe 2

echo -e $FileOutput
cat recipe.txt

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 2"

echo -e $TestSpecs
echo -e "TEST 1: ./program2exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program2exe x1 y1 x2 y2"
echo -e $ActualOutput
./program2exe

echo -e $TestSpecs
echo -e "TEST 2: ./program2exe 1 3 6 5"
echo -e $ExpectedOutput
echo -e "File saved to slope_1-3_6-5.txt"
echo -e $ActualOutput
./program2exe 1 3 6 5
echo -e $FileOutput
cat slope_1-3_6-5.txt

echo -e $TestSpecs
echo -e "TEST 3: ./program2exe 1 1 2 2"
echo -e $ExpectedOutput
echo -e "File saved to slope_1-1_2-2.txt"
echo -e $ActualOutput
./program2exe 1 1 2 2
echo -e $FileOutput
cat slope_1-1_2-2.txt


# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 3"

echo -e $TestSpecs
echo -e "TEST 1: ./program3exe"
echo -e $ExpectedOutput
echo -e "Please enter the zipcode: 1"
echo -e "Please enter the state: 2"
echo -e "Please enter the stampPrice: 3"
echo -e "Please enter the status: 4"
echo -e "Please enter the city: 5"
echo -e ""
echo -e "DELIVERY INFO"
echo -e "Delivering to"
echo -e "5, 2, 1"
echo -e "Status is 4"
echo -e "Price is \$3"
echo -e ""
echo -e $ActualOutput
echo 1 2 3 4 5 | ./program3exe

echo -e $TestSpecs
echo -e "TEST 2: ./program3exe"
echo -e $ExpectedOutput
echo -e "Please enter the zipcode: 64138"
echo -e "Please enter the state: MO"
echo -e "Please enter the stampPrice: 0.59"
echo -e "Please enter the status: D"
echo -e "Please enter the city: Kansas City"
echo -e ""
echo -e "DELIVERY INFO"
echo -e "Delivering to"
echo -e "Kansas City, MO 64138"
echo -e "Status is D"
echo -e "Price is \$0.59"
echo -e ""
echo -e $ActualOutput
echo 64138 MO 0.59 D "Kansas City" | ./program3exe




# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 4"

echo -e $TestSpecs
echo -e "TEST 1: ./program4exe"
echo -e $ExpectedOutput
echo -e "NOT ENOUGH ARGUMENTS, Expected form: program4exe adult_tickets child_tickets"
echo -e $ActualOutput
./program4exe

echo -e $TestSpecs
echo -e "TEST 2: ./program4exe 2 3"
echo -e $ExpectedOutput
echo -e "What is the tax rate? 0.9"
echo -e "Receipt saved to receipt-2-3.txt"
echo -e ""
echo -e $ActualOutput
echo 0.9 > ./program4exe 2 3
echo -e $FileOutput
cat receipt-2-3.txt
echo -e $TestCases
cat program4_tests.txt
