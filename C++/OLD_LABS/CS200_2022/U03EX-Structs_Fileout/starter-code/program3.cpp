#include <iostream> /* Use cout */
#include <string>   /* Use strings */
using namespace std;

int main() {
  /* This program doesn't take in any input arguments,
  instead we're going to get input from the user while the
  program is running, using cin statements. */

  // Add code here

  /* Quit program with code 0 (no errors) */
  return 0;
}