# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

BuildingProgram="${Color_Off}-------------------------------------------------"
TestSpecs="\n ${Green}-------------------------------------------------"
ExpectedOutput="\n${Green}EXPECTED OUTPUT:"
ActualOutput="\n${Yellow}ACTUAL OUTPUT:"
FileOutput="\n${Purple}TEXT OUTPUT:"
TestCases="\n${Cyan}TEST CASES:"
ProgramCode="${Blue}-------------------------------------------------"
ProgramDone="\n\n ${White}------- PRESS A KEY TO CONTINUE -------------------------------------------------"
Header="\n\n${White}-------------------------------------------------\n"

clear

echo -e $BuildingProgram
echo -e "Build program 1..."
clang++ program1/*.cpp -o program1exe

echo -e $BuildingProgram
echo -e "Build program 2..."
clang++ program2/*.cpp -o program2exe

echo -e $BuildingProgram
echo -e "Build program 3..."
clang++ program3/*.cpp -o program3exe

echo -e $BuildingProgram
echo -e "Build program 4..."
clang++ program4/*.cpp -o program4exe

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 1"

echo -e $TestSpecs
echo -e "TEST 1: ./program1exe"
echo -e $ExpectedOutput
echo -e "Call DoSomething1(), DoSomething2(), then quit"
echo 1 2 3 | ./program1exe

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 2"

echo -e $TestSpecs
echo -e "TEST 1: ./program2exe"
echo -e $ExpectedOutput
echo -e "Expected form: program2exe input_file"
echo -e $ActualOutput
./program2exe

echo -e $TestSpecs
echo -e "TEST 2: ./program3exe item_prices.txt"
echo -e $ExpectedOutput
echo -e "Display contents of file"
echo -e $ActualOutput
./program2exe item_prices.txt

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 3"

echo -e $TestSpecs
echo -e "TEST 1: ./program3exe"
echo -e $ExpectedOutput
echo -e "Expected form: program3exe input_file"
echo -e $ActualOutput
./program3exe

echo -e $TestSpecs
echo -e "TEST 2: ./program3exe sum - inputs 2 and 3"
echo -e $ExpectedOutput
echo -e "Sum: 5"
echo -e $ActualOutput
echo 2 3 | ./program3exe sum

echo -e $TestSpecs
echo -e "TEST 3: ./program3exe area - inputs 2 and 3"
echo -e $ExpectedOutput
echo -e "Area: 6"
echo -e $ActualOutput
echo 2 3 | ./program3exe area

echo -e $TestSpecs
echo -e "TEST 4: ./program3exe slope - inputs 1 3 2 6"
echo -e $ExpectedOutput
echo -e "Slope: 3"
echo -e $ActualOutput
echo 1 3 2 6 | ./program3exe slope

echo -e $TestSpecs
echo -e "TEST 5: ./program3exe tests"
echo -e $ExpectedOutput
echo -e "(Tests)"
echo -e $ActualOutput
./program3exe tests

# ----------------------------------------------
echo -e $ProgramDone
read anykey

echo -e "${Header} PROGRAM 4"

echo -e $TestSpecs
echo -e "TEST 1: ./program4exe - inputs I S X"
echo -e $ExpectedOutput
echo -e "Character is initialized and saved"
echo -e $ActualOutput
echo "I S X" | ./program4exe

echo -e $TestSpecs
echo -e "TEST 2: ./program4exe - inputs L X"
echo -e $ExpectedOutput
echo -e "Character is loaded"
echo -e $ActualOutput
echo "L X" | ./program4exe

echo -e $TestSpecs
echo -e "TEST 2: ./program4exe - inputs E X"
echo -e $ExpectedOutput
echo -e "Character is edited"
echo -e $ActualOutput
echo "E Luna 1 2 X" | ./program4exe



