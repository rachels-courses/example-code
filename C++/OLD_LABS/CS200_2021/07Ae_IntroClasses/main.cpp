#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Die.hpp"
#include "Card.hpp"

void Program1()
{
    cout << endl << "DICE" << endl;

    cout << endl << "SIX-SIDED: " << endl;
    // Dice program goes here
    Die die1;
    cout << die1.Roll() << endl;
    cout << die1.Roll() << endl;
    cout << die1.Roll() << endl;

    cout << endl << "MANY SIDES:" << endl;
    int sides;
    cout << "How many sides? ";
    cin >> sides;

    Die die2( sides );
    cout << die2.Roll() << endl;
    cout << die2.Roll() << endl;
    cout << die2.Roll() << endl;
}

void Program2()
{
    cout << endl << "CARDS" << endl;
    // Playing card program goes here

    Card card1;
    card1.SetRank( "Q" );
    card1.SetSuit( 'D' );
    card1.Display();
}

// You don't need to modify main()
int main()
{
    bool done = false;
    srand( time( NULL ) );

    while ( !done )
    {
        cout << endl << "MAIN MENU" << endl;
        cout << "-------------------" << endl;
        cout << "1. Dice" << endl;
        cout << "2. Cards" << endl;
        cout << "0. Quit" << endl;

        int choice;
        cin >> choice;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 0 ) { done = true; }
    }

    return 0;
}
