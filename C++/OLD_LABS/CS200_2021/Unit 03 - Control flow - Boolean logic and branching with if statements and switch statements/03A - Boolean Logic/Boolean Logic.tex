\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Boolean logic}
\newcommand{\laTitle}       {}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../latex-stuff/rachwidgets}
\usepackage{../../latex-stuff/rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ }

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

    %\tableofcontents
    
\chapter*{03Ae: Boolean logic}
    \section*{Turn-in instructions}
        
        This is a non-coding assignment. You can submit your work in any of these ways:

        \begin{itemize}
            \item   Print out this document, fill out the questions, scan/photograph to turn in.
            \item   Open this document up in an editor, fill it out on the computer, save and turn in.
            \item   Type out your answers in a text editor / MS Word / LibreOffice, save and turn in.
        \end{itemize}

    \newpage
    \subsection*{Propositional Logic}

    \begin{intro}{\ }
        A \textbf{proposition} is a statement which has truth value: it is either true (T) or false (F).
        \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}

        The statement doesn't have to necessarily be TRUE, it could also
        be FALSE, but it has to be unambiguously so. ~\\

        \begin{tabular}{ | l | l | l | }
            \hline
            \textbf{Statement} & \textbf{Proposition?} & \textbf{Result?}
            \\ \hline
            2 + 3 = 5 & Yes & True
            \\ \hline
            2 + 2 = 5 & Yes & False
            \\ \hline
            This class has 30 students & Yes & False\footnote{Probably :)}
            \\ \hline
            Is the movie good? & No &
            \\ \hline
        \end{tabular}
    \end{intro}
    
    
    \stepcounter{question} \question{\thequestion}{Propositional?}{12\%}
        For the following, mark whether the statement is a \textbf{proposition} and,
        if it is, mark whether it is \textbf{true} or \textbf{false}.

        \begin{center}
            \begin{tabular}{l l | c | c}
                & \textbf{statement} & \textbf{is proposition?} & \textbf{is true/false?}
                \\ \hline
                a. & 10 + 20 = 30
                & & \\ \hline
                
                b. & 2 times an integer is always even.
                & & \\ \hline

                c. & $4 < 5$
                & & \\ \hline

                d. & $2 + 2$
                & & \\ \hline

                e. & $x \cdot y$ is odd.
                & & \\ \hline

                f. & $5 + 1$ is odd.
                & & \\ \hline
            \end{tabular}
        \end{center}
        
        
        
        
        
    \newpage

    \subsection*{Compound Propositions}

    \begin{intro}{\ }
        We can also create a \textbf{compound proposition} using the logical operators
        for AND \texttt{\&\&}, OR \texttt{||}, and NOT \texttt{!}. When we're writing out
        a compound proposition, we will usually assign each proposition a
        \textbf{propositional variable}.

        ~\\ If we have the propositions
        
        \begin{itemize}
         \item  \texttt{paulInCourse} meaning ``Paul is taking discrete math'', and
         \item  \texttt{paulHasCalculator} meaning ``Paul has a calculator''   
        \end{itemize}
        
        then we can build the compound propositions like:
        
        \begin{itemize}
            \item[] \texttt{paulInCourse \&\& paulHasCalculator}: \\ 
                    Paul is taking discrete math and Paul has a calculator
            \item[] \texttt{paulInCourse || paulHasCalculator}: \\
                    Paul is taking discrete math or Paul has a calculator
            \item[] \texttt{!paulInCourse}: \\
                    Paul is NOT taking discrete math
            \item[] \texttt{!paulHasCalculator}: \\
                    Paul does NOT have a calculator.
        \end{itemize}

        The result of a compound proposition depends on the value of
        each proposition it is made up of. Let's say we're working with
        generic boolean variables \texttt{p}, \texttt{q}, and \texttt{r}
        that can contain either true or false:

        \begin{enumerate}
            \item A compound proposition \texttt{p \&\& q \&\& r} is \textbf{only true}
                if all propositions are true; it will be \textbf{false} if
                one or more of the propositions is false.
            \item A compound proposition \texttt{p || q || r} is \textbf{true}
                if one or more of the propositions is true; it is \textbf{only false}
                if all propositions are false.
            \item A compound proposition \texttt{!p} is \textbf{true} only
                if the proposition is false; it is only \textbf{false} if
                the proposition is true.
        \end{enumerate}

    \end{intro}
    
    \newpage
    \stepcounter{question} \question{\thequestion}{Compound statements}{12\%}
        \paragraph{a. a AND b:} ~\\

        \begin{tabular}{ | l  c | c | p{6cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            a. &        \texttt{a \&\& b} &       $a = true, b = true$ &       TRUE   \\ \hline
            b. &        \texttt{a \&\& b} &       $a = true, b = false$ &       FALSE   \\ \hline
            c. &        \texttt{a \&\& b} &       $a = false, b = true$ &       FALSE   \\ \hline
            d. &        \texttt{a \&\& b} &       $a = false, b = false$ &       FALSE   \\ \hline
        \end{tabular}

        \paragraph{b. a OR b:} ~\\

        \begin{tabular}{ | l  c | c | p{6cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            e. &        \texttt{a || b} &       $a = true, b = true$ &     \\ \hline
            f. &        \texttt{a || b} &       $a = true, b = false$ &    \\ \hline
            g. &        \texttt{a || b} &       $a = false, b = true$ &    \\ \hline
            h. &        \texttt{a || b} &       $a = false, b = false$ &   \\ \hline
        \end{tabular}

        \paragraph{c. Combinations:} ~\\

        \begin{tabular}{ | l  c | c | p{6cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            i.  &        \texttt{a \&\& !b} &       $a = true, b = false$ &
            \\ 
                & \multicolumn{3}{ p{12cm} |} { \small \texttt{b} is false, so read it as \texttt{!(false)}. What is ``not-false'' equivalent to? }
            \\  &  \multicolumn{3}{c | }{} \\
                & \multicolumn{3}{ p{11cm} |} { \small So then, \texttt{a} is true, and \texttt{!b} is \fitb[2cm], so \texttt{a \&\& !b} is... }
                
            \\ \hline
            
            j.  &        \texttt{a || !b} &        $a = false, b = true$ &   
            \\ & & &
            \\ \hline
                        
            k.  &        \texttt{!(a \&\& b)} &       $a = false, b = true$ &  
            \\ 
                & \multicolumn{3}{ p{12cm} |} { \small First, look at \texttt{a \&\& b}. What is its result? }
            \\  &  \multicolumn{3}{c | }{} \\
                & \multicolumn{3}{ p{12cm} |} { \small Based on that, what is \texttt{!(a \&\& b)}? }
            \\ \hline
            
            l. &        \texttt{!(a || b)} &        $a = false, b = false$ &  
            \\ & & & 
            \\ \hline
        \end{tabular}
    
    \newpage
    \stepcounter{question} \question{\thequestion}{Compound statements 2}{18\%}
        For the following, ``translate" the following English statements
        into compound propositions.

        ~\\
        \texttt{milk}: There is milk in the fridge \\
        \texttt{eggs}: There are eggs in the fridge \\
        \texttt{cheese}: There is cheese in the fridge
        
        \begin{enumerate}
            \item[a.]   There is milk in the fridge \textbf{or} there is cheese in the fridge.
                        \vspace{1cm}
                        
            \item[b.]   There is milk in the fridge \textbf{and} there is cheese in the fridge.
                        \vspace{1cm}
            
            \item[c.]   There is milk in the fridge but (\textbf{and}) there are no eggs in the fridge.
                        ~\\ \color{blue} \texttt{milk \&\& !eggs} \color{black}
                        
            \item[d.]   There is cheese in the fridge but (\textbf{and}) there is no milk in the fridge.
                        \vspace{1cm}
                        
            \item[e.]   It is \textbf{not} true that... there is milk \textbf{or} cheese in the fridge.
                        ~\\ \color{blue} \texttt{!( milk || cheese )} \color{black}
                        
            \item[f.]   It is \textbf{not} true that... there is milk \textbf{and} cheese in the fridge.
                        \vspace{1cm}
                        
            \item[g.]   There is \textbf{not} milk in the fridge \textbf{and} there is \textbf{not} cheese in the fridge.
                        ~\\ \color{blue} \texttt{!milk \&\& !cheese} \color{black}
                        
            \item[h.]   There is \textbf{not} milk in the fridge \textbf{or} there is \textbf{not} cheese in the fridge.
                        \vspace{1cm}
                        
            \item[i.]   There is milk in the fridge, \textbf{or} there is cheese in the fridge, but (\textbf{and}) \textbf{not} both.
                        \vspace{1cm}
        \end{enumerate}
    



    \newpage

    \subsection*{Truth Tables}

    \begin{intro}{\ }
        When we're working with compound propositional statements,
        the result of the compound depends on the true/false values
        of each proposition it is built up of.
        ~\\
        We can diagram out all possible states of a compound proposition
        by using a \textbf{truth table}. In a truth table, we list
        all propositional variables first on the left, as well as
        all possible combinations of their states, and then
        the compound statement's result on the right.

        ~\\

        \begin{tabular}{ l l l }
            Truth table for AND:
            &
            Truth table for OR:
            &
            Truth table for NOT:
            \\

            \begin{tabular}{ | c | c || c | }
                \hline
                \texttt{p} & \texttt{q} & \texttt{p \&\& q}
                \\ \hline
                T & T & T 
                \\ \hline
                T & F & F 
                \\ \hline
                F & T & F 
                \\ \hline
                F & F & F 
                \\ \hline

            \end{tabular} 
            &

            \begin{tabular}{ | c | c || c | }
                \hline
                \texttt{p} & \texttt{q} & \texttt{p || q}
                \\ \hline
                T & T & T \\ \hline
                T & F & T \\ \hline
                F & T & T \\ \hline
                F & F & F \\ \hline

            \end{tabular}
            &

            \begin{tabular}{ | c || c | }
                \hline
                \texttt{p} & \texttt{!p} \\ \hline
                T & F \\ \hline
                F & T \\ \hline

            \end{tabular}

        \end{tabular}
    \end{intro}
    
    
    \stepcounter{question} \question{\thequestion}{Truth tables}{18\%}
        Complete the following truth tables. ~\\
        
        ~\\
        a.
        \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} || p{3cm} | }
            \hline
            \texttt{p} & \texttt{q} & \texttt{!q} & \texttt{p \&\& !q} \\ \hline
            T & T & F & \\ \hline
            T & F & T & \\ \hline
            F & T &  & \\ \hline
            F & F &  & \\ \hline
        \end{tabular}

        ~\\~\\
        b.
        \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{2cm} || p{3cm} | }
            \hline
            \texttt{p} & \texttt{q} & \texttt{!p} & \texttt{!q} & \texttt{!p || !q} \\ \hline
            T & T & & & \\ \hline
            T & F & & & \\ \hline
            F & T & & & \\ \hline
            F & F & & & \\ \hline
        \end{tabular}
        
    \newpage
    \begin{intro}{ Truth tables and boolean logic with programs }
        These examples using \texttt{p} and \texttt{q} are generalized
        representations of boolean expressions, where \texttt{p} and \texttt{q} represent
        some kind of boolean variables. But how do we apply this more to our
        programming?
        ~\\
        
        Let's say we're writing a program with the \texttt{milk}, \texttt{cheese},
        and \texttt{eggs} variables from earlier. We want an if statement to check for
        ``There is milk in the fridge, or there is cheese in the fridge, but (and) not both.''
        and our programmer has written this:
        
        The programmer has written this condition to check for invalid input: ~\\ ~\\
        \texttt{ if ( !( milk || cheese ) ) }
        
        ~\\ Let's use a truth table to make sure it works as intended... ~\\
        
        \begin{tabular}{ | c | c || c || c | }
            \hline
            Bool var & Bool var & (Helper) & Outcome \\
            \hline 
            \texttt{milk} & \texttt{cheese} & \texttt{milk || cheese} & \texttt{!(milk || cheese)}
            \\ \hline
            T & T & T & F
            \\ \hline
            T & F & T & F
            \\ \hline
            F & T & T  & F
            \\ \hline
            \cellcolor{yellow} F & \cellcolor{yellow} F & F & \cellcolor{yellow} T
            \\ \hline
        \end{tabular}
        
        ~\\
        From our truth table, we can validate the logic: Our outcome \texttt{!(milk || cheese)}
        is only valid when both \texttt{milk} and \texttt{cheese} are FALSE.
        This doesn't line up with the requested logic of there being milk \textit{or} cheese
        \textit{but not both}.
        ~\\
        
        With this information, we can retool the condition and try again... How about ~\\
        \texttt{ if ( milk || cheese \&\& !(milk \&\& cheese) ) } ? ~\\~\\        
        \begin{tabular}{ | c | c || c || c || c || c | }
            \hline 
            \texttt{m} & \texttt{c} & \texttt{m || c} & \texttt{m \&\& c} & \texttt{!(m \&\& c)} & \texttt{ (m || c) \&\& !(m \&\&c) }
            \\ \hline
            T & T & T & T & F & F
            \\ \hline     
            \cellcolor{yellow} T & \cellcolor{yellow} F & T & F & T & \cellcolor{yellow} T
            \\ \hline     
            \cellcolor{yellow} F & \cellcolor{yellow} T & T & F & T & \cellcolor{yellow} T
            \\ \hline     
            F & F & F & F & T & F
            \\ \hline
        \end{tabular}
        
        ~\\ In this case, the two scenarios that result in true are: 
        (1) \texttt{milk} is TRUE and \texttt{cheese} is FALSE, and 
        (2) \texttt{milk} is FALSE and \texttt{cheese} is TRUE, so this logic is correct.
        
    \end{intro}
    
    \newpage
    \stepcounter{question} \question{\thequestion}{Truth tables 2}{40\%}
        Write out the truth tables for the questions from Q3:
        
        \begin{itemize}
            \item[a.]   There is milk in the fridge \textbf{or} there is cheese in the fridge. ~\\
                        \large
                        \begin{tabular}{| c | c || p{8cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & 
                            \\ \hline
                            T & T & \\ \hline
                            T & F & \\ \hline
                            F & T & \\ \hline
                            F & F & \\ \hline
                        \end{tabular}
                        \normalsize
                        
            \item[b.]   There is milk in the fridge \textbf{and} there is cheese in the fridge. ~\\
                        \large
                        \begin{tabular}{| c | c || p{8cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & 
                            \\ \hline
                            T & T & \\ \hline
                            T & F & \\ \hline
                            F & T & \\ \hline
                            F & F & \\ \hline
                        \end{tabular}
                        \normalsize
            
            \item[c.]   There is milk in the fridge but (\textbf{and}) there are no eggs in the fridge. ~\\
                        \large
                        \begin{tabular}{| c | c || p{2cm} || p{6cm} |} \hline
                            \texttt{milk} & \texttt{eggs} & \texttt{!eggs} & \texttt{milk \&\& !eggs}
                            \\ \hline
                            T & T & & \\ \hline
                            T & F & & \\ \hline
                            F & T & & \\ \hline
                            F & F & & \\ \hline
                        \end{tabular}
                        \normalsize
                        
            \item[d.]   There is cheese in the fridge but (\textbf{and}) there is no milk in the fridge.~\\
                        \large
                        \begin{tabular}{| c | c || p{8cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & 
                            \\ \hline
                            T & T & \\ \hline
                            T & F & \\ \hline
                            F & T & \\ \hline
                            F & F & \\ \hline
                        \end{tabular}
                        \normalsize
            
            \newpage
            \item[e.]   It is \textbf{not} true that... there is milk \textbf{or} cheese in the fridge.~\\
                        \large
                        \begin{tabular}{| c | c ||p{4cm} || p{4cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & \small \texttt{milk || cheese} & \small \texttt{!( milk || cheese )}
                            \\ \hline
                            T & T & & \\ \hline
                            T & F & & \\ \hline
                            F & T & & \\ \hline
                            F & F & & \\ \hline
                        \end{tabular}
                        \normalsize
                        
            \item[f.]   It is \textbf{not} true that... there is milk \textbf{and} cheese in the fridge.~\\
                        \large
                        \begin{tabular}{| c | c || p{8cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & 
                            \\ \hline
                            T & T & \\ \hline
                            T & F & \\ \hline
                            F & T & \\ \hline
                            F & F & \\ \hline
                        \end{tabular}
                        \normalsize
                        
            \item[g.]   There is \textbf{not} milk in the fridge \textbf{and} there is \textbf{not} cheese in the fridge.~\\
                        \large
                        \begin{tabular}{| c | c || p{2cm} | p{2cm} || p{4cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & \texttt{!milk} & \texttt{!cheese} & \small \texttt{!milk \&\& !cheese}
                            \\ \hline
                            T & T & & & \\ \hline
                            T & F & & & \\ \hline
                            F & T & & & \\ \hline
                            F & F & & & \\ \hline
                        \end{tabular}
                        \normalsize
                        
            \item[h.]   There is \textbf{not} milk in the fridge \textbf{or} there is \textbf{not} cheese in the fridge.~\\
                        \large
                        \begin{tabular}{| c | c || p{8cm} |} \hline
                            \texttt{milk} & \texttt{cheese} & 
                            \\ \hline
                            T & T & \\ \hline
                            T & F & \\ \hline
                            F & T & \\ \hline
                            F & F & \\ \hline
                        \end{tabular}
                        \normalsize
                        \vspace{1cm}
            
        \end{itemize}

\end{document}

