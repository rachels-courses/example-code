#include <iostream>
using namespace std;

// Specify some test cases for students to test with as well.

int main()
{
    cout << "Branching with If statements" << endl << endl;

    float bankBalance;

    cout << "Enter a bank balance: ";
    cin >> bankBalance;

    cout << endl;

    // -----------------------------------------------------------------------------------
    // ---------------------------- IF statement -----------------------------------------

    if ( bankBalance < 0 )
    {
        cout << "!! ALERT: YOUR ACCOUNT IS OVERDRAWN !!" << endl << endl;
    }

    // -----------------------------------------------------------------------------------
    // ---------------------------- IF/ELSE statement ------------------------------------

    float amount;

    cout << "How much do you want to deposit? ";
    cin >> amount;

    if ( amount > 0 )
    {
        bankBalance += amount;
        cout << "Balance is now $" << bankBalance << endl << endl;
    }
    else
    {
        cout << "ERROR: " << amount << " is an invalid deposit amount!" << endl;
    }

    // -----------------------------------------------------------------------------------
    // ---------------------------- IF/ELSE IF/ELSE statement ----------------------------

    cout << "How much do you want to withdraw? ";
    cin >> amount;

    if ( amount <= 0 )
    {
        cout << "ERROR: Cannot withdraw a 0 or negative amount!" << endl;
    }
    else if ( amount > bankBalance )
    {
        cout << "ERROR: Cannot withdraw more than balance amount!" << endl;
    }
    else
    {
        bankBalance -= amount;
        cout << "Balance is now $" << bankBalance << endl << endl;
    }


    return 0;
}
