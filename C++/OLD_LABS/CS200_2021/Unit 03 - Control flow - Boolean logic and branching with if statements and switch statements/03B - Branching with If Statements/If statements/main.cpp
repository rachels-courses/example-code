#include <iostream>
using namespace std;

int main()
{
    // -----------------------------------------------------------------------------------
    // ---------------------------- IF statement -----------------------------------------
    cout << "PROGRAM 1" << endl;
    float bankBalance, withdrawAmount;

    cout << "What is your bank balance? ";
    cin >> bankBalance;

    cout << "How much to withdraw? ";
    cin >> withdrawAmount;

    bankBalance -= withdrawAmount;

    cout << "Balance: " << bankBalance;
    if ( bankBalance < 0 )
    {
        cout << " (OVERDRAWN)";
    }

    cout << endl << endl;

    // -----------------------------------------------------------------------------------
    // ---------------------------- IF/ELSE statement ------------------------------------
    cout << "PROGRAM 2" << endl;
    float earnedPoints, totalPoints;

    cout << "How many points did you score on the assignment? ";
    cin >> earnedPoints;

    cout << "How many total points was the assignment? ";
    cin >> totalPoints;

    float result = earnedPoints / totalPoints;

    if ( result >= 0.5 )
    {
        cout << "Pass" << endl;
    }
    else
    {
        cout << "Fail" << endl;
    }

    cout << endl;

    // -----------------------------------------------------------------------------------
    // ---------------------------- IF/ELSE IF/ELSE statement ----------------------------
    cout << "PROGRAM 3" << endl;
    int charge;
    cout << "Enter your phone charge %: ";
    cin >> charge;

    if ( charge >= 95 )
    {
        cout << "[****]" << endl;
    }
    else if ( charge >= 75 )
    {
        cout << "[***_]" << endl;
    }
    else if ( charge >= 50 )
    {
        cout << "[**__]" << endl;
    }
    else if ( charge >= 25 )
    {
        cout << "[*___]" << endl;
    }
    else
    {
        cout << "[____]" << endl;
    }

    cout << endl;

    // -----------------------------------------------------------------------------------
    // ---------------------------- And && operator --------------------------------------
    cout << "PROGRAM 4" << endl;
    cout << "1. Coffee" << endl;
    cout << "2. Tea" << endl;
    cout << "3. Water" << endl;
    cout << "4. Soda" << endl;
    cout << "5. Juice" << endl;
    int choice;
    cout << "Select a drink: ";
    cin >> choice;

    if ( choice >= 1 && choice <= 5 )
    {
        cout << "Good choice!" << endl;
    }
    else
    {
        cout << "Invalid choice!" << endl;
    }

    cout << endl;

    // -----------------------------------------------------------------------------------
    // ---------------------------- Or || operator ---------------------------------------
    cout << "PROGRAM 5" << endl;
    int choice2;
    cout << "Enter a number between 1 and 10: ";
    cin >> choice2;

    if ( choice2 < 1 || choice2 > 10 )
    {
        cout << "Invalid choice!" << endl;
    }
    else
    {
        cout << "Valid choice!" << endl;
    }

    return 0;
}
