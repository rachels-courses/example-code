#include <iostream>
#include <cmath>
using namespace std;

// Specify some test cases for students to test with as well.

int main()
{
    char operation;

    cout << "Enter what kind of math operation you want to do:" << endl;
    cout << "+ for addition" << endl;
    cout << "- for subtraction" << endl;
    cout << "* for multiplication" << endl;
    cout << "/ for division" << endl;
    cout << "s for square root" << endl;
    cout << "p for power" << endl;
    cout << endl;

    cout << "Choice: ";
    cin >> operation;

    cout << endl;

    switch( operation )
    {
        case '+':
        {
            float num1, num2;
            cout << "Enter two numbers, separated by a space: ";
            cin >> num1 >> num2;
            float result = num1 + num2;
            cout << num1 << " + " << num2 << " = " << result << endl;
        }
        break;

        case '-':
        {
            float num1, num2;
            cout << "Enter two numbers, separated by a space: ";
            cin >> num1 >> num2;
            float result = num1 - num2;
            cout << num1 << " - " << num2 << " = " << result << endl;
        }
        break;

        case '*':
        {
            float num1, num2;
            cout << "Enter two numbers, separated by a space: ";
            cin >> num1 >> num2;
            float result = num1 * num2;
            cout << num1 << " * " << num2 << " = " << result << endl;
        }
        break;

        case '/':
        {
            float num1, num2;
            cout << "Enter two numbers, separated by a space: ";
            cin >> num1 >> num2;

            if ( num2 == 0 )
            {
                cout << "Cannot divide by zero!" << endl;
            }
            else
            {
                float result = num1 / num2;
                cout << num1 << " / " << num2 << " = " << result << endl;
            }
        }
        break;

        case 's':
        {
            int num;
            cout << "Enter the number to get the square root of: ";
            cin >> num;
            float root = sqrt( num );
            cout << "sqrt(" << num << ") = " << root << endl;
        }
        break;

        case 'p':
        {
            int base, exponent;
            cout << "Enter the base number: ";
            cin >> base;
            cout << "Enter the exponent: ";
            cin >> exponent;
            int result = pow( base, exponent );
            cout << base << "^" << exponent << " = " << result << endl;
        }
        break;

        default:
        cout << "Invalid choice!" << endl;
    }



    return 0;
}
