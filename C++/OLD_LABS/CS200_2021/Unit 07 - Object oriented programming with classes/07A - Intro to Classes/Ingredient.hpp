#ifndef _INGREDIENT_HPP
#define _INGREDIENT_HPP

#include <string>
using namespace std;

class Ingredient
{
    public:
    void Setup( string name, float amount, string unit );
    void Display();

    private:
    string m_name;
    string m_unit;
    float m_amount;
};

void Ingredient::Setup( string name, float amount, string unit )
{
    m_name = name;
    m_amount = amount;
    m_unit = unit;
}

void Ingredient::Display()
{
    cout << m_amount << " " << m_unit << " of " << m_name << endl;
}


#endif
