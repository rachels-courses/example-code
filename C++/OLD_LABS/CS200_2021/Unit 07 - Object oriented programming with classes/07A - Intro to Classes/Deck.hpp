#ifndef _DECK_HPP
#define _DECK_HPP

#include "Card.hpp"

class Deck
{
    public:
    Deck();
    void CreateDeck();
    void DisplayCard( int s, int r );

    private:
    Card m_cards[4][13];
};

Deck::Deck()
{
    CreateDeck();
}

void Deck::CreateDeck()
{
    const int SUITS = 4;
    const int RANKS = 13;
    const char suits [] = { 'C' , 'D' , 'H' , 'S' };
    const string ranks [] = {   "A" , "1" , "2" , "3" , "4" ,
                                "5", "6", "7", "8", "9",
                                "10" , "J" , "Q" , "K" };

    for ( int s = 0; s < SUITS; s++ )
    {
        for ( int r = 0; r < RANKS; r++ )
        {
            m_cards[s][r].Setup( ranks[r], suits[s] );
        }
    }
}

void Deck::DisplayCard( int s, int r )
{
    cout << "Card [" << s << "][ " << r << "] is: ";
    m_cards[s][r].DisplayName();
    cout << endl;
}

#endif
