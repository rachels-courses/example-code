#ifndef _RECIPE_HPP
#define _RECIPE_HPP

#include <string>
using namespace std;

#include "Ingredient.hpp"

class Recipe
{
    public:
    Recipe();
    void SetName( string name );
    void SetInstructions( string instructions );
    void AddIngredient( string name, float amount, string unit );
    void Display();

    private:
    string m_name;
    string m_instructions;
    Ingredient m_ingredients[10];
    int m_totalIngredients;
};

Recipe::Recipe()
{
    m_totalIngredients = 0;
}

void Recipe::SetName( string name )
{
    m_name = name;
}

void Recipe::SetInstructions( string instructions )
{
    m_instructions = instructions;
}

void Recipe::AddIngredient( string name, float amount, string unit )
{
    if ( m_totalIngredients == 10 )
    {
        cout << "Ingredient list is full!" << endl;
        return;
    }

    m_ingredients[m_totalIngredients].Setup( name, amount, unit );
    m_totalIngredients++;
}

void Recipe::Display()
{
    cout << m_name << endl << endl;
    for ( int i = 0; i < m_totalIngredients; i++ )
    {
        m_ingredients[i].Display();
    }

    cout << m_instructions << endl;
}


#endif
