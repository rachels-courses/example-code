#include "Question.hpp"

#include <iostream>
using namespace std;

int main()
{
    int score = 0;
    int totalQuestions = 3;

    // Set up questions

    FillInTheBlankQuestion question1;
    question1.SetQuestion( "What is the best programming language?" );
    question1.SetAnswer( "C++" );

    TrueFalseQuestion question2;
    question2.SetQuestion( "Pineapple belongs on pizza." );
    question2.SetAnswer( true );

    MultipleChoiceQuestion question3;
    question3.SetQuestion( "What was the best video game of 2019?" );
    string answers[] = { "Control", "The Outer Wilds", "Death Stranding", "Sekiro" };
    question3.SetAnswers( answers, 1 );

    // Run quiz

    cout << endl << "QUIZZER" << endl;
    cout << "--------------------------------------------" << endl;

    score += question1.AskQuestion();
    score += question2.AskQuestion();
    score += question3.AskQuestion();

    cout << endl << "RESULTS" << endl;
    cout << score << " correct out of " << totalQuestions << endl;

    return 0;
}
