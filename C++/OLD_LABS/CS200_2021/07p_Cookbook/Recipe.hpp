#ifndef _RECIPE_HPP
#define _RECIPE_HPP

#include <string>
using namespace std;

#include "Ingredient.hpp"

class Recipe
{
    public:
    Recipe();
    void AddIngredient( string name, float amount, string unit );
    void Display();

    // Getters and Setters
    void SetName( string name );
    string GetName();
    void SetInstructions( string instructions );
    string GetInstructions();
    void SetSource( string source );
    string GetSource();

    private:
    string m_name;
    string m_instructions;
    string m_source;
    Ingredient m_ingredients[20];
    const int MAX_INGREDIENTS;
    int m_totalIngredients;
};

#endif
