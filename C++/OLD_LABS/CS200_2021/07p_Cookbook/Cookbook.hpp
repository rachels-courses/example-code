#ifndef _COOK_BOOK_HPP
#define _COOK_BOOK_HPP

#include "Recipe.hpp"

#include <string>
using namespace std;

class Cookbook
{
    public:
    Cookbook();
    void SetTitle( string title );
    string GetTitle();
    void DisplayRecipeList();
    void DisplayRecipe( int index );
    void LoadRecipes( string filename );

    private:
    string m_title;
    Recipe m_recipes[10];
    int m_totalRecipes;
};

#endif
