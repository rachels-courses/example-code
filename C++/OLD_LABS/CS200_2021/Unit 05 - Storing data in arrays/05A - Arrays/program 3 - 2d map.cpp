#include <iostream>
using namespace std;

int main()
{
    const int WIDTH = 30;
    const int HEIGHT = 10;
    
    string gameMap[WIDTH][HEIGHT];
    
    for ( int y = 0; y < HEIGHT; y++ )
    {
        for ( int x = 0; x < WIDTH; x++ )
        {
            if ( x == 0 || x == WIDTH - 1 || y == 0 || y == HEIGHT-1 )
            {
                gameMap[x][y] = "#";
            }
            else
            {
                gameMap[x][y] = ".";
            }
        }
    }
    
    
    for ( int y = 0; y < HEIGHT; y++ )
    {
        for ( int x = 0; x < WIDTH; x++ )
        {
            cout << gameMap[x][y];
        }
        cout << endl;
    }
    
    
    return 0;
}
