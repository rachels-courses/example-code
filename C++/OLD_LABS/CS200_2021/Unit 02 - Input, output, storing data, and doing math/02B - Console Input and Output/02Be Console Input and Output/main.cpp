#include <iostream>
#include <string>
using namespace std;

// Recipe from here:
// https://www.allrecipes.com/recipe/10813/best-chocolate-chip-cookies/

int main()
{
    cout << "Cookie Recipe" << endl;
    cout << endl;

    cout << "INGREDIENTS" << endl;
    cout << "----------------------------------" << endl;
    cout << "1 cup butter, softened" << endl;
    cout << "1 cup white sugar" << endl;
    cout << "1 cup packed brown sugar" << endl;
    cout << "2 eggs" << endl;
    cout << "2 teaspoons vanilla extract" << endl;
    cout << "1 teaspoon baking soda" << endl;
    cout << "2 teaspoons hot water" << endl;
    cout << "1/2 teaspoon salt" << endl;
    cout << "3 cups all-purpose flour" << endl;
    cout << "2 cups semisweet chocolate" << endl;
    cout << "----------------------------------" << endl;
    cout << endl;

    cout << "DIRECTIONS" << endl;
    cout << "1. Preheat oven to 350 degrees F" << endl << endl;
    cout << "2. Cream together the butter, white sugar, and brown sugar until smooth." << endl
         << "   Beat in the eggs one at a time, then stir in the vanilla." << endl
         << "   Dissolve baking soda in hot water. Add to batter along with salt." << endl
         << "   Stir in flour, chocolate chips, and nuts." << endl
         << "   Drop by large spoonfuls onto ungreased pans." << endl << endl;
    cout << "3. Bake for about 10 minutes in the preheated oven." << endl;

    return 0;
}
