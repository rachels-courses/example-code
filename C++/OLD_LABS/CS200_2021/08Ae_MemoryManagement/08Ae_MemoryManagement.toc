\contentsline {chapter}{\numberline {1}Introduction}{2}%
\contentsline {section}{\numberline {1.1}About}{2}%
\contentsline {section}{\numberline {1.2}Turn-in instructions}{2}%
\contentsline {section}{\numberline {1.3}Memory addresses}{3}%
\contentsline {paragraph}{Question 1\ - Bits and Bytes}{3}%
\contentsline {paragraph}{Question 2\ - Bits and Bytes}{4}%
\contentsline {section}{\numberline {1.4}Binary, Decimal, and Hexadecimal}{5}%
\contentsline {paragraph}{Example:}{6}%
\contentsline {paragraph}{Example:}{6}%
\contentsline {paragraph}{Question 3\ - Binary and Hexadecimal}{6}%
\contentsline {section}{\numberline {1.5}Arrays and memory}{7}%
\contentsline {paragraph}{Question 4\ - Filling space}{7}%
\contentsline {paragraph}{Question 5\ - Filling space}{8}%
\contentsline {paragraph}{Question 6\ - Resizing arrays}{9}%
\contentsline {section}{\numberline {1.6}Addresses and Pointers}{10}%
\contentsline {paragraph}{Question 7\ - Addresses of variables}{11}%
\contentsline {paragraph}{Question 8\ - Pointing to addresses}{12}%
\contentsline {paragraph}{Question 9\ - Pointing to addresses}{13}%
\contentsline {chapter}{\numberline {2}Solutions}{14}%
