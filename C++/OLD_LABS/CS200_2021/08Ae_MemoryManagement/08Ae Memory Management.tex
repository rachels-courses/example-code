\input{../../Exercise-Head}
\newcommand{\laClass}       {CS 200}
\newcommand{\laSemester}    {Spring 2021}
\newcommand{\laTitle}       {08Ae: Memory Management}
\newcounter{question}

\toggletrue{answerkey}
\togglefalse{answerkey}

\chapter*{ \laTitle }

\section*{Instructions}

    Read through the exercise and answer the questions. Turn in your answers on Canvas once done.
    
    ~\\ These are all fine:
    \begin{itemize}
        \item   Print out pages, fill out, scan/photograph to turn in.
        \item   Turn PDF into image files, fill out in paint program, save and turn in.
        \item   Write out answers on a separate sheet of paper, scan/photograph to turn in. (Please number the answers)
        \item   Type out answers / draw answers in a separate file, save and turn in. (Please number the answers)
    \end{itemize}

\newpage
\section*{Memory and programs}

    A program that is currently running has four main parts of memory:
    
    \begin{center}
        \begin{tabular}{| c |} \hline
            \textbf{SOMEPROGRAM.EXE} \\ \hline
            Heap \\ \hline
            Stack \\ \hline
            Static/Global \\ \hline
            Code \\ \hline
        \end{tabular}
    \end{center}
    
    The operating system itself is responsible for giving memory to programs,
    so we only have to worry about memory management at the \textit{program level}.
    (You will learn more about how Operating Systems deal with memory in an
    Operating Systems course).

    \paragraph{The Stack} is where our normal variables get allocated.
    This includes declaring variables inside a function, or as part of
    a function's parameter list.
    When we \textbf{allocate space in the stack}, it will \textbf{automatically
    be de-allocated} once we leave the function.
    The stack has \textbf{limited space}.
    If you accidentally call the same function in an infinite loop,
    you will eventually get a \textbf{stack overflow} once the stack space
    is out of available memory.
    
    \paragraph{The Heap} is a special part of memory.
    We have \textbf{(virtually) unlimited heap space} to allocate memory in.
    However, with the heap, we must \textbf{manually allocate and deallocate memory} ourselves,
    and all the memory must be accessed via a \textbf{pointer variable}.
    
    \paragraph{The Code} portion of memory is where the program's code is stored.
    
    \paragraph{The Static/Global} portion of memory is where global variables are
    stored (but you SHOULDN'T be using global variables!) and static variables
    (which you'll learn about later on.)
    
    \newpage
    \stepcounter{question}
    \begin{simplequestion}{\thequestion}{Stack, Global, Code space}
        Identify which parts of the following
        program would be part of the \textbf{stack space}, \textbf{global space},
        or \textbf{code space}.
        
\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

const int TOTAL_CARS = 5;

int main()
{
    int participants;
    cout << "How many people? ";
    cin >> participants;
    
    int peoplePerCar = participants / TOTAL_CARS;
    
    cout << "Fit " << peoplePerCar
        << " people per car." << endl;

    return 0;
}
\end{lstlisting}

        \begin{enumerate}
            \item[a.]   \texttt{const int TOTAL\_CARS} ~\\ 
                        \solution   { $\ocircle$ Stack \tab \radio Global \tab $\ocircle$ Code }
                                    { $\ocircle$ Stack \tab $\ocircle$ Global \tab $\ocircle$ Code }
            \item[b.]   \texttt{int participants} ~\\
                        \solution   { \radio Stack \tab $\ocircle$ Global \tab $\ocircle$ Code }
                                    { $\ocircle$ Stack \tab $\ocircle$ Global \tab $\ocircle$ Code }
            \item[c.]   \texttt{int peoplePerCar} ~\\
                        \solution   { \radio Stack \tab $\ocircle$ Global \tab $\ocircle$ Code }
                                    { $\ocircle$ Stack \tab $\ocircle$ Global \tab $\ocircle$ Code }
            \item[d.]   The program code ~\\
                        \solution   { $\ocircle$ Stack \tab $\ocircle$ Global \tab \radio Code }
                                    { $\ocircle$ Stack \tab $\ocircle$ Global \tab $\ocircle$ Code }
        \end{enumerate}
        
    \end{simplequestion}

    \newpage
    \section*{Memory addresses}
        \subsection*{Binary and decimal number systems}
            Binary is a base-2 number system, meaning we have the values 0 and 1 to work with.
            
            ~\\ Decimal is a base-10 number system (the one we use every day), meaning we have values 0 through 9 to work with.
            
            ~\\ To specify the base we're talking about,
            we can write binary numbers as (\texttt{0})$_2$ to specify that it's base-2. A decimal number can be written with
            a subscript of 10 like (0)$_{10}$ to show that it's a decimal number.
    
        \subsection*{Bits and bytes}
            
            A single bit in memory can store a value of \texttt{0} or \texttt{1}. If we want to store more data, we need more bits.
            
            ~\\ If we have two bits, we can store four values: 
            ~\\ \texttt{00} (0), \texttt{01} (1), \texttt{10} (2), \texttt{11} (3).
            
            ~\\ If we have three bits, we can store eight values:
            ~\\ \texttt{000} (0), \texttt{001} (1), \texttt{010} (2), \texttt{011} (3), 
                \texttt{100} (4), \texttt{101} (5), \texttt{110} (6), \texttt{111} (7).
                
            ~\\ The amount of data we can store is equivalent to $2^n$, where we have $n$ bits.
            
            ~\\ With most programming we do, we work with \textbf{bytes} instead of bits, where 1 byte = 8 bits.
            With 1 byte, we can store 0 to $2^8-1$, or 0 through 255. ~\\
            0 is (\texttt{0000 0000})$_2$ and 255 is (\texttt{1111 1111})$_2$.
    
    
        \subsection*{Variable sizes}
        When we create a variable in our program, memory is set aside in RAM to store the value we're going to store.
        How much RAM allocated is based on the size of the variable you've declared: \texttt{bool} and \texttt{char} are 1 byte,
        \texttt{int} and \texttt{float} are 4 bytes, and a \texttt{double} is 8 bytes.
    
    \newpage
    \stepcounter{question}
    \begin{simplequestion}{\thequestion}{Sizes of data types}
        This table represents a series of bytes in memory:
        \begin{center}
        \begin{tabular}{| l | c | c | c | c | c | c | c | c |} \hline
            Address & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                    & \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
            \\ \hline
        \end{tabular}
        \end{center}
        
        Shade in the appropriate amount of blocks for each data type, starting with position 0
        and going forward however many bytes are needed for the data type.
        
        \begin{enumerate}
            \item[a.]   A \texttt{bool}:
                        \begin{center}
                        \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                            0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                            \cellcolor{colorblind_medium_blue} \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
                            \\ \hline
                        \end{tabular}
                        \end{center}
                        
            \item[b.]   An \texttt{int}:
                        \solution{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }
                        
            \item[c.]   A \texttt{char}:
                        \solution{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \cellcolor{colorblind_medium_blue} \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }
                        
            \item[d.]   A \texttt{double}:
                        \solution{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }
                        
            \item[e.]   A \texttt{float}:
                        \solution{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \cellcolor{colorblind_medium_blue} \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }{
                            \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c |} \hline
                                0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \hline
                                \tab & \tab & \tab & \tab & \tab & \tab & \tab & \tab
                                \\ \hline
                            \end{tabular}
                            \end{center}
                        }
        \end{enumerate}
        
        * Note that actual memory addresses don't use decimal numbers (0 through 9) to represent memory addresses. Usually, these are base-16 numbers (0-9, A-F).
    
    \end{simplequestion}
    
    \newpage
    \section*{Address-of operator \&}
    
        In C++, we can use the \textbf{address-of operator} (\&) to access the address of any variable.
    
\begin{lstlisting}[style=code]
int num;        // Declare integer
cout << &num;   // Display the address
\end{lstlisting}

        The variable's memory address will be different each time we run the program; Whatever memory is
        free and available will be allocated once the variable is declared. The memory address will look
        like a hexidecimal number (base-16 number) like this: \texttt{0x7ffd3a24cc70} (this is because converting between
        binary and hex is easy). ~\\
        
        \begin{center}
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x0 & 0x1 & 0x2 & 0x3 & 0x4 & 0x5 & 0x6 & 0x7 & 0x8 & 0x9 & 0xa & 0xb \\ \hline
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} num } & & & & & & & &
            \\ \hline
        \end{tabular}
        \end{center}
        
        \footnotesize{(*Note: Addresses may not always start at an address ending with 0; these are just examples.)}
        \normalsize
        ~\\
    
        If we're creating an array, each element of the array will be \textbf{contiguous in memory}.
        An array of 3 integers will essentially be like declaring 3 different integers together, but
        they will be neighbors in memory.
    
\begin{lstlisting}[style=code]
int arr[3];
cout << &arr[0] << endl;        // Would show address 0
cout << &arr[1] << endl;        // Would show address 4
cout << &arr[2] << endl;        // Would show address 8
\end{lstlisting}
    
        \begin{center}
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x0 & 0x1 & 0x2 & 0x3 & 0x4 & 0x5 & 0x6 & 0x7 & 0x8 & 0x9 & 0xa & 0xb \\ \hline
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} arr[0] } &
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_orange} arr[1] } &
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_gray} arr[2] }
            \\ \hline
        \end{tabular}
        \end{center}
    
    \newpage
    \stepcounter{question}
    \begin{simplequestion}{\thequestion}{Address of variables}
        Given four variables declared:
        ~\\ \texttt{int age;}
        ~\\ \texttt{char choice;}
        ~\\ \texttt{float price;}
        ~\\ \texttt{bool quit;}
        
        ~\\
        We have a diagram of memory where these variables were allocated:
        
        \begin{center}
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x00 & 0x01 & 0x02 & 0x03 & 0x04 & 0x05 & 0x06 & 0x07 & 0x08 & 0x09 & 0x0a & 0x0b \\ \hline
            ... & ... & \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age } & 
            ... & ... & ... & ... & \multicolumn{1}{|c|}{ \cellcolor{colorblind_light_gray} quit } & ... 
            \\ \hline
        \end{tabular}
        
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x0c & 0x0d & 0x0e & 0x0f & 0x10 & 0x11 & 0x12 & 0x13 & 0x14 & 0x15 & 0x16 & 0x17 \\ \hline
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_orange} price } & ... & ... & ... & 
            \multicolumn{1}{|c|}{ \cellcolor{colorblind_light_blue} choice } & ... & ... & ... &... 
            \\ \hline
        \end{tabular}
        \end{center}
        
        ~\\
        
        Identify the memory address for each:
        
        \begin{enumerate}
            \item[a.]   \texttt{\&age}      \solution{0x02}{ \vspace{1cm} }
            \item[b.]   \texttt{\&choice}   \solution{0x13}{ \vspace{1cm} }
            \item[c.]   \texttt{\&price}    \solution{0x0c}{ \vspace{1cm} }
            \item[d.]   \texttt{\&quit}     \solution{0x0a}{ \vspace{1cm} }
        \end{enumerate}
    \end{simplequestion}
    
    \newpage
    \section*{Pointer variables}
    
        There are a special type of variables in C++ called \textbf{pointers}. Pointer 
        variables are just another type of variable - you give it a name, and it stores some value -
        except instead of storing strings or ints or floats, a \textbf{pointer stores a memory address}.
        You can assign a value to a pointer by using the address-of operator to access another variable's address.
    
\begin{lstlisting}[style=code]
int myNum;                  // Normal integer variable
int * myPointer = &myNum;   // Store myNum's address
\end{lstlisting}

        You can update a pointer's value any time throughout the program; during some parts,
        it might point to \texttt{integer1}, other times it might point to \texttt{integer2}.
    
    \stepcounter{question}
    \begin{simplequestion}{\thequestion}{Pointing to addresses}
        Given the diagram of memory:
        
        \begin{center}
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x00 & 0x01 & 0x02 & 0x03 & 0x04 & 0x05 & 0x06 & 0x07 & 0x08 & 0x09 & 0x0a & 0x0b \\ \hline
            ... & ... & \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age1 } 
            & ... & ... & \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age2 }
            \\ \hline
        \end{tabular}
        
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x0c & 0x0d & 0x0e & 0x0f & 0x10 & 0x11 & 0x12 & 0x13 & 0x14 & 0x15 & 0x16 & 0x17 \\ \hline
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age3 } &
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age4 }
            & ... & ... & ... & 
            \\ \hline
        \end{tabular}
        \end{center}
        
        A variable has been declared...
        
\begin{lstlisting}[style=code]
int * myPointer;
\end{lstlisting}

        Identify the address stored in the pointer when the pointer is pointing to different variables' addresses:
        
        \begin{enumerate}
            \item[a.]   \texttt{myPointer = \&age1;} ~\\ myPointer is pointing to age1, so the address it's storing is:
                        \solution{ 0x02 }{ \vspace{0.5cm} }
                        
            \item[b.]   \texttt{myPointer = \&age2;} ~\\ myPointer is pointing to age2, so the address it's storing is:
                        \solution{ 0x08 }{ \vspace{0.5cm} }
                        
            \item[c.]   \texttt{myPointer = \&age3;} ~\\ myPointer is pointing to age3, so the address it's storing is:
                        \solution{ 0x0c }{ \vspace{0.5cm} }
        \end{enumerate}
    \end{simplequestion}
    
    \newpage
    \section*{Dereferencing pointers to get values}
    
        Why would we even bother to store the address of a variable? Well, we can use the \textbf{de-reference} operator
        to look inside that memory address to \textbf{read and write} in that location.
    
\begin{lstlisting}[style=code]
// Output what is currently stored in the memory address
cout << *myPointer;

// Overwrite the value at that address
cin >> *myPointer;
\end{lstlisting}

    \stepcounter{question}
    \begin{simplequestion}{\thequestion}{Pointing to addresses}
        These variables have been declared and assigned values:
        
\begin{lstlisting}[style=code]
int age1 = 35;
int age2 = 18;
int age3 = 24;
int age4 = 52;
\end{lstlisting}

        ~\\ Here they are in memory:
        
        ~\\
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x00 & 0x01 & 0x02 & 0x03 & 0x04 & 0x05 & 0x06 & 0x07 & 0x08 & 0x09 & 0x0a & 0x0b \\ \hline
            ... & ... & \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age1 } 
            & ... & ... & \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age2 }
            \\ \hline
        \end{tabular}
        ~\\
        \begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
            0x0c & 0x0d & 0x0e & 0x0f & 0x10 & 0x11 & 0x12 & 0x13 & 0x14 & 0x15 & 0x16 & 0x17 \\ \hline
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age3 } &
            \multicolumn{4}{|c|}{ \cellcolor{colorblind_light_blue} age4 }
            & ... & ... & ... & 
            \\ \hline
        \end{tabular}
        
        ~\\ Given this information, identify the \textbf{value} stored when...
        
        \begin{enumerate}
            \item[a.]   \texttt{myPointer} is pointing to memory address 0x08. The value is: \solution{ 18 }{ \vspace{1cm} }
            \item[b.]   \texttt{myPointer} is pointing to memory address 0x02. The value is: \solution{ 35 }{ \vspace{1cm} }
            \item[c.]   \texttt{myPointer} is pointing to memory address 0x10. The value is: \solution{ 52 }{ \vspace{1cm} }
                        
        \end{enumerate}
    \end{simplequestion}
        
    

\input{../../Exercise-Foot}
