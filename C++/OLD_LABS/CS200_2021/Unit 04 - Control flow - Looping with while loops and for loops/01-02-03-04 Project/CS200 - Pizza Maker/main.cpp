#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << "PIZZA MAKER" << endl;

    const float BASE_PRICE      = 8.00;
    const float SAUCE_PRICE     = 0.53;
    const float CHEESE_PRICE    = 0.73;
    const float MEATS_PRICE     = 1.25;
    const float VEGGIES_PRICE   = 0.83;

    string pizza = "";
    float price = BASE_PRICE;
    int menuChoice;
    bool done = false;
    while ( !done )
    {
        cout << endl;
        cout << "Current pizza: " << pizza << endl;
        cout << "Price: $" << price << endl;
        cout << endl;
        cout << "TOPPINGS MENU" << endl;
        cout << "1. Sauces" << endl;
        cout << "2. Add Cheese" << endl;
        cout << "3. Meats" << endl;
        cout << "4. Veggies" << endl;
        cout << "5. Reset pizza" << endl;
        cout << "6. Checkout" << endl;
        cout << ">> ";
        cin >> menuChoice;

        if ( menuChoice == 1 )
        {
            cout << endl << "SAUCES" << endl;
            cout << "1. Tomato" << endl;
            cout << "2. Alfredo" << endl;
            cout << "3. Barbecue" << endl;
            cout << "4. Buffalo" << endl;
            cout << ">> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )
            {
                pizza += "Tomato sauce, ";
                price += SAUCE_PRICE;
            }
            else if ( menuChoice == 2 )
            {
                pizza += "Alfredo sauce, ";
                price += SAUCE_PRICE;
            }
            else if ( menuChoice == 3 )
            {
                pizza += "Barbecue sauce, ";
                price += SAUCE_PRICE;
            }
            else if ( menuChoice == 4 )
            {
                pizza += "Buffalo sauce, ";
                price += SAUCE_PRICE;
            }
            else
            {
                cout << "Invalid choice!" << endl;
            }
        }
        else if ( menuChoice == 2 )
        {
            pizza += "Cheese, ";
            price += CHEESE_PRICE;
        }
        else if ( menuChoice == 3 )
        {
            cout << endl << "MEATS" << endl;
            cout << "1. Pepperoni" << endl;
            cout << "2. Sausage" << endl;
            cout << "3. Ham" << endl;
            cout << "4. Chicken" << endl;
            cout << "5. Bacon" << endl;
            cout << ">> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )
            {
                pizza += "Pepperoni, ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 2 )
            {
                pizza += "Sausage, ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 3 )
            {
                pizza += "Ham, ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 4 )
            {
                pizza += "Chicken, ";
                price += MEATS_PRICE;
            }
            else if ( menuChoice == 5 )
            {
                pizza += "Bacon, ";
                price += MEATS_PRICE;
            }
            else
            {
                cout << "Invalid choice!" << endl;
            }
        }
        else if ( menuChoice == 4 )
        {
            cout << endl << "VEGGIES" << endl;
            cout << "1. Mushrooms" << endl;
            cout << "2. Black olives" << endl;
            cout << "3. Pineapple" << endl;
            cout << "4. Tomatoes" << endl;
            cout << ">> ";
            cin >> menuChoice;

            if ( menuChoice == 1 )
            {
                pizza += "Mushrooms, ";
                price += VEGGIES_PRICE;
            }
            else if ( menuChoice == 2 )
            {
                pizza += "Black olives, ";
                price += VEGGIES_PRICE;
            }
            else if ( menuChoice == 3 )
            {
                pizza += "Pineapple, ";
                price += VEGGIES_PRICE;
            }
            else if ( menuChoice == 4 )
            {
                pizza += "Tomatoes, ";
                price += VEGGIES_PRICE;
            }
            else
            {
                cout << "Invalid choice!" << endl;
            }
        }
        else if ( menuChoice == 5 )
        {
            pizza = "";
            price = BASE_PRICE;
        }
        else if ( menuChoice == 6 )
        {
            cout << endl;
            cout << "CHECKOUT" << endl;
            cout << "Pizza: " << pizza << endl;
            cout << "Price: $" << price << endl;
            done = true;
        }
        else
        {
            cout << "INVALID MENU CHOICE" << endl;
        }
    }

    cout << endl << "THANK YOU FOR ORDERING FROM BRICKOLINI PIZZERIA" << endl;

    return 0;
}
