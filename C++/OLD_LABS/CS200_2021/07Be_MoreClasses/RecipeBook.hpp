#ifndef _RECIPE_BOOK_HPP
#define _RECIPE_BOOK_HPP

#include "Recipe.hpp"

#include <string>
using namespace std;

class RecipeBook
{
    public:
    RecipeBook();
    void DisplayRecipes();

    private:
    string m_title;
    Recipe m_recipes[10];
    int m_totalRecipes;
};

#endif
