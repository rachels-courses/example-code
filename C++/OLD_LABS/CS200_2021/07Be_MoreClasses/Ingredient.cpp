#include "Ingredient.hpp"

#include <iostream>
using namespace std;

/**
Set the Ingredient's private member variables m_name, m_amount, and m_unit
to the values passed in by the name, amount, and unit parameters.
*/
void Ingredient::Setup( string name, float amount, string unit )
{
    m_name = name;
    m_amount = amount;
    m_unit = unit;
}

/**
Display the m_amount and m_unit and m_name of each ingredient.
e.g., "5 cup(s) of sugar" where 5 is m_amount, cup(s) is m_unit, and sugar is m_name.
*/
void Ingredient::Display()
{
    cout << m_amount << " " << m_unit << " of " << m_name << endl;
}

/**
Return the value of m_name.
*/
string Ingredient::GetName()
{
    return m_name;
}

/**
Return the value of m_unit.
*/
string Ingredient::GetUnit()
{
    return m_unit;
}

/**
Return the value of m_amount.
*/
float Ingredient::GetAmount()
{
    return m_amount;
}

