#include <iostream>
using namespace std;

#include "Recipe.hpp"

int main()
{
    Recipe recipe;

    // Setup default
    recipe.SetName( "Butter" );
    recipe.SetSource( "old family recipe" );
    recipe.SetInstructions( "Melt butter. Enjoy." );
    recipe.AddIngredient( "Butter", 1, "stick" );
    recipe.AddIngredient( "Butter", 2, "sticks" );
    recipe.AddIngredient( "Butter", 3, "sticks" );

    bool done = false;
    while ( !done )
    {
        cout << endl << "RECIPE EDITOR" << endl;
        cout << endl << "Editing recipe:" << endl;
        recipe.Display();
        cout << endl << "Main menu:" << endl;
        cout << "1. Set name" << endl;
        cout << "2. Set instructions" << endl;
        cout << "3. Set source" << endl;
        cout << "4. Add ingredient" << endl;
        cout << "5. Save" << endl;
        cout << "6. Quit" << endl;
        cout << endl;

        int choice;
        cout << ">> ";
        cin >> choice;

        if      ( choice == 1 )     // Set name
        {
            cout << endl << "SET NAME" << endl
                << "----------------------------------" << endl;
            string temp;
            cout << "Enter recipe name: ";
            cin.ignore();
            getline( cin, temp );

            recipe.SetName( temp );
        }
        else if ( choice == 2 )     // Set instructions
        {
            cout << endl << "SET INSTRUCTIONS" << endl
                << "----------------------------------" << endl;
            string temp;
            cout << "Enter instructions: ";
            cin.ignore();
            getline( cin, temp );

            recipe.SetInstructions( temp );
        }
        else if ( choice == 3 )     // Set source
        {
            cout << endl << "SET SOURCE" << endl
                << "----------------------------------" << endl;
            string temp;
            cout << "Enter source URL: ";
            cin.ignore();
            getline( cin, temp );

            recipe.SetSource( temp );
        }
        else if ( choice == 4 )     // Add ingredient
        {
            cout << endl << "ADD INGREDIENT" << endl
                << "----------------------------------" << endl;
            string name, unit;
            float amount;
            cout << "Enter ingredient name: ";
            cin.ignore();
            getline( cin, name );
            cout << "Enter unit of measurement: ";
            getline( cin, unit );
            cout << "Enter amount: ";
            cin >> amount;

            recipe.AddIngredient( name, amount, unit );
        }
        else if ( choice == 5 )     // Save
        {
            cout << endl << "SAVE" << endl
                << "----------------------------------" << endl;

            string filename;
            cout << "Enter a filename to save it under (include .txt at the end): ";
            cin.ignore();
            getline( cin, filename );
            recipe.Save( filename );
        }
        else if ( choice == 6 ) // Quit
        {
            cout << endl << "QUIT" << endl
                << "----------------------------------" << endl;
            done = true;
        }
    }

    cout << "Goodbye!" << endl;

    return 0;
}
