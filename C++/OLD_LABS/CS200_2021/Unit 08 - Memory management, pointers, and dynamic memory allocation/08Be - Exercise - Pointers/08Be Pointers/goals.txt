08Be Pointers Exercise

BASICS:
* Declare pointer variables
* Work with the Address-of Operator
* Work with "nullptr"
* Dereference pointer variables
* Pointers with classes

APPLICATION:
* Choosing one of several items to point to in a little application
