#ifndef _PROGRAM_FUNCTIONS
#define _PROGRAM_FUNCTIONS

#include <iostream>
#include <string>
using namespace std;

void Program1_ExploringAddresses()
{
    /*
    The & symbol, when placed before a variable name, is known as the "address-of operator".
    It is used to get the address of that variable in memory.

    Pointer variables store addresses in memory, and so they are used to point to
    different variables' addresses at different times in a program.

    When a pointer is NOT in use, it is best practice to point it to "nullptr" to avoid memory errors.
    */

    Menu::Header( "Exploring addresses" );

    int int1 = 25, int2 = 50, int3 = 75;        // Integer variables.
    int * ptrInt = nullptr;                     // Pointer variable, initialized to nullptr for safety.
    bool done = false;
    while ( !done )
    {
        cout << "Integer values:" << endl;
        Menu::DrawTable<int>( { "int1", "int2", "int3" }, { int1, int2, int3 } );

        cout << "Integer addresses: " << endl;
        Menu::DrawTable<int*>( { "&int1", "&int2", "&int3" }, { &int1, &int2, &int3 } );

        cout << endl << "ptrInt is pointing to address: " << ptrInt << endl << endl;

        // Students implement this part
        int pointToWhich;
        cout << "Point ptrInt to which?" << endl
            << "1. Point ptrInt to int1's address" << endl
            << "2. Point ptrInt to int2's address" << endl
            << "3. Point ptrInt to int3's address" << endl
            << "4. Point ptrInt to nullptr" << endl
            << "0. Quit subprogram" << endl
            << ">> ";
        cin >> pointToWhich;
        cout << "You entered: " << pointToWhich << endl;

        if      ( pointToWhich == 1 ) { ptrInt = &int1; }
        else if ( pointToWhich == 2 ) { ptrInt = &int2; }
        else if ( pointToWhich == 3 ) { ptrInt = &int3; }
        else if ( pointToWhich == 4 ) { ptrInt = nullptr; }
        else if ( pointToWhich == 0 ) { done = true; }

        cout << endl << endl;
    }
}

void Program2_DereferencingPointers()
{
    /*
    The * symbol, when written before a pointer variable's name, is known as the "dereference operator".

    When a pointer is pointing to some address in memory we can look at the
    address it's pointing to, but we can also look INTO that address to read and write
    data at that address. When we do this, it's called "dereferencing" the pointer.
    */

    Menu::Header( "Dereferencing pointers" );
    string student1 = "Anuraj S.", student2 = "Rahaf A.", student3 = "Alexa S.";    // String variables.
    string * ptrStudent = nullptr;                                                  // a string pointer, initialized to nullptr.
    bool done = false;

    while ( !done )
    {
        cout << "Students:" << endl;
        Menu::DrawTable<string>(        { "student1",   "student2", "student3" },
                                        { student1,     student2,   student3 } );
        Menu::DrawTable<string*>( {},   { &student1,    &student2,  &student3 } );

        cout << endl << "ptrStudent is pointing to address: " << ptrStudent << endl << endl;

        // Students implement this part
        int choice;
        cout << "Do what?" << endl
            << "1. Point ptrStudent to student1's address" << endl
            << "2. Point ptrStudent to student2's address" << endl
            << "3. Point ptrStudent to student3's address" << endl
            << "4. Point ptrStudent to nullptr" << endl
            << "5. Read from ptrStudent" << endl
            << "6. Write to ptrStudent" << endl
            << "0. Quit subprogram" << endl
            << ">> ";
        cin >> choice;
        cout << "You entered: " << choice << endl;

        if      ( choice == 1 ) { ptrStudent = &student1; }
        else if ( choice == 2 ) { ptrStudent = &student2; }
        else if ( choice == 3 ) { ptrStudent = &student3; }
        else if ( choice == 4 ) { ptrStudent = nullptr; }
        else if ( choice == 5 )
        {
            cout << endl;
            if ( ptrStudent == nullptr )
            {
                cout << "ERROR: ptrStudent is pointing to nullptr and cannot be viewed right now." << endl;
            }
            else
            {
                cout << "ptrStudent is pointing to address: " << ptrStudent << "," << endl
                     << "which is storing data: " << *ptrStudent << endl;
            }
        }
        else if ( choice == 6 )
        {
            cout << endl;
            if ( ptrStudent == nullptr )
            {
                cout << "ERROR: ptrStudent is pointing to nullptr and cannot be updated right now." << endl;
            }
            else
            {
                cout << "Enter new student name for address " << ptrStudent << ": ";
                cin.ignore();
                getline( cin, *ptrStudent );
            }
        }
        else if ( choice == 0 ) { done = true; }

        cout << endl << endl;
    }
}

struct Employee
{
    string name;
    float payPerHour;

    Employee( string n, float pph )
    {
        name = n;
        payPerHour = pph;
    }

    void Display()
    {
        cout << name << ", $" << payPerHour << "/hr" << endl;
    }
};

void Program3_PointersAndClasses()
{
    /*
    The -> operator is a special operator that does two things: It dereferences a class AND accesses a member of the class.
    If we have a pointer to a class, we can access a member like this:
        classPtr->Display();
    Or we could do it the long way:
        (*classPtr).Display();
    */
    Menu::Header( "Classes and Pointers" );

    Employee
        employee1( "Rai S.",    13.45 ),
        employee2( "Rose M.",   15.50 ),
        employee3( "Andre N.",  15.15 );
    Employee * ptrEmployee = nullptr;
    bool done = false;
    while ( !done )
    {
        cout << "Employees:" << endl;
        Menu::DrawTable<string>(        { "employee1",          "employee2",            "employee3" },
                                        { employee1.name,       employee2.name,         employee3.name } );
        Menu::DrawTable<float>( {},     { employee1.payPerHour, employee2.payPerHour,   employee3.payPerHour } );
        Menu::DrawTable<Employee*>( {}, { &employee1,           &employee2,             &employee3 } );

        cout << endl << "ptrEmployee is pointing to address: " << ptrEmployee << endl << endl;

        // Students implement this part
        int choice;
        cout << "Do what?" << endl
            << "1. Point ptrEmployee to employee1's address" << endl
            << "2. Point ptrEmployee to employee2's address" << endl
            << "3. Point ptrEmployee to employee3's address" << endl
            << "4. Point ptrEmployee to nullptr" << endl
            << "5. Read from ptrEmployee" << endl
            << "6. Write to ptrEmployee" << endl
            << "0. Quit subprogram" << endl
            << ">> ";
        cin >> choice;
        cout << "You entered: " << choice << endl;

        if      ( choice == 1 ) { ptrEmployee = &employee1; }
        else if ( choice == 2 ) { ptrEmployee = &employee2; }
        else if ( choice == 3 ) { ptrEmployee = &employee3; }
        else if ( choice == 4 ) { ptrEmployee = nullptr; }
        else if ( choice == 5 )
        {
            cout << endl;
            if ( ptrEmployee == nullptr )
            {
                cout << "ERROR: ptrEmployee is pointing to nullptr and cannot be viewed right now." << endl;
            }
            else
            {
                ptrEmployee->Display();
            }
        }
        else if ( choice == 6 )
        {
            cout << endl;
            if ( ptrEmployee == nullptr )
            {
                cout << "ERROR: ptrEmployee is pointing to nullptr and cannot be updated right now." << endl;
            }
            else
            {
                cout << "1. Update name \t 2. Update pay per hour" << endl;
                cin >> choice;

                if ( choice == 1 )
                {
                    cout << "Enter a new name for the employee: ";
                    cin.ignore();
                    getline( cin, ptrEmployee->name );
                }
                else if ( choice == 2 )
                {
                    cout << "Enter a new wage for the employee: ";
                    cin >> ptrEmployee->payPerHour;
                }
            }
        }
        else if ( choice == 0 ) { done = true; }

        cout << endl << endl;
    }
}

#endif
