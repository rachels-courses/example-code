#ifndef _GROCERY_STORE_HPP
#define _GROCERY_STORE_HPP

#include "datastructure/DynamicArray.hpp"

class GroceryStore
{
    public:
    GroceryStore();
    ~GroceryStore();

    void MainMenu();

    void CustomerMainMenu();
    void AdminMainMenu();

    private:
    DynamicArray m_inventory;
    DynamicArray m_shoppingCart;

    void Admin_AddProduct();
    void Admin_UpdateStock();

    void Customer_AddToCart();
    void Customer_RemoveFromCart();
    void Customer_ViewCart();
    void Customer_Checkout();

    void ViewStock();

    void LoadData();
    void SaveData();
};

#endif
