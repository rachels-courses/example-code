#ifndef _SHOP_ITEM_HPP
#define _SHOP_ITEM_HPP

#include <string>
using namespace std;

class ShopItem
{
    public:
    // Getters
    string GetName() const;
    float GetPrice() const;
    int GetQuantity() const;

    // Setters
    void SetName( const string name );
    void SetPrice( const float price );
    void SetQuantity( const int quantity );

    // Additional helpers
    void Setup( const string name, const float price, const int quantity );

    private:
    // Private member variables
    string m_name;
    float m_price;
    int m_quantity;

    friend class ShopItemTester;
};

#endif
