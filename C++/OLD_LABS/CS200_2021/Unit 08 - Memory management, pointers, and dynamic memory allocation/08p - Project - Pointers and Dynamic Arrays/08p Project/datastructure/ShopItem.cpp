#include "ShopItem.hpp"

// Student implements each of these from scratch

//! Returns the value stored in m_name
string ShopItem::GetName() const
{
    return m_name;
}

//! Returns the value stored in m_price
float ShopItem::GetPrice() const
{
    return m_price;
}

//! Returns the value stored in m_quantity
int ShopItem::GetQuantity() const
{
    return m_quantity;
}

//! Assigns a value to the private member variable m_name
void ShopItem::SetName( const string name )
{
    m_name = name;
}

//! Assigns a value to the private member variable m_price
void ShopItem::SetPrice( const float price )
{
    m_price = price;
}

//! Assigns a value to the private member variable m_quantity
void ShopItem::SetQuantity( const int quantity )
{
    m_quantity = quantity;
}

//! Sets up each of the private member variables: m_name, m_price, and m_quantity
void ShopItem::Setup( const string name, const float price, const int quantity )
{
    SetName( name );
    SetPrice( price );
    SetQuantity( quantity );
}
