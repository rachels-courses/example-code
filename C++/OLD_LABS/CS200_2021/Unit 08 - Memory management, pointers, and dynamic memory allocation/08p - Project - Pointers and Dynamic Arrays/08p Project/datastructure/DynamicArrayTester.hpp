#ifndef _DYNAMIC_ARRAY_TESTER_HPP
#define _DYNAMIC_ARRAY_TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "../cutest/TesterBase.hpp"
#include "../utilities/Menu.hpp"
#include "../utilities/StringUtil.hpp"
#include "../utilities/Logger.hpp"
#include "../utilities/NotImplementedException.hpp"

#include "DynamicArray.hpp"

class DynamicArrayTester : public TesterBase
{
public:
    DynamicArrayTester( string filename )
        : TesterBase( filename )
    {
        AddTest(TestListItem("DynamicArrayConstructor()",   bind(&DynamicArrayTester::DynamicArrayConstructor, this)));
        AddTest(TestListItem("AddItem()",                   bind(&DynamicArrayTester::AddItem, this)));
        AddTest(TestListItem("UpdateStock()",               bind(&DynamicArrayTester::UpdateStock, this)));
        AddTest(TestListItem("Size()",                      bind(&DynamicArrayTester::Size, this)));
        AddTest(TestListItem("IsFull()",                    bind(&DynamicArrayTester::IsFull, this)));
        AddTest(TestListItem("IsEmpty()",                   bind(&DynamicArrayTester::IsEmpty, this)));
        AddTest(TestListItem("IsValidIndex()",              bind(&DynamicArrayTester::IsValidIndex, this)));
        AddTest(TestListItem("GetItemPtr()",                bind(&DynamicArrayTester::GetItemPtr, this)));
        AddTest(TestListItem("AllocateSpace()",             bind(&DynamicArrayTester::AllocateSpace, this)));
        AddTest(TestListItem("DeallocateSpace()",           bind(&DynamicArrayTester::DeallocateSpace, this)));
        AddTest(TestListItem("Resize()",                    bind(&DynamicArrayTester::Resize, this)));
    }

    virtual ~DynamicArrayTester() { }

private:
    int DynamicArrayConstructor();
    int AddItem();
    int UpdateStock();
    int Size();
    int IsFull();
    int IsEmpty();
    int IsValidIndex();
    int GetItemPtr();
    int AllocateSpace();
    int DeallocateSpace();
    int Resize();
};

int DynamicArrayTester::DynamicArrayConstructor()
{
    StartTestSet( "DynamicArrayTester::DynamicArray Constructor", { } );
    Logger::Out( "Beginning test", "DynamicArrayTester::DynamicArrayConstructor" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/
    {   /********************************************** TEST BEGIN */
        StartTest( "Make sure m_itemArray is initialized to nullptr" );
        Set_Comments( "Make sure that the constructor initialized the pointer to nullptr to prevent memory errors." );

        DynamicArray arr;

        Set_ExpectedOutput( "m_itemArray", "nullptr" );
        if ( arr.m_itemArray != nullptr )
        {
            Set_ActualOutput( "m_itemArray", "NOT NULLPTR!" );
            TestFail();
        }
        else
        {
            Set_ActualOutput( "m_itemArray", "nullptr" );
            TestPass();
        }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Make sure m_totalItems is initialized to 0" );
        Set_Comments( "We need to make sure the program knows that we haven't stored anything in the array yet." );

        DynamicArray arr;

        int expected    = Set_ExpectedOutput( "m_totalItems", 0 );
        int actual      = Set_ActualOutput( "m_totalItems", arr.m_totalItems );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Make sure m_arraySize is initialized to 0" );
        Set_Comments( "We haven't allocated any space for the dynamic array at the beginning so this needs to be set to 0." );

        DynamicArray arr;

        int expected    = Set_ExpectedOutput( "m_arraySize", 0 );
        int actual      = Set_ActualOutput( "m_arraySize", arr.m_arraySize );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int DynamicArrayTester::AddItem()
{
    StartTestSet( "DynamicArrayTester::AddItem", { "AllocateSpace", "Resize", "IsFull" } );
    Logger::Out( "Beginning test", "DynamicArrayTester::AddItem" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        DynamicArray arr;
        try
        {
            arr.m_itemArray = new ShopItem[ 5 ];
            arr.AddItem( ShopItem() );
            delete [] arr.m_itemArray;
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { delete [] arr.m_itemArray; return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if item is added to correct location in empty array." );
        Set_Comments( "" );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[ 5 ];
        ShopItem newItem;
        newItem.Setup( "pencil", 0.59, 2 );
        arr.AddItem( newItem );

        string expected    = Set_ExpectedOutput( "Item at position 0", newItem.GetName() );
        string actual      = Set_ActualOutput( "Item at position 0", arr.m_itemArray[0].GetName() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        delete [] arr.m_itemArray;

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Make sure space is allocated if m_itemArray is nullptr." );
        Set_Comments( "Also check m_totalItems." );

        DynamicArray arr;
        ShopItem si;
        si.Setup( "candy", 0.50, 5 );
        arr.AddItem( si );

        int expectedTotal = Set_ExpectedOutput( "m_totalItems", 1 );
        int actualTotal = Set_ActualOutput( "m_totalItems", arr.m_totalItems );

        Set_ExpectedOutput( "m_itemArray", "NOT nullptr" );

        if ( arr.m_itemArray == nullptr )
        {
            Set_ActualOutput( "m_itemArray", "IS nullptr" );
            TestFail();
        }
        else if ( actualTotal != expectedTotal )
        {
            TestFail();
        }
        else
        {
            Set_ActualOutput( "m_itemArray", "NOT nullptr" );
            TestPass();
        }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Make sure a resize occurs if the array is full." );
        Set_Comments( "" );

        ShopItem testArr[5];
        testArr[0].Setup( "pencil", 0.59, 5 );
        testArr[1].Setup( "pen",    0.99, 3 );
        testArr[2].Setup( "folder", 1.29, 2 );
        testArr[3].Setup( "paper",  1.50, 1 );
        testArr[4].Setup( "cheese", 2.50, 3 );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[ 4 ];
        arr.m_totalItems = 4;
        arr.m_arraySize = 4;

        for ( int i = 0; i < 4; i++ )
        {
            arr.m_itemArray[i] = testArr[i];
        }

        arr.AddItem( testArr[4] );

        int expected    = Set_ExpectedOutput( "m_totalItems", 5 );
        int actual      = Set_ActualOutput( "m_totalItems", arr.m_totalItems );

        bool allMatch = true;

        for ( int i = 0; i < 5; i++ )
        {
            Set_ExpectedOutput( "m_itemArray[" + StringUtil::ToString( i ) + "]", testArr[i].GetName() );
            Set_ActualOutput( "m_itemArray[" + StringUtil::ToString( i ) + "]", arr.m_itemArray[i].GetName() );

            if (    // Operator overloading is a CS 235 topic, le sigh.
                arr.m_itemArray[i].GetName() != testArr[i].GetName() ||
                arr.m_itemArray[i].GetPrice() != testArr[i].GetPrice() ||
                arr.m_itemArray[i].GetQuantity() != testArr[i].GetQuantity()
            )
            {
                allMatch = false;
            }
        }

        if ( actual != expected )   { TestFail(); }
        else if ( !allMatch )       { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int DynamicArrayTester::Size()
{
    StartTestSet( "DynamicArrayTester::Size", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::Size" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            arr.Size();
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if Size() returns correct value when m_totalItems is set." );
        Set_Comments( "Setting m_totalItems to 0" );

        DynamicArray arr;
        arr.m_totalItems = 0;

        int expected    = Set_ExpectedOutput( "Size()", 0 );
        int actual      = Set_ActualOutput( "Size()", arr.Size() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if Size() returns correct value when m_totalItems is set." );
        Set_Comments( "Setting m_totalItems to 5" );

        DynamicArray arr;
        arr.m_totalItems = 5;

        int expected    = Set_ExpectedOutput( "Size()", 5 );
        int actual      = Set_ActualOutput( "Size()", arr.Size() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int DynamicArrayTester::IsFull()
{
    StartTestSet( "DynamicArrayTester::IsFull", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::IsFull" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            arr.IsFull();
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 5 and m_arraySize to 5." );

        DynamicArray arr;
        arr.m_totalItems = 5;
        arr.m_arraySize = 5;

        bool expected    = Set_ExpectedOutput( "IsFull()", true );
        bool actual      = Set_ActualOutput( "IsFull()", arr.IsFull() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 2 and m_arraySize to 5." );

        DynamicArray arr;
        arr.m_totalItems = 2;
        arr.m_arraySize = 5;

        bool expected    = Set_ExpectedOutput( "IsFull()", false );
        bool actual      = Set_ActualOutput( "IsFull()", arr.IsFull() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int DynamicArrayTester::IsEmpty()
{
    StartTestSet( "DynamicArrayTester::IsEmpty", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::IsEmpty" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            arr.IsEmpty();
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 0." );

        DynamicArray arr;
        arr.m_totalItems = 0;

        bool expected    = Set_ExpectedOutput( "IsEmpty()", true );
        bool actual      = Set_ActualOutput( "IsEmpty()", arr.IsEmpty() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 2." );

        DynamicArray arr;
        arr.m_totalItems = 2;

        bool expected    = Set_ExpectedOutput( "IsEmpty()", false );
        bool actual      = Set_ActualOutput( "IsEmpty()", arr.IsEmpty() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int DynamicArrayTester::IsValidIndex()
{
    StartTestSet( "DynamicArrayTester::IsValidIndex", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::IsValidIndex" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        DynamicArray arr;
        try
        {
            DynamicArray arr;
            arr.IsValidIndex( 0 );
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if item is added to correct location in empty array." );
        Set_Comments( "Setting m_totalItems to 5, checking index -1." );

        DynamicArray arr;
        arr.m_totalItems = 5;

        bool expected    = Set_ExpectedOutput( "IsValidIndex()", false );
        bool actual      = Set_ActualOutput( "IsValidIndex()", arr.IsValidIndex( -1 ) );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if item is added to correct location in empty array." );
        Set_Comments( "Setting m_totalItems to 5, checking index 0." );

        DynamicArray arr;
        arr.m_totalItems = 5;

        bool expected    = Set_ExpectedOutput( "IsValidIndex()", true );
        bool actual      = Set_ActualOutput( "IsValidIndex()", arr.IsValidIndex( 0 ) );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if item is added to correct location in empty array." );
        Set_Comments( "Setting m_totalItems to 5, checking index 2." );

        DynamicArray arr;
        arr.m_totalItems = 5;

        bool expected    = Set_ExpectedOutput( "IsValidIndex()", true );
        bool actual      = Set_ActualOutput( "IsValidIndex()", arr.IsValidIndex( 2 ) );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if item is added to correct location in empty array." );
        Set_Comments( "Setting m_totalItems to 5, checking index 5." );

        DynamicArray arr;
        arr.m_totalItems = 5;

        bool expected    = Set_ExpectedOutput( "IsValidIndex()", false );
        bool actual      = Set_ActualOutput( "IsValidIndex()", arr.IsValidIndex( 5 ) );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}


int DynamicArrayTester::AllocateSpace()
{
    StartTestSet( "DynamicArrayTester::AllocateSpace", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::AllocateSpace" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        DynamicArray arr;
        try
        {
            arr.AllocateSpace( 2 );
            TestPass();         // Passes if NotImplementedException NOT thrown
            delete [] arr.m_itemArray;
        }
        catch( NotImplementedException& ex ) { if ( arr.m_itemArray != nullptr ) { delete [] arr.m_itemArray; } return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if AllocateSpace turns m_itemArray from nullptr to a memory address." );
        Set_Comments( "Also check m_arraySize" );

        DynamicArray arr;
        arr.AllocateSpace( 2 );

        Set_ExpectedOutput( "m_arraySize", 2 );
        Set_ActualOutput( "m_arraySize", arr.m_arraySize );

        Set_ExpectedOutput( "m_itemArray", string( "NOT nullptr" ) );

        if ( arr.m_itemArray == nullptr )
        {
            TestFail();
            Set_ActualOutput( "m_itemArray", string( "IS nullptr" ) );
        }
        else if ( arr.m_arraySize != 2 )
        {
            Set_ActualOutput( "m_itemArray", string( "NOT nullptr" ) );
            TestFail();
        }
        else                        { TestPass(); }

        if ( arr.m_itemArray != nullptr )
        {
            delete [] arr.m_itemArray;
        }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if AllocateSpace will ignore the command if m_itemArray is already pointing to a memory address." );
        Set_Comments( "Don't inadvertantly lose data by clearing out and reallocating the array. :(" );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[ 10 ];

        ShopItem* ptr1 = arr.m_itemArray;
        arr.AllocateSpace( 20 );
        ShopItem* ptr2 = arr.m_itemArray;

        Set_ExpectedOutput( "New memory IS NOT allocated" );

        if ( ptr1 != ptr2 )
        {
            Set_ActualOutput( "New memory IS allocated, memory address was changed." );
            TestFail();
        }
        else                        { TestPass(); }

        delete [] arr.m_itemArray;

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int DynamicArrayTester::DeallocateSpace()
{
    StartTestSet( "DynamicArrayTester::DeallocateSpace", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::DeallocateSpace" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            arr.DeallocateSpace();
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if m_itemArray is nullptr after DeallocateSpace()" );
        Set_Comments( "Note that I can't actually verify that you DEALLOCATED the space correctly; just that you reset the pointer to nullptr." );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[ 10 ];
        arr.DeallocateSpace();

        int expectedItems   = Set_ExpectedOutput( "m_totalItems", 0 );
        int actualItems     = Set_ExpectedOutput( "m_totalItems", arr.m_totalItems );

        int expectedSize   = Set_ExpectedOutput( "m_arraySize", 0 );
        int actualSize     = Set_ExpectedOutput( "m_arraySize", arr.m_arraySize );

        Set_ExpectedOutput( "m_itemArray", string( "IS nullptr" ) );

        if ( arr.m_itemArray != nullptr )
        {
            Set_ActualOutput( "m_itemArray", string( "NOT nullptr" ) );
            TestFail();
        }
        else if ( actualItems != expectedItems )
        {
            TestFail();
        }
        else if ( actualSize != expectedSize )
        {
            TestFail();
        }
        else
        {
            TestPass();
            Set_ActualOutput( "m_itemArray", string( "IS nullptr" ) );
        }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 2." );

        DynamicArray arr;
        arr.m_totalItems = 2;

        bool expected    = Set_ExpectedOutput( "IsEmpty()", false );
        bool actual      = Set_ActualOutput( "IsEmpty()", arr.IsEmpty() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}


int DynamicArrayTester::GetItemPtr()
{
    StartTestSet( "DynamicArrayTester::GetItemPtr", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::GetItemPtr" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            arr.GetItemPtr( -5 );
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 0." );

        DynamicArray arr;
        arr.m_totalItems = 0;

        bool expected    = Set_ExpectedOutput( "IsEmpty()", true );
        bool actual      = Set_ActualOutput( "IsEmpty()", arr.IsEmpty() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if IsFull() returns correct value." );
        Set_Comments( "Setting m_totalItems to 2." );

        DynamicArray arr;
        arr.m_totalItems = 2;

        bool expected    = Set_ExpectedOutput( "IsEmpty()", false );
        bool actual      = Set_ActualOutput( "IsEmpty()", arr.IsEmpty() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}


int DynamicArrayTester::Resize()
{
    StartTestSet( "DynamicArrayTester::Resize", {} );
    Logger::Out( "Beginning test", "DynamicArrayTester::Resize" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        DynamicArray arr;
        try
        {
            arr.m_itemArray = new ShopItem[ 5 ];
            arr.Resize( 10 );
            TestPass();         // Passes if NotImplementedException NOT thrown
            if ( arr.m_itemArray != nullptr ) { delete [] arr.m_itemArray; }
        }
        catch( NotImplementedException& ex ) { if ( arr.m_itemArray != nullptr ) { delete [] arr.m_itemArray; } return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to see if Resize() updates the m_itemArray address." );
        Set_Comments( "New memory is allocated during a Resize() so m_itemArray should be pointing to a new location." );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[ 5 ];
        arr.m_totalItems = 4;
        arr.m_arraySize = 5;
        ShopItem* ptr1 = arr.m_itemArray;
        arr.Resize( 10 );
        ShopItem* ptr2 = arr.m_itemArray;

        Set_ExpectedOutput( "m_itemArray", string( "New address" ) );

        if ( ptr2 == nullptr )
        {
            Set_ActualOutput( "m_itemArray", string( "is nullptr now??" ) );
            TestFail();
        }
        else if ( ptr1 == ptr2 )
        {
            Set_ActualOutput( "m_itemArray", string( "Still pointing to old address" ) );
        }
        else                        { TestPass(); }

        if ( arr.m_itemArray != nullptr ) { delete [] arr.m_itemArray; }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "See if Resize() copies all the old items over" );

        ShopItem testArr[4];
        testArr[0].Setup( "pencil", 0.59, 5 );
        testArr[1].Setup( "pen",    0.99, 3 );
        testArr[2].Setup( "folder", 1.29, 2 );
        testArr[3].Setup( "paper",  1.50, 1 );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[ 5 ];
        arr.m_totalItems = 4;
        arr.m_arraySize = 5;

        for ( int i = 0; i < 4; i++ )
        {
            arr.m_itemArray[i] = testArr[i];
        }

        arr.Resize( 10 );

        bool allMatch = true;

        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput( "m_itemArray[" + StringUtil::ToString( i ) + "]", testArr[i].GetName() );
            Set_ActualOutput( "m_itemArray[" + StringUtil::ToString( i ) + "]", arr.m_itemArray[i].GetName() );

            if (    // Operator overloading is a CS 235 topic, le sigh.
                arr.m_itemArray[i].GetName() != testArr[i].GetName() ||
                arr.m_itemArray[i].GetPrice() != testArr[i].GetPrice() ||
                arr.m_itemArray[i].GetQuantity() != testArr[i].GetQuantity()
            )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )    { TestFail(); }
        else                { TestPass(); }

        if ( arr.m_itemArray != nullptr ) { delete [] arr.m_itemArray; }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}


int DynamicArrayTester::UpdateStock()
{
    StartTestSet( "DynamicArrayTester::UpdateStock", { "IsValidIndex", "ShopItem::Setup", "ShopItem::GetQuantity", "ShopItem::SetQuantity" } );
    Logger::Out( "Beginning test", "DynamicArrayTester::UpdateStock" );

    /* CHECK IF THIS FUNCTION HAS BEEN IMPLEMENTED BEFORE DOING ADDITIONAL TESTS **/
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            DynamicArray arr;
            arr.UpdateStock( -1, -1 );
            TestPass();         // Passes if NotImplementedException NOT thrown
        }
        catch( NotImplementedException& ex ) { return FunctionNotYetImplemented( ex ); }
    } /* TEST END **************************************************************/

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to make sure no quantities are updated when the index is invalid." );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[2];
        arr.m_itemArray[0].Setup( "pencils",    0.59, 2 );
        arr.m_itemArray[1].Setup( "pens",       0.99, 4 );
        arr.m_totalItems = 2;
        arr.m_arraySize = 2;

        arr.UpdateStock( -1, -2 );

        Set_ExpectedOutput( "m_itemArray[0].quantity", 0 );
        Set_ActualOutput( "m_itemArray[0].quantity", arr.m_itemArray[0].GetQuantity() );

        Set_ExpectedOutput( "m_itemArray[1].quantity", 4 );
        Set_ActualOutput( "m_itemArray[1].quantity", arr.m_itemArray[1].GetQuantity() );

        if      ( arr.m_itemArray[0].GetQuantity() != 2 )   { TestFail(); }
        else if ( arr.m_itemArray[1].GetQuantity() != 4 )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        StartTest( "Check to make sure quantity is NOT updated if it will become negative with the change." );
        Set_Comments( "Total pencils is 2. If we try to remove 3 or more, it should ignore the command." );

        DynamicArray arr;
        arr.m_itemArray = new ShopItem[2];
        arr.m_itemArray[0].Setup( "pencils",    0.59, 2 );
        arr.m_itemArray[1].Setup( "pens",       0.99, 4 );
        arr.m_totalItems = 2;
        arr.m_arraySize = 2;

        arr.UpdateStock( 0, -3 );

        int expected    = Set_ExpectedOutput( "m_itemArray[0].quantity", 2 );
        int actual      = Set_ActualOutput( "m_itemArray[0].quantity", arr.m_itemArray[0].GetQuantity() );

        if ( actual != expected )   { TestFail(); }
        else                        { TestPass(); }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

#endif
