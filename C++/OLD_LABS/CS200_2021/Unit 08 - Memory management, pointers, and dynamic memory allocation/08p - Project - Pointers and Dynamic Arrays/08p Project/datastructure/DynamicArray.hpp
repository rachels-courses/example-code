// DynamicArray.hpp - STUDENTNAME

#ifndef _DYNAMIC_ARRAY_HPP
#define _DYNAMIC_ARRAY_HPP

#include "ShopItem.hpp"

class DynamicArray
{
    public:
    DynamicArray();
    ~DynamicArray();

    void AddItem( ShopItem newItem );
    void UpdateStock( int index, int amount );
    void Display() const;
    void AllocateSpace( const int size );
    void Clear();

    int Size() const;
    bool IsFull() const;
    bool IsEmpty() const;
    bool IsValidIndex( int index ) const;

    ShopItem* GetItemPtr( int index ) const;

    private:
    ShopItem*   m_itemArray;
    int         m_arraySize;
    int         m_totalItems;

    void DeallocateSpace();
    void Resize( const int newSize );

    void ImplementMe( string functionName ) const;

    friend class DynamicArrayTester;
};

#endif
