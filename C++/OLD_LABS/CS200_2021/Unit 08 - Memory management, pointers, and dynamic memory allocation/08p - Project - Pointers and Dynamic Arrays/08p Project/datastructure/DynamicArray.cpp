#include "DynamicArray.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

#include "../utilities/Logger.hpp"
#include "../utilities/Menu.hpp"
#include "../utilities/NotImplementedException.hpp"

/**
Initialize the array by setting the item array to "nullptr", total items to 0, and array size to 0.
*/
DynamicArray::DynamicArray()
{
    Logger::Out( "Constructor", "DynamicArray::DynamicArray" );
//    ImplementMe( "DynamicArray constructor" );    // TODO: Remove me once you've implemented this function!!

    m_itemArray = nullptr;
    m_totalItems = 0;
    m_arraySize = 0;
}

/**
Call DeallocateSpace to clean up the array.
*/
DynamicArray::~DynamicArray()
{
    Logger::Out( "Destructor", "DynamicArray::~DynamicArray" );
//    DeallocateSpace();
}

/**
Display each product's index, name, price, and quantity.
*/
void DynamicArray::Display() const
{
    Logger::Out( "Function begin", "DynamicArray::Display" );
    cout << left
        << setw( 10 ) << "INDEX"
        << setw( 15 ) << "NAME"
        << setw( 10 ) << "PRICE"
        << setw( 10 ) << "QUANTITY" << endl;
    Menu::DrawHorizontalBar( 80, '-' );
    for ( int i = 0; i < m_totalItems; i++ )
    {
        cout << left
            << setw( 10 ) << i
            << setw( 15 ) << m_itemArray[i].GetName()
            << setw( 10 ) << m_itemArray[i].GetPrice()
            << setw( 10 ) << m_itemArray[i].GetQuantity() << endl;
    }
}

/**
Add a new item to the array.
ERROR CHECK 1: If the array is nullptr, then call AllocateSpace first before continuing.
ERROR CHECK 2: If the array is full, then call Resize fist before continuing.
*/
void DynamicArray::AddItem( ShopItem newItem )
{
    Logger::Out( "Function begin", "DynamicArray::AddItem" );
//    ImplementMe( "AddItem" );    // TODO: Remove me once you've implemented this function!!

    // ERROR CHECKS:
    if ( m_itemArray == nullptr )
    {
        AllocateSpace( 10 );
    }
    else if ( IsFull() )
    {
        Resize( m_arraySize + 10 );
    }

    m_itemArray[ m_totalItems ] = newItem;
    m_totalItems++;
}

/**
Find an item in the array whose index is "index", then adjust its quantity by the "amount" passed in.
ERROR CHECK: If the current quantity minus the amount is less than 0, then this is an invalid operation - don't do it!
*/
void DynamicArray::UpdateStock( int index, int amount )
{
    Logger::Out( "Function begin", "DynamicArray::UpdateStock" );
//    ImplementMe( "UpdateStock" );    // TODO: Remove me once you've implemented this function!!

    if ( !IsValidIndex( index ) )
    {
        cout << "Error: Index is invalid!" << endl;
        return;
    }

    // ERROR CHECK:
    int adjustedQuantity = m_itemArray[ index ].GetQuantity() + amount;

    if ( adjustedQuantity < 0 )
    {
        cout << "Error: Cannot make quantity of item negative!" << endl;
        return;
    }

    m_itemArray[ index ].SetQuantity( adjustedQuantity );
}

/**
Return the total amount of items that are stored in the array.
*/
int DynamicArray::Size() const
{
    Logger::Out( "Function begin", "DynamicArray::Size" );
//    ImplementMe( "Size" );    // TODO: Remove me once you've implemented this function!!

//    return -1; // TODO: This is a plceholder. Delete me!
    return m_totalItems;
}

/**
Returns whether the array is currently full (true) or not (false).
*/
bool DynamicArray::IsFull() const
{
    Logger::Out( "Function begin", "DynamicArray::IsFull" );
//    ImplementMe( "IsFull" );    // TODO: Remove me once you've implemented this function!!

//    return false; // TODO: This is a plceholder. Delete me!
    return ( m_totalItems == m_arraySize );
}

/**
Returns whether the array is currently empty (true) or not (false).
*/
bool DynamicArray::IsEmpty() const
{
    Logger::Out( "Function begin", "DynamicArray::IsEmpty" );
//    ImplementMe( "IsEmpty" );    // TODO: Remove me once you've implemented this function!!

//    return false; // TODO: This is a plceholder. Delete me!
    return ( m_totalItems == 0 );
}

/**
Check if the index passed in is valid.
*/
bool DynamicArray::IsValidIndex( int index ) const
{
    Logger::Out( "Function begin", "DynamicArray::IsValidIndex" );
//    ImplementMe( "IsValidIndex" );    // TODO: Remove me once you've implemented this function!!

//    return false; // TODO: This is a plceholder. Delete me!
    return ( index >= 0 && index < m_totalItems );
}

/**
Return the address of the item at the index passed in.
ERROR CHECK: If the index is invalid, return "nullptr" instead.
*/
ShopItem* DynamicArray::GetItemPtr( int index ) const
{
    Logger::Out( "Function begin", "DynamicArray::GetItemPtr" );
//    ImplementMe( "GetItemPtr" );    // TODO: Remove me once you've implemented this function!!

    if ( !IsValidIndex( index ) )
    {
        return nullptr;
    }

    return &m_itemArray[ index ];
}

/**
Allocate space for the dynamic array.
ERROR CHECK: If the item array is not pointing to "nullptr", then you should NOT allocate space! Display an error message instead.
*/
void DynamicArray::AllocateSpace( const int size )
{
    Logger::Out( "Function begin", "DynamicArray::AllocateSpace" );
//    ImplementMe( "AllocateSpace" );    // TODO: Remove me once you've implemented this function!!

    // ERROR CHECK:
    if ( m_itemArray != nullptr )
    {
        cout << "Error: Array is already allocated! Don't want to lose data." << endl;
        return;
    }

    m_arraySize = size;
    m_itemArray = new ShopItem[ size ];
}

/**
If the array is not pointing to nullptr, then free the space, set the item array to point to nullptr, then set total items and array size to 0.
*/
void DynamicArray::DeallocateSpace()
{
    Logger::Out( "Function begin", "DynamicArray::DeallocateSpace" );
//    ImplementMe( "DeallocateSpace" );    // TODO: Remove me once you've implemented this function!!

    if ( m_itemArray != nullptr )
    {
        delete [] m_itemArray;
        m_itemArray = nullptr;
        m_totalItems = 0;
        m_arraySize = 0;
    }
}

void DynamicArray::Clear()
{
    DeallocateSpace();
}

/**
Resize the array so that there are more spaces to add new items to.
*/
void DynamicArray::Resize( const int newSize )
{
    Logger::Out( "Function begin", "DynamicArray::Resize" );
//    ImplementMe( "Resize" );    // TODO: Remove me once you've implemented this function!!

    // Allocate more memory
    ShopItem * biggerArray = new ShopItem[ newSize ];

    // Copy items from old array to new
    for ( int i = 0; i < m_arraySize; i++ )
    {
        biggerArray[i] = m_itemArray[i];
    }

    // Free old memory
    delete [] m_itemArray;

    // Point to bigger array
    m_itemArray = biggerArray;

    // Update variables
    m_arraySize = newSize;
}


void DynamicArray::ImplementMe( string functionName ) const
{
    throw NotImplementedException( functionName + " not implemented! (Or ImplementMe() wasn't removed!)" );
}


