#ifndef _SHOP_ITEM_TESTER_HPP
#define _SHOP_ITEM_TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "../cutest/TesterBase.hpp"
#include "../utilities/Menu.hpp"
#include "../utilities/StringUtil.hpp"
#include "../utilities/Logger.hpp"
#include "../utilities/NotImplementedException.hpp"

#include "DynamicArray.hpp"

class ShopItemTester : public TesterBase
{
public:
    ShopItemTester( string filename )
        : TesterBase( filename )
    {
        AddTest(TestListItem("Setup()",         bind(&ShopItemTester::Setup, this)));
        AddTest(TestListItem("GetName()",       bind(&ShopItemTester::GetName, this)));
        AddTest(TestListItem("GetPrice()",      bind(&ShopItemTester::GetPrice, this)));
        AddTest(TestListItem("GetQuantity()",   bind(&ShopItemTester::GetQuantity, this)));
        AddTest(TestListItem("SetName()",       bind(&ShopItemTester::SetName, this)));
        AddTest(TestListItem("SetPrice()",      bind(&ShopItemTester::SetPrice, this)));
        AddTest(TestListItem("SetQuantity()",   bind(&ShopItemTester::SetQuantity, this)));
    }

    virtual ~ShopItemTester() { }

private:
    int Setup();
    int GetName();
    int GetPrice();
    int GetQuantity();
    int SetName();
    int SetPrice();
    int SetQuantity();
};

int ShopItemTester::Setup()
{
    StartTestSet( "ShopItemTester::Setup", { } );
    Logger::Out( "Beginning test", "ShopItemTester::Setup" );

    string expectedNames[] = { "itemA", "itemB", "itemC" };
    float expectedPrices[] = { 1.23, 3.45, 199.99 };
    int expectedQuantities[] = { 10, 20, 30 };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Check if passing items into Setup sets the private member variables" );

            ShopItem item;
            item.Setup( expectedNames[i], expectedPrices[i], expectedQuantities[i] );

            string actualName = item.m_name;
            float actualPrice = item.m_price;
            int actualQuantity = item.m_quantity;

            Set_ExpectedOutput  ( "m_name", expectedNames[i] );
            Set_ActualOutput    ( "m_name", actualName );

            Set_ExpectedOutput  ( "m_price", expectedPrices[i] );
            Set_ActualOutput    ( "m_price", actualPrice );

            Set_ExpectedOutput  ( "m_quantity", expectedQuantities[i] );
            Set_ActualOutput    ( "m_quantity", actualQuantity );

            bool allValuesCorrect = true;
            string failMessage = "";
            if      ( actualName != expectedNames[i] )
            {
                allValuesCorrect = false;
                failMessage += "m_name was not the expected value! ";
            }
            if ( actualPrice != expectedPrices[i] )
            {
                allValuesCorrect = false;
                failMessage += "m_price was not the expected value! ";
            }
            if ( actualQuantity != expectedQuantities[i] )
            {
                allValuesCorrect = false;
                failMessage += "<br>";
                failMessage += "m_quantity was not the expected value! ";
            }

            if ( allValuesCorrect )
            {
                TestPass();
            }
            else
            {
                TestFail( failMessage );
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

int ShopItemTester::GetName()
{
    StartTestSet( "ShopItemTester::GetName", { } );
    Logger::Out( "Beginning test", "ShopItemTester::GetName" );

    string expectedNames[] = { "itemA", "itemB", "itemC" };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Set m_name, check GetName()" );

            ShopItem item;
            item.m_name = expectedNames[i];
            string actualName = item.GetName();

            Set_ExpectedOutput  ( "GetName()", expectedNames[i] );
            Set_ActualOutput    ( "GetName()", actualName );

            if ( actualName != expectedNames[i] )
            {
                TestFail( "GetName() did not return the expected value!" );
            }
            else
            {
                TestPass();
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

int ShopItemTester::GetPrice()
{
    StartTestSet( "ShopItemTester::GetPrice", { } );
    Logger::Out( "Beginning test", "ShopItemTester::GetPrice" );

    float expectedPrices[] = { 1.23, 3.45, 199.99 };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Set m_price, check GetPrice()" );

            ShopItem item;
            item.m_price = expectedPrices[i];
            float actualPrice = item.GetPrice();

            Set_ExpectedOutput  ( "GetPrice()", expectedPrices[i] );
            Set_ActualOutput    ( "GetPrice()", actualPrice );

            if ( actualPrice != expectedPrices[i] )
            {
                TestFail( "GetPrice() did not return the expected value!" );
            }
            else
            {
                TestPass();
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

int ShopItemTester::GetQuantity()
{
    StartTestSet( "ShopItemTester::GetQuantity", { } );
    Logger::Out( "Beginning test", "ShopItemTester::GetQuantity" );

    int expectedQuantities[] = { 10, 20, 30 };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Set m_quantity, check GetQuantity()" );

            ShopItem item;
            item.m_quantity = expectedQuantities[i];
            int actualQuantity = item.GetQuantity();

            Set_ExpectedOutput  ( "GetQuantity()", expectedQuantities[i] );
            Set_ActualOutput    ( "GetQuantity()", actualQuantity );

            if ( actualQuantity != expectedQuantities[i] )
            {
                TestFail( "GetQuantity() did not return the expected value!" );
            }
            else
            {
                TestPass();
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

int ShopItemTester::SetName()
{
    StartTestSet( "ShopItemTester::SetName", { } );
    Logger::Out( "Beginning test", "ShopItemTester::SetName" );

    string expected[] = { "itemA", "itemB", "itemC" };
//    float expected[] = { 1.23, 3.45, 199.99 };
//    int expected[] = { 10, 20, 30 };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Use SetName() and check the value of m_name afterwards" );

            ShopItem item;
            item.SetName( expected[i] );
            string actual = item.m_name;

            Set_ExpectedOutput  ( "m_name", expected[i] );
            Set_ActualOutput    ( "m_name", actual );

            if ( actual != expected[i] )
            {
                TestFail( "m_name was not the expected value!" );
            }
            else
            {
                TestPass();
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

int ShopItemTester::SetPrice()
{
    StartTestSet( "ShopItemTester::SetPrice", { } );
    Logger::Out( "Beginning test", "ShopItemTester::SetPrice" );

//    string expected[] = { "itemA", "itemB", "itemC" };
    float expected[] = { 1.23, 3.45, 199.99 };
//    int expected[] = { 10, 20, 30 };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Use SetName() and check the value of m_name afterwards" );

            ShopItem item;
            item.SetPrice( expected[i] );
            float actual = item.m_price;

            Set_ExpectedOutput  ( "m_price", expected[i] );
            Set_ActualOutput    ( "m_price", actual );

            if ( actual != expected[i] )
            {
                TestFail( "m_price was not the expected value!" );
            }
            else
            {
                TestPass();
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

int ShopItemTester::SetQuantity()
{
    StartTestSet( "ShopItemTester::SetQuantity", { } );
    Logger::Out( "Beginning test", "ShopItemTester::SetQuantity" );

//    string expected[] = { "itemA", "itemB", "itemC" };
//    float expected[] = { 1.23, 3.45, 199.99 };
    int expected[] = { 10, 20, 30 };

    for ( int i = 0; i < 3; i++ )
    {
        {
            /********************************************** TEST BEGIN */
            StartTest( "Use SetName() and check the value of m_name afterwards" );

            ShopItem item;
            item.SetQuantity( expected[i] );
            int actual = item.m_quantity;

            Set_ExpectedOutput  ( "m_quantity", expected[i] );
            Set_ActualOutput    ( "m_quantity", actual );

            if ( actual != expected[i] )
            {
                TestFail( "m_quantity was not the expected value!" );
            }
            else
            {
                TestPass();
            }

            FinishTest();
        }   /********************************************** TEST END */
    }

    FinishTestSet();
    return TestResult();
}

#endif
