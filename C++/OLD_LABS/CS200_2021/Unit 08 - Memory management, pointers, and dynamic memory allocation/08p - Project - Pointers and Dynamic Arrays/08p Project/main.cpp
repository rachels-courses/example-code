// 08Bp - Don't update this file

#include "utilities/Logger.hpp"
#include "datastructure/ShopItemTester.hpp"
#include "datastructure/DynamicArrayTester.hpp"
#include "GroceryStore.hpp"

int main()
{
    Logger::Setup();

    // Run tests
    ShopItemTester tester1( "../ShopItemTester.html" );
    tester1.Start();

    DynamicArrayTester tester2( "../DynamicArrayTester.html" );
    tester2.Start();

    // Start program
    GroceryStore store;
    store.MainMenu();

    Logger::Cleanup();
}
