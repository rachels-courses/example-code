#include "GroceryStore.hpp"

#include "utilities/Menu.hpp"

#include <fstream>
#include <string>
using namespace std;

GroceryStore::GroceryStore()
{
    LoadData();
}

GroceryStore::~GroceryStore()
{
    SaveData();
}

// Student implements these
void GroceryStore::MainMenu()
{
    Menu::Header( "HOME" );
    bool done = false;
    while ( !done )
    {
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Log in as CUSTOMER",
            "Log in as ADMIN"
        }, true, true );

        if      ( choice == 1 ) { CustomerMainMenu(); }
        else if ( choice == 2 ) { AdminMainMenu(); }
        else                    { done = true; }
    }
}

void GroceryStore::CustomerMainMenu()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "CUSTOMER MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "View all products",
            "Add to cart",
            "Remove from cart",
            "View cart",
            "Checkout"
        }, true, true );

        if      ( choice == 1 ) { ViewStock(); }
        else if ( choice == 2 ) { Customer_AddToCart(); }
        else if ( choice == 3 ) { Customer_RemoveFromCart(); }
        else if ( choice == 4 ) { Customer_ViewCart(); }
        else if ( choice == 5 ) { Customer_Checkout(); }
        else                    { done = true; }
    }
}

void GroceryStore::AdminMainMenu()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Add product",
            "Update stock",
            "View stock"
        }, true, true );

        if      ( choice == 1 ) { Admin_AddProduct(); }
        else if ( choice == 2 ) { Admin_UpdateStock(); }
        else if ( choice == 3 ) { ViewStock(); }
        else                    { done = true; }
    }
}

void GroceryStore::Admin_AddProduct()
{
    string name;
    float price;
    int quantity;

    cout << "New product name: ";
    cin.ignore();
    getline( cin, name );

    cout << "Product price: ";
    cin >> price;

    cout << "Product quantity: ";
    cin >> quantity;

    ShopItem newItem;
    newItem.Setup( name, price, quantity );

    m_inventory.AddItem( newItem );
}

void GroceryStore::Admin_UpdateStock()
{
    m_inventory.Display();

    cout << "Enter index of item to update: ";
    int index;
    cin >> index;

    cout << "Enter a positive amount to add stock," << endl
        << "or a negative amount to remove stock: ";
    int amount;
    cin >> amount;

    m_inventory.UpdateStock( index, amount );
}

void GroceryStore::ViewStock()
{
    m_inventory.Display();
}

void GroceryStore::LoadData()
{
    ifstream input( "../save.txt" );
    string buffer;
    int totalItems;

    input >> buffer;      // Skip "TOTAL_ITEMS"
    input >> totalItems;

    m_inventory.AllocateSpace( totalItems );

    string name;
    float price;
    int quantity;
    while ( getline( input, buffer ) )
    {
        if ( buffer == "INDEX" )
        {
        }
        else if ( buffer == "PRICE" )
        {
            input >> price;
        }
        else if ( buffer == "QUANTITY" )
        {
            input >> quantity;
        }
        else if ( buffer == "NAME" )
        {
            getline( input, name );
        }
        else if ( buffer == "ITEM_END" )
        {
            ShopItem newItem;
            newItem.Setup( name, price, quantity );
            m_inventory.AddItem( newItem );
        }
    }
}

void GroceryStore::SaveData()
{
    ofstream output( "../save.txt" );

    output << "TOTAL_ITEMS " << m_inventory.Size() << endl;

    for ( int i = 0; i < m_inventory.Size(); i++ )
    {
        ShopItem* ptr = m_inventory.GetItemPtr( i );

        output << "ITEM_BEGIN"  << endl;
        output << "INDEX"       << endl << i << endl;
        output << "PRICE"       << endl << ptr->GetPrice() << endl;
        output << "QUANTITY"    << endl << ptr->GetQuantity() << endl;
        output << "NAME"        << endl << ptr->GetName() << endl;
        output << "ITEM_END"    << endl;
    }
}

void GroceryStore::Customer_AddToCart()
{
    m_inventory.Display();

    cout << "Enter index of item to add to cart: ";
    int index;
    cin >> index;

    cout << "How many? ";
    int quantity;
    cin >> quantity;

    ShopItem * ptrItem = m_inventory.GetItemPtr( index );

    if ( ptrItem == nullptr )
    {
        cout << "Error: Item not found!" << endl;
        return;
    }

    if ( ptrItem->GetQuantity() < quantity )
    {
        cout << "Error: Not enough in stock to add " << quantity << " item(s)!" << endl;
        return;
    }

    // Add to cart
    ShopItem cartItem;
    cartItem.Setup( ptrItem->GetName(), ptrItem->GetPrice(), quantity );
    m_shoppingCart.AddItem( cartItem );

    // Remove from supply
    ptrItem->SetQuantity( ptrItem->GetQuantity() - quantity );
}

void GroceryStore::Customer_RemoveFromCart()
{
    m_inventory.Display();

    cout << "Enter index of item to remove from cart: ";
    int index;
    cin >> index;

    ShopItem * ptrItem = m_shoppingCart.GetItemPtr( index );
    if ( ptrItem == nullptr )
    {
        cout << "Error: Item not found!" << endl;
        return;
    }

    ptrItem->SetQuantity( 0 );
}

void GroceryStore::Customer_ViewCart()
{
    m_shoppingCart.Display();
}

void GroceryStore::Customer_Checkout()
{
    float subTotal = 0;
    float tax = 0.09;

    cout << left
        << setw( 4 ) << "#"
        << setw( 15 ) << "NAME"
        << setw( 10 ) << "PRICE"
        << setw( 10 ) << "QUANTITY"
        << endl;
    Menu::DrawHorizontalBar( 80, '-' );

    for ( int i = 0; i < m_shoppingCart.Size(); i++ )
    {
        ShopItem * ptrItem = m_shoppingCart.GetItemPtr( i );
        if ( ptrItem == nullptr )
        {
            continue;
        }

        subTotal += ptrItem->GetPrice() * ptrItem->GetQuantity();

        cout << left
            << setw( 4 ) << (i+1)
            << setw( 15 ) << ptrItem->GetName()
            << setw( 10 ) << ptrItem->GetPrice()
            << setw( 10 ) << ptrItem->GetQuantity()
            << endl;
    }

    float totalPrice = subTotal + ( subTotal * tax );

    cout << endl << left
        << setw( 15 ) << "SUBTOTAL:" << setw( 10 ) << subTotal << endl
        << setw( 15 )<< "TOTAL TAX:" << setw( 10 ) << (subTotal * tax) << endl
        << setw( 15 ) << "TOTAL:" << setw( 10 ) << totalPrice << endl;

    m_shoppingCart.Clear();
}






