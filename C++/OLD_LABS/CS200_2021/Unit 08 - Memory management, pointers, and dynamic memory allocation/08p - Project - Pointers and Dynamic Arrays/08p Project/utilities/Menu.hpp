#ifndef _MENU
#define _MENU

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <limits>
#include <map>
#include <functional>
using namespace std;

class Menu
{
    public:
    // OUTPUT
    static void Header( const string& header );
    static void DrawHorizontalBar( int width, char symbol = '-' );

    // MENUS and INPUT/OUTPUT
    static void ShowMenu( const vector<string> options, bool vertical = true, bool zeroToQuit = false );
    static int ShowIntMenuWithPrompt( const vector<string> options, bool vertical = true, bool zeroToQuit = false );
    static string ShowStringMenuWithPrompt( const vector<string> options, bool vertical = true );
    static function<void(void)> ShowCallbackMenuWithPrompt( map<string, function<void(void)> > options, bool vertical = true );
    static int GetValidChoice( int min, int max, const string& message = "" );
    static string GetStringChoice( const string& message = "" );
    static string GetStringLine( const string& message = "" );
    static int GetIntChoice( const string& message = "" );
    template <class T> static void DrawTable( const vector<string>& headers, const vector<T>& data );

    // HANDY TRICKS
    static void ClearScreen();
    static void Pause();
    static void PrintPwd();

    private:
    static bool s_lastCinWasStream;

    static string CinStreamString();
    static int CinStreamInt();
    static string CinGetlineString();
};

template <class T>
void Menu::DrawTable( const vector<string>& headers, const vector<T>& data )
{
    int totalColumns = data.size();
    int screenColumns = 80;
    int blocksPerColumn = screenColumns / totalColumns;

    if ( headers.size() != 0 )
    {
        for ( auto& header : headers )
        {
            cout << left << setw( blocksPerColumn ) << header;
        }
        cout << endl;
        DrawHorizontalBar( 80, '-' );
    }

    for ( auto& dataItem : data )
    {
        cout << left << setw( blocksPerColumn ) << dataItem;
    }
    cout << endl;
}

#endif

