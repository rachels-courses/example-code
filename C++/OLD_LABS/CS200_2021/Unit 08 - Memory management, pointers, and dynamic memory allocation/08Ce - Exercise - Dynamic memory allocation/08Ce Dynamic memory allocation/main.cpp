// 08Ce - Don't update this file

#include <string>
using namespace std;
#include "utilities/Menu.hpp"
#include "ProgramFunctions.hpp"

int main()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Allocating and Deallocating Memory - Single Item",
            "Allocating and Deallocating Memory - Dynamic Array",
            "Using dynamic arrays",
            "Using dynamic items"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Program1_SingleHeapItems(); }
        else if ( choice == 2 ) { Program2_MultipleHeapItems(); }
        else if ( choice == 3 ) { Program3_ApplicationOfMultipleHeapItems(); }
        else if ( choice == 4 ) { Program4_ApplicationOfSingleHeapItems(); }
    }
}
