#ifndef _PROGRAM_FUNCTIONS
#define _PROGRAM_FUNCTIONS

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

void Program1_SingleHeapItems()
{
    /*
    We can use the "new" and "delete" keywords to allocate and deallocate memory
    for one or more items. Items created this way must be created via pointers,
    and their memory will be stored in "Heap" memory (normal variables are stored on the "Stack".)

    There is limited Stack memory (thus running into a Stack Overflow in some occasions),
    but there is unlimited Heap memory (restricted by the amount of RAM you have.)
    */

    Menu::Header( "Allocating and Deallocating Memory - Single Item" );

    // Create a pointer variable of any data type
    string * ptr;

    // Allocate memory to the pointer by using VARIABLENAME = new DATATYPE;
    ptr = new string;

    // Output the address in memory by just outputting the pointer.
    cout << "Address: " << ptr << endl;

    // Assign a value to the variable by dereferencing the pointer.
    *ptr = "Hello";

    // Output the variable value by dereferencing the pointer.
    cout << "Value: " << *ptr << endl;

    // Free the memory allocated by using delete VARIABLENAME;
    delete ptr;

    // Set the pointer to nullptr to be safe
    ptr = nullptr;
    cout << "Address: " << ptr << endl;

    cout << endl;
}

void Program2_MultipleHeapItems()
{
    /*
    Until you get into a data structures class, you will probably be using Dynamic Arrays
    more frequently than a single dynamic variable. For Dynamic Arrays, you can make an
    array of any size at RUNTIME, whereas before (with a normal, vanilla array) you had
    to know its size at COMPILETIME.
    */

    Menu::Header( "Allocating and Deallocating Memory - Dynamic Array" );

    // Ask the user how big to make the new array. Store the size in an int variable.
    int size;
    cout << "How big should the array be? ";
    cin >> size;
    cout << "You entered: " << size << endl;

    // Create a pointer of any data type.
    int * ptr;

    // Allocate a dynamic array by using VARIABLENAME = new DATATYPE[ size ];
    ptr = new int[ size ];

    // Output the address in memory by just outputting the pointer.
    cout << "Address: " << ptr << endl;

    // Fill the array with items (could be random). Use a for loop that goes from 0 to size to fill the array.
    // Output the INDEX and VALUE of each element in the array.
    for ( int i = 0; i < size; i++ )
    {
        ptr[i] = rand() % 100;
        cout << "ptr[" << i << "] = " << ptr[i] << endl;
    }

    // Free the memory for the array using delete [] VARIABLENAME;
    delete [] ptr;

    // Set the pointer to nullptr to be safe
    ptr = nullptr;
    cout << "Address: " << ptr << endl;

    cout << endl;
}

void Program3_ApplicationOfMultipleHeapItems()
{
    /*
    Let's use a dynamic array for an actual small program.
    */

    Menu::Header( "Using dynamic arrays" );

    // Ask the user how many items they're purchasing. Store size in an integer variable.
    int size;
    cout << "How many items are you buying? ";
    cin >> size;
    cout << "You entered: " << size << endl;

    // Create a dynamic array of floats to store prices in.
    float * prices = new float[ size ];
    cout << "Address: " << prices << endl << endl;

    // Create a float to store the total of the prices, initialize it to 0.
    float total = 0;

    // Use a loop to have the user enter all the prices. Also add each value onto the total variable.
    for ( int i = 0; i < size; i++ )
    {
        cout << "Enter price #" << (i+1) << ": ";
        cin >> prices[i];
        cout << "You entered: " << prices[i] << endl;
        total += prices[i];
    }

    cout << endl << "Total cost: " << total << endl;

    // Free the memory and reset pointer to point ton ullptr
    delete [] prices;
    prices = nullptr;
    cout << endl << "Address: " << prices << endl;
}

struct Node
{
    string data;
    Node* ptrNext;
    Node* ptrPrev;

    Node()
    {
        ptrNext = nullptr;
        ptrPrev = nullptr;
    }

    void Display()
    {
        cout << left << setw( 6 ) << "Prev: ";
        if ( ptrPrev == nullptr )   { cout << left << setw( 18 ) << "nullptr"; }
        else                        { cout << left << setw( 18 ) << ptrPrev->data; }

        cout << left << setw( 6 ) << "This: ";
        cout << left << setw( 18 ) << data;

        cout << left << setw( 6 ) << "Next: ";
        if ( ptrNext == nullptr )   { cout << left << setw( 18 ) << "nullptr"; }
        else                        { cout << left << setw( 18 ) << ptrNext->data; }

        cout << endl;
    }
};

void Program4_ApplicationOfSingleHeapItems()
{
    /*
    In a data structures class, you'll learn about Linked Lists. These are built by
    allocating only as much memory is needed as each item is added to the list.

    Each Node is a single Node item allocated on the heap. Also, the Nodes contain
    pointers to other nodes as "previous" or "next" items. So, we're using pointers
    in two different ways! Pointing to existing memory and allocating new memory.
    */

    Menu::Header( "Using dynamic items" );

    // Create four node pointers and allocate memory for each one.
    // e.g.: Node * node1 = new Node;
    Node * node1 = new Node;
    Node * node2 = new Node;
    Node * node3 = new Node;
    Node * node4 = new Node;

    // Output each node's address.
    // e.g. cout << node1 << endl;
    cout << "node1's address: " << node1 << endl;
    cout << "node2's address: " << node2 << endl;
    cout << "node3's address: " << node3 << endl;
    cout << "node4's address: " << node4 << endl;
    cout << endl;

    // Set up each node's data (node1->data) to something.
    node1->data = "Aardvark";
    node2->data = "Banana";
    node3->data = "Carp";
    node4->data = "Dog";

    // Set node1's ptrNext to node2, node2's ptrNext to node3, and node3's ptrNext to node 4.
    // e.g.: node1->ptrNext = node2;
    node1->ptrNext = node2;
    node2->ptrNext = node3;
    node3->ptrNext = node4;

    // Set node4's ptrPrev to node3, node3's ptrPrev to node2, and node2's ptrPrev to node1.
    node4->ptrPrev = node3;
    node3->ptrPrev = node2;
    node2->ptrPrev = node1;

    // Call the Display() function for each node
    node1->Display();
    node2->Display();
    node3->Display();
    node4->Display();

    // Free memory for each of the nodes, reset them to nullptr.
    delete node1; node1 = nullptr;
    delete node2; node2 = nullptr;
    delete node3; node3 = nullptr;
    delete node4; node4 = nullptr;

    // Output each node's address.
    // e.g. cout << node1 << endl;
    cout << endl;
    cout << "node1's address: " << node1 << endl;
    cout << "node2's address: " << node2 << endl;
    cout << "node3's address: " << node3 << endl;
    cout << "node4's address: " << node4 << endl;
}

#endif
