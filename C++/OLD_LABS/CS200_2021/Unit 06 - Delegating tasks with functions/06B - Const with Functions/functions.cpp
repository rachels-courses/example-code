#include "functions.hpp"

#include <iostream>
using namespace std;

void DisplayMainMenu( int petCount )
{
    cout << endl
        << "-----------------------" << endl
        << "       MAIN MENU       " << endl
        << "-----------------------" << endl
        << "(" << petCount << ") pets" << endl << endl;

    cout << "1. Initialize pets" << endl
         << "2. Add new pet" << endl
         << "3. Display pets" << endl
         << "4. Quit" << endl << endl;
}

int GetChoice( int min, int max )
{
    int choice;
    cout << "(" << min << "-" << max << "): ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid selection. Try again." << endl;
        cout << "(" << min << "-" << max << "): ";
        cin >> choice;
    }

    return choice;
}

void InitializePets( string pets[], int maxPets, int& petCount )
{
    cout << endl;
    cout << "-----------------" << endl;
    cout << " INITIALIZE PETS " << endl;
    cout << "-----------------" << endl;

    cout << "How many pets do you have? ";
    petCount = GetChoice( 0, maxPets );

    cout << endl;

    cin.ignore();
    for ( int i = 0; i < petCount; i++ )
    {
        cout << "Enter name for pet #" << i << ": ";
        getline( cin, pets[i] );
    }
}

void DisplayPets( const string pets[], int petCount )
{
    cout << "----------" << endl;
    cout << " PET LIST " << endl;
    cout << "----------" << endl;
    for ( int i = 0; i < petCount; i++ )
    {
        cout << i << ". \t" << pets[i] << endl;
    }
}

void AddPet( string pets[], int maxPets, int& petCount )
{
    cout << "---------" << endl;
    cout << " ADD PET " << endl;
    cout << "---------" << endl;

    if ( petCount + 1 >= maxPets )
    {
        cout << "ERROR: Cannot add pet! Pet list is full!" << endl;
        return;
    }

    cout << "Enter new pet name: ";
    cin.ignore();
    getline( cin, pets[ petCount ] );

    petCount++;
}
