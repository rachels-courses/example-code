#ifndef _TESTS_HPP
#define _TESTS_HPP

#include "functions.hpp"

void RunTests();
void Test_PercentToDecimal();
void Test_PricePlusTax();
void Test_CountChange();

#endif
