#include <iostream>     // cin/cout
#include <string>       // string data type
using namespace std;

#include "functions.hpp"
#include "tests.hpp"

int main()
{
    bool done = false;
    int userChoice;
    while ( !done )
    {
        DisplayMenu();
        userChoice = GetChoice( 0, 4 );

        switch( userChoice )
        {
            case 0: {
                RunTests();
            } break;

            case 1: {
                cout << "-------------------------------" << endl;
                cout << "PercentToDecimal" << endl << endl;

                float percent;
                cout << "Enter a percent: ";
                cin >> percent;
                float decimal = PercentToDecimal( percent );
                cout << "Decimal: " << decimal << endl;
            } break;

            case 2: {
                cout << "-------------------------------" << endl;
                cout << "PricePlusTax" << endl << endl;

                float price, tax;
                cout << "Enter price: ";
                cin >> price;
                cout << "Enter tax: ";
                cin >> tax;
                float ppt = PricePlusTax( price, tax );
                cout << "Price plus tax: " << ppt << endl;
            } break;

            case 3: {
                cout << "-------------------------------" << endl;
                cout << "CountChange" << endl << endl;

                int quarters, dimes, nickels, pennies;
                cout << "Enter # of quarters: ";
                cin >> quarters;
                cout << "Enter # of dimes: ";
                cin >> dimes;
                cout << "Enter # of nickels: ";
                cin >> nickels;
                cout << "Enter # of pennies: ";
                cin >> pennies;

                float result = CountChange( quarters, dimes, nickels, pennies );
                cout << "Total money: " << result << endl;
            } break;

            case 4: {
                done = true;
            } break;
        }
    }

    return 0;
}
