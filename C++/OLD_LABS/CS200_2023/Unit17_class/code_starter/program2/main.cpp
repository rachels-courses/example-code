#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
using namespace std;

#include "Card.h"
#include "UnitTests.h"

int main() {
  bool done = false;
  srand(time(NULL));

  CardTest();

  // MAKE SURE TESTS ALL PASS BEFORE STARTING ON THE PROGRAM.

  cout << endl << "CARD PROGRAM" << endl;

  const string RANKS[13] = {"A", "2", "3",  "4", "5", "6", "7",
                            "8", "9", "10", "J", "Q", "K"};
  const char SUITS[4] = {'D', 'H', 'C', 'S'};

  string randomRank = RANKS[rand() % 13];
  char randomSuit = SUITS[rand() % 4];

  bool guessedRank = false;
  bool guessedSuit = false;
  int totalGuesses = 0;

  // 1. Create a `secretCard` variable whose data type is the Card type.

  // 2. Use `secretCard.Setup(randomRank, randomSuit);` to set up the card.

  // 3. Create a while loop. Loop while (guessedRank && guessedSuit) == false.
  //   WITHIN THE WHILE LOOP:

  //    3a. IF `guessedSuit` is false, THEN:
  //      Ask the user to guess a suit (D, H, C, or S). Store their answer in a char `guess` variable.
  //      Check to see if the `guess` is equal to the `secretCard.GetSuit()`.
  //          IF it matches, display "Correct!" and set `guessedSuit` to true.
  //          ELSE display "Nope!"

  //    3b. ELSE:
  //      Ask the user to guess a rank (A, 2-10, J, Q, or K). Store their answer in a string `guess` variable.
  //      Check to see if the `guess` is equal to the `secretCard.GetRank()`.
  //        IF it matches, display "Correct!" and set `guessedRank` to true.
  //        ELSE: display "Nope!"

  //    3c. AFTER THE IF/ELSE STATEMENT, add 1 to `totalGuesses`.


  // AFTER THE WHILE LOOP:
  // 4. Display "It took you `totalGuesses` guesses to figure out that the card was..."
  // 5. Use `secretCard.Display();` to display the card's info.

  return 0;
}
