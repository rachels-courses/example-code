/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include "UnitTests.h"

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Card.h"

void CardTest()
{
    cout << string( 80, '-' ) << endl;
    cout << endl << "CARD TESTS" << endl;

    int testsPass = 0;
    int totalTests = 0;

    {
        Card card;
        card.SetRank( "Q" );
        if ( card.rank == "Q" )     { testsPass++; cout << "[PASS] "; }
        else                        { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 1: Use card.SetRank(\"Q\"); to set the card rank." << endl;
        cout << "\t Card card;      card.SetRank(\"Q\");" << endl;
        cout << "\t EXPECTATION:    card.rank is \"Q\"." << endl;
        cout << "\t ACTUAL:         card.rank is \"" << card.rank << "\"." << endl;
        cout << endl;
    }

    {
        Card card;
        card.SetRank( "3" );
        if ( card.rank == "3" )     { testsPass++; cout << "[PASS] "; }
        else                        { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 2: Use card.SetRank(\"3\"); to set the card rank." << endl;
        cout << "\t Card card;      card.SetRank(\"3\");" << endl;
        cout << "\t EXPECTATION:    card.rank is \"3\"." << endl;
        cout << "\t ACTUAL:         card.rank is \"" << card.rank << "\"." << endl;
        cout << endl;
    }

    {
        Card card;
        card.rank = "5";
        if ( card.GetRank() == "5" )    { testsPass++; cout << "[PASS] "; }
        else                            { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 3: Set card.rank to \"5\", check card.GetRank() result." << endl;
        cout << "\t Card card;      card.rank = \"5\". " << endl;
        cout << "\t EXPECTATION:    card.GetRank() is \"5\"." << endl;
        cout << "\t ACTUAL:         card.GetRank() is \"" << card.GetRank() << "\"." << endl;
        cout << endl;
    }

    {
        Card card;
        card.rank = "K";
        if ( card.GetRank() == "K" )    { testsPass++; cout << "[PASS] "; }
        else                            { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 4: Set card.rank to \"K\", check card.GetRank() result." << endl;
        cout << "\t Card card;      card.rank = \"K\". " << endl;
        cout << "\t EXPECTATION:    card.GetRank() is \"K\"." << endl;
        cout << "\t ACTUAL:         card.GetRank() is \"" << card.GetRank() << "\"." << endl;
        cout << endl;
    }

    {
        Card card;
        card.SetSuit( 'D' );
        if ( card.suit == 'D' )     { testsPass++; cout << "[PASS] "; }
        else                        { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 5: Use card.SetSuit( 'D' ); to set the card suit." << endl;
        cout << "\t Card card;      card.SetSuit('D');" << endl;
        cout << "\t EXPECTATION:    card.suit is 'D'." << endl;
        cout << "\t ACTUAL:         card.suit is '" << card.suit << "'." << endl;
        cout << endl;
    }

    {
        Card card;
        card.SetSuit( 'H' );
        if ( card.suit == 'H' )     { testsPass++; cout << "[PASS] "; }
        else                        { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 6: Use card.SetSuit( 'H' ); to set the card suit." << endl;
        cout << "\t Card card;      card.SetSuit('H');" << endl;
        cout << "\t EXPECTATION:    card.suit is 'H'." << endl;
        cout << "\t ACTUAL:         card.suit is '" << card.suit << "'." << endl;
        cout << endl;
    }

    {
        Card card;
        card.suit = 'S';
        if ( card.GetSuit() == 'S' )    { testsPass++; cout << "[PASS] "; }
        else                            { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 7: Set card.suit to 'S', check card.GetSuit() result." << endl;
        cout << "\t Card card;      card.suit = 'S'. " << endl;
        cout << "\t EXPECTATION:    card.GetSuit() is 'S'." << endl;
        cout << "\t ACTUAL:         card.GetSuit() is '" << card.GetRank() << "'." << endl;
        cout << endl;
    }

    {
        Card card;
        card.suit = 'C';
        if ( card.GetSuit() == 'C' )    { testsPass++; cout << "[PASS] "; }
        else                            { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 8: Set card.suit to 'C', check card.GetSuit() result." << endl;
        cout << "\t Card card;      card.suit = 'C'. " << endl;
        cout << "\t EXPECTATION:    card.GetSuit() is 'C'." << endl;
        cout << "\t ACTUAL:         card.GetSuit() is '" << card.GetSuit() << "'." << endl;
        cout << endl;
    }

    {
        Card card;
        card.Setup( "A", 'D' );
        if      ( card.rank != "A" )    { cout << "[FAIL] "; }
        else if ( card.suit != 'D' )    { cout << "[FAIL] "; }
        else                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 9: Use card.Setup( \"A\", 'D' ); and check rank and suit." << endl;
        cout << "\t Card card;      card.Setup( \"A\", 'D' ); " << endl;
        cout << "\t EXPECTATION:    card.rank is \"A\"." << endl;
        cout << "\t ACTUAL:         card.rank is \"" << card.rank << "\"." << endl;
        cout << "\t EXPECTATION:    card.suit is 'D'." << endl;
        cout << "\t ACTUAL:         card.suit is '" << card.suit << "'." << endl;
        cout << endl;
    }

    {
        Card card;
        card.Setup( "5", 'H' );
        if      ( card.rank != "5" )    { cout << "[FAIL] "; }
        else if ( card.suit != 'H' )    { cout << "[FAIL] "; }
        else                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 10: Use card.Setup( \"5\", 'H' ); and check rank and suit." << endl;
        cout << "\t Card card;      card.Setup( \"5\", 'H' ); " << endl;
        cout << "\t EXPECTATION:    card.rank is \"5\"." << endl;
        cout << "\t ACTUAL:         card.rank is \"" << card.rank << "\"." << endl;
        cout << "\t EXPECTATION:    card.suit is 'H'." << endl;
        cout << "\t ACTUAL:         card.suit is '" << card.suit << "'." << endl;
        cout << endl;
    }

    cout << endl << string( 80, '*' ) << endl
        << testsPass << " tests passed out of " << totalTests << " total tests"
        << endl << string( 80, '*' ) << endl;
}
