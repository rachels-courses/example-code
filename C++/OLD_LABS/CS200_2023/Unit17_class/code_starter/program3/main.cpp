#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
using namespace std;

#include "Student.h"
#include "UnitTests.h"

int main() {
  bool done = false;
  srand(time(NULL));

  StudentTest();

  // MAKE SURE TESTS ALL PASS BEFORE STARTING ON THE PROGRAM.

  cout << endl << "STUDENT PROGRAM" << endl;

  // 1. Declare a Student object named `student`.

  // 2. Ask the user to enter the student name. Create a `newName` string variable, and put the user's input into this.

  // 3. Call `student.Setup(newName);` to set the student's name.

  // 4. Ask the user to enter how many grades. Create a `totalGrades` integer variable and store the user's input in this.

  // 5. Create a for loop with a `count` variable. Start at `count = 1`, 
  //  and go until `count <= totalGrades`, adding 1 to count for each update.
  //  WITHIN THE FOR LOOP:

  //    5a. Ask the user to enter a grade.
  //    5b. Create a `newGrade` float variable. Get the user's input and store it in this variable.
  //    5c. Call `student.AddGrade(grade);` to add the grade to the list.

  // AFTER THE FOR LOOP:
  // 6. Use `student.Display();` to display the results.

  return 0;
}
