#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
using namespace std;

#include "Die.h"
#include "UnitTests.h"

int main() {
  bool done = false;
  srand(time(NULL));

  DiceTest();

  // MAKE SURE TESTS ALL PASS BEFORE STARTING ON THE PROGRAM.

  cout << endl << "DICE PROGRAM" << endl;
  
  string player1name = "Hero", player2name = "Goblin";
  int player1hp = 43, player2hp = 35;
  int player1ac = 19, player2ac = 15;
  
  int toHit;
  int damage;
  int round = 1;
  
  // 1. Declare a new Die called `d20` and set it to have 20 sides.

  // 2. Declare a new Die called `d4` and set it to have 4 sides.
  Die d20(20);
  Die d4(4);
  
  // 3. Create a while loop: While the player1hp is greater than 0 and the player2hp is greater than 0, keep looping.
  // Within the loop:

  //   3a. Display the current `round`, as well as `player1name`, `player1hp`, `player2name`, and `player2hp`.
  //      Make sure everything is labeled clearly so the program is easy to use.

  //   3b. Display `player1name` 's TURN

  //   3c. Assign a value to the `toHit` variable: `toHit = d20.Roll() + 4;`

  //   3d. Display "d20+4 to hit...", and then the value of `toHit`.

  //   3e. IF the `toHit` amount is greater than or equal to the `player2ac`, then:
  //      Display "`player1name` hits `player2name`"
  //      Calculate damage: `damage = d4.Roll();`
  //      Display the amount of damage.
  //      Subtract `damage` from the `player2hp`.
  //   3e. ELSE display "`player1name` misses!"

  //   3f. Repeat steps 3b - 3e, except for player2 attacking player1.

  //   3g. Add 1 to `round` at the end of the while loop

  // AFTER THE WHILE LOOP:
  // 4. Display the "RESULTS", which should be each player's name and their remaining HP.

  return 0;
}
