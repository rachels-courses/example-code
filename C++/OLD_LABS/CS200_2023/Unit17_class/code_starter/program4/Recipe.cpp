#include "Recipe.h"

#include <iostream>
using namespace std;

Recipe::Recipe()
{
}

void Recipe::SetName( string name )
{
}

void Recipe::SetInstructions( string instructions )
{
}

void Recipe::AddIngredient( string name, float amount, string unit )
{
}

void Recipe::Display()
{
    cout << m_name << endl << endl;
    for ( int i = 0; i < m_totalIngredients; i++ )
    {
        m_ingredients[i].Display();
    }

    cout << m_instructions << endl;
}
