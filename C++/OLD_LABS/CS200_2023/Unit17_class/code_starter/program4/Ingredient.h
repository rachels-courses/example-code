#ifndef _INGREDIENT_HPP
#define _INGREDIENT_HPP

#include <string>
using namespace std;

class Ingredient
{
    public:
    void Setup( string name, float amount, string unit );
    void Display();

    private:
    string m_name;
    string m_unit;
    float m_amount;

    friend void RecipeTest();
};

#endif
