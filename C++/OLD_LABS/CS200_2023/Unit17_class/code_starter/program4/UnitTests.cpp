/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include "UnitTests.h"

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Recipe.h"

void RecipeTest()
{
    cout << string( 80, '-' ) << endl;
    cout << endl << "RECIPE TESTS" << endl;

    int testsPass = 0;
    int totalTests = 0;

    {
        Ingredient ing;
        ing.Setup( "sugar", 1, "cup" );
        if      ( ing.m_name != "sugar" ) { cout << "[FAIL] "; }
        else if ( ing.m_unit != "cup" ) { cout << "[FAIL] "; }
        else if ( ing.m_amount < 0.9999 || ing.m_amount > 1.0001 ) { cout << "[FAIL] "; }
        else { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 1: Create an Ingredient. Use ing.Setup(\"sugar\", 1, \"cup\"); Verify private member variables." << endl;
        cout << "\t Ingredient ing; ing.Setup(\"sugar\", 1, \"cup\");" << endl;
        cout << "\t EXPECTATION:    ing.m_name is \"sugar\"." << endl;
        cout << "\t ACTUAL:         ing.m_name is \"" << ing.m_name << "\"." << endl;
        cout << "\t EXPECTATION:    ing.m_unit is \"cup\"." << endl;
        cout << "\t ACTUAL:         ing.m_unit is \"" << ing.m_unit << "\"." << endl;
        cout << "\t EXPECTATION:    ing.m_amount is 1." << endl;
        cout << "\t ACTUAL:         ing.m_amount is " << ing.m_amount << "." << endl;
        cout << endl;
    }

    {
        Recipe recipe;
        recipe.SetName( "Sugar cookies" );
        if      ( recipe.m_name != "Sugar cookies" )    { cout << "[FAIL] "; }
        else                                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 2: Create a Recipe. Set its name. Check that m_name is updated." << endl;
        cout << "\t Recipe recipe;  recipe.SetName(\"Sugar cookies\");" << endl;
        cout << "\t EXPECTATION:    recipe.m_name is \"Sugar cookies\"." << endl;
        cout << "\t ACTUAL:         recipe.m_name is \"" << recipe.m_name << "\"." << endl;
        cout << endl;
    }

    {
        Recipe recipe;
        recipe.SetName( "Fruit salad" );
        if      ( recipe.m_name != "Fruit salad" )    { cout << "[FAIL] "; }
        else                                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 3: Create a Recipe. Set its name. Check that m_name is updated." << endl;
        cout << "\t Recipe recipe;  recipe.SetName(\"Fruit salad\");" << endl;
        cout << "\t EXPECTATION:    recipe.m_name is \"Fruit salad\"." << endl;
        cout << "\t ACTUAL:         recipe.m_name is \"" << recipe.m_name << "\"." << endl;
        cout << endl;
    }

    {
        Recipe recipe;
        recipe.SetInstructions( "Mix fruit together." );
        if      ( recipe.m_instructions != "Mix fruit together." )      { cout << "[FAIL] "; }
        else                                                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 4: Create a Recipe. Set its name. Check that m_instructions is updated." << endl;
        cout << "\t Recipe recipe;  recipe.SetInstructions(\"Mix fruit together.\");" << endl;
        cout << "\t EXPECTATION:    recipe.m_instructions is \"Mix fruit together.\"." << endl;
        cout << "\t ACTUAL:         recipe.m_instructions is \"" << recipe.m_name << "\"." << endl;
        cout << endl;
    }

    {
        Recipe recipe;
        recipe.SetInstructions( "Melt butter. Eat melted butter." );
        if      ( recipe.m_instructions != "Melt butter. Eat melted butter." )      { cout << "[FAIL] "; }
        else                                                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 5: Create a Recipe. Set its name. Check that m_instructions is updated." << endl;
        cout << "\t Recipe recipe;  recipe.SetInstructions(\"Melt butter. Eat melted butter.\");" << endl;
        cout << "\t EXPECTATION:    recipe.m_instructions is \"Melt butter. Eat melted butter.\"." << endl;
        cout << "\t ACTUAL:         recipe.m_instructions is \"" << recipe.m_name << "\"." << endl;
        cout << endl;
    }

    {
        Recipe recipe;
        recipe.AddIngredient( "Sugar", 2, "cups" );
        if      ( recipe.m_ingredients[0].m_name != "Sugar" )      { cout << "[FAIL] recipe.m_ingredients[0].m_name "; }
        else if ( recipe.m_ingredients[0].m_unit != "cups" )       { cout << "[FAIL] recipe.m_ingredients[0].m_unit "; }
        else if ( recipe.m_ingredients[0].m_amount < 1.99999 ||
                  recipe.m_ingredients[0].m_amount > 2.00001 )     { cout << "[FAIL] recipe.m_ingredients[0].m_amount "; }
        else if ( recipe.m_totalIngredients != 1 )                 { cout << "[FAIL] m_totalIngredients "; }
        else                                                       { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 6. Create a Recipe. Add an ingredient. Make sure ingredient was added." << endl;
        cout << "\t Recipe recipe;  recipe.AddIngredient(\"Sugar\", 2, \"cups\");" << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[0].m_name is \"Sugar\"." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[0].m_name is \"" << recipe.m_ingredients[0].m_name << "\"." << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[0].m_unit is \"cups\"." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[0].m_unit is \"" << recipe.m_ingredients[0].m_unit << "\"." << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[0].m_amount is 2." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[0].m_amount is " << recipe.m_ingredients[0].m_amount << "." << endl;
        cout << "\t EXPECTATION:    recipe.m_totalIngredients is 1." << endl;
        cout << "\t ACTUAL:         recipe.m_totalIngredients is " << recipe.m_totalIngredients << "." << endl;
        cout << endl;
    }

    {
        Recipe recipe;
        recipe.AddIngredient( "Sugar", 2, "cups" );
        recipe.AddIngredient( "Butter", 1, "stick" );
        if      ( recipe.m_ingredients[0].m_name != "Sugar" )      { cout << "[FAIL] recipe.m_ingredients[0].m_name "; }
        else if ( recipe.m_ingredients[0].m_unit != "cups" )       { cout << "[FAIL] recipe.m_ingredients[0].m_unit "; }
        else if ( recipe.m_ingredients[0].m_amount < 1.99999 ||
                  recipe.m_ingredients[0].m_amount > 2.00001 )     { cout << "[FAIL] recipe.m_ingredients[0].m_amount "; }

        else if ( recipe.m_ingredients[1].m_name != "Butter" )      { cout << "[FAIL] recipe.m_ingredients[1].m_name "; }
        else if ( recipe.m_ingredients[1].m_unit != "stick" )       { cout << "[FAIL] recipe.m_ingredients[1].m_unit "; }
        else if ( recipe.m_ingredients[1].m_amount < 0.99999 ||
                  recipe.m_ingredients[1].m_amount > 1.00001 )     { cout << "[FAIL] recipe.m_ingredients[1].m_amount "; }

        else if ( recipe.m_totalIngredients != 2 )                 { cout << "[FAIL] m_totalIngredients "; }
        else                                                       { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 6. Create a Recipe. Add an ingredient. Make sure ingredient was added." << endl;
        cout << "\t Recipe recipe;  recipe.AddIngredient(\"Sugar\", 2, \"cups\"); recipe.AddIngredient(\"Butter\", 1, \"stick\");" << endl;

        cout << "\t EXPECTATION:    recipe.m_ingredients[0].m_name is \"Sugar\"." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[0].m_name is \"" << recipe.m_ingredients[0].m_name << "\"." << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[0].m_unit is \"cups\"." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[0].m_unit is \"" << recipe.m_ingredients[0].m_unit << "\"." << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[0].m_amount is 2." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[0].m_amount is " << recipe.m_ingredients[0].m_amount << "." << endl;


        cout << "\t EXPECTATION:    recipe.m_ingredients[1].m_name is \"Butter\"." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[1].m_name is \"" << recipe.m_ingredients[1].m_name << "\"." << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[1].m_unit is \"stick\"." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[1].m_unit is \"" << recipe.m_ingredients[1].m_unit << "\"." << endl;
        cout << "\t EXPECTATION:    recipe.m_ingredients[1].m_amount is 1." << endl;
        cout << "\t ACTUAL:         recipe.m_ingredients[1].m_amount is " << recipe.m_ingredients[1].m_amount << "." << endl;

        cout << "\t EXPECTATION:    recipe.m_totalIngredients is 2." << endl;
        cout << "\t ACTUAL:         recipe.m_totalIngredients is " << recipe.m_totalIngredients << "." << endl;
        cout << endl;
    }

    cout << endl << string( 80, '*' ) << endl
        << testsPass << " tests passed out of " << totalTests << " total tests"
        << endl << string( 80, '*' ) << endl;
}
