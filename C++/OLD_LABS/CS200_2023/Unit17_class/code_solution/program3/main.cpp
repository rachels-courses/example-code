#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Student.h"
#include "UnitTests.h"

int main()
{
    bool done = false;
    srand( time( NULL ) );
    
    StudentTest();

    cout << endl << "STUDENT PROGRAM" << endl;
    Student student;

    cout << "Student name? ";
    cin.ignore();
    string name;
    getline( cin, name );

    student.Setup( name );

    int totalGrades;
    cout << "How many grades do you want to add? (1-10): ";
    cin >> totalGrades;

    for ( int count = 1; count <= totalGrades; count++ )
    {
        cout << "Enter grade #" << count << " (4.0 = A, 3.0 = B, 2.0 = C, 1.0 = D, 0.0 = F): ";
        float grade;
        cin >> grade;
        student.AddGrade( grade );
    }

    cout << endl;
    student.Display();

    return 0;
}
