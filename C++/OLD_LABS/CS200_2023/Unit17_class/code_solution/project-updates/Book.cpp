#include "Book.h"

#include <iostream>
#include <iomanip>
using namespace std;

string Book::Setup( string newTitle, string newAuthor, string newIsbn, float newPrice, int newRating )
{
    m_title = newTitle;
    m_author = newAuthor;
    m_isbn = newIsbn;
    m_price = newPrice;
    m_rating = newRating;
}

int Book::SetQuantity( int newQuantity )
{
    m_quantity = newQuantity;
}

float Book::GetPrice()
{
    return m_price * m_quantity;
}

void Book::Display()
{
  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;
  cout << setw( COL0 ) << "x"
       << setw( COL1 ) << m_title
       << setw( COL2 ) << m_author
       << setw( COL3 ) << m_price
       << setw( COL4 ) << m_rating
       << setw( COL5 ) << m_isbn
       << endl;
}

void Book::Edit()
{
  cout << endl;
  cout << "Current title: " << m_title << endl;
  cout << "New title:     ";
  cin.ignore();
  getline( cin, m_title );

  cout << endl;
  cout << "Current author: " << m_author << endl;
  cout << "New author:     ";
  getline( cin, m_author );

  cout << endl;
  cout << "Current price: $" << m_price << endl;
  cout << "New price:     $";
  cin >> m_price;

  cout << endl;
  cout << "Current isbn: " << m_isbn << endl;
  cout << "New isbn:     ";
  cin >> m_isbn;

  cout << endl;
  cout << "Current rating: " << m_rating << endl;
  cout << "New rating:     ";
  cin >> m_rating;

  cout << endl << "Book updated." << endl;
}
