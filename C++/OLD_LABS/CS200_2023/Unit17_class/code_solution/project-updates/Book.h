#ifndef _BOOK
#define _BOOK

#include <string>
#include <iostream>
using namespace std;

class Book
{
public:
    string Setup( string newTitle, string newAuthor, string newIsbn, float newPrice, int newRating );
    int SetQuantity( int newQuantity );
    void Display();
    void Edit();
    
    string GetTitle();
    string GetAuthor();
    string GetIsbn();
    float GetPrice();
    int GetRating();
    int GetQuantity();

private:
    string m_title;
    string m_author;
    string m_isbn;
    float m_price;
    int m_rating;
    int m_quantity;
};

#endif
