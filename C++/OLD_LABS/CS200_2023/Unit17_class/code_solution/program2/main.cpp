#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Card.h"
#include "UnitTests.h"

int main()
{
    bool done = false;
    srand( time( NULL ) );
    
    CardTest();

    cout << endl << "CARD PROGRAM" << endl;

    const string RANKS[13] = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
    const char SUITS[4] = { 'D', 'H', 'C', 'S' };

    string randomRank = RANKS[ rand() % 13 ];
    char randomSuit = SUITS[ rand() % 4 ];

    Card secretCard;
    secretCard.Setup( randomRank, randomSuit );

    bool guessedRank = false;
    bool guessedSuit = false;
    int totalGuesses = 0;

    while ( ( guessedRank && guessedSuit ) == false )
    {
        if ( !guessedSuit )
        {
            cout << "Guess SUIT (D, H, C, S): ";
            char guess;
            cin >> guess;

            if ( guess == secretCard.GetSuit() )
            {
                cout << "CORRECT!" << endl;
                guessedSuit = true;
            }
            else
            {
                cout << "NOPE!" << endl;
            }
        }
        else
        {
            cout << "Guess RANK (A, 2-10, J, Q, K): ";
            string guess;
            cin >> guess;

            if ( guess == secretCard.GetRank() )
            {
                cout << "CORRECT!" << endl;
                guessedRank = true;
            }
            else
            {
                cout << "NOPE!" << endl;
            }
        }
        totalGuesses++;
        cout << endl;
    }

    cout << "It took you " << totalGuesses << " guesses to figure out the card was: ";
    secretCard.Display();
    cout << endl << endl;

    return 0;
}
