#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Recipe.h"
#include "UnitTests.h"

int main()
{
    bool done = false;
    srand( time( NULL ) );

	RecipeTest();

    cout << endl << "RECIPE PROGRAM" << endl;

    Recipe recipe;

    string input;
    cout << "Recipe name: ";
    cin.ignore();
    getline( cin, input );
    recipe.SetName( input );

    cout << "Instructions: ";
    getline( cin, input );
    recipe.SetInstructions( input );

    cout << "Total ingredients: (1-10): ";
    int totalIngredients;
    cin >> totalIngredients;

    cout << endl;
    string name, unit;
    float amount;
    for ( int count = 1; count <= totalIngredients; count++ )
    {
        cout << "INGREDIENT " << count << ":" << endl;
        cout << "* Name of ingredient: ";
        cin.ignore();
        getline( cin, name );
        cout << "* Measurement unit (e.g., cups, tsp): ";
        getline( cin, unit );
        cout << "* Amount (e.g., 1, 2, 0.5): ";
        cin >> amount;
        recipe.AddIngredient( name, amount, unit );
        cout << endl;
    }

    cout << endl << "FINALIZED RECIPE:" << endl;
    recipe.Display();

    return 0;
}
