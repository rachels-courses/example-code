#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Die.h"
#include "UnitTests.h"

// You don't need to modify main()
int main()
{
    bool done = false;
    srand( time( NULL ) );
    
    DiceTests();


    cout << endl << "DICE PROGRAM" << endl;
    Die d20(20);
    Die d4(4);

    string player1name = "Hero";
    string player2name = "Goblin";

    int player1hp = 43;
    int player2hp = 35;

    int player1ac = 19;
    int player2ac = 15;

    int toHit;
    int damage;
    int round = 1;

    while ( player1hp > 0 && player2hp > 0 )
    {
        cout << string( 40, '*' ) << endl;
        cout << "ROUND " << round << endl;
        cout << player1name << "'s HP: " << player1hp << "/43" << endl;
        cout << player2name << "'s HP: " << player2hp << "/35" << endl << endl;

        // PLAYER 1
        cout << player1name << "'s TURN" << endl;
        toHit = d20.Roll() + 4;
        cout << "d20+6 to hit... " << toHit << endl;

        if ( toHit >= player2ac )
        {
            cout << player1name << " hits " << player2name << "... " << endl;
            damage = d4.Roll();
            cout << "1d4 for damage... " << damage << " damage to " << player2name << endl;
            player2hp -= damage;
        }
        else
        {
            cout << player1name << " misses!" << endl;
        }
        cout << endl;

        // PLAYER 2
        cout << player2name << "'s TURN" << endl;
        toHit = d20.Roll() + 7;
        cout << "d20+7 to hit... " << toHit << endl;

        if ( toHit >= player1ac )
        {
            cout << player2name << " hits " << player1name << "... " << endl;
            damage = d4.Roll() + d4.Roll() + 4;
            cout << "2d4+4 for damage... " << damage << " damage to " << player1name << endl;
            player1hp -= damage;
        }
        else
        {
            cout << player2name << " misses!" << endl;
        }
        cout << endl;

        round++;
    }

    cout << endl << endl << "RESULTS:" << endl;
    cout << player1name << "'s HP: " << player1hp << "/43" << endl;
    cout << player2name << "'s HP: " << player2hp << "/35" << endl << endl;

    return 0;
}
