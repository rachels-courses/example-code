/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#include "UnitTests.h"

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Die.h"
#include "Card.h"
#include "Recipe.h"
#include "Student.h"

void DiceTest()
{
    cout << string( 80, '-' ) << endl;
    cout << endl << "DICE TESTS" << endl;

    int testsPass = 0;
    int totalTests = 0;

    {
        Die die;
        if ( die.sides == 6 )   { testsPass++; cout << "[PASS] "; }
        else                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 1: The Die default constructor should initialize sides to 6." << endl;
        cout << "\t Die die;" << endl;
        cout << "\t EXPECTATION:    die.sides to be 6" << endl;
        cout << "\t ACTUAL:         die.sides is " << die.sides << endl;
        cout << endl;
    }

    {
        Die die(12);
        if ( die.sides == 12 )  { testsPass++; cout << "[PASS] "; }
        else                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 2: The Die parameterized constructor should initialize sides to amount passed in." << endl;
        cout << "\t Die die(12);" << endl;
        cout << "\t EXPECTATION:    die.sides to be 12" << endl;
        cout << "\t ACTUAL:         die.sides is " << die.sides << endl;
        cout << endl;
    }

    {
        Die die(20);
        if ( die.sides == 20 )  { testsPass++; cout << "[PASS] "; }
        else                    { cout << "[FAIL] "; }
        totalTests++;
        cout << "TEST 3: The Die parameterized constructor should initialize sides to amount passed in." << endl;
        cout << "\t Die die(20);" << endl;
        cout << "\t EXPECTATION:    die.sides to be 20" << endl;
        cout << "\t ACTUAL:         die.sides is " << die.sides << endl;
        cout << endl;
    }

    {
        Die die;
        int roll = die.Roll();
        if ( roll < 1 )                 { cout << "[FAIL] "; }
        else if ( roll > die.sides )    { cout << "[FAIL] "; }
        else                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 4: The Die roll should return a value between 1 and the amount of sides." << endl;
        cout << "\t Die die;" << endl;
        cout << "\t EXPECTATION:    die.Roll(); returns a result that should be between 1 and " << die.sides << endl;
        cout << "\t ACTUAL:         die.Roll() returned result: " << roll << endl;
        cout << endl;
    }

    {
        Die die(20);
        int roll = die.Roll();
        if ( roll < 1 )                 { cout << "[FAIL] "; }
        else if ( roll > die.sides )    { cout << "[FAIL] "; }
        else                            { testsPass++; cout << "[PASS] "; }
        totalTests++;
        cout << "TEST 5: The Die roll should return a value between 1 and the amount of sides." << endl;
        cout << "\t Die die;" << endl;
        cout << "\t EXPECTATION:    die.Roll(); returns a result that should be between 1 and " << die.sides << endl;
        cout << "\t ACTUAL:         die.Roll() returned result: " << roll << endl;
        cout << endl;
    }

    cout << endl << string( 80, '*' ) << endl
        << testsPass << " tests passed out of " << totalTests << " total tests"
        << endl << string( 80, '*' ) << endl;
}
