#include "functions.h"
#include <iostream>
#include <string>
using namespace std;

// Function definitions

void DisplayMenu() {
  cout << string(30, '-') << endl;
  cout << "MAIN MENU" << endl;

  // TODO: Add main menu
  cout << "1. Show favorite movie" << endl;
  cout << "2. Show favorite book" << endl;
  cout << "3. EXIT" << endl;
}

int GetChoice(int min, int max) {
  int choice;
  cin >> choice;

  while ( choice < min || choice > max )
  {
    cout << "INVALID ENTRY! Try again: ";
    cin >> choice;
  }

  return choice;
}

void DoSomething1() {
  // TODO: Do something
  cout << "Favorite movie is THE LOST SKELETON OF CADAVRA" << endl;
}

void DoSomething2() {
  // TODO: Do something
  cout << "Favorite book is BECAUSE, INTERNET" << endl;
}
