#ifndef _MATH_H
#define _MATH_H

float Sum( float number1, float number2 );
float Area( float width, float length );
float Slope( float x1, float y1, float x2, float y2 );

#endif
