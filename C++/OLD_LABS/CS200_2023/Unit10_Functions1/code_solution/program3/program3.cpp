#include <fstream>  /* Use ifstream */
#include <iostream> /* Use cout */
using namespace std;

#include "MathTester.h"

int main() {

  Test_Sum();
  Test_Area();
  Test_Slope();

  return 0;
}
