#include "MathTester.h"
#include "Math.h"

#include <iomanip>
#include <iostream>
using namespace std;

void Test_Sum()
{
  float input1, input2;
  float expectedOutput, actualOutput;
  cout << left;
  
  // ------------------------------------------------------ TEST 1
  cout << endl << "Test_Sum 1... ";
  
  // Set up variables
  input1 = 2;
  input2 = 3;
  expectedOutput = 5;
  actualOutput = Sum( input1, input2 );
  
  // Check if pass or fail
  if ( actualOutput == expectedOutput ) { cout << "PASS" << endl; }
  else { cout << "FAIL" << endl; }
  
  // Display values of the variables
  cout << setw( 20 ) << "input1: "         << setw( 20 ) << input1;
  cout << setw( 20 ) << "input2: "         << setw( 20 ) << input2 << endl;
  cout << setw( 20 ) << "expectedOutput: " << setw( 20 ) << expectedOutput;
  cout << setw( 20 ) << "actualOutput: "   << setw( 20 ) << actualOutput << endl;
  
  
  // ------------------------------------------------------ TEST 2
}

void Test_Area()
{
  float input1, input2;
  float expectedOutput, actualOutput;
  cout << left;
  
  // ------------------------------------------------------ TEST 1
  cout << endl << "Test_Area 1... ";
  
  // Set up variables
  input1 = 3;
  input2 = 5;
  expectedOutput = 15;
  actualOutput = Area( input1, input2 );
  
  // Check if pass or fail
  if ( actualOutput == expectedOutput ) { cout << "PASS" << endl; }
  else { cout << "FAIL" << endl; }
  
  // Display values of the variables
  cout << setw( 20 ) << "input1: "         << setw( 20 ) << input1;
  cout << setw( 20 ) << "input2: "         << setw( 20 ) << input2 << endl;
  cout << setw( 20 ) << "expectedOutput: " << setw( 20 ) << expectedOutput;
  cout << setw( 20 ) << "actualOutput: "   << setw( 20 ) << actualOutput << endl;
  
  
  // ------------------------------------------------------ TEST 2
}

void Test_Slope()
{
  float inputX1, inputY1, inputX2, inputY2;
  float expectedOutput, actualOutput;
  cout << left;
  
  // ------------------------------------------------------ TEST 1
  cout << endl << "Test_Slope 1... ";
  
  // Set up variables
  inputX1 = 1;
  inputY1 = 3;
  inputX2 = 2;
  inputY2 = 6;
  expectedOutput = 3;
  actualOutput = Slope( inputX1, inputY1, inputX2, inputY2 );
  
  // Check if pass or fail
  if ( actualOutput == expectedOutput ) { cout << "PASS" << endl; }
  else { cout << "FAIL" << endl; }
  
  // Display values of the variables
  cout << setw( 20 ) << "inputX1: "        << setw( 20 ) << inputX1;
  cout << setw( 20 ) << "inputY1: "        << setw( 20 ) << inputY1 << endl;
  cout << setw( 20 ) << "inputX2: "        << setw( 20 ) << inputX2;
  cout << setw( 20 ) << "inputY2: "        << setw( 20 ) << inputY2 << endl;
  cout << setw( 20 ) << "expectedOutput: " << setw( 20 ) << expectedOutput;
  cout << setw( 20 ) << "actualOutput: "   << setw( 20 ) << actualOutput << endl;
  
  
  // ------------------------------------------------------ TEST 2
}
