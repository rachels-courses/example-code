#include "Math.h"

float Sum( float number1, float number2 )
{
  return number1 + number2;
}

float Area( float width, float length )
{
  return width * length;
}

float Slope( float x1, float y1, float x2, float y2 )
{
  return (y2 - y1) / (x2 - x1);
}
