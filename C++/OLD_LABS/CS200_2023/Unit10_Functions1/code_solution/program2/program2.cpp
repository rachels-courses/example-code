#include "Math.h"

#include <iostream>
using namespace std;

int main()
{
  float result;
  
  cout << endl << "TEST 1: Sum numbers 5 and 6" << endl;
  result = Sum( 5, 6 );
  cout << "EXPECTED OUTPUT: 11  \t ACTUAL OUTPUT: " << result << endl;
  
  cout << endl << "TEST 2: Sum numbers -10 and 100" << endl;
  result = Sum( -10, 100 );
  cout << "EXPECTED OUTPUT: 90  \t ACTUAL OUTPUT: " << result << endl;
  
  
  cout << endl << "TEST 3: Get area with width=5, length=3" << endl;
  result = Area( 5, 3 );
  cout << "EXPECTED OUTPUT: 15  \t ACTUAL OUTPUT: " << result << endl;
  
  cout << endl << "TEST 4: Get area with width=10, length=5" << endl;
  result = Area( 10, 5 );
  cout << "EXPECTED OUTPUT: 50  \t ACTUAL OUTPUT: " << result << endl;
  
  
  cout << endl << "TEST 5: Get slope with points (5, 6) and (7, 3)" << endl;
  result = Slope( 5, 6, 7, 3 );
  cout << "EXPECTED OUTPUT: -1.5  \t ACTUAL OUTPUT: " << result << endl;
  
  cout << endl << "TEST 6: Get slope with points (1, 2) and (2, 4)" << endl;
  result = Slope( 1, 2, 2, 4 );
  cout << "EXPECTED OUTPUT: 2  \t ACTUAL OUTPUT: " << result << endl;
  
  
  
  
  
  return 0;
}
