#ifndef _PRODUCT
#define _PRODUCT

#include <string>
using namespace std;

// Create your struct here
struct Product
{
  float price;
  int amount;
  string name;
};

#endif
