#include <iostream> /* Use cout */
#include <iomanip>
#include <string>   /* Use strings */
using namespace std;

#include "Product.h"


int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " ???" << endl;
    return 1; /* Exit with error code 1 */
  }

  // Add code here
  Product product2;
  Product product3;

  Product product1;
  product1.price = 1.99;
  product1.name = "Bean Burrito";
  
  float price2 = 1.79;
  string name2 = "Crunchy Taco";
  int amount2;
  
  float price3 = 5.19;
  string name3 = "Chicken Quesadilla";
  int amount3;
  
  cout << "How many " << product1.name << "s would you like to purchase? ";
  cin >> product1.amount;
  cout << "How many " << product1.name << "s would you like to purchase? ";
  cin >> product2.amount;
  cout << "How many " << product1.name << "s would you like to purchase? ";
  cin >> product3.amount;
  cout << endl;
  
  cout << fixed << setprecision( 2 );
  cout << "RECEIPT" << endl;
  cout << product1.amount << " " << product1.name << "(s) at " << product1.price << "$"<< endl;
  cout << product2.amount << " " << product2.name << "(s) at " << product2.price << "$" << endl;
  cout << product3.amount << " " << product3.name << "(s) at " << product3.price << "$" << endl;
  cout << endl;
  float  totalPrice = (product1.amount * product1.price) + (product2.amount * product2.price) + (product3.amount * product3.price);
  cout << "Total: $" << totalPrice << endl;

  /* Quit program with code 0 (no errors) */
  return 0;
}
