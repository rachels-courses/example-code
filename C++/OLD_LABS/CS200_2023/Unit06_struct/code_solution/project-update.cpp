#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

struct Book
{
  string title;
  string author;
  string isbn;
  float price;
  int quantity;
  int rating;
};

int main()
{
  Book book1, book2, book3;
  
  book1.title =  "Because, Internet";
  book2.title =  "Masters of Doom";
  book3.title =  "The Pragmatic Programmer";

  book1.author =  "Gretchen McCulloch";
  book2.author =  "David Kushner";
  book3.author =  "Andrew Hunt";

  book1.isbn =  "978-0735210936";
  book2.isbn =  "978-0812972153";
  book3.isbn =  "978-0201616224";

  book1.price = 14.49;
  book2.price = 17.99;
  book3.price = 49.6;

  book1.rating = 5;
  book2.rating = 4;
  book3.rating = 3;

  cout << "BOOKSTORE" << endl;
  cout << endl;
  cout << "Title: "  << book1.title << endl;
  cout << "Author: " << book1.author << endl;
  cout << "ISBN: "   << book1.isbn << endl;
  cout << "Price: $" << book1.price << endl;
  cout << "Rating: " << book1.rating << "/5"<< endl;
  
  cout << endl;
  cout << "Title: "  << book2.title << endl;
  cout << "Author: " << book2.author << endl;
  cout << "ISBN: "   << book2.isbn << endl;
  cout << "Price: $" << book2.price << endl;
  cout << "Rating: " << book2.rating << "/5"<< endl;
  
  cout << endl;
  cout << "Title: "  << book3.title << endl;
  cout << "Author: " << book3.author << endl;
  cout << "ISBN: "   << book3.isbn << endl;
  cout << "Price: $" << book3.price << endl;
  cout << "Rating: " << book3.rating << "/5"<< endl;
  
  cout << endl;
  cout << "How many copies of " << book1.title << "? ";
  cin >> book1.quantity;
  cout << "How many copies of " << book2.title << "? ";
  cin >> book2.quantity;
  cout << "How many copies of " << book3.title << "? ";
  cin >> book3.quantity;
  cout << endl;
  
  float  totalPrice = 
    (book1.quantity * book1.price) + 
    (book2.quantity * book2.price) + 
    (book3.quantity * book3.price);
  
  cout << fixed << setprecision(2);
  cout << "Total Price: $" << totalPrice << endl;
  cout << endl;
  cout << "PROGRAM END" << endl;
  return 0;
}
