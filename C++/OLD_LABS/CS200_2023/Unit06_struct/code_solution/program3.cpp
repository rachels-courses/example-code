#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

#include "Book.h"

int main()
{
  Book book;
  book.price = 9.99;
  
  cout << "Enter book title: ";
  getline(cin, book.title);
  cin.ignore();
  cout << "Enter author name: ";
  getline(cin, book.author);
  cout << "Enter ISBN: ";
  cin >> book.isbn;
  cout << "How many do you want to purchase? ";
  cin >> book.quantity;
  cout << endl;
  
  cout << "ORDER INFO " << endl;
  cout << endl;
  cout << "Title: " << book.title << endl;
  cout << "Author: " << book.author << endl;
  cout << "ISBN: " << book.isbn << endl;
  cout << fixed << setprecision(2);
  cout << "Price: $" << book.price << endl;
  cout << endl;
  
  float totalPrice = book.price * book.quantity;
  cout << "Total price: $" << totalPrice << endl;
  
  return 0;
}
