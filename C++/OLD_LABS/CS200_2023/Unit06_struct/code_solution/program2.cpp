#include <iostream>
#include <string>
using namespace std;

struct Address
{
  string name;
  string streetAddress;
  string city;
  string state;
  int zipcode;
};

int main()
{
  Address address;

  cout << "POST OFFICE " << endl;
  cout << endl;
  cout << "Enter name of recipient : ";
  getline( cin, address.name );
  cout << "Enter zip code: ";
  cin >> address.zipcode;
  cout << "Enter state: " ;
  cin >> address.state;
  cin.ignore();
  cout << "Enter city: ";
  getline( cin, address.city );
  cout << "Enter street address: ";
  getline( cin, address.streetAddress );
  cout << endl;
  cout << "PACKAGE TO BE SHIPPED TO: " << endl;
  cout << address.name << endl;
  cout << address.streetAddress << endl;
  cout << address.city << ", " << address.state << " " << address.zipcode << endl;
  
  return 0;
}
