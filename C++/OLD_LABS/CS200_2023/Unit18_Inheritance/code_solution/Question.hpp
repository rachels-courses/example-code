#ifndef _QUESTION_HPP
#define _QUESTION_HPP

#include <iostream>
#include <string>
using namespace std;

class Question
{
    public:
    void SetQuestion( string question );
    void DisplayQuestion();

    protected:
    string m_question;
};

class FillInTheBlankQuestion : public Question
{
    public:
    void SetAnswer( string answer );
    bool AskQuestion();

    private:
    string m_answer;
};

class TrueFalseQuestion : public Question
{
    public:
    void SetAnswer( bool answer );
    bool AskQuestion();

    private:
    bool m_answer;
};

class MultipleChoiceQuestion : public Question
{
    public:
    void SetAnswers( string answers[4], int correctIndex );
    bool AskQuestion();

    private:
    string m_answers[4];
    int m_correctIndex;
};

#endif
