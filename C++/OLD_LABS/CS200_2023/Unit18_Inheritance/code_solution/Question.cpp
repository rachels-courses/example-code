#include "Question.hpp"

#include <iostream>
using namespace std;

void Question::SetQuestion( string question )
{
    m_question = question;
}

void Question::DisplayQuestion()
{
    cout << endl << "QUESTION" << endl;
    cout << m_question << endl << endl;
}

void FillInTheBlankQuestion::SetAnswer( string answer )
{
    m_answer = answer;
}

bool FillInTheBlankQuestion::AskQuestion()
{
    DisplayQuestion();

    cout << "Your answer: ";
    string answer;
    getline( cin, answer );

    cout << endl;
    if ( answer == m_answer )
    {
        cout << "Correct!" << endl;
        return true;
    }
    else
    {
        cout << "Wrong!" << endl;
        return false;
    }
}

void TrueFalseQuestion::SetAnswer( bool answer )
{
    m_answer = answer;
}

bool TrueFalseQuestion::AskQuestion()
{
    DisplayQuestion();

    cout << "0. false" << endl;
    cout << "1. true" << endl;
    cout << "Your answer: ";
    bool choice;
    cin >> choice;

    cout << endl;
    if ( choice == m_answer )
    {
        cout << "Correct!" << endl;
        return true;
    }
    else
    {
        cout << "Wrong!" << endl;
        return false;
    }
}

void MultipleChoiceQuestion::SetAnswers( string answers[4], int correctIndex )
{
    for ( int i = 0; i < 4; i++ )
    {
        m_answers[i] = answers[i];
    }
    m_correctIndex = correctIndex;
}

bool MultipleChoiceQuestion::AskQuestion()
{
    DisplayQuestion();

    for ( int i = 0; i < 4; i++ )
    {
        cout << i << ". " << m_answers[i] << endl;
    }

    int index;
    cout << "Your answer: ";
    cin >> index;

    cout << endl;
    if ( index == m_correctIndex )
    {
        cout << "Correct!" << endl;
        return true;
    }
    else
    {
        cout << "Wrong!" << endl;
        return false;
    }
}






