#include "Question.hpp"

#include <iostream>
using namespace std;

int main()
{
    int score = 0;
    int totalQuestions = 3;

    // TODO: Set up questions

    cout << endl << "QUIZZER" << endl;
    cout << "--------------------------------------------" << endl;

    // TODO: Run quiz

    cout << endl << "RESULTS" << endl;
    cout << score << " correct out of " << totalQuestions << endl;

    return 0;
}
