#include <iostream>

int main()
{
  std::cout << "Title: Because, Internet" << std::endl;
  std::cout << "Author: Gretchen McCulloch" << std::endl;
  std::cout << "ISBN: 978-0735210936" << std::endl;

  std::cout << "Title: Masters of Doom" << std::endl;
  std::cout << "Author: David Kushner" << std::endl;
  std::cout << "ISBN: 978-0812972153" << std::endl;

  std::cout << "Title: The Pragmatic Programmer" << std::endl;
  std::cout << "Author: Andrew Hunt" << std::endl;
  std::cout << "ISBN: 978-0201616224" << std::endl;
  
  return 0;
}
