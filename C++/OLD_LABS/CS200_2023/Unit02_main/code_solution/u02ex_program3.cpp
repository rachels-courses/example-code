#include <iostream>

int main(int argCount, char* args[])
{
  std::cout << "FAVORITE BOOK" << std::endl;
  std::cout << std::endl;
  std::cout << "Title: Because, Internet" << std::endl;
  std::cout << "Author: Gretchen McCulloch" << std::endl;
  std::cout << "ISBN: 978-0735210936" << std::endl;
  std::cout << std::endl;
  std::cout << "My rating: " << args[1] << "/5" << std::endl;
  return 0;
}
