#include <iostream>
using namespace std;

int main() {
  // 1. Create a program loop. Within the program loop...

  // 2. Display a numbered menu of vending machine items.
  // 3. Ask the user what they want to check the price of. Get their selection
  // as an integer.

  // 4. Use a switch statement to check the value of the user's choice:
  // * Case choice 1, show a price of $1.29
  // * Case choice 2, show a price of $1.59
  // * Case choice 3, show a price of $2.10

  bool done = false;
  while (!done) {
    cout << string(80, '-') << endl;
    cout << "VENDING MACHINE" << endl;
    cout << "1. Can of soda" << endl;
    cout << "2. Bottle of water" << endl;
    cout << "3. Bag of chips" << endl;
    cout << endl;
    cout << "What do you want to check the price of? ";

    int choice;
    cin >> choice;

    switch (choice) {
    case 1:
      cout << "Price: $1.29" << endl;
      break;

    case 2:
      cout << "Price: $1.59" << endl;
      break;

    case 3:
      cout << "Price: $2.10" << endl;
      break;

    default:
      cout << "UNKNOWN ITEM!" << endl;
    }
  }

  return 0;
}
