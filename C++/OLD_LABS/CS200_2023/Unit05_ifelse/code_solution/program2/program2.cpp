#include <iomanip>
#include <iostream>
using namespace std;

int main(int argc, char *args[]) {
  float balance = stof(args[1]);
  float withdraw = stof(args[2]);

  cout << fixed << setprecision(2);
  cout << "Balance is: $" << balance << endl;

  if (withdraw <= balance) {
    balance -= withdraw;
  } else {
    cout << "ERROR: Cannot withdraw more than the balance!" << endl;
  }

  cout << "Balance is now: $" << balance << endl;

  return 0;
}