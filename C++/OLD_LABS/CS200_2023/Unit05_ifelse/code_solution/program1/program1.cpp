#include <iostream>
#include <iomanip>
using namespace std;

int main( int argc, char* args[] )
{
  float balance = stof( args[1] );

  cout << fixed << setprecision( 2 );
  cout << "Balance is: $" << balance;
  
  if ( balance < 0 )
  {
    cout << " (OVERDRAWN)";
  }
  
  cout << endl;
  
  return 0;
}