#include <iomanip>
#include <iostream>
using namespace std;

int main(int argc, char *args[]) {
  if ( argc < 3 )
  {
    cout << "NOT ENOUGH ARGUMENTS!" << endl;
    cout << "Expected: " << args[0]
      << " balance withdraw" << endl;
    return 1;
  }
  
  float balance = stof(args[1]);
  float withdraw = stof(args[2]);
  
  if ( balance < 0 )
  {
    cout << "INVALID BALANCE! Cannot be negative" << endl;
    return 2;
  }
  
  if ( withdraw <= 0 )
  {
    cout << "INVALID WITHDRAW! Cannot be 0 or less!" << endl;
    return 3;
  }

  cout << fixed << setprecision(2);
  cout << "Balance is: $" << balance << endl;

  if (withdraw <= balance) {
    balance -= withdraw;
  } else {
    cout << "ERROR: Cannot withdraw more than the balance!" << endl;
  }

  cout << "Balance is now: $" << balance << endl;

  return 0;
}
