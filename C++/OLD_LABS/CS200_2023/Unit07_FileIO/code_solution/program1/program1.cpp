// Purchases v3

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

#include "Product.h"

int main() {
  // 1. DECLARE Product variables
  Product product1, product2, product3;

  // 2. SET UP each product's name and price
  product1.name = "Bean Burrito";
  product1.price = 1.99;

  product2.name = "Crunchy Taco";
  product2.price = 1.79;

  product3.name = "Chicken Quesadilla";
  product3.price = 5.19;

  // 3. ASK THE USER how many of each item
  cout << "= TACO BELL =" << endl << endl;

  cout << "How many " << product1.name << " would you like to purchase? ";
  cin >> product1.amount;

  cout << "How many " << product2.name << " would you like to purchase? ";
  cin >> product2.amount;

  cout << "How many " << product3.name << " would you like to purchase? ";
  cin >> product3.amount;

  // 4. CREATE a receipt file and print out the items and final price
  float totalPrice = product1.amount * product1.price +
                     product2.amount * product2.price +
                     product3.amount * product3.price;

  // 5. CREATE an output file stream variable named "output" and open up the
  // file "receipt.txt".
  ofstream output("receipt.txt");

  // 6. USE setprecision and fixed to set up currency output for "output" (not
  // "cout").
  output << fixed << setprecision(2);

  // 7. WRITE the receipt information out to the text file (use "output", not
  // "cout"). Include each product's AMOUNT, NAME, and PRICE. Put the total
  // price at the end.
  output << "RECEIPT" << endl;
  output << product1.amount << " of " << product1.name << "(s) at $"
         << product1.price << endl;
  output << product2.amount << " of " << product2.name << "(s) at $"
         << product2.price << endl;
  output << product3.amount << " of " << product3.name << "(s) at $"
         << product3.price << endl;
  output << endl << "TOTAL PRICE: $" << totalPrice << endl;

  output.close();

  cout << endl << "Receipt saved to receipt.txt" << endl;
}