// Purchases v3

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

#include "Product.h"

int main() {
  // COPY AND PASTE YOUR CODE FROM v2 OF THE PURCHASING PROGRAM
  // FROM THE U06EX ASSIGNMENT

  // 1. DECLARE Product variables

  // 2. SET UP each product's name and price

  // 3. ASK THE USER how many of each item

  // 4. CREATE a receipt file and print out the items and final price

  // 5. CREATE an output file stream variable named "output" and open up the
  // file "receipt.txt".

  // 6. USE setprecision and fixed to set up currency output for "output" (not
  // "cout").

  // 7. WRITE the receipt information out to the text file (use "output", not
  // "cout"). Include each product's AMOUNT, NAME, and PRICE. Put the total
}