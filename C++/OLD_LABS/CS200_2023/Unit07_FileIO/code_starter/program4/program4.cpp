#include <fstream>
#include <iostream>
#include <string>
using namespace std;

#include "Account.h"

int main(int argCount, char *args[]) {
  // 1. CHECK THAT THERE ARE ENOUGH ARGUMENTS
  // (Expected: 2, the program name and the input txt file)
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << args[0] << " INPUTFILE" << endl;
    return 1;
  }

  // 2. CREATE an Account variable named `myAccount`.

  

  // 3. CREATE an input file stream variable named `input`. Open the file stored
  // in `args[1]`.

  

  // 4. READ in the account name, a string, using `getline(input,
  // myAccount.name);`

  

  // 5. READ the account balance, a float, using `input >> myAccount.balance;`

  

  // 6. CLOSE the input file

  

  // 7. DISPLAY the current account name and balance, ask the user to enter
  // updated info for each.

  

  // 8. CREATE an output file stream variable named `output`. Open `args[1]` as
  // the file.

  

  // 9. WRITE the new account name and balance to the file, each on their own
  // line

  

  // 10. CLOSE the output file.

  

  // 11. DISPLAY a message to the screen that the file has been updated. Use
  // args[1] to tell the user which file it is.

  

  return 0;
}