#include <fstream>
#include <iostream>
#include <string>
using namespace std;

#include "Character.h"

int main(int argCount, char *args[]) {
  // 1. CHECK THAT THERE ARE ENOUGH ARGUMENTS
  // (Expected: 2, the program name and the input txt file)
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << args[0] << " INPUTFILE" << endl;
    return 1;
  }

  // 2. CREATE a myCharacter variable whose data type is a Character

  

  // 3. CREATE an input file stream variable. Use args[1] as the file to open.

  

  // 4. READ the character's name and job with getline

  

  // 5. READ the character's level and money with >>

  

  // 6. CLOSE the input file

  

  // 7. DISPLAY character information and ask for updates

  

  // 9. CREATE an output file stream and open the same character file (args[1])

  

  // 10. WRITE the updated character information back into the file in this
  // order, each on their own line: Name Job Level Money

  

  // 11. DISPLAY a message to the screen that the character file was updated


  return 0;
}