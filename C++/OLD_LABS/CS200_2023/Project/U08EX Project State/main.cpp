# include <fstream>
# include <iostream>
# include <iomanip>
# include "Book.h"
using namespace std;

int main(int argCount, char* args[])
{
  ifstream input("books.txt");
  
  Book book1;
  Book book2;
  Book book3;

  getline(input, book1.title);
  getline(input, book1.author);
  getline(input, book1.isbn);
  input >> book1.price;
  input >> book1.rating;
  input.ignore();
  
  getline(input, book2.title);
  getline(input, book2.author);
  getline(input, book2.isbn);
  input >> book2.price;
  input >> book2.rating;
  input.ignore();
  
  getline(input, book3.title);
  getline(input, book3.author);
  getline(input, book3.isbn);
  input >> book3.price;
  input >> book3.rating;
  input.ignore();

  input.close();
  

//  book1.title = "Physics for Scientists and Engineers 9th Edition";
//  book2.title = "They Say, I Say";
//  book3.title = "Elementary Linear Algebra 8th Edition";
//  book1.author = "Raymond A. Serway";
//  book2.author = "Gerald Graff";
//  book3.author = "Ron Larson";
//  book1.isbn = "978-1133954156";
//  book2.isbn = "978-0393538700";
//  book3.isbn = "978-1337881616";
//  book1.price = 149.99;
//  book2.price = 139.99;
//  book3.price = 159.99;
//  book1.rating = 4;
//  book2.rating = 4;
//  book3.rating = 3; 
  
  cout << "BOOKSTORE" << endl;
  cout << "" << endl;
  cout << "Title: " << book1.title << endl;
  cout << "Author: " << book1.author << endl;
  cout << "ISBN: " << book1.isbn << endl;
  cout << "Price: $" << book1.price << endl;
  cout << "Rating: " << book1.rating << "/5" << endl;
  cout << "" << endl;
  
  cout << "Title: " << book2.title << endl;
  cout << "Author: " << book2.author << endl;
  cout << "ISBN: " << book2.isbn << endl;
  cout << "Price: $" << book2.price << endl;
  cout << "Rating: " << book2.rating << "/5" << endl;
  cout << "" << endl;
  `
  cout << "Title: " << book3.title << endl;
  cout << "Author: " << book3.author << endl;
  cout << "ISBN: " << book3.isbn << endl;
  cout << "Price: $" << book3.price << endl;
  cout << "Rating: " << book3.rating << "/5" << endl;
  cout << "" << endl;

  cout << "How many copies of " << book1.title << "? ";
  cin >> book1.quantity;

  if (book1.quantity < 0)
  {
    cout << "Invalid. Number of copies cannot be negative. It will be set to zero " <<
    "by default." << endl;
    book1.quantity = 0;
  }

  cout << "" << endl;

  cout << "How many copies of " << book2.title << "? ";
  cin >> book2.quantity;

  if (book2.quantity < 0)
  {
    cout << "Invalid. Number of copies cannot be negative. It will be set to zero " <<
    "by default." << endl;
    book2.quantity = 0;
  }
  
  cout << "" << endl;

  cout << "How many copies of " << book3.title << "? ";
  cin >> book3.quantity;

  if (book3.quantity < 0)
  {
    cout << "Invalid. Number of copies cannot be negative. It will be set to zero " <<
    "by default." << endl;
    book3.quantity = 0;
  }

  cout << "" << endl;

  float totalPrice = book1.quantity * book1.price + book2.quantity * book2.price + 
  book3.quantity * book3.price;

  cout << "" << endl;
  cout << "Total Price: $" << totalPrice << endl;

  ofstream output(args[1]);

  output << book1.title << endl;
  output << book1.author << endl;
  output << book1.isbn << endl;
  output << book1.price << endl;
  output << book1.rating << endl;
  output << book2.title << endl;
  output << book2.author << endl;
  output << book2.isbn << endl;
  output << book2.price << endl;
  output << book2.rating << endl;
  output << book3.title << endl;
  output << book3.author << endl;
  output << book3.isbn << endl;
  output << book3.price << endl;
  output << book3.rating << endl;

  output.close();
  
  return 0;

}
