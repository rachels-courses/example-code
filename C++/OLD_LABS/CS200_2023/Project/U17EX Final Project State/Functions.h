#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <vector>
#include <string>
using namespace std;

#include "Book.h"

void DisplayBooks( const vector<Book>& books );
float CalculatePrice( const vector<Book>& books );

void LoadBooks( string filename, vector<Book>& books );
void SaveBooks( string filename, const vector<Book>& books );
void EditBook( Book& book );

void DisplayMainMenu();
int GetChoice( int min, int max );
int GetQuantity();

#endif