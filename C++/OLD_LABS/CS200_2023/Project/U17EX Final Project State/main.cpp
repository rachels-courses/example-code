#include <iomanip>
#include <iostream>
using namespace std;

#include "Book.h"
#include "Functions.h"

int main() {
  vector<Book> books;
  LoadBooks("books.txt", books);

  bool done = false;
  while (!done) {
    cout << endl << "MAIN MENU" << endl;
    DisplayBooks(books);
    DisplayMainMenu();
    int choice = GetChoice(0, 2);

    switch (choice) {
    case 0:
      done = true;
      break;

    case 1: {
      int index;
      cout << "Edit which book? ";
      index = GetChoice(0, books.size() - 1);
      EditBook(books[index]);
    } break;

    case 2: {
      for (unsigned int i = 0; i < books.size(); i++) {
        cout << endl << "How many copies of " << books[i].GetTitle() << "? ";
        int amount;
        cin >> amount;
        books[i].SetQuantity(amount);
      }

      float totalPrice = CalculatePrice(books);
      cout << fixed << setprecision(2);
      cout << endl << "Total price: $" << totalPrice << endl;
    } break;
    }
  }

  SaveBooks("books.txt", books);

  return 0;
}