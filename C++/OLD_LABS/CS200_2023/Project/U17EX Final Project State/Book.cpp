#include "Book.h"

#include <iomanip>
#include <iostream>
using namespace std;

void Book::Display() const {
  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;
  cout << setw(COL1) << m_title << setw(COL2) << m_author << setw(COL3)
       << m_price << setw(COL4) << m_rating << setw(COL5) << m_isbn << endl;
}

void Book::Edit() {
  cout << "Book info:" << endl;
  Display();

  cout << "Enter new title: ";
  cin.ignore();
  getline(cin, m_title);

  cout << "Enter new author: ";
  getline(cin, m_author);

  cout << "Enter new ISBN: ";
  cin >> m_isbn;

  cout << "Enter new price: $";
  cin >> m_price;

  cout << "Enter new rating: ";
  cin >> m_rating;

  cout << endl << "Book updated." << endl;
}

void Book::SetTitle(string newTitle) { m_title = newTitle; }
void Book::SetAuthor(string newAuthor) { m_author = newAuthor; }
void Book::SetIsbn(string newIsbn) { m_isbn = newIsbn; }
void Book::SetPrice(float newPrice) { m_price = newPrice; }
void Book::SetRating(int newRating) { m_rating = newRating; }
void Book::SetQuantity(int newQuantity) { m_quantity = newQuantity; }

string Book::GetTitle() const { return m_title; }
string Book::GetAuthor() const { return m_author; }
string Book::GetIsbn() const { return m_isbn; }
float Book::GetPrice() const { return m_price; }
int Book::GetRating() const { return m_rating; }
int Book::GetQuantity() const { return m_quantity; }

float Book::GetTotalPrice() const { return m_quantity * m_price; }