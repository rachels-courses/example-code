#include "Functions.h"

#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

void DisplayBooks(const vector<Book> &books) {

  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;

  cout << setw(COL0) << "#" << setw(COL1) << "TITLE" << setw(COL2) << "AUTHOR"
       << setw(COL3) << "PRICE" << setw(COL4) << "RATE" << setw(COL5) << "ISBN"
       << endl
       << string(80, '-') << endl;

  for (unsigned int i = 0; i < books.size(); i++) {
    cout << setw(COL0) << i;
    books[i].Display();
  }
}

float CalculatePrice(const vector<Book> &books) {
  float total = 0;
  for (unsigned int i = 0; i < books.size(); i++) {
    total += books[i].GetTotalPrice();
  }
  return total;
}

void LoadBooks(string filename, vector<Book> &books) {
  ifstream input(filename);
  unsigned int total;
  input >> total;

  string newTitle, newAuthor, newIsbn;
  float newPrice;
  int newRating;

  for (unsigned int i = 0; i < total; i++) {
    input.ignore();
    getline(input, newTitle);
    getline(input, newAuthor);
    getline(input, newIsbn);
    input >> newPrice;
    input >> newRating;

    Book newBook;
    newBook.SetTitle(newTitle);
    newBook.SetAuthor(newAuthor);
    newBook.SetIsbn(newIsbn);
    newBook.SetPrice(newPrice);
    newBook.SetRating(newRating);
    books.push_back(newBook);
  }
}

void SaveBooks(string filename, const vector<Book> &books) {
  ofstream output(filename);
  output << books.size() << endl;

  for (unsigned int i = 0; i < books.size(); i++) {
    output << books[i].GetTitle() << endl;
    output << books[i].GetAuthor() << endl;
    output << books[i].GetIsbn() << endl;
    output << books[i].GetPrice() << endl;
    output << books[i].GetRating() << endl;
  }
}

void EditBook(Book &book) {
  book.Edit();
}

void DisplayMainMenu() {
  cout << endl;
  cout << "0. Save and quit" << endl;
  cout << "1. Edit book" << endl;
  cout << "2. Purchase books" << endl;
}

int GetChoice(int min, int max) {
  int choice;
  cout << "(" << min << "-" << max << "): ";
  cin >> choice;

  while (choice < min || choice > max) {
    cout << "ERROR! Invalid selection" << endl;
    cout << "(" << min << "-" << max << "): ";
    cin >> choice;
  }

  return choice;
}

int GetQuantity() {
  int choice;
  cout << ">> ";
  cin >> choice;

  while (choice < 0) {
    cout << " ERROR! Cannot have a negative quantity!" << endl;
    cout << ">> ";
    cin >> choice;
  }

  return choice;
}
