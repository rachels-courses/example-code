#ifndef _BOOK
#define _BOOK

#include <string>
using namespace std;

class Book {
public:
  void Display() const;
  void Edit();

  void SetTitle(string newTitle);
  void SetAuthor(string newAuthor);
  void SetIsbn(string newIsbn);
  void SetPrice(float newPrice);
  void SetRating(int newRating);
  void SetQuantity(int newQuantity);

  string GetTitle() const;
  string GetAuthor() const;
  string GetIsbn() const;
  float GetPrice() const;
  int GetRating() const;
  int GetQuantity() const;

  float GetTotalPrice() const;

private:
  string m_title;
  string m_author;
  string m_isbn;
  float m_price;
  int m_rating;
  int m_quantity;
};

#endif