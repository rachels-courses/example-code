#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

void DisplayMainMenu()
{
  cout << endl;
  cout << "0. Save and quit" << endl;
  cout << "1. Edit book" << endl;
  cout << "2. Purchase books" << endl;
}

int GetChoice( int min, int max )
{
  int choice;
  cout << "Enter a number between " << min << " and " << max << ": ";
  cin >> choice;
  
  while ( choice < min || choice > max )
  {
    cout << "INVALID SELECTION! Out of range!" << endl << endl;
    cout << "Enter a number between " << min << " and " << max << ": ";
    cin >> choice;
  }
  
  return choice;
}

int GetQuantity()
{
  int choice;
  cout << ">> ";
  cin >> choice;
  
  while ( choice < 0 )
  {
    cout << "ERROR! Cannot have a negative quantity!" << endl;
    cin >> choice;    
  }
  
  return choice;
}

void LoadBooks( string filename, Book& book1, Book& book2, Book& book3 )
{
  ifstream input( filename );

  getline( input, book1.title );
  getline( input, book1.author );
  getline( input, book1.isbn );
  input >> book1.price;
  input >> book1.rating;
  input.ignore();
  
  getline( input, book2.title );
  getline( input, book2.author );
  getline( input, book2.isbn );
  input >> book2.price;
  input >> book2.rating;
  input.ignore();
  
  getline( input, book3.title );
  getline( input, book3.author );
  getline( input, book3.isbn );
  input >> book3.price;
  input >> book3.rating;
  input.ignore();

  input.close();
}

void SaveBooks( string filename, const Book& book1, const Book& book2, const Book& book3 )
{
  ofstream output( filename );

  output << book1.title << endl;
  output << book1.author << endl;
  output << book1.isbn << endl;
  output << book1.price << endl;
  output << book1.rating << endl;
  output << book2.title << endl;
  output << book2.author << endl;
  output << book2.isbn << endl;
  output << book2.price << endl;
  output << book2.rating << endl;
  output << book3.title << endl;
  output << book3.author << endl;
  output << book3.isbn << endl;
  output << book3.price << endl;
  output << book3.rating << endl;

  output.close();  
}

void DisplayBooks( Book book1, Book book2, Book book3 )
{
  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;
  
  cout << setw( COL0 ) << "#"
       << setw( COL1 ) << "TITLE"
       << setw( COL2 ) << "AUTHOR"
       << setw( COL3 ) << "PRICE" 
       << setw( COL4 ) << "RATE"
       << setw( COL5 ) << "ISBN"
       << endl << string(80, '-') << endl;
  
  cout << setw( COL0 ) << "1"
       << setw( COL1 ) << book1.title
       << setw( COL2 ) << book1.author
       << setw( COL3 ) << book1.price
       << setw( COL4 ) << book1.rating
       << setw( COL5 ) << book1.isbn 
       << endl;
       
  cout << setw( COL0 ) << "2"
       << setw( COL1 ) << book2.title
       << setw( COL2 ) << book2.author
       << setw( COL3 ) << book2.price
       << setw( COL4 ) << book2.rating
       << setw( COL5 ) << book2.isbn 
       << endl;
       
  cout << setw( COL0 ) << "3"
       << setw( COL1 ) << book3.title
       << setw( COL2 ) << book3.author
       << setw( COL3 ) << book3.price
       << setw( COL4 ) << book3.rating
       << setw( COL5 ) << book3.isbn 
       << endl;
}

float CalculatePrice( Book book1, Book book2, Book book3 )
{
  return book1.price * book1.quantity
       + book2.price * book2.quantity
       + book3.price * book3.quantity;
}

void EditBook( Book& book )
{
  cout << endl;
  cout << "Current title: " << book.title << endl;
  cout << "New title:     ";
  cin.ignore();
  getline( cin, book.title );
  
  cout << endl;
  cout << "Current author: " << book.author << endl;
  cout << "New author:     ";
  getline( cin, book.author );
  
  cout << endl;
  cout << "Current price: $" << book.price << endl;
  cout << "New price:     $";
  cin >> book.price;
  
  cout << endl;
  cout << "Current isbn: " << book.isbn << endl;
  cout << "New isbn:     ";
  cin >> book.isbn;
  
  cout << endl;
  cout << "Current rating: " << book.rating << endl;
  cout << "New rating:     ";
  cin >> book.rating;
  
  cout << endl << "Book updated." << endl;
}
