#ifndef _FUNCTIONS
#define _FUNCTIONS

#include "Book.h"

void DisplayBooks( Book book1, Book book2, Book book3 );
float CalculatePrice( Book book1, Book book2, Book book3 );

void DisplayMainMenu();
int GetChoice( int min, int max );
int GetQuantity();

void LoadBooks( string filename, Book& book1, Book& book2, Book& book3 );
void SaveBooks( string filename, const Book& book1, const Book& book2, const Book& book3 );
void EditBook( Book& book );

#endif
