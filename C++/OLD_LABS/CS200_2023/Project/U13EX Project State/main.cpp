#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

#include "Book.h"
#include "Functions.h"

int main()
{
  Book book1;
  Book book2;
  Book book3;
  
  LoadBooks( "books.txt", book1, book2, book3 );
  
  bool done = false;
  while ( !done )
  {
    cout << endl << "MAIN MENU" << endl << endl;
    DisplayBooks( book1, book2, book3 );
    DisplayMainMenu();
    int choice = GetChoice( 0, 2 );
    
    if ( choice == 0 )
    {
      done = true;
    }
    else if ( choice == 1 )
    {
      cout << endl << "EDIT BOOK" << endl << endl;
      cout << "Edit which book?" << endl;
      int whichBook = GetChoice( 1, 3 );
      
      if ( whichBook == 1 )
      {
        EditBook( book1 );
      }
      else if ( whichBook == 2 )
      {
        EditBook( book2 );
      }
      else if ( whichBook == 3 )
      {
        EditBook( book3 );
      }
    }
    else if ( choice == 2 )
    {
      cout << endl << "PURCHASE BOOKS" << endl << endl;
      
      cout << endl << "How many copies of " << book1.title << "? ";
      book1.quantity = GetQuantity();

      cout << endl << "How many copies of " << book2.title << "? ";
      book2.quantity = GetQuantity();

      cout << endl << "How many copies of " << book3.title << "? ";
      book3.quantity = GetQuantity();
      
      cout << endl;

      float totalPrice = CalculatePrice( book1, book2, book3 );

      cout << "Total Price: $" << totalPrice << endl;
    }
  }
  
  SaveBooks( "books.txt", book1, book2, book3 );

  
  return 0;
}
