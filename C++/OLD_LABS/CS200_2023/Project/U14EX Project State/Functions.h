#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <vector>
using namespace std;

#include "Book.h"

void DisplayBooks( const vector<Book>& books );
float CalculatePrice( const vector<Book>& books );

void DisplayMainMenu();
int GetChoice( int min, int max );
int GetQuantity();

void LoadBooks( string filename, vector<Book>& books );
void SaveBooks( string filename, const vector<Book>& books );
void EditBook( Book& book );

#endif
