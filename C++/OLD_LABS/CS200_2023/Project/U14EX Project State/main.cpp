#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
using namespace std;

#include "Book.h"
#include "Functions.h"

int main()
{
  vector<Book> books;

  LoadBooks( "books.txt", books );

  bool done = false;
  while ( !done )
  {
    cout << endl << "MAIN MENU" << endl << endl;
    DisplayBooks( books );
    DisplayMainMenu();
    int choice = GetChoice( 0, 2 );

    if ( choice == 0 )
    {
      done = true;
    }
    else if ( choice == 1 )
    {
      cout << endl << "EDIT BOOK" << endl << endl;
      cout << "Edit which book?" << endl;
      int whichBook = GetChoice( 0, books.size() );

      EditBook( books[whichBook] );
    }
    else if ( choice == 2 )
    {
      cout << endl << "PURCHASE BOOKS" << endl << endl;

      unsigned int index = 0;
      while ( index < books.size() )
      {
        cout << endl << "How many copies of " << books[index].title << "? ";
        books[index].quantity = GetQuantity();
        index++;
      }

      cout << endl;

      float totalPrice = CalculatePrice( books );

      cout << "Total Price: $" << totalPrice << endl;
    }
  }

  SaveBooks( "books.txt", books );


  return 0;
}
