#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

void DisplayMainMenu()
{
  cout << endl;
  cout << "0. Save and quit" << endl;
  cout << "1. Edit book" << endl;
  cout << "2. Purchase books" << endl;
}

int GetChoice( int min, int max )
{
  int choice;
  cout << "Enter a number between " << min << " and " << max << ": ";
  cin >> choice;

  while ( choice < min || choice > max )
  {
    cout << "INVALID SELECTION! Out of range!" << endl << endl;
    cout << "Enter a number between " << min << " and " << max << ": ";
    cin >> choice;
  }

  return choice;
}

int GetQuantity()
{
  int choice;
  cout << ">> ";
  cin >> choice;

  while ( choice < 0 )
  {
    cout << "ERROR! Cannot have a negative quantity!" << endl;
    cin >> choice;
  }

  return choice;
}

void LoadBooks( string filename, vector<Book>& books )
{
  ifstream input( filename );

  unsigned int total;
  input >> total;

  unsigned int index = 0;
  while ( index < total )
  {
    input.ignore();

    Book newBook;
    getline( input, newBook.title );
    getline( input, newBook.author );
    getline( input, newBook.isbn );
    input >> newBook.price;
    input >> newBook.rating;

    books.push_back( newBook );

    index++;
  }

  input.close();
}

void SaveBooks( string filename, const vector<Book>& books )
{
  ofstream output( filename );

  output << books.size() << endl;

  unsigned int index = 0;
  while ( index < books.size() )
  {
    output << books[index].title << endl;
    output << books[index].author << endl;
    output << books[index].isbn << endl;
    output << books[index].price << endl;
    output << books[index].rating << endl;
    index++;
  }

  output.close();
}

void DisplayBooks( const vector<Book>& books )
{
  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;

  cout << setw( COL0 ) << "#"
       << setw( COL1 ) << "TITLE"
       << setw( COL2 ) << "AUTHOR"
       << setw( COL3 ) << "PRICE"
       << setw( COL4 ) << "RATE"
       << setw( COL5 ) << "ISBN"
       << endl << string(80, '-') << endl;

  unsigned int index = 0;
  while ( index < books.size() )
  {
    cout << setw( COL0 ) << index
         << setw( COL1 ) << books[index].title
         << setw( COL2 ) << books[index].author
         << setw( COL3 ) << books[index].price
         << setw( COL4 ) << books[index].rating
         << setw( COL5 ) << books[index].isbn
         << endl;
    index++;
  }
}

float CalculatePrice( const vector<Book>& books )
{
  float total = 0;

  unsigned int index = 0;
  while ( index < books.size() )
  {
    total += books[index].price * books[index].quantity;
    index++;
  }

  return total;
}

void EditBook( Book& book )
{
  cout << endl;
  cout << "Current title: " << book.title << endl;
  cout << "New title:     ";
  cin.ignore();
  getline( cin, book.title );

  cout << endl;
  cout << "Current author: " << book.author << endl;
  cout << "New author:     ";
  getline( cin, book.author );

  cout << endl;
  cout << "Current price: $" << book.price << endl;
  cout << "New price:     $";
  cin >> book.price;

  cout << endl;
  cout << "Current isbn: " << book.isbn << endl;
  cout << "New isbn:     ";
  cin >> book.isbn;

  cout << endl;
  cout << "Current rating: " << book.rating << endl;
  cout << "New rating:     ";
  cin >> book.rating;

  cout << endl << "Book updated." << endl;
}
