#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

#include "Student.h"
#include "Functions.h"

int main() {
  cout << "POINTING TO OBJECT ADDRESSES" << endl << endl;

  // 1. Create Student variables and assign values

  // 2. Create a Student pointer and initialize it to nullptr

  // 3. Display table of variable values

  // 4. Ask the user which variable to point to.

  // 5. Assign ptr to an address based on user's selection

  // 6. Display the address and value via the ptr

  // 7. Ask the user to enter a new value for this item, assign via the ptr.

  // 8. Display updated table

  return 0;
}
