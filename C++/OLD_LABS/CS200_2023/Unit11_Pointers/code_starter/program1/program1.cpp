#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  cout << "VARIABLE SIZES AND ADDRESSES" << endl << endl;
  
  // 1. Declare variables
  float myFloat = 9.99;

  cout << left;
  cout << setw( 20 ) << "VARIABLE"
       << setw( 10 ) << "VALUE"
       << setw( 10 ) << "SIZE"
       << setw( 20 ) << "ADDRESS"
       << endl << string(60, '-') << endl;
   
  // 2. Display name, value, size, and memory address of each item    
  cout << setw( 20 ) << "myFloat"
       << setw( 10 ) << myFloat
       << setw( 10 ) << sizeof( myFloat )
       << setw( 20 ) << &myFloat
       << endl;
       
  
  return 0;
}
