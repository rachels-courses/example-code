#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct Student
{
  string name;
  float gpa;
};

void DisplayTable( Student& student1, Student& student2, Student& student3 )
{
  cout << left << fixed << setprecision( 1 );
  cout << setw( 20 ) << "VARIABLE"
       << setw( 20 ) << "ADDRESS"
       << setw( 20 ) << "NAME"
       << setw( 20 ) << "GPA"
       << endl << string( 80, '-' ) << endl;
  
  cout << setw( 20 ) << "student1"
       << setw( 20 ) << &student1 
       << setw( 20 ) << student1.name
       << setw( 20 ) << student1.gpa
       << endl;
  
  cout << setw( 20 ) << "student2"
       << setw( 20 ) << &student2 
       << setw( 20 ) << student2.name
       << setw( 20 ) << student2.gpa
       << endl;
  
  cout << setw( 20 ) << "student3"
       << setw( 20 ) << &student3 
       << setw( 20 ) << student3.name
       << setw( 20 ) << student3.gpa
       << endl;
  cout << endl;
}

int main()
{
  cout << "POINTING TO OBJECT ADDRESSES" << endl << endl;
  
  // 1. Create Student variables and assign values
  Student student1, student2, student3;
  
  student1.name = "Fuu";
  student1.gpa = 4.0;
  
  student2.name = "Umi";
  student2.gpa = 3.0;
  
  student3.name = "Hikaru";
  student3.gpa = 2.0;
  
  // 2. Create a float pointer and initialize it to nullptr
  Student * ptr = nullptr;
  
  // 3. Display table of variable values
  DisplayTable( student1, student2, student3 );
  
  cout << "ptr is currently pointing to: " << ptr << endl;
  
  // 4. Ask the user which variable to point to.
  cout << endl;
  int choice;
  cout << "Point to which variable? (1, 2, 3): ";
  cin >> choice;
  
  // 5. Assign ptr to an address based on user's selection
  if ( choice == 1 )
  {
    ptr = &student1;
  }
  else if ( choice == 2 )
  {
    ptr = &student2;
  }
  else if ( choice == 3 )
  {
    ptr = &student3;
  }
  else
  {
      cout << "INVALID SELECTION!" << endl;
      return 1;
  }
  
  // 6. Display the address and value via the ptr
  cout << endl << "ptr is now pointing to: " << ptr << endl;
  cout << "Name at that address is: " << ptr->name << endl;
  cout << "GPA at that address is:  " << ptr->gpa << endl;
  
  // 7. Ask the user to enter a new value for this item, assign via the ptr.
  cout << "Enter an updated name: ";
  cin >> ptr->name;
  cout << "Enter an updated GPA:  ";
  cin >> ptr->gpa;
  
  cout << endl;
  
  // 8. Display updated table
  DisplayTable( student1, student2, student3 );
  
  return 0;
}
