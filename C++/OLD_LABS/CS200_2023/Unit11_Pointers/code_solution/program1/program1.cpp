#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  cout << "VARIABLE SIZES AND ADDRESSES" << endl << endl;
  
  // 1. Declare variables
  int myInteger = 10;
  float myFloat = 9.99;
  double myDouble = 7.77;
  string myString = "Hello";
  bool myBool = true;
  
  // 2. Display name, value, size, and memory address of each item
  cout << left;
  cout << setw( 20 ) << "VARIABLE"
       << setw( 10 ) << "VALUE"
       << setw( 10 ) << "SIZE"
       << setw( 20 ) << "ADDRESS"
       << endl << string(60, '-') << endl;
       
  cout << setw( 20 ) << "myFloat"
       << setw( 10 ) << myFloat
       << setw( 10 ) << sizeof( myFloat )
       << setw( 20 ) << &myFloat
       << endl;
       
  cout << setw( 20 ) << "myDouble"
       << setw( 10 ) << myDouble
       << setw( 10 ) << sizeof( myDouble )
       << setw( 20 ) << &myDouble
       << endl;
       
  cout << setw( 20 ) << "myString"
       << setw( 10 ) << myString
       << setw( 10 ) << sizeof( myString )
       << setw( 20 ) << &myString
       << endl;
       
  cout << setw( 20 ) << "myInteger"
       << setw( 10 ) << myInteger
       << setw( 10 ) << sizeof( myInteger )
       << setw( 20 ) << &myInteger
       << endl;
       
  cout << setw( 20 ) << "myBool"
       << setw( 10 ) << myBool
       << setw( 10 ) << sizeof( myBool )
       << setw( 20 ) << &myBool
       << endl;
  
  return 0;
}
