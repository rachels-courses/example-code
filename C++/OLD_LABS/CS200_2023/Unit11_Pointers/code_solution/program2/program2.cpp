#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void DisplayTable( float& price1, float& price2, float& price3 )
{
  cout << left;
  cout << setw( 20 ) << "VARIABLE"
       << setw( 20 ) << "VALUE"
       << setw( 20 ) << "ADDRESS"
       << endl << string( 60, '-' ) << endl;
  
  cout << setw( 20 ) << "price1"
       << setw( 20 ) << price1
       << setw( 20 ) << &price1 << endl;
  
  cout << setw( 20 ) << "price2"
       << setw( 20 ) << price2
       << setw( 20 ) << &price2 << endl;
  
  cout << setw( 20 ) << "price3"
       << setw( 20 ) << price3
       << setw( 20 ) << &price3 << endl;
  cout << endl;
}

int main()
{
  cout << "POINTING TO VARIABLE ADDRESSES" << endl << endl;
  
  // 1. Create float variables and assign values
  float price1 = 3.99;
  float price2 = 4.99;
  float price3 = 5.99;
  
  // 2. Create a float pointer and initialize it to nullptr
  float * ptr = nullptr;
  
  // 3. Display table of variable values
  DisplayTable( price1, price2, price3 );
  
  cout << "ptr is currently pointing to: " << ptr << endl;
  
  // 4. Ask the user which variable to point to.
  cout << endl;
  int choice;
  cout << "Point to which variable? (1, 2, 3): ";
  cin >> choice;
  
  // 5. Assign ptr to an address based on user's selection
  if ( choice == 1 )
  {
    ptr = &price1;
  }
  else if ( choice == 2 )
  {
    ptr = &price2;
  }
  else if ( choice == 3 )
  {
    ptr = &price3;
  }
  else
  {
      cout << "INVALID SELECTION!" << endl;
      return 1;
  }
  
  // 6. Display the address and value via the ptr
  cout << endl << "ptr is now pointing to: " << ptr << endl;
  cout << "Value of item at that address is: " << *ptr << endl;
  
  // 7. Ask the user to enter a new value for this item, assign via the ptr.
  cout << "Enter an updated value: ";
  cin >> *ptr;
  cout << endl;
  
  // 8. Display updated table
  DisplayTable( price1, price2, price3 );
  
  return 0;
}
