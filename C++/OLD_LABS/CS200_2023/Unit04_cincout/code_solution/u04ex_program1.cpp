#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  float price1 = 1.99;
  string name1 = "Bean Burrito";
  int amount1;
  
  float price2 = 1.79;
  string name2 = "Crunchy Taco";
  int amount2;
  
  float price3 = 5.19;
  string name3 = "Chicken Quesadilla";
  int amount3;
  
  cout << "How many " << name1 << "s would you like to purchase? ";
  cin >> amount1;
  
  cout << "How many " << name2 << "s would you like to purchase? ";
  cin >> amount2;
  
  cout << "How many " << name3 << "s would you like to purchase? ";
  cin >> amount3;
  
  cout << fixed << setprecision( 2 );
  
  cout << endl << "RECEIPT" << endl;
  cout << amount1 << " " << name1 << "(s) at $" << price1 << endl;
  cout << amount2 << " " << name2 << "(s) at $" << price2 << endl;
  cout << amount3 << " " << name3 << "(s) at $" << price3 << endl;
  
  float totalPrice = amount1 * price1 + amount2 * price2 + amount3 * price3;
  
  cout << endl << "Total: $" << totalPrice << endl;
  
  
  return 0;
}
