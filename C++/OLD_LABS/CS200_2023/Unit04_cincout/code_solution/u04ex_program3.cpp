#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  string title = "Because, Internet";
  string author = "Gretchen McCulloch";
  string isbn = "978-0735210936";

  float price = 9.99;
  int quantity;
  
  cout << "Enter book title: ";
  getline( cin, title );
  cout << "Enter author name: ";
  getline( cin, author );
  cout << "Enter ISBN: ";
  getline( cin, isbn );
  cout << "How many do you want to purchase? ";
  cin >> quantity;

  cout << endl << "ORDER INFO" << endl;
  cout << endl;
  cout << "Title:  " << title << endl;
  cout << "Author: " << author << endl;
  cout << "ISBN:   " << isbn << endl;
  cout << "Price: $" << price << endl;
  cout << endl;
  
  float totalPrice = price * quantity;
  
  cout << fixed << setprecision( 2 );
  
  cout << "Total price: $" << totalPrice << endl;

  return 0;
}
