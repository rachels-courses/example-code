#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  string name;
  string streetAddress;
  string city;
  string state;
  int zipcode;
  
  cout << "POST OFFICE" << endl << endl;
  
  cout << "Enter name of recipient: ";
  getline( cin, name );
  
  cout << "Enter zip code: ";
  cin >> zipcode;
  
  cout << "Enter state: ";
  cin >> state;
  
  cin.ignore();
  
  cout << "Enter city: ";
  getline( cin, city );
  
  cout << "Enter street address: ";
  getline( cin, streetAddress );
  
  cout << endl;
  cout << "PACKAGE TO BE SHIPPED TO: " << endl;
  cout << name << endl;
  cout << streetAddress << endl;
  cout << city << ", " << state << " " << zipcode << endl;
  
  return 0;
}
