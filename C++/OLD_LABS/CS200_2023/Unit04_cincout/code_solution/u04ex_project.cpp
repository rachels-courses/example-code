#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  cout << "BOOKSTORE" << endl;
  
  // Book 1
  string title1 = "Because, Internet";
  string author1 = "Gretchen McCulloch";
  string isbn1 = "978-0735210936";
  float price1 = 14.49;
  int rating1 = 5;
  int quantity1;
  
  cout << endl;
  cout << "Title:  " << title1 << endl;
  cout << "Author: " << author1 << endl;
  cout << "ISBN:   " << isbn1 << endl;
  cout << "Price:  $" << price1 << endl;
  cout << "Rating: " << rating1 << "/5" << endl;

  // Book 2
  string title2 = "Masters of Doom";
  string author2 = "David Kushner";
  string isbn2 = "978-0812972153";
  float price2 = 17.99;
  int rating2 = 4;
  int quantity2;
  
  cout << endl;
  cout << "Title:  " << title2 << endl;
  cout << "Author: " << author2 << endl;
  cout << "ISBN:   " << isbn2 << endl;
  cout << "Price:  $" << price2 << endl;
  cout << "Rating: " << rating2 << "/5" << endl;

  // Book 3
  string title3 = "The Pragmatic Programmer";
  string author3 = "Andrew Hunt";
  string isbn3 = "978-0201616224";
  float price3 = 49.60;
  int rating3 = 3.5;
  int quantity3;
  
  cout << endl;
  cout << "Title:  " << title3 << endl;
  cout << "Author: " << author3 << endl;
  cout << "ISBN:   " << isbn3 << endl;
  cout << "Price:  $" << price3 << endl;
  cout << "Rating: " << rating3 << "/5" << endl;
  
  cout << endl << "How many copies of " << title1 << "? ";
  cin >> quantity1;
  
  cout << "How many copies of " << title2 << "? ";
  cin >> quantity2;
  
  cout << "How many copies of " << title3 << "? ";
  cin >> quantity3;
  
  float totalPrice = quantity1 * price1 + quantity2 * price2 + quantity3 * price3;
  
  cout << fixed << setprecision( 2 );
  
  cout << endl << "Total price: $" << totalPrice << endl;
  
  cout << endl << "PROGRAM END" << endl;
  
  return 0;
}
