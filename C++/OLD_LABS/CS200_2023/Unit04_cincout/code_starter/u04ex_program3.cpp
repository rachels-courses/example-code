#include <iostream>
#include <string>
using namespace std;

int main(int argCount, char *args[])
{
  string title = "Because, Internet";
  string author = "Gretchen McCulloch";
  string isbn = "978-0735210936";

  int rating = stoi(args[1]);
  float price = 9.99;

  cout << "FAVORITE BOOK" << endl;
  cout << endl;
  cout << "Title: " << endl;
  cout << "Author: " << endl;
  cout << "ISBN: " << endl;
  cout << "Price: $" << price << endl;
  cout << endl;
  cout << "My rating: " << rating << "/5" << endl;

  return 0;
}
