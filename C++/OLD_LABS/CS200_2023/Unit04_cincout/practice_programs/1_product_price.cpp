#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  string productName1;
  float productPrice1;
  
  string productName2;
  float productPrice2;
  
  string productName3;
  float productPrice3;
  
  cout << "Enter product 1 name: ";
  getline( cin, productName1 );
  cout << "Enter product 1 price: ";
  cin >> productPrice1;
  
  cin.ignore();
  cout << "Enter product 2 name: ";
  getline( cin, productName2 );
  cout << "Enter product 2 price: ";
  cin >> productPrice2;
  
  cin.ignore();
  cout << "Enter product 3 name: ";
  getline( cin, productName3 );
  cout << "Enter product 3 price: ";
  cin >> productPrice3;
  
  cout << fixed << setprecision( 2 );
  cout << endl << "MENU:" << endl;
  cout << productName1 << " $" << productPrice1 << endl;
  cout << productName2 << " $" << productPrice2 << endl;
  cout << productName3 << " $" << productPrice3 << endl;
  
  return 0;
}
