#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
using namespace std;

/**
@param int min     The minimum valid value
@param int max     The maximum valid value
@return            The user's selection, must be between min and max.
*/
int GetChoice( int min, int max )
{
  int choice;
  cout << "Enter your choice: ";
  cin >> choice;
  
  while ( choice < min || choice > max )
  {
    cout << "INVALID! Try again: ";
    cin >> choice;
  }
  
  return choice;
}

void Menu_CreateMonster( vector<Monster>& monsterList )
{
  cout << endl << string( 80, '-' ) << endl;
  cout << endl << "CREATE MONSTER" << endl;
  
  // Create a new Monster object variable

  
  // Ask the user to enter the monster's name, store it in `newMonster.name`.

  
  // Ask the user to enter the monster's level, store it in `newMonster.level`.

  
  // Ask the user to enter the monster's gold, store it in `newMonster.gold`.

  
  // Use the `monsterList` vector's `push_back` function to add `newMonster` to the array.

}

void Menu_ViewAllMonsters( const vector<Monster>& monsterList )
{
  cout << endl << string( 80, '-' ) << endl;
  cout << endl << "VIEW ALL MONSTERS" << endl;
  
  cout << left;
  cout << setw( 10 )  << "INDEX"
       << setw( 20 ) << "NAME"
       << setw( 10 ) << "LEVEL"
       << setw( 10 ) << "GOLD"
       << endl << string( 50, '-' ) << endl;
  
  // Use an `index` variable and a while loop to iterate over all the monsters, display each one's index, name, level, and gold.

}

void Menu_ViewOneMonster( const  vector<Monster>& monsterList )
{
  cout << endl << string( 80, '-' ) << endl;
  cout << endl << "VIEW ONE MONSTER" << endl;
  
  // Ask the user to enter the index of one of the monsters.
  // Index must be between 0 and monsterList.size()-1.

  
  // Display the information for the monster at that index in `monsterList`.

}

