#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

int main()
{
  // OPEN THE DATA FILE
  // 1. Create a string to store a `filename`. Store "companies.txt".
  // 2. Create an input file stream that opens up the filename.
  //      * If opening the input file fails, display an error and exit with a return code.

  
  // CREATE THE DATA STRUCTURE
  // 3. Create a vector of strings, name this item the `vectorArray`.
  
  
  // LOAD FROM THE FILE AND INTO THE DATA STRUCTURE
  // 4. Create a string variable named `buffer`. This is where data will be loaded to before it's stored.
  // 5. Create a while loop that continues looping while `input` is able to read into the `buffer`. Within the loop:
  //    * Use the vectorArray's `push_back` function, passing in the `buffer` data.
  //    * You don't need to do a SIZE CHECK because we should have allocated exactly as much space as we need for this array.

  
  // DISPLAY LOADED DATA FROM THE DATA STRUCTURE
  // 9. Create an unsigned integer named `index` and initialize it to 0.
  // 10. Create a while loop. While `index` is less than the `vectprArray.size()`, do the following:
  //     * Display the current `index` and display the element at the index position,`vectorArray[index]`.
  //     * Add 1 to `index` 

  
  return 0;
}
