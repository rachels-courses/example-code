#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

void LoadBooks( string filename, vector<Book>& books )
{
  // Open the input file stream


  // The first item is the total amount of books. Read this into an integer.


  // Create an index variable and use a while loop to go while the index is less than the total.
  
    // Create a new Book object.
    
    
    // Load in the book's title, author, isbn, price, and rating from the text file.
    

    // Use the books' push_back function to add the new book to the array.
    

    // Add 1 to the index.

}

void SaveBooks( string filename, const vector<Book>& books )
{
  // Create an output file stream and open the filename

  // Output the amount of items in the books vector first

  // Use an index and a while loop to iterate over all the books in the vector.
  // Write out each book's title, author, isbn, price, and rating to the file.

}

void DisplayBooks( const vector<Book>& books )
{
  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;

  cout << setw( COL0 ) << "#"
       << setw( COL1 ) << "TITLE"
       << setw( COL2 ) << "AUTHOR"
       << setw( COL3 ) << "PRICE"
       << setw( COL4 ) << "RATE"
       << setw( COL5 ) << "ISBN"
       << endl << string(80, '-') << endl;
  
  // Iterate through all the books, displaying each one's
  // title, author, price, rating, and isbn.
}
