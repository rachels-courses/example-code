#include <iostream>
#include <string>
#include <vector>
#include <array>
using namespace std;

#include "Monster.h"
#include "Functions.h"

int main()
{
  // Create a vector of Monster objects named `monsterList`
  vector<Monster> monsterList;
  
  // Create a program loop
  bool done = false;
  while ( !done )
  {
    // Display a main menu
    cout << endl << string( 80, '-' ) << endl;
    cout << endl << "MAIN MENU" << endl;
    cout << monsterList.size() << " total monsters(s)" << endl << endl;
    cout << "0. Exit" << endl;
    cout << "1. Create new monster" << endl;
    cout << "2. View all monsters" << endl;
    cout << "3. View one monster" << endl;
    
    // Get the user's selection and call the appropriate function
    int choice = GetChoice( 0, 3 );    
    switch( choice )
    {
      case 0: done = true; break;
      case 1: Menu_CreateMonster( monsterList );   break;
      case 2: Menu_ViewAllMonsters( monsterList ); break;
      case 3: Menu_ViewOneMonster( monsterList );  break;
    }
  }
   
  
  return 0;
}
