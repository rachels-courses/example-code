#include <iostream>
#include <fstream>
#include <string>
#include <array>
using namespace std;

int main()
{
  // OPEN THE DATA FILE
  // 1. Create a string to store a `filename`. Store "topics.txt".
  // 2. Create an input file stream that opens up the filename.
  //      * If opening the input file fails, display an error and exit with a return code.
  string filename = "topics.txt";
  ifstream input( filename );
  if ( input.fail() )
  {
    cout << "ERROR: Could not open file \"" << filename << "\"!" << endl;
    return 1;
  }
  
  // CREATE THE DATA STRUCTURE
  // 3. Create an array object of strings of size 20 named `infoList`.
  array<string,20> infoList;
  
  // LOAD FROM THE FILE AND INTO THE DATA STRUCTURE
  // 4. Create a string variable named `buffer`. This is where data will be loaded to before it's stored.
  // 5. Create an unsigned integer named `index` and initialize it to 0.
  // 6. Create a while loop that continues looping while `input` is able to read into the `buffer`. Within the loop:
  //    * Assign the element in `infoList` at the `index` position to the `buffer`'s value.
  //    * Add 1 to `index`.
  //    * If `index` is greater than or equal to the `infoList.size()`, use break to stop reading.
  string buffer;
  unsigned int index = 0;  
  cout << endl << "Reading in \"" << filename << "\"..." << endl;
  while ( input >> buffer )
  {
    infoList[index] = buffer;
    index++;
    
    if ( index >= infoList.size() )
    {
      // Array is full
      break;
    }
  }
  
  // DISPLAY LOADED DATA FROM THE DATA STRUCTURE
  // 8. Reinitialize `index` to 0.
  // 9. Create a while loop. While `index` is less than the `infoList.size()`, do the following:
  //    * Display the current `index` and display the element at the index position, `infoList[index]`.
  //    * Add 1 to `index` 
  cout << endl << "All items loaded:" << endl;
  index = 0;
  while ( index < infoList.size() )
  {
    cout << index << ". " << infoList[index] << endl;
    index++;
  } 
  
  return 0;
}
