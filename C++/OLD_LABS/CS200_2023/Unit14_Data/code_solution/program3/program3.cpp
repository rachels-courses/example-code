#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
  // OPEN THE DATA FILE
  // 1. Create a string to store a `filename`. Store "emails.txt".
  // 2. Create an input file stream that opens up the filename.
  //      * If opening the input file fails, display an error and exit with a return code.
  string filename = "emails.txt";
  ifstream input( filename );
  if ( input.fail() )
  {
    cout << "ERROR: Could not open file \"" << filename << "\"!" << endl;
    return 1;
  }
  
  // CREATE THE DATA STRUCTURE
  // 3. Create an unsigned integer variable named `size`.
  // 4. Read in the first item from the `input` into the `size` variable.
  // 5. Create a dynamic array of strings using a pointer. Initialize its size to `size`.
  unsigned int size;
  input >> size;  
  string* dynamicArray = new string[size];
  
  // LOAD FROM THE FILE AND INTO THE DATA STRUCTURE
  // 6. Create a string variable named `buffer`. This is where data will be loaded to before it's stored.
  // 7. Create an unsigned integer named `index` and initialize it to 0.
  // 8. Create a while loop that continues looping while `input` is able to read into the `buffer`. Within the loop:
  //    * Assign the element in `infoList` at the `index` position to the `buffer`'s value.
  //    * Add 1 to `index`.
  //    * You don't need to do a SIZE CHECK because we should have allocated exactly as much space as we need for this array.
  string buffer;
  unsigned int index = 0;  
  cout << endl << "Reading in \"" << filename << "\"..." << endl;
  while ( input >> buffer )
  {
    dynamicArray[index] = buffer;
    index++;
  }
  
  // DISPLAY LOADED DATA FROM THE DATA STRUCTURE
  // 9. Reinitialize `index` to 0.
  // 10. Create a while loop. While `index` is less than the `size`, do the following:
  //     * Display the current `index` and display the element at the index position, `dynamicArray[index]`.
  //     * Add 1 to `index` 
  cout << endl << "All items loaded:" << endl;
  index = 0;
  while ( index < size )
  {
    cout << index << ". " << dynamicArray[index] << endl;
    index++;
  } 
  
  // 11. Free the memory allocated
  delete [] dynamicArray;
  
  return 0;
}
