#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <vector>
using namespace std;

#include "Book.h"

void LoadBooks( string filename, vector<Book>& books );
void SaveBooks( string filename, const vector<Book>& books );
void DisplayBooks( const vector<Book>& books );

#endif
