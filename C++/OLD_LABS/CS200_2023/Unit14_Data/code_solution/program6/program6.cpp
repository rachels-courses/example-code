#include <iostream>
#include <array>
#include <vector>
using namespace std;

#include "Book.h"
#include "Functions.h"

int main()
{
  // Create a vector of Book objects
  vector<Book> bookList;
  
  // Call the LoadBooks function
  LoadBooks( "books.txt", bookList );
  
  // Call the DisplayBooks function
  DisplayBooks( bookList );
  
  // Create a new Book object.
  // Ask the user to enter the title, author, isbn, price, and rating for this book.
  cout << endl << "NEW BOOK" << endl;
  Book newBook;
  cout << "Title:  ";
  getline( cin, newBook.title );
  cout << "Author: ";
  getline( cin, newBook.author );
  cout << "ISBN:   ";
  cin >> newBook.isbn;
  cout << "PRICE: $";
  cin >> newBook.price;
  cout << "RATING: ";
  cin >> newBook.rating;
  // Push the new book to the book vector.
  bookList.push_back( newBook );
  
  // Call the SaveBooks function.
  SaveBooks( "books.txt", bookList );
  
  
  return 0;
}
