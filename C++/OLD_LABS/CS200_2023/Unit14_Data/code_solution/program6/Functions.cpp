#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

void LoadBooks( string filename, vector<Book>& books )
{
  // Open the input file stream
  ifstream input( filename );

  // The first item is the total amount of books. Read this into an integer.
  unsigned int total;
  input >> total;

  // Create an index variable and use a while loop to go while the index is less than the total.
  unsigned int index = 0;
  while ( index < total )
  {
    input.ignore();

    // Create a new Book object.
    Book newBook;
    
    // Load in the book's title, author, isbn, price, and rating from the text file.
    getline( input, newBook.title );
    getline( input, newBook.author );
    getline( input, newBook.isbn );
    input >> newBook.price;
    input >> newBook.rating;

    // Use the books' push_back function to add the new book to the array.
    books.push_back( newBook );

    // Add 1 to the index.
    index++;
  }

  input.close();
}

void SaveBooks( string filename, const vector<Book>& books )
{
  // Create an output file stream and open the filename
  ofstream output( filename );

  // Output the amount of items in the books vector first
  output << books.size() << endl;

  // Use an index and a while loop to iterate over all the books in the vector.
  // Write out each book's title, author, isbn, price, and rating to the file.
  unsigned int index = 0;
  while ( index < books.size() )
  {
    output << books[index].title << endl;
    output << books[index].author << endl;
    output << books[index].isbn << endl;
    output << books[index].price << endl;
    output << books[index].rating << endl;
    index++;
  }

  output.close();
}

void DisplayBooks( const vector<Book>& books )
{
  cout << left;
  const int COL0 = 5, COL1 = 30, COL2 = 20, COL3 = 10, COL4 = 10, COL5 = 20;

  cout << setw( COL0 ) << "#"
       << setw( COL1 ) << "TITLE"
       << setw( COL2 ) << "AUTHOR"
       << setw( COL3 ) << "PRICE"
       << setw( COL4 ) << "RATE"
       << setw( COL5 ) << "ISBN"
       << endl << string(80, '-') << endl;
  
  // Iterate through all the books, displaying each one's
  // title, author, price, rating, and isbn.
  unsigned int index = 0;
  while ( index < books.size() )
  {
    cout << setw( COL0 ) << index
         << setw( COL1 ) << books[index].title
         << setw( COL2 ) << books[index].author
         << setw( COL3 ) << books[index].price
         << setw( COL4 ) << books[index].rating
         << setw( COL5 ) << books[index].isbn
         << endl;
    index++;
  }
}
