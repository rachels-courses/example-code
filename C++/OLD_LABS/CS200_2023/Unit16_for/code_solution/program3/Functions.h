#ifndef _FUNCTIONS
#define _FUNCTIONS

#include "Book.h"
#include <vector>
using namespace std;

void DisplayBooks( const vector<Book>& books );

#endif