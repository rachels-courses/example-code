#include "Functions.h"

#include <iomanip>
#include <iostream>
using namespace std;

void DisplayBooks(const vector<Book> &books) {
  const int COL0 = 4;
  const int COL1 = 25;
  const int COL2 = 25;
  const int COL3 = 8;
  const int COL4 = 8;
  const int COL5 = 15;

  cout << left << fixed << setprecision(2);
  cout
    << setw(COL0) << "ID" 
    << setw(COL1) << "TITLE"
    << setw(COL2) << "AUTHOR" 
    << setw(COL3) << "PRICE" 
    << setw(COL4) << "RATING"
    << setw(COL5) << "ISBN" << endl
    << string(80, '-') << endl;
  
  for (unsigned int i = 0; i < books.size(); i++) {
    cout << setw(COL0) << i
      << setw( COL1) << books[i].title
      << setw( COL2) << books[i].author
      << setw( COL3 ) << books[i].price
      << setw( COL4 ) << books[i].rating
      << setw( COL5 ) << books[i].isbn
      << endl;
  }
}