#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "Functions.h"

int main()
{
  const int ARR_SIZE = 4;
  string catsArray[ARR_SIZE];
  catsArray[0] = "Kabe";
  catsArray[1] = "Luna";
  catsArray[2] = "Pixel";
  catsArray[3] = "Korra";

  cout << endl << "CATS ARRAY:" << endl;
  Display( catsArray, 4 );
  
  vector<string> dogsArray;
  dogsArray.push_back("Daisy");
  dogsArray.push_back("Buddy");
  dogsArray.push_back("Freya");

  cout << endl << "DOGS VECTOR:" << endl;
  Display( dogsArray );
  
  return 0;
}