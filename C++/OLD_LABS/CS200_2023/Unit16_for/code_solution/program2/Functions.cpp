#include "Functions.h"

#include <string>
#include <array>
#include <iostream>
using namespace std;


void Display( string arr[], int size )
{
  for ( int i = 0; i < size; i++ )
    {
      cout << i << ". " << arr[i] << endl;
    }
}

void Display( vector<string> arr )
{
  for ( unsigned int i = 0; i < arr.size(); i++ )
    {
      cout << i << ". " << arr[i] << endl;
    }
}