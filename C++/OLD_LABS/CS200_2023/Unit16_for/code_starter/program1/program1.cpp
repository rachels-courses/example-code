#include <iostream>
using namespace std;

int main(int argCount, char *args[]) {
  // Error check
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS!" << endl;
    cout << "Expected form: " << args[0] << " low high" << endl;
    return 1;
  }

  // Get argument values
  int low = stoi(args[1]);
  int high = stoi(args[2]);

  // Error check
  if (low == 0) {
    cout << "low value not allowed to be 0!" << endl;
    return 2;
  }

  cout << endl << "COUNT UP:" << endl;
  // Create a for loop that:
  // * starts at `low`, 
  // * goes until `high`, and 
  // * adds on by 1 each time
  // TODO: Create for loop here!
  cout << endl;

  cout << endl << "COUNT DOWN:" << endl;
  // Create a for loop that:
  // * starts at `high`, 
  // * goes until `low`, and 
  // * subtracts 1 each time
  // TODO: Create for loop here!
  cout << endl;

  cout << endl << "MULTIPLY UP:" << endl;
  // Create a for loop that:
  // * starts at `low`, 
  // * goes until `high`, and 
  // * multiplies by 2 each time
  // TODO: Create for loop here!
  cout << endl;

  cout << endl << "DIVIDE DOWN:" << endl;
  // Create a for loop that:
  // * starts at `high`, 
  // * goes until `low`, and 
  // * divides by 2 each time
  // TODO: Create for loop here!
  cout << endl;

  return 0;
}