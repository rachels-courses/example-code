#include "Functions.h"
#include "Book.h"

int main()
{
  vector<Book> bookList;

  Book book1;
  book1.title = "Because, Internet";
  book1.author = "Gretchen McCulloch";
  book1.isbn = "978-0735210936";
  book1.price = 14.49;
  book1.rating = 5;
  book1.quantity = 1;
  bookList.push_back( book1 ); // Add book1 to bookList
  
  Book book2;
  book2.title = "Masters of Doom";
  book2.author = "David Kushner";
  book2.isbn = "978-0812972153";
  book2.price = 17.99;
  book2.rating = 4;
  book2.quantity = 1;
  bookList.push_back( book2 ); // Add book2 to bookList
  
  Book book3;
  book3.title = "The Pragmatic Programmer";
  book3.author = "Andrew Hunt";
  book3.isbn = "978-0201616224";
  book3.price = 49.60;
  book3.rating = 3;
  book3.quantity = 1;
  bookList.push_back( book3 ); // Add book3 to bookList

  // DisplayBooks(bookList);

  return 0;
}