// !! You don't need to edit this file !!

#include "CatalogProgram.h"
#include <cstdlib>
#include <iomanip>
#include <iostream>
using namespace std;

void CatalogProgram::Run() {
  m_courseManager.LoadCourses("courses.txt");
  Menu_Main();
}

void CatalogProgram::Menu_Main() {
  bool done = false;
  while (!done) {
    Header("MAIN MENU");
    vector<string> options = {"EXIT",
                              "Run unit tests",
                              "View full course list",
                              "View course by INDEX",
                              "Search for course by DEPARTMENT",
                              "Search for course by TITLE",
                              "Search for course by HOURS",
                              "Search for course by DESCRIPTION",
                              "Save course list sorted by DEPARTMENT",
                              "Save course list sorted by TITLE",
                              "Save course list sorted by HOURS"};
    DisplayNumberMenu(options);

    int choice = GetValidInput(0, options.size() - 1);

    switch (choice) {
    case 0:
      done = true;
      break;
    case 1:
      Menu_RunUnitTests();
      break;
    case 2:
      Menu_ViewCourseList();
      break;
    case 3:
      Menu_ViewCourseByIndex();
      break;
    case 4:
      Menu_SearchCoursesByDepartment();
      break;
    case 5:
      Menu_SearchCoursesByTitle();
      break;
    case 6:
      Menu_SearchCoursesByHours();
      break;
    case 7:
      Menu_SearchCoursesByDescription();
      break;
    case 8:
      Menu_SaveListByDepartment();
      break;
    case 9:
      Menu_SaveListByTitle();
      break;
    case 10:
      Menu_SaveListByHours();
      break;
    }
  }
}

void CatalogProgram::Menu_RunUnitTests() {
  Header("RUN UNIT TESTS");

  m_courseManager.RunUnitTests();

  EnterToContinue();
}

void CatalogProgram::Menu_ViewCourseList() {
  cout << endl;
  m_courseManager.DisplayCourseTable();
  EnterToContinue();
}

void CatalogProgram::Menu_ViewCourseByIndex() {
  cout << "Please enter the index of the course." << endl;
  int index = GetValidInput(0, m_courseManager.GetCourseCount());
  cout << endl;

  m_courseManager.DisplayCourse(index);

  EnterToContinue();
}

void CatalogProgram::Menu_SearchCoursesByDepartment() {
  Header("SEARCH COURSES BY DEPARTMENT");
  string searchTerm = "";
  cin.ignore();
  cout << "Enter text to search by: ";
  getline(cin, searchTerm);

  vector<Course> matchingCourses = m_courseManager.SearchCoursesByDepartment(
      m_courseManager.GetCourses(), searchTerm);

  cout << endl
       << matchingCourses.size() << " total matches found:" << endl
       << endl;

  m_courseManager.DisplayCourseTable(matchingCourses);

  EnterToContinue(false);
}

void CatalogProgram::Menu_SearchCoursesByTitle() {
  Header("SEARCH COURSES BY TITLE");
  string searchTerm = "";
  cin.ignore();
  cout << "Enter text to search by: ";
  getline(cin, searchTerm);

  vector<Course> matchingCourses = m_courseManager.SearchCoursesByTitle(
      m_courseManager.GetCourses(), searchTerm);

  cout << endl
       << matchingCourses.size() << " total matches found:" << endl
       << endl;

  m_courseManager.DisplayCourseTable(matchingCourses);

  EnterToContinue(false);
}

void CatalogProgram::Menu_SearchCoursesByHours() {
  Header("SEARCH COURSES BY HOURS");
  string searchTerm = "";
  cin.ignore();
  cout << "Enter text to search by: ";
  getline(cin, searchTerm);

  vector<Course> matchingCourses = m_courseManager.SearchCoursesByHours(
      m_courseManager.GetCourses(), searchTerm);

  cout << endl
       << matchingCourses.size() << " total matches found:" << endl
       << endl;

  m_courseManager.DisplayCourseTable(matchingCourses);

  EnterToContinue(false);
}

void CatalogProgram::Menu_SearchCoursesByDescription() {
  Header("SEARCH COURSES BY DESCRIPTION");
  string searchTerm = "";
  cin.ignore();
  cout << "Enter text to search by: ";
  getline(cin, searchTerm);

  vector<Course> matchingCourses = m_courseManager.SearchCoursesByDescription(
      m_courseManager.GetCourses(), searchTerm);

  cout << endl
       << matchingCourses.size() << " total matches found:" << endl
       << endl;

  for (unsigned int i = 0; i < matchingCourses.size(); i++) {
    cout << endl << "RESULT " << (i + 1) << ": " << endl;
    matchingCourses[i].FullDisplay();
  }

  EnterToContinue(false);
}

void CatalogProgram::Menu_SaveListByDepartment() {
  Header("SAVE COURSE LIST SORTED BY DEPARTMENT");
  string filename = "courses-by-department.txt";

  m_courseManager.SaveCourses(filename, m_courseManager.GetCourses());

  cout << "Saved to \"" << filename << "\"" << endl;

  EnterToContinue();
}

void CatalogProgram::Menu_SaveListByTitle() {
  Header("SAVE COURSE LIST SORTED BY TITLE");
  string filename = "courses-by-title.txt";

  cout << endl << "Currently sorting with BUBBLE SORT..." << endl;
  vector<Course> sortedCourses =
      m_courseManager.BubbleSortCoursesByTitle(m_courseManager.GetCourses());
  cout << endl << "Done sorting. Saving file..." << endl;

  m_courseManager.SaveCourses(filename, sortedCourses);

  cout << "Saved to \"" << filename << "\"" << endl;

  EnterToContinue();
}

void CatalogProgram::Menu_SaveListByHours() {
  Header("SAVE COURSE LIST SORTED BY HOURS");
  string filename = "courses-by-hours.txt";

  cout << endl << "Currently sorting with INSERTION SORT..." << endl;
  vector<Course> sortedCourses =
      m_courseManager.InsertionSortCoursesByHours(m_courseManager.GetCourses());
  cout << endl << "Done sorting. Saving file..." << endl;

  m_courseManager.SaveCourses(filename, sortedCourses);

  cout << "Saved to \"" << filename << "\"" << endl;

  EnterToContinue();
}

void CatalogProgram::Clear() { cout << string(25, '\n'); }

void CatalogProgram::Header(string text) {
#if defined(WIN32) || defined(_WIN32) ||                                       \
    defined(__WIN32) && !defined(__CYGWIN__)
  system("cls");
#else
  system("clear");
#endif

  const int SCREEN_WIDTH = 80;
  string bar1 = "- " + text + " ";
  int remainingWidth = SCREEN_WIDTH - bar1.size();
  cout << bar1 << string(remainingWidth, '-');
  cout << endl;
}

int CatalogProgram::GetValidInput(int min, int max) {
  int choice;
  cout << "(" << min << " - " << max << "): ";
  cin >> choice;

  while (choice < min || choice > max) {
    cout << "INVALID SELECTION! TRY AGAIN!" << endl;
    cout << "(" << min << " - " << max << "): ";
    cin >> choice;
  }

  return choice;
}

void CatalogProgram::DisplayNumberMenu(vector<string> options) {
  for (unsigned int i = 0; i < options.size(); i++) {
    cout << i << ". " << options[i] << endl;
  }
}

void CatalogProgram::EnterToContinue(bool ignore) {
  cout << endl << "PRESS ENTER TO CONTINUE" << endl;
  string blah;
  if (ignore) {
    cin.ignore();
  }
  getline(cin, blah);
}
