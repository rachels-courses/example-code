#include <iostream>
using namespace std;

int main( int argCount, char* args[] )
{
  // Check to make sure enough arguments were passed in
  if ( argCount < 2 )
  {
    cout << "NOT ENOUGH ARGUMENTS!" << endl;
    cout << "Expected form:" << endl;
    cout << args[0] << " BATTERYCHARGE" << endl;
    return 1;
  }

  // 1. Create a float variable to store the battery charge,
  // get its value from args[1].

  // 2. Display the current battery charge with a % after
  
  // 3. Use an if/else if statement to draw a "battery" based
  // on the charge %:
  // 90 and above:    [####]
  // 75-90:           [###_]
  // 40-75:           [##__]
  // 10-40:           [#___]
  // Below 10:        [____]
  
  return 0;
}
