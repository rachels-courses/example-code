#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
using namespace std;

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

int main()
{
    // I'm trying to make a typewriter effect ;P
    string text = "HELLO! Tonight is a coworking night. \n";
    text += "No new topics this week! \n";
    text += "Let me know if you have any questions! \n\n";
    text += "I am here, I just have my camera off cuz I feel kinda sick. :[";

    int counter = 0;
    while ( true )
    {
        cout << text.at( counter );
//        cout << counter;
        cout.flush();
        usleep( 10000 );
        counter++;

        if ( counter == text.size() )
        {
            cout << endl;
            system( "sleep 5s" );
            ClearScreen();
            counter = 0;
        }
    }


    return 0;
}
