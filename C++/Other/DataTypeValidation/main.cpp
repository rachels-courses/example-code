#include <iostream>         // cin and cout
#include <string>           // strings, stoi
#include <stdexcept>        // try/catch stuff
using namespace std;

string GetStringInput()
{
    string strValue;
    cin >> strValue;
    return strValue;
}

int GetIntInput()
{
    string strValue = GetStringInput();
    int intValue;

    // Is this a valid int?
    try
    {
        intValue = stoi( strValue );
    }
    catch( const std::invalid_argument& ex )
    {
        cout << "Cannot convert \"" << strValue << "\" to int!" << endl;
        return -1;  // Return this as a default
    }

    return intValue;
}

float GetFloatInput()
{
    string strValue = GetStringInput();
    float floatValue;

    // Is this a valid float?
    try
    {
        floatValue = stof( strValue );
    }
    catch( const std::invalid_argument& ex )
    {
        cout << "Cannot convert \"" << strValue << "\" to float!" << endl;
        return -1;  // Return this as a default
    }

    return floatValue;
}

int main()
{
    bool done = false;
    while ( !done )
    {
        cout << string( 30, '-' ) << "MAIN MENU" << string( 30, '-' ) << endl;
        cout << "1. Loop again" << endl;
        cout << "2. Quit" << endl;
        cout << ">> ";

        int choice = GetIntInput();

        cout << endl << "YOU ENTERED: " << choice << endl;

        if ( choice == 2 )
        {
            done = true;
        }

        cout << endl;
    }

    return 0;
}
