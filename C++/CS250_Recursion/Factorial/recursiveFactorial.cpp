#include <iostream>
using namespace std;

int Factorial_Verbose( int n, int level )
{
    cout << string( level, ' ' ) << n << "!" << endl;
    // Terminating case
    if ( n == 0 ) {
        cout << string( level, ' ' ) << n << "! = 1" << endl;
        return 1;
    }
    if ( n == 1 ) {
        cout << string( level, ' ' ) << n << "! = 1" << endl;
        return 1;
    }


    // Recursive case
    int n1Value = Factorial( n - 1, level+1 );

    cout << string( level, ' ' ) << "CONTINUE ... " << n << "!" << endl;

    cout << string( level, ' ' ) << "(n-1)! : " << n1Value << endl;

    int result = n * n1Value;
    cout << string( level, ' ' ) << "n * (n-1)! = " << n << " * " << n1Value << " = " << result << endl;

    return result;

//    return n * Factorial( n - 1 );
}

int Factorial( int n )
{
    // Terminating case
    if ( n == 0 ) { return 1; }     // 0! = 1
    if ( n == 1 ) { return 1; }     // 1! = 1

    // Recursive case
    return n * Factorial( n - 1 );  // n * (n-1)!
}

int main()
{
    int n;

    cout << "Enter value for n: ";
    cin >> n;

    int result = Factorial( n, 0 );
    cout << endl << "FINAL RESULT: " << result << endl;

    return 0;
}
