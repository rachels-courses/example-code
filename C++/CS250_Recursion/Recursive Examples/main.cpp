#include <iostream>     // Use input and output streams
using namespace std;    // Use the STandarD library

#include "functions.hpp"

int main()
{
    // PROGRAM STARTS HERE

    bool done = false;
    while ( !done )
    {
        cout << endl;
        cout << "******************************" << endl;
        cout << "* Recursion                  *" << endl;
        cout << "******************************" << endl;
    cout << "\a";
        cout << "1. Test1" << endl;
        cout << "2. Test2" << endl;
        cout << "3. Test3" << endl;
        cout << "4. Test4" << endl;
        cout << "0. Quit" << endl;

        int choice;
        cout << endl << "SELECTION: ";
        cin >> choice;

        if      ( choice == 1 ) { Test_Set1(); }
        else if ( choice == 2 ) { Test_Set2(); }
        else if ( choice == 3 ) { Test_Set3(); }
        else if ( choice == 4 ) { Test_Set4(); }
        else if ( choice == 0 )
        {
            done = true;
        }
    }

    // Don't automatically quit (Visual Studio)
    cout << endl << "Press enter to continue" << endl;
    cin.ignore();
    cin.get();

    // PROGRAM ENDS HERE
    return 0;
}
