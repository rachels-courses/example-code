#include <iostream>
using namespace std;

string Alphabet_Interative( char start, char end )
{
    string build = "";

    for ( char let = start; let <= end; let++ )
    {
        build += let;
    }

    return build;
}

string Alphabet_Recursive_Verbose( char start, char end, int level )
{
    cout << string( level, ' ' ) << "Alpha(" << start << ", " << end << ")..." << endl;
    string build = "";

    // Terminating case
    if ( start == end )
    {
        build = start;
        cout << string( level, ' ' ) << "Terminating case! = " << build << endl;
        return build;
    }

    // Recursive case
    string recursiveResult = Alphabet_Recursive_Verbose( start+1, end, level+1 );

    cout << string( level, ' ' ) << "Alpha(" << char( start+1 ) << ", " << end << ") = " << recursiveResult << endl;

    cout << string( level, ' ' ) << "Return " << start << " + " << recursiveResult << endl;

    build = start + recursiveResult;
    return build;
}

string Alphabet_Recursive( char start, char end )
{
    string build = "";

    // Terminating case
    if ( start == end )
    {
        build = start;
        return build;
    }

    // Recursive case
    build = start + Alphabet_Recursive( start+1, end );
    return build;
}

int main()
{
    char start, end;

    cout << "Enter a starting letter (A-Z): ";
    cin >> start;

    cout << "Enter a ending letter (A-Z): ";
    cin >> end;

    cout << endl << "ITERATIVE:" << endl;
    string result1 = Alphabet_Interative( start, end );
    cout << "RESULT: " << result1 << endl;

    cout << endl << "RECURSIVE:" << endl;
    string result2 = Alphabet_Recursive( start, end );
    cout << "RESULT: " << result2 << endl;

    cout << endl << "RECURSIVE (VERBOSE):" << endl;
    string result3 = Alphabet_Recursive_Verbose( start, end, 0 );
    cout << "RESULT: " << result3 << endl;

    return 0;
}
