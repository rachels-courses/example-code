#include <iostream>
#include <array>
#include <vector>
using namespace std;

#include "File.h"

int main() {
  // TRADITIONAL ARRAY ------------------------------------
  cout << "TRADITIONAL ARRAY" << endl << endl;
  const int ARRAY_SIZE = 3;
  File schoolFolder[ARRAY_SIZE];

  // Set up data
  schoolFolder[0].name = "main.cpp";
  schoolFolder[0].contents = "int main() { return 0; }";

  schoolFolder[1].name = "notes.txt";
  schoolFolder[1].contents = "Arrays can be used to store a series of items";

  schoolFolder[2].name = "grades.txt";
  schoolFolder[2].contents = "20/20, 25/30, 15/15";

  // Display data
  cout << "File #" << 0 << ": " << schoolFolder[0].name << ", Contents: " << schoolFolder[0].contents << endl;
  cout << "File #" << 1 << ": " << schoolFolder[1].name << ", Contents: " << schoolFolder[1].contents << endl;
  cout << "File #" << 2 << ": " << schoolFolder[2].name << ", Contents: " << schoolFolder[2].contents << endl;

  // ARRAY OBJECT ------------------------------------
  cout << endl << "ARRAY OBJECT" << endl << endl;
  array<File, 3> workFolder;

  // Set up data
  workFolder[0].name = "raise.txt";
  workFolder[0].contents = "5% of $90,000 is $4,500";

  workFolder[1].name = "applic.txt";
  workFolder[1].contents = "Garmin, Cerner, H&R Block";

  workFolder[2].name = "todo.txt";
  workFolder[2].contents = "Look busy, don't lose job, find other job??";

  // Display data
  cout << "File #" << 0 << ": " << workFolder[0].name << ", Contents: " << workFolder[0].contents << endl;
  cout << "File #" << 1 << ": " << workFolder[1].name << ", Contents: " << workFolder[1].contents << endl;
  cout << "File #" << 2 << ": " << workFolder[2].name << ", Contents: " << workFolder[2].contents << endl;


  // ARRAY OBJECT ------------------------------------
  cout << endl << "VECTOR OBJECT" << endl << endl;
  vector<File> gamesFolder;

  File newFile;

  // Set up data
  newFile.name = "Unreal Tournament";
  newFile.contents = "UT.exe";
  gamesFolder.push_back(newFile);
  
  newFile.name = "Final Fantasy XIV";
  newFile.contents = "ffxiv.exe";
  gamesFolder.push_back(newFile);
  
  newFile.name = "Project Zomboid";
  newFile.contents = "zomboid.exe";
  gamesFolder.push_back(newFile);
  
  newFile.name = "Monster Hunter Rise";
  newFile.contents = "MHR.exe";
  gamesFolder.push_back(newFile);

  // Display data
  size_t index = 0;
  while ( index < gamesFolder.size() )
    {
      cout << "File #" << index << ": " << gamesFolder[index].name << ", Contents: " << gamesFolder[index].contents << endl;
      index++;
    }

  
  return 0;
}