#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
  cout << "VECTOR OBJECT!" << endl << endl;
  
  // Declare array
  vector<string> courses;

  // Set up each element
  courses.push_back("CS200");
  courses.push_back("CS210");
  courses.push_back("ASL120");
  courses.push_back("CIS204");

  // Display each element's value
  cout << "Array size is " << courses.size() << endl << endl;
  cout << "courses[0] = " << courses[0] << endl;
  cout << "courses[1] = " << courses[1] << endl;
  cout << "courses[2] = " << courses[2] << endl;
  cout << "courses[3] = " << courses[3] << endl;
  
  return 0;
}