#include <iostream>
#include <string>
#include <array>
using namespace std;

void InitializeMovies( array<string,4>& movieTitles )
{
  movieTitles[0] = "Back to the Future";
  movieTitles[1] = "Home Alone";
  movieTitles[2] = "Alien";
  movieTitles[3] = "Terminator 2";
}

void DisplayMovies( array<string, 4> movieTitles )
{
  for ( size_t i = 0; i < movieTitles.size(); i++ )
  {
    cout << i << ". " << movieTitles[i] << endl;
  }
}

int main()
{
  array<string,4> movieTitles;
  
  InitializeMovies( movieTitles );
  DisplayMovies( movieTitles );
  
  
  return 0;
}
