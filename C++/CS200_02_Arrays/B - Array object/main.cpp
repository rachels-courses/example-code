#include <iostream>
#include <string>
#include <array>
using namespace std;

int main() {
  cout << "ARRAY OBJECT!" << endl << endl;
  
  // Declare array
  array<string, 4> courses;

  // Set up each element
  courses[0] = "CS200";
  courses[1] = "CS210";
  courses[2] = "ASL120";
  courses[3] = "CIS204";

  // Display each element's value
  cout << "Array size is " << courses.size() << endl << endl;
  cout << "courses[0] = " << courses[0] << endl;
  cout << "courses[1] = " << courses[1] << endl;
  cout << "courses[2] = " << courses[2] << endl;
  cout << "courses[3] = " << courses[3] << endl;
  
  return 0;
}