#include <iostream>
#include <string>
using namespace std;

int main() {
  cout << "TRADITIONAL ARRAY!" << endl << endl;
  
  // Declare array
  const int ARRAY_SIZE = 4;
  string courses[ARRAY_SIZE];

  // Set up each element
  courses[0] = "CS200";
  courses[1] = "CS210";
  courses[2] = "ASL120";
  courses[3] = "CIS204";

  // Display each element's value
  cout << "Array size is " << ARRAY_SIZE << endl << endl;
  cout << "courses[0] = " << courses[0] << endl;
  cout << "courses[1] = " << courses[1] << endl;
  cout << "courses[2] = " << courses[2] << endl;
  cout << "courses[3] = " << courses[3] << endl;
  
  return 0;
}