import java.util.Scanner;

public class BeerAndAge
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );

        System.out.print( "Do you want a beer? " );
        String wantBeer = input.next();

        if ( wantBeer.equals( "yes" ) )
        {
                // Want a beer
                System.out.print( "Wait a minute, what is your age? " );
                int age = input.nextInt();

                if ( age < 21 )
                {
                        System.out.println( "You can't have a beer!" );
                }
                else
                {
                        System.out.println( "Here's a beer!" );
                        System.out.println( "-------" );
                        System.out.println( "|_-_-_|\\" );
                        System.out.println( "|     | |" );
                        System.out.println( "|     |/" );
                        System.out.println( "-------" );
                }
        }
        
        else if ( wantBeer.equals( "no" ) )
        {
                // Don't want a beer
                System.out.println( "OK, here is some tea." );
        }

        else
        {
                // Other answer!
                System.out.println( "I don't know what \"" + wantBeer + "\" means!" );
        }
        
    }
}
