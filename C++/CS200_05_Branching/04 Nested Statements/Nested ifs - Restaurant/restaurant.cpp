#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    cout << "RESTAURANT" << endl;

    cout << "1. Hamburger ($10)" << endl;
    cout << "2. Fries     ($5)" << endl;
    cout << "3. Beer      ($12)" << endl;
    cout << "4. Soda      ($4)" << endl;

    float price;

    int menuChoice;
    cout << "What do you want to order? ";
    cin >> menuChoice;


    if ( menuChoice == 1 ) // hamburger
    {
        price = 10;
    }
    else if ( menuChoice == 2 ) // fries
    {
        price = 5;
    }
    // WAY 1:
    else if ( menuChoice == 3 ) // beer
    {
        int age;
        cout << "What is your age? ";
        cin >> age;

        // Card the user
        if ( age >= 21 )
        {
            price = 12;
        }
        else
        {
            cout << "You can't have any beer." << endl;
            price = 0;
        }
    }
    else if ( menuChoice == 4 ) // soda
    {
        price = 4;
    }

    cout << endl << "Price: $" << price << endl;

    return 0;
}

