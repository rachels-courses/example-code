#include <iostream>
#include <string>
using namespace std;

int main()
{
	float num1, num2, result;
	char operation;

	cout << "Enter first number: ";
	cin >> num1;

	cout << "Enter operation (+, -, *, /): ";
	cin >> operation;

	cout << "Enter second number: ";
	cin >> num2;

	switch( operation )
	{
		case '+':
		result = num1 + num2;
		break;

		case '-':
		result = num1 - num2;
		break;

		case '*':
		result = num1 * num2;
		break;

		case '/':
		result = num1 / num2;
		break;

		default:
		cout << "UNKNOWN OPERATION" << endl;
	}

	cout << num1 << operation << num2 << " = " << result << endl;


	return 0;
}
