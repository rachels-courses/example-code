#include <iomanip>
#include <iostream>
using namespace std;

int main() {
  cout << "PET ADOPTION" << endl << endl;
  cout << "Adopt a pet!" << endl;
  cout << "B. Bird adoption: $40" << endl;
  cout << "C. Cat adoption: $50" << endl;
  cout << "D. Dog adoption: $100" << endl;

  char choice;
  cout << "Choose one: ";
  cin >> choice;

  float price = 0;

  cout << endl;

  switch (choice) {
  case 'B': // if ( choice == 'B' || choice == 'b' )
  case 'b':
    price = 40;
    cout << "BIRD SELECTED" << endl;
    break;

  case 'C': // if ( choice == 'C' || choice == 'c' )
  case 'c':
    price = 50;
    cout << "CAT SELECTED" << endl;
    break;

  case 'D': // if ( choice == 'D' || choice == 'd' )
  case 'd':
    price = 100;
    cout << "DOG SELECTED" << endl;
    break;

  default:
    cout << "INVALID CHOICE" << endl;
  }

  cout << fixed << setprecision(2);
  cout << endl << "Price: $" << price << endl;

  return 0;
}