#include <iomanip>
#include <iostream>
using namespace std;

int main() {
  cout << "SANDWICH SHOP" << endl;
  cout << "1. Grilled Cheese" << endl;
  cout << "2. BLT" << endl;
  cout << "3. Turkey Club" << endl;

  cout << endl << "Your choice: ";

  int choice;
  cin >> choice;

  float price;

  cout << endl;

  switch (choice) {
  case 1:
    cout << "GRILLED CHEESE SELECTED" << endl;
    price = 5.99;
    break;

  case 2:
    cout << "BLT SELECTED" << endl;
    price = 6.95;
    break;

  case 3:
    cout << "TURKEY CLUB SELECTED" << endl;
    price = 6.50;
    break;

  default:
    cout << "INVALID SELECTION" << endl;
  }

  cout << fixed << setprecision(2);
  cout << endl << "Price: $" << price << endl;

  return 0;
}