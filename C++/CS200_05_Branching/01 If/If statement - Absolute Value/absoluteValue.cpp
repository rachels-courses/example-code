#include <iostream>
using namespace std;

int main() {
  float number;
  float absValue;

  cout << "Enter a value (positive or negative): ";
  cin >> number;
  absValue = number;

  if (number < 0) {
    absValue = -absValue;
  }

  cout << "The absolute value of " << number << " is " << absValue << endl;

  return 0;
}