#include <iostream>
using namespace std;

int main() {
  cout << "CAN YOU VOTE?" << endl << endl;

  int age;
  cout << "Enter your age: ";
  cin >> age;

  if ( age < 18 )
  {
    cout << "Under 18! You cannot vote!" << endl;
  }
  else
  {
    cout << "18 or over! You can vote!" << endl;
  }

  return 0;
}