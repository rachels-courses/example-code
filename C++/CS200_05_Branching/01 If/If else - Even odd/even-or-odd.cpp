#include <iostream>
using namespace std;

int main() {
  cout << "EVEN OR ODD" << endl << endl;
  
  int number;
  cout << "Enter a number: ";
  cin >> number;

  if ( number % 2 == 0 )
  {
    cout << "EVEN! 2 evenly divides " << number << "!" << endl;
  }
  else
  {
    cout << "ODD!" << endl;
  }

  return 0;
}