#include <iostream>
using namespace std;

int main() {
  cout << "POSITIVE OR NEGATIVE?" << endl << endl;
  
  float number;
  cout << "Enter a number: ";
  cin >> number;

  if ( number < 0 )
  {
    cout << "The number is negative" << endl;
  }
  else
  {
    cout << "The number is either positive or 0." << endl;
  }
  
  return 0;
}