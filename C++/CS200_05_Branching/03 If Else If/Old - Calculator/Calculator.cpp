#include <iostream>
using namespace std;

int main() {
  cout << "CALCULATOR" << endl << endl;

  float num1, num2, result;

  cout << "Enter number 1: ";
  cin >> num1;

  cout << "Enter number 2: ";
  cin >> num2;

  cout << endl << "What kind of operation?" << endl;
  cout << "1. Add" << endl;
  cout << "2. Subtract" << endl;
  cout << "3. Multiply" << endl;
  cout << "4. Divide" << endl;

  int choice;
  cin >> choice;

  if ( choice == 1 )
  {
    result = num1 + num2;
    cout << "Result: " << result << endl;
  }
  else if ( choice == 2 )
  {
    result = num1 - num2;
    cout << "Result: " << result << endl;
  }
  else if ( choice == 3 ) 
  {
    result = num1 * num2;
    cout << "Result: " << result << endl;
  }
  else if ( choice == 4 )
  {
    if ( num2 == 0 ) {
      cout << "ERROR! Cannot divide by 0!" << endl;
    }
    else
    {
      result = num1 / num2;
      cout << "Result: " << result << endl;
    }
  }
  
  return 0;
}