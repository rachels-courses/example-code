#include <iostream>
#include <string>
using namespace std;

int main()
{
	/*
	 * CS 200
	 * Prerequisites:
	 * (CS 134 with a grade of "C" or higher
	 *
	 * or CIS 142 with a grade of "C" or higher
	 *
	 * or department waiver test and MATH 131 or higher)
	 *
	 * or MATH 241
	 *
	 * or department approval.
	 * */

	char grade_cs134;
	cout << "Enter grade for cs134, for 'x' for none: ";
	cin >> grade_cs134;
	grade_cs134 = toupper( grade_cs134 );

	char grade_cis142;
	cout << "Enter grade for cis142, for 'x' for none: ";
	cin >> grade_cis142;
	grade_cis142 = toupper( grade_cis142 );

	char grade_math241;
	cout << "Did you take math241? (y/n): ";
	cin >> grade_math241;
	grade_math241 = toupper( grade_math241 );

	char grade_math131;
	cout << "Did you take math131? (y/n): ";
	cin >> grade_math131;
	grade_math131 = toupper( grade_math131 );

	char department_waiver;
	cout << "Did you get department approval? (y/n): ";
	cin >> department_waiver;
	department_waiver = toupper( department_waiver );

	bool can_take_cs200 = false;

	/*
	if ( grade_cs134 == 'A' || grade_cs134 == 'B' || grade_cs134 == 'C' )
	{
		can_take_cs200 = true;
	}

	if ( grade_cis142 == 'A' || grade_cis142 == 'B' || grade_cis142 == 'C' )
	{
		can_take_cs200 = true;
	}

	if ( grade_math241 == 'Y' )
	{
		can_take_cs200 = true;
	}

	if ( grade_math131 == 'Y' && department_waiver == 'Y' )
	{
		can_take_cs200 = true;
	}

	if ( department_waiver == 'Y' )
	{
		can_take_cs200 = true;
	}
	* */
	if (
		( grade_cs134 == 'A' || grade_cs134 == 'B' || grade_cs134 == 'C' ) ||
		( grade_cis142 == 'A' || grade_cis142 == 'B' || grade_cis142 == 'C' ) ||
		( grade_math241 == 'Y' ) ||
		( grade_math131 == 'Y' && department_waiver == 'Y' ) ||
		( department_waiver == 'Y' )
	)
	{
		can_take_cs200 = true;
	}


	cout << endl;
	cout << "CAN TAKE CS200? ";

	if ( can_take_cs200 )
	{
		cout << "TRUE" << endl;
	}
	else
	{
		cout << "FALSE" << endl;
	}




	return 0;
}
