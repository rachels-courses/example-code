#include <iostream>
#include <iomanip>
using namespace std;

int main() {
  cout << "PIZZA HAT" << endl << endl;

  cout << "1. Cheese pizza" << endl;
  cout << "2. Pepperoni pizza" << endl;
  cout << "3. Pineapple + canadian Bacon pizza" << endl;
  cout << "4. Buffalo chicken pizza" << endl;
  cout << "5. Lotsa veggies pizza" << endl;
  cout << "6. Vegan veggies pizza" << endl;

  int pizzaChoice;
  cout << endl << "Which pizza would you like? ";
  cin >> pizzaChoice;

  const float BASE_PIZZA_PRICE = 9.00;
  const float VEGGIE_TOPPINGS_PRICE = 0.40;
  const float MEAT_TOPPINGS_PRICE = 0.50;
  const float CHEESE_PRICE = 0.30;
  const float SAUCE_PRICE = 0.25;

  float price;

  cout << fixed << setprecision( 2 );
  
  if ( pizzaChoice == 1 ) // Cheese pizza
  {
    cout << "Pizza ($" << BASE_PIZZA_PRICE << ") and:" << endl;
    cout << "  * Tomato sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Cheese ($" << CHEESE_PRICE << ")" << endl;
    price = BASE_PIZZA_PRICE + SAUCE_PRICE + CHEESE_PRICE;
  }
  else if ( pizzaChoice == 2 ) // Pepperoni pizza
  {
    cout << "Pizza ($" << BASE_PIZZA_PRICE << ") and:" << endl;
    cout << "  * Tomato sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Cheese ($" << CHEESE_PRICE << ")" << endl;
    cout << "  * Pepperoni ($" << MEAT_TOPPINGS_PRICE << ")" << endl;
    price = BASE_PIZZA_PRICE + SAUCE_PRICE + CHEESE_PRICE + MEAT_TOPPINGS_PRICE;
  }
  else if ( pizzaChoice == 3 ) // Pineapple / bacon pizza
  {
    cout << "Pizza ($" << BASE_PIZZA_PRICE << ") and:" << endl;
    cout << "  * Tomato sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Cheese ($" << CHEESE_PRICE << ")" << endl;
    cout << "  * Pineapple ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    cout << "  * Canadian bacon ($" << MEAT_TOPPINGS_PRICE << ")" << endl;
    price = BASE_PIZZA_PRICE + SAUCE_PRICE + CHEESE_PRICE + VEGGIE_TOPPINGS_PRICE + MEAT_TOPPINGS_PRICE;
  }
  else if ( pizzaChoice == 4 ) // Buffalo chicken pizza
  {
    cout << "Pizza ($" << BASE_PIZZA_PRICE << ") and:" << endl;
    cout << "  * Tomato sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Buffalo sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Cheese ($" << CHEESE_PRICE << ")" << endl;
    cout << "  * Chicken ($" << MEAT_TOPPINGS_PRICE << ")" << endl;
    price = BASE_PIZZA_PRICE + SAUCE_PRICE + SAUCE_PRICE + CHEESE_PRICE + MEAT_TOPPINGS_PRICE;
  }
  else if ( pizzaChoice == 5 ) // Veggies
  {
    cout << "Pizza ($" << BASE_PIZZA_PRICE << ") and:" << endl;
    cout << "  * Tomato sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Cheese ($" << CHEESE_PRICE << ")" << endl;
    cout << "  * Tomato ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    cout << "  * Black olives ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    cout << "  * Mushrooms ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    price = BASE_PIZZA_PRICE + SAUCE_PRICE + CHEESE_PRICE + VEGGIE_TOPPINGS_PRICE + VEGGIE_TOPPINGS_PRICE + VEGGIE_TOPPINGS_PRICE;
  }
  else if ( pizzaChoice == 6 ) // Vegan
  {
    cout << "Pizza ($" << BASE_PIZZA_PRICE << ") and:" << endl;
    cout << "  * Tomato sauce ($" << SAUCE_PRICE << ")" << endl;
    cout << "  * Tomato ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    cout << "  * Black olives ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    cout << "  * Mushrooms ($" << VEGGIE_TOPPINGS_PRICE << ")" << endl;
    price = BASE_PIZZA_PRICE + SAUCE_PRICE + VEGGIE_TOPPINGS_PRICE + VEGGIE_TOPPINGS_PRICE + VEGGIE_TOPPINGS_PRICE;
  }

  cout << endl << "TOTAL PRICE: $" << price << endl;

  return 0;    
}