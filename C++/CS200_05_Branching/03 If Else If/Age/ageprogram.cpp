#include <iostream>
#include <string>
using namespace std;

int main()
{
	// If statement
	int age;
	cout << "What is your age? ";
	cin >> age;

	if ( age >= 18 )
	{
		cout << "You can vote" << endl;
	}
	else
	{
		cout << "You cannot vote" << endl;
	}

	if ( age >= 21 )
	{
		cout << "You can drink" << endl;
	}
	else
	{
		cout << "You cannot drink" << endl;
	}

	if ( age >= 25 )
	{
		cout << "You get cheaper car insurance" << endl;
	}
	else
	{
		cout << "You have expensive car insurance" << endl;
	}

	cout << "Goodbye" << endl;

	return 0;
}
