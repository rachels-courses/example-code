#include <iostream>
#include <string>
using namespace std;

int main()
{
	int speed_limit;
	cout << "What's the speed limit? ";
	cin >> speed_limit;

	int driver_speed;
	cout << "How fast was the driver going? ";
	cin >> driver_speed;

	int amount_over = driver_speed - speed_limit;
	float charge = 0;

	if ( amount_over <= 0 )
	{
		cout << "NOT SPEEDING" << endl;
		cout << "Charge them for broken tail light" << endl;
	}
	else if ( amount_over <= 10 )
	{
		charge = 100;
	}
	else if ( amount_over <= 20 )
	{
		charge = 250;
	}
	else
	{
		charge = 450;
		cout << "SEND TO JAIL" << endl;
	}

	cout << "CHARGE: $" << charge << endl;

	return 0;
}
