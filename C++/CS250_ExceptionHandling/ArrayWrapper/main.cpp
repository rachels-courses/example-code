#include "Menu.hpp"
#include <iostream>
using namespace std;

#include "ArrayWrapper.hpp"

int main()
{
    bool done = false;

    ArrayWrapper arr;

    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Add item",
            "Remove item",
            "View all items",
            "Quit"
        } );

        switch( choice )
        {
            case 1: // add item
            {
                Menu::Header( "Add item" );
                int index;
                string data;

                cout << "Insert to what index? ";
                cin >> index;
                cout << "Enter word: ";
                cin >> data;

                try
                {
                    arr.Add( data, index );
                }
                catch( out_of_range ex )
                {
                    cout << "Out of range error: " << ex.what() << endl;
                }
                catch( invalid_argument ex )
                {
                    cout << "Invalid argument error: " << ex.what() << endl;
                }
            }
            break;

            case 2: // remove item
            {
                Menu::Header( "Remove item" );
                int index;

                cout << "Insert to what index? ";
                cin >> index;

                try
                {
                    arr.Remove( index );
                }
                catch( out_of_range ex )
                {
                    cout << "Out of range error: " << ex.what() << endl;
                }
                catch( invalid_argument ex )
                {
                    cout << "Invalid argument error: " << ex.what() << endl;
                }
            }
            break;

            case 3: // view all items
                Menu::Header( "View all items" );
                arr.Display();
            break;

            case 4: // quit
                done = true;
            break;
        }
    }

    return 0;
}
