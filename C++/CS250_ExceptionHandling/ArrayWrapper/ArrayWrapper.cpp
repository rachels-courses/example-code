#include "ArrayWrapper.hpp"

#include <iostream>
#include <exception>
using namespace std;

ArrayWrapper::ArrayWrapper()
    : ARRAY_SIZE( 20 )
{
    Clear();
}

void ArrayWrapper::Clear()
{
    m_totalElements = 0;
    for ( int i = 0; i < ARRAY_SIZE; i++ )
    {
        m_arr[i] = "";
    }
}

void ArrayWrapper::Add( string item, int index )
{
    // Error checking
    if ( index < 0 || index >= 20 )
    {
        // Invalid index
        throw out_of_range( "Index is out of range!" );
    }
    else if ( m_arr[index] != "" )
    {
        // Design error - There is already data here!
        throw invalid_argument( "Item is already at that index!" );
    }

    m_arr[index] = item;
    m_totalElements++;
}

void ArrayWrapper::Remove( int index )
{
    // Error checking
    if ( index < 0 || index >= 20 )
    {
        // Invalid index
        throw out_of_range( "Index is out of range!" );
    }
    else if ( m_arr[index] == "" )
    {
        // Design error - There is already data here!
        throw invalid_argument( "That index is already empty!" );
    }

    m_arr[index] = "";
    m_totalElements--;
}

void ArrayWrapper::Display() const
{
    for ( int i = 0; i < ARRAY_SIZE; i++ )
    {
        cout << i << ". " << m_arr[i] << endl;
    }
}

