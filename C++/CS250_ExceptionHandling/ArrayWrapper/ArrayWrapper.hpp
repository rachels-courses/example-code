#ifndef _ARRAY_WRAPPER_HPP
#define _ARRAY_WRAPPER_HPP

#include <string>
using namespace std;

class ArrayWrapper
{
    public:
    ArrayWrapper();

    void Clear();
    void Add( string item, int index );
    void Remove( int index );
    void Display() const;

    private:
    const int   ARRAY_SIZE;
    int         m_totalElements;
    string      m_arr[20];
};

#endif
