#include <iostream>
#include <stdexcept>
using namespace std;

// THIS FUNCTION COULD **CAUSE** A PROBLEM          (it should detect and throw)
float KidsPerCar( int totalKids, int totalCars )
{
    // Error state
    if ( totalCars == 0 )
    {
        throw invalid_argument( "Cannot divide by 0!" );
    }

    return totalKids / totalCars;
}

// THIS FUNCTION **CALLS** THE PROBLEM FUNCTION     (it should try and catch)
int main()
{
    cout << "FIELD TRIP PLANNER" << endl;

    cout << "How many kids? ";
    int kids;
    cin >> kids;

    cout << "How many cars? ";
    int cars;
    cin >> cars;

    float kpc = 0;

    try
    {
        kpc = KidsPerCar( kids, cars );
    }
    catch( const invalid_argument& ex )
    {
        cout << endl << "ERROR CAUGHT" << endl;
        cout << ex.what() << endl;
    }

    if ( kpc != 0 )
    {
        cout << endl;
        cout << "You should have about " << kpc << " kids per car." << endl;
    }

    cout << endl;
    cout << "GOODBYE" << endl;

    return 0;
}
