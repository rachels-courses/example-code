#include <iostream>
#include <cstdlib>
using namespace std;

#include "customExceptions.hpp"

void CheckPrinterState( bool& printerIsOn, bool& printerHasPaper )
{
    printerIsOn = true;
    printerHasPaper = true;
}

void PrintDocument( bool printerIsOn, bool printerHasPaper )
{
    if ( !printerIsOn )
    {
        throw PrinterOffline( "Printer ID 1234" );
    }

    if ( !printerHasPaper )
    {
        throw PrinterOutOfPaper( "Printer ID 1234" );
    }

    system( "lp document.txt" );    // Tell the Linux system to print document.txt
}

int main()
{
    bool printerIsOn;
    bool printerHasPaper;

    CheckPrinterState( printerIsOn, printerHasPaper );

    try
    {
        PrintDocument( printerIsOn, printerHasPaper );
    }
    catch( const PrinterOffline& ex )
    {
        cout << ex.what() << endl;
    }
    catch( const PrinterOutOfPaper& ex )
    {
        cout << ex.what() << endl;
    }

    cout << endl;
    cout << "GOODBYE" << endl;

    return 0;
}
