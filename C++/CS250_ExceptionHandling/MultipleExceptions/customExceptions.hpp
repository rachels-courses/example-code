#ifndef _CUSTOM_EXCEPTIONS
#define _CUSTOM_EXCEPTIONS

#include <stdexcept>
using namespace std;

class PrinterOffline : public runtime_error
{
    public:
    PrinterOffline( string what ) : runtime_error( "Printer offline: " + what ) { ; }
};

class PrinterOutOfPaper : public runtime_error
{
    public:
    PrinterOutOfPaper( string what ) : runtime_error( "Printer out of paper: " + what ) { ; }
};


#endif
