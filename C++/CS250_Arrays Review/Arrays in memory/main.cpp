#include <iostream>
using namespace std;

int main()
{
    cout << "The size of a int is:      " << sizeof( int )      << " bytes." << endl;
    cout << "The size of a float is:    " << sizeof( float )    << " bytes." << endl;
    cout << "The size of a double is:   " << sizeof( double )   << " bytes." << endl;
    cout << endl;

    const int SIZE = 10;

    int intArr[SIZE];
    float floatArr[SIZE];
    double doubleArr[SIZE];

    cout << "Size of a int array with "     << SIZE << " elements: " << sizeof( intArr ) << " bytes." << endl;
    cout << "Size of a float array with "   << SIZE << " elements: " << sizeof( floatArr ) << " bytes." << endl;
    cout << "Size of a double array with "  << SIZE << " elements: " << sizeof( doubleArr ) << " bytes." << endl;
    cout << endl;

    cout << "Memory addresses for the int array:" << endl;
    for ( int i = 0; i < SIZE; i++ )
    {
        cout << "Element " << i << ": " << &(intArr[i]) << endl;
    }
    cout << endl;

    cout << "Memory addresses for the float array:" << endl;
    for ( int i = 0; i < SIZE; i++ )
    {
        cout << "Element " << i << ": " << &(floatArr[i]) << endl;
    }
    cout << endl;

    cout << "Memory addresses for the double array:" << endl;
    for ( int i = 0; i < SIZE; i++ )
    {
        cout << "Element " << i << ": " << &(doubleArr[i]) << endl;
    }
    cout << endl;

    return 0;
}
