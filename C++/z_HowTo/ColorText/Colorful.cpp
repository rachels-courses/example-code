#include "Colorful.h"
#include <iostream>
using namespace std;

vector<int> Colorful::foregrounds = {
	30, 31, 32, 33, 34, 35, 36, 37, 90, 91, 92, 93, 94, 95, 96, 97
};

vector<int> Colorful::backgrounds = {
	40, 41, 42, 43, 44, 45, 46, 47, 100, 101, 102, 103, 104, 105, 106, 107
};

void Colorful::SetStyle(int bgIndex, int fgIndex)
{
	if (fgIndex < 0 || fgIndex >= foregrounds.size())
	{
		cout << "INVALID FG COLOR INDEX. MUST BE BETWEEN 0 AND " << foregrounds.size() - 1 << endl;
	}
	if (bgIndex < 0 || bgIndex >= backgrounds.size())
	{
		cout << "INVALID BG COLOR INDEX. MUST BE BETWEEN 0 AND " << foregrounds.size() - 1 << endl;
	}

	cout << "\033[3;" << foregrounds[fgIndex] << ";" << backgrounds[bgIndex] << "m";
}

void Colorful::Reset()
{
	cout << "\033[0m";
}