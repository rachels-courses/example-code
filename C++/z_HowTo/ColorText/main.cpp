#include <iostream>
#include <vector>
using namespace std;

#include "Colorful.h"

int main()
{
	for (int fg = 0; fg < Colorful::foregrounds.size(); fg++)
	{
		for (int bg = 0; bg < Colorful::backgrounds.size(); bg++)
		{
			Colorful::SetStyle(bg, fg);
			cout << "BG: " << bg << ", FG: " << fg;
			Colorful::Reset();
			cout << endl;
		}
		cout << endl;
	}

	Colorful::Reset();

	cout << "GOODBYE!" << endl;

	return 0;
}