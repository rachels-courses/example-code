#ifndef _COLORFUL
#define _COLORFUL

#include <vector>
using namespace std;

class Colorful
{
public:
	static void SetStyle(int bgIndex, int fgIndex);
	static void Reset();

	static vector<int> foregrounds;
	static vector<int> backgrounds;
};

/*
FROM https://stackoverflow.com/questions/4053837/colorizing-text-in-the-console-with-c

Name            FG  BG
Black           30  40
Red             31  41
Green           32  42
Yellow          33  43
Blue            34  44
Magenta         35  45
Cyan            36  46
White           37  47
Bright Black    90  100
Bright Red      91  101
Bright Green    92  102
Bright Yellow   93  103
Bright Blue     94  104
Bright Magenta  95  105
Bright Cyan     96  106
Bright White    97  107
*/
#endif
