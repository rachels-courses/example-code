#include <iostream>
#include <string>
#include <map>
using namespace std;

#include "CsvParser.hpp"
#include "Helper.hpp"
#include "Customer.hpp"

void LoadData( map<int, Customer>& customers );
void SaveData( map<int, Customer>& customers );

int main()
{
    map<int, Customer> customers;

    LoadData( customers );

    // Create customer
    Customer newCustomer;
    cout << "New customer id: ";
    cin >> newCustomer.id;

    cout << "New customer name: ";
    cin.ignore();
    getline( cin, newCustomer.name );

    cout << "New customer email: ";
    getline( cin, newCustomer.email );

    // Add customer to map
    customers[ newCustomer.id ] = newCustomer;

    SaveData( customers );

    return 0;
}

void LoadData( map<int, Customer>& customers )
{
    CsvDocument doc;

    try
    {
        doc = CsvParser::Parse( "Customers.csv" );
    }
    catch( ... )
    {
        // No customer file; new customer list.
        return;
    }

    // Load customer data
    Customer newCustomer;

    for ( const auto& row : doc.rows )
    {
        for ( int col = 0; col < row.size(); col++ )
        {
            // What field are we looking at? Convert to the correct data type in our object.
            if      ( doc.header[col] == "id" )     { newCustomer.id = Helper::StringToInt( row[col] ); }
            else if ( doc.header[col] == "name" )   { newCustomer.name = row[col]; }
            else if ( doc.header[col] == "email" )  { newCustomer.email = row[col]; }
        }

        // Save customer to the map
        customers[ newCustomer.id ] = newCustomer;
    }

    cout << customers.size() << " customers loaded" << endl;
}

void SaveData( map<int, Customer>& customers )
{
    CsvDocument doc;
    // Specify what the fields are in the document
    doc.header.push_back( "id" );
    doc.header.push_back( "name" );
    doc.header.push_back( "email" );

    for ( const auto& customer : customers )
    {
        // Create a list of cells for one customer row entry
        vector<string> cells;
        cells.push_back( Helper::ToString( customer.second.id ) );  // id
        cells.push_back( customer.second.name );                    // name
        cells.push_back( customer.second.email );                   // email
        doc.rows.push_back( cells );
    }

    CsvParser::Save( "Customers.csv", doc );
}

