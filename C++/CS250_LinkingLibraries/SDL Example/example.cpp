#include <iostream>
using namespace std;

#include <SDL.h>

int main( int argc, char* args[] )
{
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    SDL_Init( SDL_INIT_EVERYTHING );
    window = SDL_CreateWindow
    (
        "Example SDL Program",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        640,
        480,
        SDL_WINDOW_SHOWN
    );

    renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );

    SDL_Rect exampleRect;
    exampleRect.x = 100;
    exampleRect.y = 50;
    exampleRect.w = 300;
    exampleRect.h = 350;

    bool done = false;
    while ( !done )
    {
        SDL_Event e;
        while( SDL_PollEvent( &e ) != 0 )
        {
            if( e.type == SDL_QUIT )
            {
                done = true;
            }
        }

        SDL_SetRenderDrawColor( renderer, 200, 200, 255, 255 );
        SDL_RenderClear( renderer );

        SDL_SetRenderDrawColor( renderer, 100, 100, 255, 255 );
        SDL_RenderFillRect( renderer, &exampleRect );

        SDL_RenderPresent( renderer );
    }

    SDL_DestroyRenderer( renderer );
    SDL_DestroyWindow( window );
    SDL_Quit();

    return 0;
}
