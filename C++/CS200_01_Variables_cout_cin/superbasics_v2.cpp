#include <iostream>
#include <string>
using namespace std;

int main()
{
  int my_integer = 10;
  float my_float = 9.99;
  char my_char = '$';
  bool my_bool = true;
  string my_text = "Hello!";
  
  cout << "my_integer: " << my_integer << endl;
  cout << "my_float:   " << my_float << endl;
  cout << "my_char:    " << my_char << endl;
  cout << "my_bool:    " << my_bool << endl;
  cout << "my_text:    " << my_text << endl;
  
  cout << endl;
  
  cout << "Enter an integer: ";
  cin >> my_integer;
  
  cout << "Enter a float: ";
  cin >> my_float;
  
  cout << "Enter a char: ";
  cin >> my_char;
  
  cout << "Enter a bool (0 = false, 1 = true): ";
  cin >> my_bool;
  
  cin.ignore(); // Flush buffer
  
  cout << "Enter some text: ";
  getline( cin, my_text );
  
  cout << endl;
  cout << "UPDATED VALUES:" << endl;
  
  cout << "my_integer: " << my_integer << endl;
  cout << "my_float:   " << my_float << endl;
  cout << "my_char:    " << my_char << endl;
  cout << "my_bool:    " << my_bool << endl;
  cout << "my_text:    " << my_text << endl;
  
  return 0;
}
