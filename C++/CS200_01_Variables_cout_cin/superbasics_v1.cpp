#include <iostream>
#include <string>
using namespace std;

int main()
{
  int my_integer = 10;
  float my_float = 9.99;
  char my_char = '$';
  bool my_bool = true;
  string my_text = "Hello!";
  
  cout << "my_integer: " << my_integer << endl;
  cout << "my_float:   " << my_float << endl;
  cout << "my_char:    " << my_char << endl;
  cout << "my_bool:    " << my_bool << endl;
  cout << "my_text:    " << my_text << endl;
  
  return 0;
}
