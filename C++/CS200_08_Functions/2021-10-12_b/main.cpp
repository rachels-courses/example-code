#include "functions.h"

#include <iostream>
using namespace std;

int main()
{
    Test_TotalPrice();

    cout << endl;

    const int ARRAY_SIZE = 5;

    // Parallel Arrays
    float ticketPrices[ARRAY_SIZE];
    ticketPrices[0] = 3.99;
    ticketPrices[1] = 5.99;
    ticketPrices[2] = 7.99;
    ticketPrices[3] = 9.99;
    ticketPrices[4] = 11.99;

    string ticketPriceTimes[ARRAY_SIZE];
    ticketPriceTimes[0] = "Early morning";
    ticketPriceTimes[1] = "Lunchtime";
    ticketPriceTimes[2] = "Afternoon";
    ticketPriceTimes[3] = "Evening";
    ticketPriceTimes[4] = "Midnight premier";

    DisplayTicketPrices( ARRAY_SIZE, ticketPrices, ticketPriceTimes );


    string moviesShowing[ARRAY_SIZE];
    moviesShowing[0] = "The Lost Skeleton of Cadavra";
    moviesShowing[1] = "The Room";
    moviesShowing[2] = "Dark and Stormy Night";
    moviesShowing[3] = "Arrival";
    moviesShowing[4] = "The Matrix";




    return 0;
}
