#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <string>
using namespace std;

float TotalPrice( float ticketPrice, int ticketAmount );

void Test_TotalPrice();

void DisplayTicketPrices( int arrSize, const float ticketPrices[], const string ticketPriceTimes[] );

#endif
