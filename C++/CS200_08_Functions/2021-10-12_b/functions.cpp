#include "functions.h"

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

float TotalPrice( float ticketPrice, int ticketAmount )
{
    return ticketPrice * ticketAmount;
}

void Test_TotalPrice()
{
    float input1_tp;
    float input2_ta;
    float expectedOutput;
    float actualOutput;

    cout << endl << "TEST 1" << endl;
    input1_tp = 2.00;
    input2_ta = 3;
    expectedOutput = 6.00;
    actualOutput = TotalPrice( input1_tp, input2_ta );

    cout << "* Input 1: " << input1_tp << endl;
    cout << "* Input 2: " << input2_ta << endl;
    cout << "* Expected: " << expectedOutput << endl;
    cout << "* Actual: " << actualOutput << endl;

    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }


    cout << endl << "TEST 2" << endl;
    input1_tp = 10.00;
    input2_ta = 4;
    expectedOutput = 40.00;
    actualOutput = TotalPrice( input1_tp, input2_ta );

    cout << "* Input 1: " << input1_tp << endl;
    cout << "* Input 2: " << input2_ta << endl;
    cout << "* Expected: " << expectedOutput << endl;
    cout << "* Actual: " << actualOutput << endl;

    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
}

// Variables passed by value
// Arrays are passed by reference
void DisplayTicketPrices( int arrSize, const float ticketPrices[], const string ticketPriceTimes[] )
//void DisplayTicketPrices()
{
    for ( int i = 0; i < arrSize; i++ )
    {
        cout << left
            << setw( 3 ) << i
            << setw( 10 ) << ticketPrices[i]
            << setw( 30 ) << ticketPriceTimes[i]
            << endl;
    }
}
