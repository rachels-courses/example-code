// Function declarations

#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <string>
using namespace std;

// Return type (output): void
// Name: DisplayMenu
// Parameter list (inputs): empty ( )
void DisplayMenu();                     // Function Declaration

// Return type (output): int
// Name: GetChoice
// Parameter list (inputs): int min, and int max
int GetChoice( int min, int max );      // Function Declaration

int FavoriteNumber();

float Perimeter( float width, float length );

string FormatName( string first, string last );

void DisplayMoney( float money );

#endif


