// Function definitions

#include "functions.hpp"

#include <iostream>
using namespace std;

void DisplayMoney( float money )
{
    cout << "$" << money << endl;
}

string FormatName( string first, string last )
{
    return last + ", " + first;
}

float Perimeter( float width, float length )
{
    return 2 * width + 2 * length;
}

int FavoriteNumber()
{
    return 144;
}

void DisplayMenu()          // Function Definition
{
    cout << endl;
    cout << "---- main menu ----" << endl;
    cout << "1. Favorite number" << endl;
    cout << "2. Perimeter" << endl;
    cout << "3. Format name" << endl;
    cout << "4. Format money" << endl;
    cout << "5. Exit" << endl;
}

int GetChoice( int min, int max )
{
    int choice;
    cout << ">> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Out of range, try again" << endl;
        cin >> choice;
    }

    // At this point, the choice is valid
    return choice;
}
