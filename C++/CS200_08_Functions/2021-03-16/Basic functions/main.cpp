#include <iostream>
using namespace std;

#include "functions.hpp"

int main()
{
    bool done = false;
    while ( !done )
    {
        DisplayMenu();                      // Function Call

        int choice = GetChoice( 1, 5 );     // Function Call, returns an int

        switch( choice )
        {
            case 1:
            cout << FavoriteNumber() << endl;
            break;

            case 2: {
                float myWidth, myLength;

                cout << "Enter a width and length: ";
                cin >> myWidth >> myLength;

                float perimeter = Perimeter( myWidth, myLength );

                cout << "Perimeter is: " << perimeter << endl;
            } break;

            case 3: {
                cout << "Customers:" << endl;
                cout << FormatName( "John", "Doe" ) << endl;
                cout << FormatName( "Jane", "Doe" ) << endl;
            } break;

            case 4: {
                cout << "Prices:" << endl;
                DisplayMoney( 9.99 );
                DisplayMoney( 2.53 );
                DisplayMoney( 0.75 );
            } break;

            case 5:
            done = true;
            break;
        }
    }

    return 0;
}

