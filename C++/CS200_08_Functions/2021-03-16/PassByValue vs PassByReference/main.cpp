#include <iostream>
using namespace std;

void Function1( int a )     // Pass by value
{
    cout << "2. The value of a is " << a << endl;
    a = 30;
    cout << "3. The value of a is " << a << endl;
}

void Function2( int& a )     // Pass by reference
{
    cout << "2. The value of a is " << a << endl;
    a = 30;
    cout << "3. The value of a is " << a << endl;
}

int main()
{
    int num = 10;
    cout << "1. The value of num is " << num << endl;
    Function2( num );   // Function call
    cout << "4. The value of num is " << num << endl;

    return 0;
}
