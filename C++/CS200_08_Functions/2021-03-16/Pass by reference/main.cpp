#include <iostream>
#include <cmath>
using namespace std;

//float QuadraticX1( float a, float b, float c )
//{
//    float inner = sqrt( b*b - 4*a*c );
//    float numerator = -b + inner;
//    float result = numerator / 2 * a;
//    return result;
//}
//
//float QuadraticX2( float a, float b, float c )
//{
//    float inner = sqrt( b*b - 4*a*c );
//    float numerator = -b - inner;
//    float result = numerator / 2 * a;
//    return result;
//}

void Quadratic( float a, float b, float c, float& x1, float& x2 )
{
    float inner = sqrt( b*b - 4*a*c );
    float numerator = -b - inner;
    float result = numerator / 2 * a;
    x1 = result;

    inner = sqrt( b*b - 4*a*c );
    numerator = -b + inner;
    result = numerator / 2 * a;
    x2 = result;
}

int main()
{
    float a, b, c;
    a = 1;
    b = 1;
    c = -6;

    float x1;
    float x2;

    Quadratic( a, b, c, x1, x2 );

    cout << "x1 = " << x1 << endl;
    cout << "x2 = " << x2 << endl;

    return 0;
}
