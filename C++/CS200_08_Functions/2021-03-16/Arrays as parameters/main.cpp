#include <iostream>
#include <cstdlib>      // random
using namespace std;

void Value( int a )                 // Pass by value
{
    a = 30;
}

void Reference( int& a )            // Pass by reference
{
    a = 15;
}

void Init( int arr[1000] )          // Arrays AREN'T PASSED BY VALUE.
                                    // You can modify arrays
{
    for ( int i = 0; i < 1000; i++ )
    {
        arr[i] = rand() % 100;
    }
}

void Display( const int arr[1000] )
{
    for ( int i = 0; i < 1000; i++ )
    {
        cout << i << " = " << arr[i] << endl;
    }
}

int main()
{
    int num;
    Value( num );           // Does not change num
    Reference( num );       // Changes num

    int myArray[1000];
    Init( myArray );        // Calling, passing in array
    Display( myArray );

    return 0;
}
