// always use file guards
#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

// FUNCTION DECLARATIONS
int Sum( int a, int b );

float GetTax();

void DisplayMoney( float cash );

void DisplayMenu();

#endif
