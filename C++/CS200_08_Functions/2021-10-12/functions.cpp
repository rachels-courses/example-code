#include "functions.h" // always

#include <iostream>
using namespace std;

// FUNCTION DEFINITIONS

// HAS OUTPUT (int)
// HAS INPUTS (a, b)
int Sum( int a, int b )
{
    int result = a + b;
    return result;
}


// HAS OUTPUT ()
// NO INPUTS
float GetTax()
{
    return 0.09;
}


// NO OUTPUT
// HAS INPUTS ()
void DisplayMoney( float cash )
{
    cout << "$" << cash << endl;
}


// NO OUTPUT
// NO INPUTS
void DisplayMenu()
{
    cout << "1. Change movie ticket prices" << endl;
    cout << "2. Change movies showing" << endl;
    cout << "3. Change auditorium seating" << endl;
}
