#ifndef _FUNCTIONS
#define _FUNCTIONS

void PassByValue( int number );

void PassByReference( int& number );

#endif