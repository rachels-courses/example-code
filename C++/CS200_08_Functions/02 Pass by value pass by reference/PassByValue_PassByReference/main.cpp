#include <iostream>
#include <iomanip>
using namespace std;

#include "Functions.h"

int main() {
  int myNumber = 5;
  cout << left;
  
  cout << setw( 25 ) << "(main)" << "1. myNumber before calling PassByValue = " << myNumber << endl;

  PassByValue( myNumber );

  cout << setw( 25 ) << "(main)" << "4. myNumber after calling PassByValue = " << myNumber << endl;
  
  PassByReference( myNumber );

  cout << setw( 25 ) << "(main)" << "7. myNumber after calling PassByReference = " << myNumber << endl;
  
  
  return 0;
}	