#ifndef _FUNCTIONS
#define _FUNCTIONS

#include "Book.h"

int GetChoice( int min, int max );

void DisplayMainMenu();

void DisplayBook( Book book ); // Copies "book"

void SetupBook1( Book& book1 ); // Uses reference &

void SetupBook2( Book& book2 ); // Uses reference &

#endif
