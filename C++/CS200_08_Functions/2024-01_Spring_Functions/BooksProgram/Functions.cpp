#include "Functions.h"

#include <iostream>
using namespace std;

int GetChoice( int min, int max )
{  
  int choice;
  cout << ">> ";
  cin >> choice;

  // Is invalid input?
  while ( choice < min || choice > max )
    {
      cout << "Invalid selection! Try again!" << endl;
      cout << ">> ";
      cin >> choice;
    }

  return choice;
}

void DisplayMainMenu()
{
  cout << endl;
  cout << string( 80, '-' ) << endl;
  cout << "MAIN MENU" << endl;
  cout << "0. Quit" << endl;
  cout << "1. Display book 1" << endl;
  cout << "2. Display book 2" << endl;
}

void DisplayBook( Book book ) // Copy
{
  cout << "TITLE:  " << book.title << endl;
  cout << "AUTHOR: " << book.author << endl;
  cout << "ISBN:   " << book.isbn << endl;
  cout << "PRICE: $" << book.price << endl;
}

void SetupBook1( Book& book1 ) // Reference (direct access)
{
  book1.title = "The Very Hungry Caterpillar";
  book1.author = "Eric Carle";
  book1.isbn = "0399256733";
  book1.price = 27.18;
  book1.checked_out = false;
}

void SetupBook2( Book& book2 )
{
  book2.title = "No, David!";
  book2.author = "David Shannon";
  book2.isbn = "1338299581";
  book2.price = 7.62;
  book2.checked_out = false;
}
