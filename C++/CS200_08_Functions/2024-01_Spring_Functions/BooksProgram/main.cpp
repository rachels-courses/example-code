#include "Book.h"
#include "Functions.h"

#include <iostream>
using namespace std;

int main()
{
  Book book1;
  SetupBook1( book1 );
	
  Book book2;
  SetupBook2( book2 );
  
  bool running = true;
  while ( running )
    {
      // Main menu
      DisplayMainMenu();

      int choice = GetChoice( 0, 2 );
      
      if ( choice == 0 )
	{
	  running = false;
	}
      else if ( choice == 1 )
	{
	  DisplayBook( book1 );
	}
      else if ( choice == 2 )
	{
	  DisplayBook( book2 );
	}
      else
	{
	  cout << "Invalid selection!" << endl;
	}
    }
	
  cout << endl;
  cout << "GOODBYE" << endl;
	
  return 0;
}
