#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct CoordPair
{
    float x;
    float y;

    void Setup( CoordPair copyMe )
    {
        x = copyMe.x;
        y = copyMe.y;
    }

    void Setup( float x, float y )
    {
        this->x = x;
        this->y = y;
    }

    void Display()
    {
        cout << "(" << x << ", " << y << ")" << endl;
    }
};

float GetSlope( CoordPair p1, CoordPair p2 )
{
    return ( p2.y - p1.y ) / ( p2.x - p1.x );
}

int main()
{
    CoordPair beginPoint;
    beginPoint.Setup( 2, 5 );
    cout << "Begin point: ";
    beginPoint.Display();

    CoordPair endPoint;
    endPoint.Setup( beginPoint );
    endPoint.y += 1;
    endPoint.x += 3;
    cout << "End point: ";
    endPoint.Display();

    float slope = GetSlope( beginPoint, endPoint );
    cout << "Slope: " << slope << endl;

    return 0;
}
