#include <tuple>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;

struct Book
{
    string name;
    string author;
};

int main()
{
    cout << endl << "STRUCT BOOK LIST" << endl;

    vector<Book> bookList;

    Book book1;
    book1.name = "The Color of Magic";
    book1.author = "Terry Pratchett";
    bookList.push_back( book1 );

    Book book2;
    book2.name = "This Is How You Lose The Time War";
    book2.author = "Amal El-Mohtar";
    bookList.push_back( book2 );

    for ( auto book : bookList )
    {
        cout << left << setw( 50 ) << book.name << setw( 20 ) << book.author << endl;
    }

    cout << endl << "TUPLE BOOK LIST" << endl;

    auto book3 = make_tuple( "The Art of Game Design", "Jesse Schell" );
    auto book4 = make_tuple( "Six of Crows", "Leigh Bardugo" );

    vector<tuple<string, string>> secondBookList;
    secondBookList.push_back( book3 );
    secondBookList.push_back( book4 );

    for ( auto book : secondBookList )
    {
        cout << left << setw( 50 ) << get<0>(book) << setw( 20 ) << get<1>(book) << endl;
    }


    return 0;
}
