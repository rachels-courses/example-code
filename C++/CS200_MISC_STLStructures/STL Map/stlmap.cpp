#include <map>
#include <string>
#include <iostream>
using namespace std;

struct Employee
{
    string name;
    float pay;
};

int main()
{
    map<int, Employee> employees;

    employees[1579].name = "Rai";
    employees[1579].pay = 15.99;

    employees[7569].name = "Anuj";
    employees[7569].pay = 20.99;

    employees[4253].name = "Rachel";
    employees[4253].pay = 100.99;

    cout << employees.size() << " employees" << endl;

    for ( auto employee : employees )
    {
        cout << "Employee ID # " << employee.first << endl;
        cout << "Name: " << employee.second.name << endl;
        cout << "Pay: $" << employee.second.pay << endl;
        cout << endl;
    }


    return 0;
}
