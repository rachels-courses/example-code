#ifndef _PRODUCT // File guards
#define _PRODUCT

#include <string>
using namespace std;

struct Product
{
	string name;
	string state_of_origin;
	float sell_price;
	float production_cost;
};

#endif