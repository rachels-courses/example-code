#ifndef _FUNCTIONS // File guards
#define _FUNCTIONS

#include <vector>
#include <array>
using namespace std;

#include "Product.h"

void DisplayMenu();
void CreateBasicProducts(vector<Product>& products);
int GetChoice(int min, int max);

void AddProduct(vector<Product>& products);
void DisplayProducts(const vector<Product>& products);
void DisplayStates(const array<string, 55>& states);

#endif