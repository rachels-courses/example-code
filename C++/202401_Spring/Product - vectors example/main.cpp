#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <array>
using namespace std;

#include "Product.h"
#include "Functions.h"

int main()
{
	cout << left << setprecision(2) << fixed;
	array<string, 55> states;
	vector<Product> products;

	// Initializing the data:
	CreateBasicProducts(products);

	ifstream input;
	input.open("state-list.txt");

	for (int i = 0; i < states.size(); i++)
	{
		getline(input, states[i]);
	}

	bool running = true;

	while (running)
	{
		cout << string(80, '-') << endl;
		cout << products.size() << " total products" << endl;
		cout << states.size() << " total states" << endl;
		DisplayMenu();
		int choice = GetChoice(0, 3);

		cout << endl << endl;
		switch (choice)
		{
		case 0: running = false; break;
		case 1: AddProduct(products); break;
		case 2: DisplayProducts(products); break;
		case 3: DisplayStates(states); break;
		}
		cout << endl << endl;
	}


	return 0;
}