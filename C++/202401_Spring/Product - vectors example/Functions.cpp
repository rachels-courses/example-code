#include "Functions.h"

#include "Colors.h"

#include <iostream>
#include <iomanip>
using namespace std;

void DisplayMenu()
{
	cout << GREEN_ON_BLACK;
	cout << "0. QUIT" << endl;
	cout << "1. Add product" << endl;
	cout << "2. View all products" << endl;
	cout << "3. View all states" << endl;
}

int GetChoice(int min, int max)
{
	int choice;
	cout << ">> ";
	cin >> choice;

	while (choice < min || choice > max)
	{
		cout << "Invalid, must be between " << min << " and " << max << endl;
		cout << ">> ";
		cin >> choice;
	}

	return choice;
}

void CreateBasicProducts(vector<Product>& products)
{
	Product p1, p2, p3;

	p1.name = "Bread";
	p1.production_cost = 0.75;
	p1.sell_price = 3;
	p1.state_of_origin = "KS";
	products.push_back(p1);

	p2.name = "Phone";
	p2.production_cost = 100;
	p2.sell_price = 1200;
	p2.state_of_origin = "KS";
	products.push_back(p2);

	p3.name = "Cereal";
	p3.production_cost = 0.50;
	p3.sell_price = 4.50;
	p3.state_of_origin = "KS";
	products.push_back(p3);
}

void AddProduct(vector<Product>& products)
{
	cout << "ADD PRODUCT MENU" << endl << string(80, '*') << endl;
	cin.ignore();

	// ADD NEW ITEM
	Product new_product;
	cout << "ADD NEW ITEM" << endl;

	cout << "Enter product name: ";
	getline(cin, new_product.name);

	cout << "Enter state of origin: ";
	getline(cin, new_product.state_of_origin);

	cout << "Enter sell price: $";
	cin >> new_product.sell_price;
	cout << "Enter production cost: $";
	cin >> new_product.production_cost;

	products.push_back(new_product);

}

void DisplayProducts(const vector<Product>& products)
{
	cout << "DISPLAY PRODUCTS MENU" << endl << string(80, '*') << endl;

	cout << "Search term (or ENTER for none): ";
	string search;
	cin.ignore();
	getline(cin, search);

	// DISPLAY ITEMS
	cout << setw(5) << "ID"
		<< setw(20) << "NAME"
		<< setw(20) << "STATE"
		<< setw(10) << "COST"
		<< setw(10) << "PRICE" 
		<< setw(10) << "PROFIT"
		<< endl;
	cout << string(80, '-') << endl;
	for (int i = 0; i < products.size(); i++)
	{
		if (products[i].name.find(search) != string::npos)
		{
			cout << YELLOW_ON_BLACK;
		}
		else
		{
			cout << RED_ON_BLACK;
		}
		cout << setw(5) << i
			<< setw(20) << products[i].name
			<< setw(20) << products[i].state_of_origin
			<< setw(10) << products[i].production_cost
			<< setw(10) << products[i].sell_price
			<< setw(10) << (products[i].sell_price - products[i].production_cost)
			<< endl;

		cout << GREEN_ON_BLACK;
	}

}

void DisplayStates(const array<string, 55>& states)
{
	cout << "DISPLAY STATES MENU" << endl << string(80, '*') << endl;

	for (int i = 0; i < states.size(); i++)
	{
		cout << i + 1 << "\t" << states[i] << endl;
	}
}
