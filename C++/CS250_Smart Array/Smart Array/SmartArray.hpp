#ifndef _SMART_ARRAY_HPP
#define _SMART_ARRAY_HPP

#include <stdexcept>

template <typename T>
class SmartArray
{
public:
    SmartArray();

    void Add( T value );
    void Remove( int index );
    void Remove( T value );
    int Search( T value );
    T Get( int index );
    void Display();
    void Clear();
    int Size();

private:
    T m_arr[10];
    int m_itemCount;
    const int ARRAY_SIZE;
};

template <typename T>
SmartArray<T>::SmartArray()
    : ARRAY_SIZE( 10 )
{
    m_itemCount = 0;
}

template <typename T>
void SmartArray<T>::Add( T value )
{
    if ( m_itemCount == ARRAY_SIZE )
    {
        // The array is full; can't take any more items!
        throw runtime_error( "Array is full!" );
    }
    else if ( value == T() )
    {
        // Don't allow user to insert empty items!
        throw logic_error( "Can't add empty stuff to array!" );
    }

    m_arr[m_itemCount] = value;
    m_itemCount++;
}

template <typename T>
void SmartArray<T>::Remove( int index )
{
    if ( index < 0 || index >= ARRAY_SIZE )
    {
        throw range_error( "Index is out of bounds!" );
    }

    for ( int i = index; i < m_itemCount - 1; i++ )
    {
        m_arr[i] = m_arr[i+1];
    }
    m_itemCount--;
}

template <typename T>
void SmartArray<T>::Remove( T value )
{
    int index = Search( value );
    Remove( index );
}

template <typename T>
int SmartArray<T>::Search( T value )
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( m_arr[i] == value )
        {
            return i;
        }
    }

    throw runtime_error( "Item not found in array!" );
}

template <typename T>
T SmartArray<T>::Get( int index )
{
    if ( index < 0 || index >= ARRAY_SIZE )
    {
        throw range_error( "Index is out of bounds!" );
    }

    return m_arr[ index ];
}

template <typename T>
void SmartArray<T>::Display()
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        cout << i << " = " << m_arr[i] << endl;
    }
}

template <typename T>
void SmartArray<T>::Clear()
{
    m_itemCount = 0;
}

template <typename T>
int SmartArray<T>::Size()
{
    return m_itemCount;
}


#endif
