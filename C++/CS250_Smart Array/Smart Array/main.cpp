#include <iostream>
using namespace std;

#include "SmartArray.hpp"

int main()
{
    SmartArray<string> studentList;
    studentList.Add( "Nick" );
    studentList.Add( "Abhi" );
    studentList.Add( "Mike" );
    studentList.Add( "Julie" );
    studentList.Add( "Rai" );
    studentList.Add( "Asha" );

    bool done = false;
    while ( !done )
    {
        cout << endl << "------------------------" << endl;
        cout << "1. Add item" << endl;
        cout << "2. Get size" << endl;
        cout << "3. Display array" << endl;
        cout << "4. Search" << endl;
        cout << "5. Get" << endl;
        cout << "6. Clear" << endl;
        cout << "7. Remove Index" << endl;
        cout << "8. Remove Name" << endl;

        int choice;
        cin >> choice;

        if ( choice == 1 )
        {
            try
            {
                cout << "Enter a student name: ";
                string name;

                cin.ignore();
                getline( cin, name );
                studentList.Add( name );
            }
            catch ( const runtime_error& ex )
            {
                cout << "ERROR: " << ex.what() << endl;
            }
            catch ( const logic_error& ex )
            {
                cout << "ERROR: " << ex.what() << endl;
            }
        }
        else if ( choice == 2 )
        {
            cout << "We have " << studentList.Size()
                << " items stored in the array" << endl;
        }
        else if ( choice == 3 )
        {
            studentList.Display();
        }
        else if ( choice == 4 )
        {
            cout << "Enter a student name: ";
            string name;
            cin.ignore();
            getline( cin, name );

            try
            {
                int index = studentList.Search( name );
                cout << "Found at " << index << endl;
            }
            catch( const runtime_error& ex )
            {
                cout << "ERROR: " << ex.what() << endl;
            }
        }
        else if ( choice == 5 )
        {
            cout << "Enter an index: ";
            int index;
            cin >> index;

            try
            {
                string value = studentList.Get( index );
                cout << "Student at index " << index
                    << " is " << value << endl;
            }
            catch( const range_error& ex )
            {
                cout << "ERROR: " << ex.what() << endl;
            }
        }
        else if ( choice == 6 )
        {
            studentList.Clear();
        }
        else if ( choice == 7 )
        {
            cout << "Enter an index: ";
            int index;
            cin >> index;

            try
            {
                studentList.Remove( index );
            }
            catch( const range_error& ex )
            {
                cout << "ERROR: " << ex.what() << endl;
            }
        }
        else if ( choice == 8 )
        {
            cout << "Enter a student name: ";
            string name;
            cin.ignore();
            getline( cin, name );

            try
            {
                studentList.Remove( name );
            }
            catch( const runtime_error& ex )
            {
                cout << "ERROR: " << ex.what() << endl;
            }
        }
    }


    return 0;
}
