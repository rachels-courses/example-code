#include "GameProgram.h"

#include <cstdlib>
#include <ctime>

int main()
{
    srand( time( NULL ) );

    GameProgram gameProgram;
    gameProgram.Run();

    return 0;
}
