#include "GameProgram.h"

#include "Utilities/Menu.hpp"

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

void GameProgram::Run()
{
    Setup();
    Menu_Main();
    cout << " *** THANK YOU FOR CHOOSING LUDO GAMES! ***" << endl;
}

void GameProgram::Setup()
{
    m_player.Setup( "HERO", 100, 15, 10, 5, 5, 10, 15 );
    m_enemies.push_back( Character( "Ogre",     200, 20, 10, 5, 5, 10, 10 ) );
    m_enemies.push_back( Character( "Chicken",  10, 5, 5, 5, 5, 5, 5 ) );
}


// --------------------------------------------------------------------- //
// ------------------------------- MENUS ------------------------------- //
// --------------------------------------------------------------------- //

void GameProgram::Menu_Main()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Character editor",
            "Battle!"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Menu_CharacterCreator(); }
        else if ( choice == 2 ) { Menu_Battle(); }
    }
}

void GameProgram::Menu_CharacterCreator()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "CHARACTER EDITOR" );

        DisplayCharacter( "YOUR HERO", m_player );

        cout << endl;

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Set name",
            "Set stats",
            "Set equipment"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Menu_CC_SetName(); }
        else if ( choice == 2 ) { Menu_CC_SetStats(); }
        else if ( choice == 3 ) { Menu_CC_SetEquipment(); }
    }
}


void GameProgram::Menu_CC_SetName()
{
    Menu::Header( "CHARACTER EDITOR > SET NAME" );
    cout << " What shall thy name be? ";
    cin.ignore();
    getline( cin, m_player.name );
}

void GameProgram::Menu_CC_SetStats()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "CHARACTER EDITOR > SET STATS" );
        DisplayCharacter( "YOUR HERO", m_player );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Edit STRENGTH",
            "Edit DEFENSE"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 )
        {
            cout << "Enter new STRENGTH rating: ";
            cin >> m_player.str;
        }
        else if ( choice == 2 )
        {
            cout << "Enter new DEFENSE rating: ";
            cin >> m_player.def;
        }
    }
}

void GameProgram::Menu_CC_SetEquipment()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "CHARACTER EDITOR > SET EQUIPMENT" );
        DisplayCharacter( "YOUR HERO", m_player );

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Edit HIT RATE",
            "Edit MIN DAMAGE",
            "Edit MAX DAMAGE",
            "Edit ARMOR CLASS"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 )
        {
            cout << "Enter new HIT RATE: ";
            cin >> m_player.hitRate;
        }
        else if ( choice == 2 )
        {
            cout << "Enter new MIN DAMAGE: ";
            cin >> m_player.minDmg;
        }
        else if ( choice == 2 )
        {
            cout << "Enter new MAX DAMAGE: ";
            cin >> m_player.maxDmg;
        }
        else if ( choice == 2 )
        {
            cout << "Enter new ARMOR CLASS: ";
            cin >> m_player.ac;
        }
    }
}






void GameProgram::Menu_Battle()
{
    Menu::Header( "BATTLE!" );
    int randomEnemy = rand() % m_enemies.size();

    // Make copies so I don't have to reset them after battle
    Character player = m_player;
    Character enemy = m_enemies[randomEnemy];

    int round = 1;
    bool done = false;
    while ( !done )
    {
        Menu::DrawHorizontalBar( 80, '-' );
        Menu::DrawHorizontalBar( 80, '-' );
        cout << " ROUND " << round << endl;

        DisplayCharacter( "YOUR FOE", enemy );
        DisplayCharacter( "YOUR HERO", player);

        int playerChoice = Menu::ShowIntMenuWithPrompt( {
            "Attack",
            "Defend",
            "Heal"
        } );

        int enemyChoice = rand() % 3 + 1;

        // Are they defending?
        int playerDefenseBonus = 0;
        int enemyDefenseBonus = 0;

        if ( playerChoice == 2 )
        {
            playerDefenseBonus = GetDefenseBonus( player.name );
        }

        if ( enemyChoice == 2 )
        {
            enemyDefenseBonus = GetDefenseBonus( enemy.name );
        }

        // Or perhaps healing?
        if ( playerChoice == 3 )
        {
            Heal( player );
        }

        if ( enemyChoice == 3 )
        {
            Heal( enemy );
        }

        // Now deal with attacks
        if ( playerChoice == 1 )
        {
            Attack( player, enemy, enemyDefenseBonus );
        }

        if ( enemyChoice == 1 )
        {
            Attack( enemy, player, playerDefenseBonus );
        }

        round++;

        if ( player.hp <= 0 || enemy.hp <= 0 )
        {
            done = true;
        }

        cout << endl << " END OF ROUND! Press enter to continue..." << endl;
        cin.ignore();
        cin.get();
    }

    if ( player.hp <= 0 )
    {
        cout << " !! " << player.name << " IS KNOCKED OUT !! " << endl;
    }

    if ( enemy.hp <= 0 )
    {
        cout << " !! " << enemy.name << " IS KNOCKED OUT !! " << endl;
    }
}



// --------------------------------------------------------------------- //
// ------------------------------ HELPERS ------------------------------ //
// --------------------------------------------------------------------- //

void GameProgram::DisplayCharacter( string title, Character character )
{
    Menu::DrawHorizontalBar( 80, '~' );
    cout << "~ " << title << ":" << endl;
    character.Display();
    Menu::DrawHorizontalBar( 80, '~' );
    cout << endl;
}

int GameProgram::GetDefenseBonus( string name )
{
    cout << " !! " << name << " is defending..." << endl;
    return rand() % 5 + 1;
}

void GameProgram::Heal( Character& character )
{
    cout << " !! " << character.name << " is healing..." << endl;
    int healAmount = rand() % character.def + 5;
    cout << " They heal for " << healAmount << "!" << endl;
    character.Heal( healAmount );
}

void GameProgram::Attack( const Character& attacker, Character& defender, int defenderBonus )
{
    cout << " !! " << attacker.name << " is attacking " << defender.name << "..." << endl;

    int d20Roll = rand() % 20 + 1;

    int hitScore = d20Roll + attacker.hitRate;

    if ( hitScore >= defender.ac )
    {
        // Hit!
        int damage = attacker.GetDamage() - defenderBonus;

        if ( damage <= 0 )
        {
            cout << " The attack does no damage!" << endl;
        }
        else
        {
            cout << " " << defender.name << " takes " << damage << " damage!" << endl;
            defender.TakeDamage( damage );
        }
    }
    else
    {
        // Miss!
        cout << " They miss! " << defender.name << " is unscathed!" << endl;
    }
}




