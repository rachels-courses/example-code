#include "CsvParser.hpp"

#include <iostream>
using namespace std;

//struct CsvDocument
//{
//    vector<string> header;
//    vector< vector< string > > rows;
//};

CsvDocument CsvParser::Parse( string filepath )
{
    ifstream input( filepath );
    if ( input.fail() )
    {
        cout << "ERROR: Couldn't open \"" << filepath << "\"!" << endl;
        throw runtime_error( "File not found!" );
    }

    CsvDocument doc;

    // First row should be header
    string strHeader;
    getline( input, strHeader );

    // Split up by comma
    doc.header = StringUtil::Split( strHeader, "," );

    // Get remaining data
    string row;
    while ( getline( input, row ) )
    {
        doc.rows.push_back( StringUtil::CsvSplit( row, ',' ) );
    }

    input.close();

    return doc;
}

void CsvParser::Save( string filepath, const CsvDocument& doc )
{
    ofstream output( filepath );

    // Output header
    bool first = true;
    for ( const auto& colHead : doc.header )
    {
        if ( first ) { first = false; }
        else { output << ","; }
        output << colHead;
    }
    output << endl;

    // Output data
    for ( const auto& row : doc.rows )
    {
        bool first = true;
        for ( const auto& cell : row )
        {
            if ( first ) { first = false; }
            else { output << ","; }
            output << cell;
        }
        output << endl;
    }
}
