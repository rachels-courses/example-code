#include "Tester.hpp"

#include "../Objects/Auditorium.hpp"
#include "../Objects/Movie.hpp"
#include "../Objects/Showing.hpp"
#include "../Objects/Timeslot.hpp"

#include <iomanip>
using namespace std;

void Tester::Run()
{
    m_log.open( "test_result.txt" );

    Test_Timeslot();
    Test_Movie();
    Test_Auditorium();
    Test_Showing();

    m_log.close();
}

void Tester::Test_Timeslot()
{
    m_log << string( 80, '-' ) << endl << "Test_Timeslot" << endl;
    int totalTests = 0, totalPass = 0;
    const int W = 50;

    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": SetName... ";
        Timeslot item;
        item.SetName( "BOB" );
        string expectedOutput = "BOB";
        string actualOutput = item.m_name;

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": SetName... ";
        Timeslot item;
        item.SetName( "cat" );
        string expectedOutput = "cat";
        string actualOutput = item.m_name;

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": GetName... ";
        Timeslot item;
        item.m_name = "BOB";
        string expectedOutput = "BOB";
        string actualOutput = item.GetName();

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": GetName... ";
        Timeslot item;
        item.m_name = "cat";
        string expectedOutput = "cat";
        string actualOutput = item.GetName();

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": SetTicketPrice... ";
        Timeslot item;
        item.SetTicketPrice( 1.00 );
        float expectedOutput = 1.00;
        float actualOutput = item.m_ticketPrice;

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": SetTicketPrice... ";
        Timeslot item;
        item.SetTicketPrice( 9.99 );
        float expectedOutput = 9.99;
        float actualOutput = item.m_ticketPrice;

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": GetTicketPrice... ";
        Timeslot item;
        item.m_ticketPrice = 2.00;
        float expectedOutput = 2.00;
        float actualOutput = item.GetTicketPrice();

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": GetTicketPrice... ";
        Timeslot item;
        item.m_ticketPrice = 5.99;
        float expectedOutput = 5.99;
        float actualOutput = item.GetTicketPrice();

        if ( actualOutput == expectedOutput )   { m_log << "PASS"; totalPass++; }
        else                                    { m_log << "FAIL"; }

        m_log   << endl << "Expected output:    " << expectedOutput
                << endl << "Actual output:      " << actualOutput << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": Setup... ";
        Timeslot item;
        item.Setup( "AAA", 1.11 );
        string expectedOutput1 = "AAA";
        string actualOutput1 = item.m_name;
        float expectedOutput2 = 1.11;
        float actualOutput2 = item.m_ticketPrice;

        if      ( actualOutput1 != expectedOutput1 )    { m_log << "FAIL"; }
        else if ( actualOutput2 != expectedOutput2 )    { m_log << "FAIL"; }
        else                                            { m_log << "PASS"; totalPass++; }

        m_log   << endl << "Expected output 1:  " << expectedOutput1
                << endl << "Actual output 1:    " << actualOutput1
                << endl << "Expected output 2:  " << expectedOutput2
                << endl << "Actual output 2:    " << actualOutput2
                << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": Setup... ";
        Timeslot item;
        item.Setup( "BBB", 2.22 );
        string expectedOutput1 = "BBB";
        string actualOutput1 = item.m_name;
        float expectedOutput2 = 2.22;
        float actualOutput2 = item.m_ticketPrice;

        if      ( actualOutput1 != expectedOutput1 )    { m_log << "FAIL"; }
        else if ( actualOutput2 != expectedOutput2 )    { m_log << "FAIL"; }
        else                                            { m_log << "PASS"; totalPass++; }

        m_log   << endl << "Expected output 1:  " << expectedOutput1
                << endl << "Actual output 1:    " << actualOutput1
                << endl << "Expected output 2:  " << expectedOutput2
                << endl << "Actual output 2:    " << actualOutput2
                << endl << endl;
    }
    { /**************************************************************/
        m_log << left << setw(5) << "TEST " << setw(5) << ++totalTests << setw(W) << ": GetFieldNames... ";
        Timeslot item;
        vector<string> fields = item.GetFieldNames();
        string expectedOutput1 = "name";
        string actualOutput1 = fields[0];
        string expectedOutput2 = "ticketPrice";
        string actualOutput2 = fields[1];

        if      ( actualOutput1 != expectedOutput1 )    { m_log << "FAIL"; }
        else if ( actualOutput2 != expectedOutput2 )    { m_log << "FAIL"; }
        else                                            { m_log << "PASS"; totalPass++; }

        m_log   << endl << "Expected output 1:  " << expectedOutput1
                << endl << "Actual output 1:    " << actualOutput1
                << endl << "Expected output 2:  " << expectedOutput2
                << endl << "Actual output 2:    " << actualOutput2
                << endl << endl;
    }
}

void Tester::Test_Movie()
{
    m_log << string( 80, '-' ) << endl << "Test_Movie" << endl;
    int totalTests = 0, totalPass = 0;
    // Later?
}

void Tester::Test_Auditorium()
{
    m_log << string( 80, '-' ) << endl << "Test_Auditorium" << endl;
    int totalTests = 0, totalPass = 0;
    // Later?
}

void Tester::Test_Showing()
{
    m_log << string( 80, '-' ) << endl << "Test_Showing" << endl;
    int totalTests = 0, totalPass = 0;
    // Later?
}
