#include "Movie.hpp"

#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Menu.hpp"

#include <iostream>
using namespace std;

Movie::Movie()
{
    m_title = "UNSET";
    m_rating = "UNSET";
}

Movie::Movie( string title, string rating )
{
    Setup( title, rating );
}

void Movie::Setup( string title, string rating )
{
    SetTitle( title );
    SetRating( rating );
}

void Movie::SetTitle( string title )
{
    m_title = title;
}

void Movie::SetRating( string rating )
{
    m_rating = rating;
}

string Movie::GetTitle() const
{
    return m_title;
}

string Movie::GetRating() const
{
    return m_rating;
}

vector<string> Movie::GetFieldNames() const
{
    return { "title", "rating" };
}

vector<string> Movie::ToCsvCells() const
{
    vector<string> cells;
    cells.push_back( m_title );
    cells.push_back( m_rating );
    return cells;
}

ostream& operator<<( ostream& out, const Movie& item )
{
    out << item.m_title << " (" << item.m_rating << ")";
    return out;
}

istream& operator>>( istream& in, Movie& item )
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Edit title",
        "Edit rating"
    } );

    if ( choice == 1 )
    {
        cout << " New title: ";
        in.ignore();
        string title;
        getline( in, title );
        item.SetTitle( title );
    }
    else if ( choice == 2 )
    {
        cout << " New rating: ";
        string rating;
        in >> rating;
        item.SetRating( rating );
    }

    return in;
}
