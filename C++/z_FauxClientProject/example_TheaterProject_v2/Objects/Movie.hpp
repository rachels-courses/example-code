#ifndef _MOVIE_H
#define _MOVIE_H

#include <string>
#include <vector>
using namespace std;

class Movie
{
public:
    Movie();
    Movie( string title, string rating );

    void Setup( string title, string rating );

    void SetTitle( string title );
    void SetRating( string rating );

    string GetTitle() const;
    string GetRating() const;

    vector<string> GetFieldNames() const;
    vector<string> ToCsvCells() const;
    friend ostream& operator<<( ostream& out, const Movie& item );
    friend istream& operator>>( istream& in, Movie& item );

private:
    string m_title;
    string m_rating;

    friend class Tester;
};

#endif
