#include "TheaterProgram.h"

#include "Utilities/Menu.hpp"
#include "Utilities/Logger.hpp"
#include "Utilities/CsvParser.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

TheaterProgram::TheaterProgram() :
    PATH_TIMESLOTS( "data/timeslots.csv" ),
    PATH_MOVIES( "data/movies.csv" ),
    PATH_AUDITORIUMS( "data/auditoriums.csv" ),
    PATH_SHOWINGS( "data/showings.csv" )
{
    Logger::Setup( false );
    Logger::Out( "Function begin", "TheaterProgram::TheaterProgram" );
}

TheaterProgram::~TheaterProgram()
{
    Logger::Cleanup();
}

void TheaterProgram::Run()
{
    Logger::Out( "Function begin", "TheaterProgram::Run" );

    Load();
    Menu_Login();
    Save();
    cout << " *** THANK YOU FOR CHOOSING KINEJO THEATERS! ***" << endl;
}

void TheaterProgram::Save()
{
    Logger::Out( "Function begin", "TheaterProgram::Save" );
    SaveData( m_timeslots,      PATH_TIMESLOTS );
    SaveData( m_movies,         PATH_MOVIES );
    SaveData( m_auditoriums,    PATH_AUDITORIUMS );
    Save_Showings();
}

void TheaterProgram::Load()
{
    Logger::Out( "Function begin", "TheaterProgram::Load" );

    // Does the data exist?
    ifstream input( PATH_TIMESLOTS );
    if ( input.fail() )
    {
        // Couldn't find file; set defaults instead.
        SetDefaults();
        return;
    }
    input.close();

    // Load from CSV Files
    Load_Timeslots();
    Load_Movies();
    Load_Auditoriums();
    Load_Showings();
}

void TheaterProgram::SetDefaults()
{
    Logger::Out( "Function begin", "TheaterProgram::SetDefaults" );

    m_movies.push_back(     Movie( "Aliens",                "R" ) );    // 0
    m_movies.push_back(     Movie( "Atanarjuat",            "R" ) );    // 1
    m_movies.push_back(     Movie( "Chennai Express",       "NR" ) );   // 2
    m_movies.push_back(     Movie( "Digimon The Movie",     "PG" ) );   // 3
    m_movies.push_back(     Movie( "Fantasia",              "G" ) );    // 4
    m_movies.push_back(     Movie( "The Giant Spider",      "NR" ) );   // 5
    m_movies.push_back(     Movie( "Moonlight",             "R" ) );    // 6
    m_movies.push_back(     Movie( "Parasite",              "R" ) );    // 7
    m_movies.push_back(     Movie( "Sailor Moon S",         "NR" ) );   // 8
    m_movies.push_back(     Movie( "Toy Story",             "G" ) );    // 9

    m_timeslots.push_back( Timeslot( "1 Morning",     3.99 ) );
    m_timeslots.push_back( Timeslot( "2 Lunchtime",   5.99 ) );
    m_timeslots.push_back( Timeslot( "3 Afternoon",   7.99 ) );
    m_timeslots.push_back( Timeslot( "4 Evening",     9.99 ) );
    m_timeslots.push_back( Timeslot( "5 Midnight",    15.99 ) );

    m_auditoriums.push_back( Auditorium( "Audi 1", 10 ) );
    m_auditoriums.push_back( Auditorium( "Audi 2", 10 ) );
    m_auditoriums.push_back( Auditorium( "Audi 3", 15 ) );
    m_auditoriums.push_back( Auditorium( "Audi 4", 15 ) );
    m_auditoriums.push_back( Auditorium( "Audi 5", 20 ) );

    // [audi][time]
    m_showings[0][0].Setup( 3, m_auditoriums[0].GetCapacity() );
    m_showings[0][1].Setup( 5, m_auditoriums[0].GetCapacity() );
    m_showings[0][2].Setup( 1, m_auditoriums[0].GetCapacity() );
    m_showings[0][3].Setup( 0, m_auditoriums[0].GetCapacity() );
    m_showings[0][4].Setup( 1, m_auditoriums[0].GetCapacity() );

    m_showings[1][0].Setup( 4, m_auditoriums[1].GetCapacity() );
    m_showings[1][1].Setup( 0, m_auditoriums[1].GetCapacity() );
    m_showings[1][2].Setup( 5, m_auditoriums[1].GetCapacity() );
    m_showings[1][3].Setup( 2, m_auditoriums[1].GetCapacity() );
    m_showings[1][4].Setup( 6, m_auditoriums[1].GetCapacity() );

    m_showings[2][0].Setup( 8, m_auditoriums[2].GetCapacity() );
    m_showings[2][1].Setup( 0, m_auditoriums[2].GetCapacity() );
    m_showings[2][2].Setup( 1, m_auditoriums[2].GetCapacity() );
    m_showings[2][3].Setup( 5, m_auditoriums[2].GetCapacity() );
    m_showings[2][4].Setup( 7, m_auditoriums[2].GetCapacity() );

    m_showings[3][0].Setup( 9, m_auditoriums[3].GetCapacity() );
    m_showings[3][1].Setup( 0, m_auditoriums[3].GetCapacity() );
    m_showings[3][2].Setup( 1, m_auditoriums[3].GetCapacity() );
    m_showings[3][3].Setup( 9, m_auditoriums[3].GetCapacity() );
    m_showings[3][4].Setup( 0, m_auditoriums[3].GetCapacity() );

    m_showings[4][0].Setup( 4, m_auditoriums[4].GetCapacity() );
    m_showings[4][1].Setup( 3, m_auditoriums[4].GetCapacity() );
    m_showings[4][2].Setup( 4, m_auditoriums[4].GetCapacity() );
    m_showings[4][3].Setup( 7, m_auditoriums[4].GetCapacity() );
    m_showings[4][4].Setup( 8, m_auditoriums[4].GetCapacity() );
}

// --------------------------------------------------------------------- //
// ------------------------------- MENUS ------------------------------- //
// --------------------------------------------------------------------- //

void TheaterProgram::Menu_Login()
{
    Logger::Out( "Function begin", "TheaterProgram::Menu_Login" );

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LOGIN" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Admin menu",
            "Customer kiosk"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Menu_AdminMain(); }
        else if ( choice == 2 ) { Menu_Customer(); }
    }
}

void TheaterProgram::Menu_AdminMain()
{
    Logger::Out( "Function begin", "TheaterProgram::Menu_AdminMain" );

    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Set ticket prices",
            "Set auditorium seats",
            "Set movie showings",
            "Set schedule",
            "Show all data"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Menu_Admin_SetData( m_timeslots, "TICKETS" ); }
        else if ( choice == 2 ) { Menu_Admin_SetData( m_auditoriums, "AUDITORIUMS" ); }
        else if ( choice == 3 ) { Menu_Admin_SetData( m_movies, "MOVIES" ); }
        else if ( choice == 4 ) { Menu_Admin_SetSchedule(); }
        else if ( choice == 5 ) { Menu_Admin_ViewAll(); }
    }
}

void TheaterProgram::Menu_Admin_SetSchedule()
{
    Logger::Out( "Function begin", "TheaterProgram::Menu_Admin_SetSchedule" );

    bool done = false;
    while ( !done )
    {
        DisplayShowings();

        int audiIndex = Menu::GetValidChoice( 0, 5, "Enter auditorium #: " ) - 1;
        if ( audiIndex == -1 ) { return; }

        int timeIndex = Menu::GetValidChoice( 0, 5, "Enter time slot #: " ) - 1;
        if ( timeIndex == -1 ) { return; }

        cout
            << "MOVIE:  " << m_movies[ m_showings[audiIndex][timeIndex].movieIndex ] << endl
            << "AUDI:   " << m_auditoriums[audiIndex] << "" << endl
            << "TIME:   " << m_timeslots[timeIndex] << "" << endl << endl;

        cout << "Is this what you want to edit? (Y/N): ";
        char choice;
        cin >> choice;

        if ( tolower( choice ) == 'n' ) { cout << endl << endl; continue; }

        cout << endl << "Select which movie to play:" << endl;
        int movieIndex = Menu::ShowIntMenuWithPrompt( VecToString( m_movies ), true ) - 1;

        m_showings[audiIndex][timeIndex].movieIndex = movieIndex;
        m_showings[audiIndex][timeIndex].availableSeats = m_auditoriums[audiIndex].GetCapacity();

        cout << endl << "Schedule updated." << endl;
    }
}

void TheaterProgram::Menu_Admin_ViewAll()
{
    Logger::Out( "Function begin", "TheaterProgram::Menu_Admin_ViewAll" );

    Menu::Header( "ADMIN > VIEW ALL" );

    cout << "MOVIES" << endl;
    DisplayNumberedList( m_movies, true );

    Menu::DrawHorizontalBar( 80, '*' );

    cout << "AUDITORIUMS" << endl;
    DisplayNumberedList( m_auditoriums, true );

    Menu::DrawHorizontalBar( 80, '*' );

    cout << "TICKETS" << endl;
    DisplayNumberedList( m_timeslots, true );

    Menu::DrawHorizontalBar( 80, '*' );

    cout << "SCHEDULE" << endl;
    DisplayShowings();
}

void TheaterProgram::Menu_Customer()
{
    Logger::Out( "Function begin", "TheaterProgram::Menu_Customer" );

    // Later?
}



// --------------------------------------------------------------------- //
// ------------------------------ HELPERS ------------------------------ //
// --------------------------------------------------------------------- //

void TheaterProgram::DisplayShowings()
{
    const int COLW = 80 / 6;

    // HEADER
    cout << left << setw( COLW ) << "";
    for ( int t = 0; t < 5; t++ )
    {
        cout << left << setw( COLW ) << m_timeslots[t].GetName();
    }
    cout << endl << string( 80, '-' ) << endl;

    // BODY
    for ( int a = 0; a < 5; a++ )
    {
        cout << left << setw( COLW ) << m_auditoriums[a].GetName();

        for ( int t = 0; t < 5; t++ )
        {
            cout << left << setw( COLW ) << Shorten( m_movies[ m_showings[a][t].movieIndex ].GetTitle(), COLW-1 );
        }

        cout << endl;
    }
}

string TheaterProgram::Shorten( string movie, size_t length )
{
  if ( movie.size() > length )
  {
    string shorter = movie.substr( 0, length - 3 );
    shorter += "...";
    return shorter;
  }
  return movie;
}

int TheaterProgram::GetMovieIndex( string title )
{
    for ( unsigned int i = 0; i < m_movies.size(); i++ )
    {
        if ( m_movies[i].GetTitle() == title )
        {
            return i;
        }
    }
    return -1;
}

void TheaterProgram::Load_Timeslots()
{
    Logger::Out( "Function begin", "TheaterProgram::Load_Timeslots" );

    CsvDocument doc = CsvParser::Parse( PATH_TIMESLOTS );

    Timeslot build;
    for ( const auto& row : doc.rows )
    {
        string name;
        float ticketPrice;

        for ( unsigned int i = 0; i < row.size(); i++ )
        {
            if      ( doc.header[i] == "name" )         { name = row[i]; }
            else if ( doc.header[i] == "ticketPrice" )  { ticketPrice = StringUtil::StringToFloat( row[i] ); }
        }

        build.Setup( name, ticketPrice );
        m_timeslots.push_back( build );
    }

    Logger::Out( StringUtil::ToString( m_timeslots.size() ) + " records loaded", "TheaterProgram::Load_Timeslots" );
}

void TheaterProgram::Load_Movies()
{
    Logger::Out( "Function begin", "TheaterProgram::Load_Movies" );

    CsvDocument doc = CsvParser::Parse( PATH_MOVIES );

    Movie build;
    for ( const auto& row : doc.rows )
    {
        string title;
        string rating;

        for ( unsigned int i = 0; i < row.size(); i++ )
        {
            if      ( doc.header[i] == "title" )    { title = row[i]; }
            else if ( doc.header[i] == "rating" )   { rating = row[i]; }
        }

        build.Setup( title, rating );
        m_movies.push_back( build );
    }

    Logger::Out( StringUtil::ToString( m_movies.size() ) + " records loaded", "TheaterProgram::Load_Movies" );
}

void TheaterProgram::Load_Auditoriums()
{
    Logger::Out( "Function begin", "TheaterProgram::Load_Auditoriums" );

    CsvDocument doc = CsvParser::Parse( PATH_AUDITORIUMS );

    Auditorium build;
    for ( const auto& row : doc.rows )
    {
        string name;
        int capacity;

        for ( unsigned int i = 0; i < row.size(); i++ )
        {
            Logger::Out( "Header: " + doc.header[i] + ", value: " + row[i], "TheaterProgram::Load_Auditoriums" );
            if      ( doc.header[i] == "name" )         { name = row[i]; }
            else if ( doc.header[i] == "capacity" )     { capacity = StringUtil::StringToInt( row[i] ); }
        }

        build.Setup( name, capacity );
        m_auditoriums.push_back( build );
    }

    Logger::Out( StringUtil::ToString( m_auditoriums.size() ) + " records loaded", "TheaterProgram::Load_Auditoriums" );
}

void TheaterProgram::Load_Showings()
{
    Logger::Out( "Function begin", "TheaterProgram::Load_Showings" );

    CsvDocument doc = CsvParser::Parse( PATH_SHOWINGS );

    for ( const auto& row : doc.rows )
    {
        int audiIndex;
        int timeIndex;
        int movieIndex;

        for ( unsigned int i = 0; i < row.size(); i++ )
        {
            if      ( doc.header[i] == "audiIndex" )  { audiIndex = StringUtil::StringToInt( row[i] ); }
            else if ( doc.header[i] == "timeIndex" )  { timeIndex = StringUtil::StringToInt( row[i] ); }
            else if ( doc.header[i] == "movieIndex" ) { movieIndex = StringUtil::StringToInt( row[i] ); }
        }

        Logger::Out( "Saving data for showing with audiIndex=" + StringUtil::ToString( audiIndex )
                    + " and timeIndex=" + StringUtil::ToString( timeIndex )
                    + " and movieIndex=" + StringUtil::ToString( movieIndex )
                    , "TheaterProgram::Load_Showings" );

        m_showings[audiIndex][timeIndex].movieIndex = movieIndex;
        m_showings[audiIndex][timeIndex].availableSeats = m_auditoriums[audiIndex].GetCapacity();
    }
}


void TheaterProgram::Save_Showings()
{
    Logger::Out( "Function begin", "TheaterProgram::Save_Showings" );

    CsvDocument doc;
    doc.header.push_back( "audiIndex" );
    doc.header.push_back( "timeIndex" );
    doc.header.push_back( "movieIndex" );

    for ( int a = 0; a < 5; a++ )
    {
        for ( int t = 0; t < 5; t++ )
        {
            vector<string> cells;
            cells.push_back( StringUtil::ToString( a ) );                             // audiIndex
            cells.push_back( StringUtil::ToString( t ) );                             // timeIndex
            cells.push_back( StringUtil::ToString( m_showings[a][t].movieIndex ) );   // movieIndex

            doc.rows.push_back( cells );
        }
    }

    CsvParser::Save( PATH_SHOWINGS, doc );
}
