#include <iostream>
#include <iomanip>
#include <string>
#include <array>
using namespace std;

#include "Functions.h"

int main()
{
    array<string,4> movieTitles;
    array<int,4>    movieSeats;
    float           ticketPrice;

    cout << fixed << setprecision( 2 );

    // Call function
    InitializeData( movieTitles, movieSeats, ticketPrice );

    // Program loop
    bool done = false;
    while ( !done )
    {
        // Main menu
        cout << string( 80, '#' ) << endl << "MAIN MENU" << endl << string( 80, '#' ) << endl << endl;
        cout << "1. View movies" << endl;
        cout << "2. Purchase tickets" << endl;
        cout << "3. Quit" << endl;
        cout << ">> ";

        int choice;
        cin >> choice;

        switch( choice )
        {
        case 1:
            ViewMovies( movieTitles, movieSeats, ticketPrice );
            break;

        case 2:
            PurchaseTickets( movieTitles, movieSeats, ticketPrice );
            break;

        case 3:
            done = true;
            break;

        default:
            cout << "Invalid selection!" << endl;
        }

        cout << endl << endl;
    }

    cout << endl << "Goodbye" << endl;

    return 0;
}
