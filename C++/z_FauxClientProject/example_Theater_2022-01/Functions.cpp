#include "Functions.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <array>
using namespace std;

void InitializeData( array<string,4>& movieTitles, array<int,4>& movieSeats, float& ticketPrice )
{
    movieTitles[0] = "Terminator 2";
    movieSeats[0] = 10;

    movieTitles[1] = "My Neighbor Totoro";
    movieSeats[1] = 15;

    movieTitles[2] = "Back to the Future";
    movieSeats[2] = 20;

    movieTitles[3] = "The Matrix";
    movieSeats[3] = 25;

    ticketPrice = 9.99;
}

void ViewMovies( array<string,4> movieTitles, array<int,4> movieSeats, float ticketPrice )
{
        cout << endl << endl << string( 80, '#' ) << endl << "VIEW MOVIES" << endl << string( 80, '#' ) << endl << endl;
    cout << left
        << setw( 5 ) << "#"
        << setw( 20 ) << "TITLE"
        << setw( 10 ) << "SEATS"
        << setw( 10 ) << "PRICE"
        << endl << string( 80, '-' ) << endl;

    for ( size_t i = 0; i < movieTitles.size(); i++ )
    {
        cout << left
            << setw( 5 ) << i
            << setw( 20 ) << movieTitles[i]
            << setw( 10 ) << movieSeats[i]
            << setw( 10 ) << ticketPrice << endl;
    }
}

void PurchaseTickets( array<string,4> movieTitles, array<int,4>& movieSeats, float ticketPrice )
{
    cout << endl << endl << string( 80, '#' ) << endl << "PURCHASE TICKETS" << endl << string( 80, '#' ) << endl << endl;

    ViewMovies( movieTitles, movieSeats, ticketPrice );
    int index;
    cout << "Which movie? ";
    cin >> index;

    cout << "Selected movie " << index << "..." << endl;

    // Add more code
}
