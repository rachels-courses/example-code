#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <string>
#include <array>
using namespace std;

void InitializeData( array<string,4>& movieTitles, array<int,4>& movieSeats, float& ticketPrice );

void ViewMovies( array<string,4> movieTitles, array<int,4> movieSeats, float ticketPrice );

void PurchaseTickets( array<string,4> movieTitles, array<int,4>& movieSeats, float ticketPrice );

#endif
