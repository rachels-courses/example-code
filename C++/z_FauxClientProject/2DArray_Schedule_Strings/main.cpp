#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
using namespace std;

void Pause();
void ClearScreen();
string Shorten( string movie, size_t length );
void DisplayArray( string arr[], int length );

void DisplayShowings( string showings[5][5], string showingLabels[5], string audiLabels[5] );
void Save( string schedule[5][5], string showingLabels[5], string audiLabels[5] );
void Load( string schedule[5][5], string showingLabels[5], string audiLabels[5] );

int main()
{
    const int MAX_SHOWINGS = 5;
    string showingLabels[MAX_SHOWINGS] = {
        "Morning",
        "Lunchtime",
        "Afternoon",
        "Evening",
        "Midnight"
    };

    const int MAX_AUDIS = 5;
    string audiLabels[MAX_AUDIS] = {
        "Audi 1",
        "Audi 2",
        "Audi 3",
        "Audi 4",
        "Audi 5"
    };

    string schedule[MAX_SHOWINGS][MAX_AUDIS];

    // Initialize
    for ( int a = 0; a < MAX_AUDIS; a++ )
    {
        for ( int s = 0; s < MAX_SHOWINGS; s++ )
        {
            schedule[s][a] = "UNSET";
        }
    }

    // Load before program loop runs
    Load( schedule, showingLabels, audiLabels );

    bool done = false;
    while ( !done )
    {
        ClearScreen();

        DisplayShowings( schedule, showingLabels, audiLabels );

        cout << endl;
        cout << "1. Update showing names" << endl;
        cout << "2. Update auditorium names" << endl;
        cout << "3. Update schedule" << endl;
        cout << "4. Quit" << endl;

        cout << endl << ">> ";
        int choice;
        cin >> choice;

        switch( choice )
        {
            case 1:
            {
                DisplayArray( showingLabels, 5 );

                cout << endl << "Update which #";
                int index;
                cin >> index;

                cout << "Enter new showing name: ";
                cin.ignore();
                getline( cin, showingLabels[index] );
            }
            break;

            case 2:
            {
                DisplayArray( audiLabels, 5 );

                cout << endl << "Update which #";
                int index;
                cin >> index;

                cout << "Enter new audi name: ";
                cin.ignore();
                getline( cin, audiLabels[index] );
            }
            break;

            case 3:
            {
                cout << "SCHEDULE" << endl;
                DisplayShowings( schedule, showingLabels, audiLabels );

                cout << endl << "SHOWINGS" << endl;
                DisplayArray( showingLabels, 5 );
                cout << endl << "Update which showing #";
                int showingIndex;
                cin >> showingIndex;

                cout << endl << "AUDIS" << endl;
                DisplayArray( audiLabels, 5 );
                cout << endl << "Update which audi #";
                int audiIndex;
                cin >> audiIndex;

                cout << endl << "YOU ARE GOING TO UPDATE: " << schedule[showingIndex][audiIndex] << endl;
                cout << endl << "ENTER A NEW MOVIE: ";
                cin.ignore();
                getline( cin, schedule[showingIndex][audiIndex] );
            }
            break;

            case 4:
                done = true;
            break;

            default:
            cout << "Invalid selection!" << endl;
        }
        Pause();
    }

    // Save before quitting
    Save( schedule, showingLabels, audiLabels );

    return 0;
}

/*
          MORNING       LUNCHTIME     AFTERNOON     EVENING       MIDNIGHT
-----------------------------------------------------------------------------
AUDI 1    DIGIMON MO    GIANT SPID    ATANARJUAT    ALIENS        ATANARJUAT
AUDI 2    FANTASIA      ALIENS        GIANT SPID    CHENNAI EX    MOONLIGHT
AUDI 3    SAILOR MOO    CHENNAI EX    MOONLIGHT     GIANT SPID    PARASITE
AUDI 4    TOY STORY     ALIENS        ATANARJUAT    TOY STORY     ALIENS
AUDI 5    FANTASIA      DIGIMON MO    FANTASIA      PARASITE      SAILOR MOO
*/
void DisplayShowings( string schedule[5][5], string showingLabels[5], string audiLabels[5] )
{
    const int COLW = 80 / 6;

    // Draw heading
    cout << left << setw( COLW ) << ""
        << setw( COLW ) << showingLabels[0]         // This can be replaced with another loop
        << setw( COLW ) << showingLabels[1]         // I just thought this would illustrate what
        << setw( COLW ) << showingLabels[2]         // we are doing more clearly.
        << setw( COLW ) << showingLabels[3]
        << setw( COLW ) << showingLabels[4]
        << endl << string( 80, '-' ) << endl;

    // Each row: Draw audi name and then each showing at the given times
    for ( int r = 0; r < 5; r++ )
    {
        cout << left
            << setw( COLW ) << audiLabels[r]
            << setw( COLW ) << Shorten( schedule[0][r], COLW-1 )
            << setw( COLW ) << Shorten( schedule[1][r], COLW-1 )
            << setw( COLW ) << Shorten( schedule[2][r], COLW-1 )
            << setw( COLW ) << Shorten( schedule[3][r], COLW-1 )
            << setw( COLW ) << Shorten( schedule[4][r], COLW-1 )
            << endl;
    }
}

void Save( string schedule[5][5], string showingLabels[5], string audiLabels[5] )
{
    // A file for the showing labels
    ofstream file_showingLabels( "showing_labels.txt" );
    for ( int i = 0; i < 5; i++ )
    {
        file_showingLabels << showingLabels[i] << endl;
    }
    file_showingLabels.close();


    // A file for the audi labels
    ofstream file_audiLabels( "audi_labels.txt" );
    for ( int i = 0; i < 5; i++ )
    {
        file_audiLabels << audiLabels[i] << endl;
    }
    file_audiLabels.close();

    // A file for the schedule
    ofstream file_schedule( "schedule.txt" );

    for ( int a = 0; a < 5; a++ )
    {
        for ( int s = 0; s < 5; s++ )
        {
            file_schedule << "SHOWING " << s << " AUDI " << a << endl << schedule[s][a] << endl;
        }
    }

    file_schedule.close();

    cout << "Files saved: showing_labels.txt, audi_labels.txt, schedule.txt" << endl;
}

void Load( string schedule[5][5], string showingLabels[5], string audiLabels[5] )
{
    // Load in showing labels
    ifstream file_showingLabels( "showing_labels.txt" );
    for ( int i = 0; i < 5; i++ )
    {
        getline( file_showingLabels, showingLabels[i] );
    }
    file_showingLabels.close();

    // Load in showing labels
    ifstream file_audiLabels( "audi_labels.txt" );
    for ( int i = 0; i < 5; i++ )
    {
        getline( file_audiLabels, audiLabels[i] );
    }
    file_audiLabels.close();

    /*
    Format:
SHOWING 0 AUDI 0
The Lost Skeleton of Cadavra
    */
    ifstream file_schedule( "schedule.txt" );
    int showingIndex, audiIndex;
    string buffer;

    while ( file_schedule >> buffer >> showingIndex >> buffer >> audiIndex )
    {
        file_schedule.ignore();
        getline( file_schedule, schedule[showingIndex][audiIndex] );
    }

    file_schedule.close();


    cout << "Files loaded: showing_labels.txt, audi_labels.txt, schedule.txt" << endl;
}


void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}


string Shorten( string movie, size_t length )
{
  if ( movie.size() > length )
  {
    string shorter = movie.substr( 0, length - 3 );
    shorter += "...";
    return shorter;
  }
  return movie;
}

void Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        cin.get();
    #endif
}

void DisplayArray( string arr[], int length )
{
    for ( int i = 0; i < length; i++ )
    {
        cout << i << ". " << arr[i] << endl;
    }
}
