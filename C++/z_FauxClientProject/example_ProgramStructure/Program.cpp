#include "Program.h"
#include "Utilities/Helper.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

void Program::Run()
{
    LoadData();
    Menu_Login();
    SaveData();
}

// MENUS
void Program::Menu_Login()
{
    bool loginMenuDone = false;

    while ( !loginMenuDone )
    {
        Helper::ClearScreen();
        Helper::Header( "LOGIN" );
        int choice = Helper::ShowMenuGetInt( { "QUIT", "CUSTOMER", "ADMIN" }, true, false );

        switch( choice )
        {
            case 0: loginMenuDone = true;   break;  // QUIT
            case 1: Menu_Customer();        break;  // CUSTOMER
            case 2: Menu_Admin();           break;  // ADMIN
        }
    }
}

void Program::Menu_Customer()
{
    // Let's say that the customer logged in is at position 0 in m_customers,
    // but if you implemented a login system, you wouldn't hard code this here.
    m_loggedInId = 0;

    bool customerMenuDone = false;
    while ( !customerMenuDone )
    {
        Helper::ClearScreen();
        Helper::Header( "CUSTOMER MAIN MENU" );

        cout << "Welcome, " << m_customers[m_loggedInId].name << "!" << endl << endl;

        int choice = Helper::ShowMenuGetInt( { "LOG OUT", "...", "..." }, true, false );

        switch( choice )
        {
            case 0: customerMenuDone = true;        break;  // LOG OUT
            case 1: /* ... */                       break;
            case 2: /* ... */                       break;
        }
    }
}

void Program::Menu_Admin()
{
    bool adminMenuDone = false;
    while ( !adminMenuDone )
    {
        Helper::ClearScreen();
        Helper::Header( "ADMIN MAIN MENU" );
        int choice = Helper::ShowMenuGetInt( { "LOG OUT", "VIEW CUSTOMERS", "..." }, true, false );

        switch( choice )
        {
            case 0: adminMenuDone = true;           break;  // LOG OUT
            case 1: Menu_Admin_ViewCustomers();     break;  // VIEW CUSTOMERS
            case 2: /* ... */                       break;
        }
    }
}

void Program::Menu_Admin_ViewCustomers()
{
    Helper::ClearScreen();
    Helper::Header( "ADMIN > VIEW CUSTOMERS" );

    // Draw header for table
    cout << left
        << setw( 10 ) << "INDEX"
        << setw( 10 ) << "CUST.ID"
        << setw( 20 ) << "CUST.NAME"
        << endl << string( 80, '-' ) << endl;

    for ( size_t i = 0; i < m_customers.size(); i++ )
    {
        cout << left
            << setw( 10 ) << i
            << setw( 10 ) << m_customers[i].id
            << setw( 20 ) << m_customers[i].name
            << endl;
    }

    Helper::DrawHorizontalBar( 80, '-' );
    cout << "Press ENTER to return" << endl;
    string blah;
    cin.ignore();
    getline( cin, blah );
}

void Program::SaveData()
{
    // ...
}

void Program::LoadData()
{
    ifstream input( "customers.txt" );
    if ( input.fail() )
    {
        // Couldn't find file
        AddStarterData();
        return;
    }
}

void Program::AddStarterData()
{
    Customer customer;

    customer.id     = 100;
    customer.name   = "Kaz Brekker";
    m_customers.push_back( customer );

    customer.id     = 105;
    customer.name   = "Inej Ghafa";
    m_customers.push_back( customer );

    customer.id     = 110;
    customer.name   = "Jesper Fahey";
    m_customers.push_back( customer );

    customer.id     = 200;
    customer.name   = "Nina Zenik";
    m_customers.push_back( customer );

    customer.id     = 250;
    customer.name   = "Matthias Helvar";
    m_customers.push_back( customer );
}
