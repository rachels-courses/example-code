#ifndef _ENUMS_H
#define _ENUMS_H

enum class OrderStatus {
    PENDING,
    COMPLETED,
    CUSTOMER_CANCELED
};

#endif
