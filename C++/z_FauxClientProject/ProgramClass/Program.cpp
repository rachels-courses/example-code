#include "Program.h"

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;

void Program::Run()
{
    LoadData();

    Menu_Main();

    SaveData();
}

void Program::Menu_Main()
{
    bool menuDone = false;
    while ( !menuDone )
    {
        Header( "MAIN MENU" );
        cout << "0. Quit" << endl;
        cout << "1. View auditoriums" << endl;
        cout << "2. Adjust showing" << endl;

        int choice = GetChoice( 0, 2 );

        switch( choice )
        {
            case 0: menuDone = true; break;
            case 1: Menu_ViewAuditoriums(); break;
            case 2: Menu_AdjustShowing(); break;
        }
    }
}

void Program::Menu_ViewAuditoriums()
{
    Header( "VIEW AUDITORIUMS" );
    DisplayAuditoriums();
    PressEnter();
}

void Program::Menu_AdjustShowing()
{
    Header( "ADJUST SHOWING" );
    DisplayAuditoriums();

    cout << endl << "Adjust which showing?" << endl;
    int audiIndex = GetChoice( 0, m_auditoriums.size()-1 );

    cout << endl;

    cout << "How many TOTAL seats are there? ---";
    cin >> m_auditoriums[audiIndex].maxSeats;

    cout << "How many seats are OPEN? ----------";
    cin >> m_auditoriums[audiIndex].availableSeats;

    cout << "What MOVIE TITLE is showing? ------";
    cin.ignore();
    getline( cin, m_auditoriums[audiIndex].moviePlaying );

    cout << endl << string( 80, '.' ) << endl;
    cout << "UPDATED:" << endl;

    DisplayAuditoriums();
    PressEnter();
}

void Program::LoadData()
{
    ifstream input( "auditoriums.txt" );

    for ( size_t i = 0; i < m_auditoriums.size(); i++ )
    {
        input >> m_auditoriums[i].maxSeats;
        input >> m_auditoriums[i].availableSeats;
        input.ignore();
        getline( input, m_auditoriums[i].moviePlaying );
    }
}

void Program::SaveData()
{
    ofstream output( "auditoriums.txt" );

    for ( size_t i = 0; i < m_auditoriums.size(); i++ )
    {
        output << m_auditoriums[i].maxSeats << endl
            << m_auditoriums[i].availableSeats << endl
            << m_auditoriums[i].moviePlaying << endl << endl;
    }
}

void Program::DisplayAuditoriums()
{
    cout << left
        << setw( 5 )  << "#"
        << setw( 30 ) << "MOVIE"
        << setw( 15 ) << "SEATS/OPEN"
        << setw( 15 ) << "SEATS/TOTAL"
        << endl << string( 80, '-' ) << endl;

    for ( size_t i = 0; i < m_auditoriums.size(); i++ )
    {
        cout
            << setw( 5 ) << i
            << setw( 30 ) << m_auditoriums[i].moviePlaying
            << setw( 15 ) << m_auditoriums[i].availableSeats
            << setw( 15 ) << m_auditoriums[i].maxSeats
            << endl;
    }
}

int Program::GetChoice( int min, int max )
{
    int choice;
    cout << "(" << min << "-" << max << "): ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "INVALID ENTRY. TRY AGAIN." << endl << endl;
        cout << "(" << min << "-" << max << "): ";
        cin >> choice;
    }

    return choice;
}

void Program::PressEnter()
{
    cout << endl << "PRESS ENTER TO CONTINUE" << endl;
    string a;
    cin.ignore();
    getline( cin, a );
}

void Program::Header( string header )
{
    cout << string( 80, '-' ) << endl;
    string head = "| " + header + " |";
    cout << " " << head << endl << " ";
    cout << string( head.size(), '-' ) << endl;
}

