#ifndef _PROGRAM
#define _PROGRAM

#include <array>
using namespace std;

#include "Auditorium.h"

class Program
{
    public:
    void Run();

    void Menu_Main();
    void Menu_ViewAuditoriums();
    void Menu_AdjustShowing();

    void LoadData();
    void SaveData();

    int GetChoice( int min, int max );
    void PressEnter();

    void Header( string header );

    void DisplayAuditoriums();

    private:
    array<Auditorium,10> m_auditoriums;
};

#endif
