#ifndef _AUDITORIUM
#define _AUDITORIUM

#include <string>
using namespace std;

struct Auditorium
{
    int maxSeats;
    int availableSeats;
    string moviePlaying;
};

#endif
