#include <SFML/Graphics.hpp>

#include <cmath>
#include <map>
#include <string>
using namespace std;

sf::Vector2f GetRandomPosition(int screenWidth, int screenHeight)
{
    sf::Vector2f pos;
    pos.x = rand() % (screenWidth - 64) + 32;
    pos.y = rand() % (screenHeight - 64) + 32;
    return pos;
}

float GetDistance(sf::Vector2f obj1, sf::Vector2f obj2)
{
    return sqrt(pow(obj1.x - obj2.x, 2) + pow(obj1.y - obj2.y, 2));
}

int main()
{
    const int SCREEN_WIDTH = 1280;
    const int SCREEN_HEIGHT = 720;

    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Example!");
    window.setFramerateLimit(60);

    map<string, sf::Texture> textures;
    textures["bunny"].loadFromFile("GameAssets/bunny.png");
    textures["diamond"].loadFromFile("GameAssets/diamond.png");
    textures["grass"].loadFromFile("GameAssets/grass.png");
    textures["origin"].loadFromFile("GameAssets/origin.png");

    sf::Font font;
    font.loadFromFile("GameAssets/PressStart2P.ttf");

    sf::Sprite player;
    player.setTexture(textures["bunny"]);
    player.setPosition(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2); // Kind of the center of the screen
    float playerSpeed = 5;
    float playerScore = 0;

    sf::Sprite item;
    item.setTexture(textures["diamond"]);
    item.setPosition(GetRandomPosition(SCREEN_WIDTH, SCREEN_HEIGHT));

    sf::Text scoreText;
    scoreText.setFont(font);
    scoreText.setCharacterSize(30);
    scoreText.setFillColor(sf::Color::White);
    scoreText.setString("Score: " + to_string(int(playerScore)));
    scoreText.setPosition(900, 5);

    sf::Text playerPosText;
    playerPosText.setFont(font);
    playerPosText.setCharacterSize(15);

    vector<sf::Sprite> groundTiles;
    sf::Sprite ground;
    ground.setTexture(textures["grass"]);
    for (int y = 0; y < SCREEN_HEIGHT; y += 32)
    {
        for (int x = 0; x < SCREEN_WIDTH; x += 32)
        {
            ground.setPosition(x, y);
            groundTiles.push_back(ground);
        }
    }

    sf::Sprite origin;
    origin.setTexture(textures["origin"]);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        sf::Vector2f playerPos = player.getPosition();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) { playerPos.x -= playerSpeed; }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) { playerPos.x += playerSpeed; }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) { playerPos.y -= playerSpeed; }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) { playerPos.y += playerSpeed; }

        player.setPosition(playerPos);
        playerPosText.setString(to_string(int(playerPos.x)) + "," + to_string(int(playerPos.y)));
        playerPosText.setPosition(playerPos);

        // Check if picked up collectable
        if (GetDistance(player.getPosition(), item.getPosition()) <= 16)
        {
            // Move the collectable
            item.setPosition(GetRandomPosition(SCREEN_WIDTH, SCREEN_HEIGHT));
            playerScore++;
            scoreText.setString("Score: " + to_string(int(playerScore)));
        }

        window.clear();
        // Draw the background tiles
        for (auto& tile : groundTiles)
        {
            window.draw(tile);
        }

        // Draw item
        window.draw(item);

        // Draw player
        window.draw(player);
        window.draw(playerPosText);

        // Draw text
        window.draw(scoreText);

        window.draw(origin);

        // Draw everything to the screen
        window.display();
    }

    return 0;
}