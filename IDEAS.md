# CS 200 level

## Variables, input/output with cin and cout
* **24 hr to 12 hr time** - Use Modulus to convert a 24-hr time code to a 12-hr time code.
* **Raise** - Calculate how much a person will get after some % raise.
* Older ideas:
    * **Recipe** - (1) Display recipe, all as text. (2) Add floats/doubles for ingredient amounts. Hard code, but concatenate to the ingredient list strings. (3) Allow user to enter a ratio, modify the ingredient list by that ratio.
    * **Song Lyrics** - Simply output a few lines of song lyrics to the screen
    * **Mad Lib** - Ask the user to input several string variables, and output a story with it.
    * **Slope** - Have the user enter x1, y1, x2, and y2, and calculate the slope from that.
    * **Distance** - Have the user enter x1, y1, x2, and y2, and calculate the distance from that. (sqrt)
    * **Quadratic Formula** - have the user enter a, b, and c, and calculate the two x values. (sqrt)
    * **Area and Perimeter** - Calculate the areas of rectangles, trianges, circles, etc.
    * **Price Plus Tax** - Have the user enter a price and a tax rate, compute the price plus tax
    * **Velocity** - Have the user enter initial velocity, time, and acceleration, and calculate velocity.
    * **Fractions** - Have user enter separate numerator1, denominator1, numerator2, denominator2, as integers. Calculate the product of the fractions, and display as a fraction (keep numerator and denominator separate).

## If/else branching
* **Fake web browser?** - Have user enter in a "URL", and present them a different (text-based) page based on what they entered.
* Older ideas:
    * **Item Prices** - Have the user enter the name of an item, return the price of that item.
    * **Tax Rates** - Have the user enter the state, return the tax rate
    * **Item Prices and Tax Rate** - Combine item/price and state/tax into one
    * **Calories** - Have the user input a food ingredient, return the calories
    * **Translation** - Have the user input a word in English, return the translation in another language
    * **Can Vote** - Ask the user their age. Display whether they can vote or not.
    * **Generation** - Ask the user the year they were born in. Display the name of their generation.
    * **Cities** - Have the user enter a city. Display whether it is in Missouri or Kansas (OR in boolean expression)
    * **Letter Grade** - Calculate letter grade from earnedPoints and totalPoints

## File input and output with ifstream and ofstream
* Output:
    * Output different types of plaintext files: HTML, CSV, CPP

* Input:
    * Data files to populate the program

* Both:
    * **Save and load data** - Use the text output as a data file so that the user can resume their work after closing and reopening the program.
    * **Randomized mad lib** - Store a series of verbs, nouns, adjectives, then generate a mad lib story randomly selecting terms from those lists. Output to a text file. (Could input a list of verbs / nouns / adjectives to make it easier.)
    * **Search utility** - Write a program to search a given file for some contents, give stats on results.
    * **File diff** - Write a program that diffs two files and gives a % of "similarities" as a result.


## Switch branching
* **Main menu** - Select options from a main menu, call other functions based on which was selected.
* **Directions** - User can select N, S, E, W (or n, s, e, w) to move between areas of a map.
* **Vending machine** - User can select a # which corresponds to a specific item in a vending machine.
* Older ideas:
    * **Calculator** - Have user enter two numbers and an operation and do a different math operation based on their input.

## Functions
* No return, no parameters
    * Display a main menu.
* No return, yes parameters
    * Format and display item/array to the screen.
    * Take in data and output a text file.
* Yes return, yes parameters
    * Math operations - Slope, area, perimeter, circumference.
    * Input validation.
    * Make updates to an array.
* Yes return, no parameters
    * Getter functions; or hard-coded data like "city tax"?
    * Loads data from a hard-coded file, returns the file or some data.

* Pass-by-reference
    * Initialize array.
    * Const pass by reference - Array display - Show how many bytes are copied over vs. the size of a reference. (Do this with a class object, perhaps).

## Pointers and memory
* Older ideas:
    * **Text Adventure** - Have a series of rooms with names and descriptions, who point to their neighbor Rooms to the north, south, east, and west.

## While loops
* Basic games:
    * **Number guesser** - Have the user guess a random secret number; tell them if they're too high or too low.
    * **Rock paper scissors** - Use random numbers to have the player play vs. the computer in Rock Paper Scissors.
* Older ideas:
    * **Input validation** - Make sure the user is inputting a valid value (between two numbers, or a certain amount of inputs), before letting the program continue executing.
    * **Keep running** - Keep the program running until the user selects "quit" from an internal menu.
    * **Simulation** - Have a simulation of some ecosystem, where supplies are used up and re-generated. Use a while loop to simulate time passing, with updates printed to the screen each time.

## Storing data with arrays, dynamic arrays, and vectors
* Really basic:
    * **Calculate average of array/vector**
    * **Find min/max in array**
* **Tic-tac-toe** - Use a 2D array to represent a tic tac toe board. Create a game that 2 humans can play. Have the program check for a winner after each turn.
* **Sudoku** - Generate a Sudoku board, have a basic interface where user can input numbers, validate entry after input.
* **Figure primes** - Given a number, figure out all prime factors and store them in a vector, then display the results.
* **Calendar / scheduler** - A basic organizer interface where you can view calendar / to do list / events. Maybe export visual calendar to a .txt file.
* **Remove duplicates** - Remove duplicate items in an array, shift everything left to cover gaps.
* Older ideas:
    * **RPG Battle** - Create an array of HPs for each player, and you have to choose who to attack. NPCs randomly choose the index of the player to attack on their turn.

## String library
* **Ebook reader** - Load in a book's text (.txt file from Gutenberg) and create an interface to read through, search terms, etc.
* Text analysis
    * **Vowel counter**
* Cipers / conversions
    * **Text <-> Morse Code converter**
    * **Text <-> Braile converter**
    * **Caesar ciphers, other ciphers**
    * **Text <-> Binary**

## For loops
* **Generate truth tables** - Nested for loops
* **Palindrome checker** - Check if a given string is a palindrome.
* **Draw 2D shapes** - I think this is an annoying assignment but it could be good to think in a grid... Fuck the "draw a triangle" thing though, that's useless, what's the use case?? Don't give students annoying assignments.
* **Cards** - Use nested for-loops to generate a deck of standard playing cards. (+Arrays)
* **Coin toss** - How many different results are there given `n` coin tosses? (Discrete math)
    * Average result of coin toss
* **Die roll** - How many different results are there given `n` die rolls? (Discrete math)
    * Average result of die roll
    * Possible sums of die roll, probability of each sum
* **Timer** - But not really a timer. Maybe. Just something to do a countdown.
* Older ideas:
    * **Calendar** - Generate months, weeks, and days of a year.
    * **Averages** - Use a for-loop to average together some data.

## Structs and classes / OOP
* Older ideas:
    * **Library** - Maintain an array of Book objects, such as adding, updating, and removing books from the library, setting books as checked-in or checked-out, etc.
    * **Course** - Maintain an array of courses, teachers, and students. A course has a teacher and an array of students, or a student could have an array of classes.

## Inheritance
* Older ideas:
    * **Quizzer** - Have an application where the user can run a quiz or create a new quiz. There is a base Question interface class, with subclasses for each type of question (True/False, Multiple Choice, Fill in the Blank, etc.)

## Recursion basics
* Older ideas:
    * **Filesystem mapper** - Write a program that recurses through all folders on the hard drive and prints out a file that lists all items on the computer.
    * **Factorial** - Calculate `n!` recursively
    * **Fibonacci** - Calculate the `n`th Fibonacci number recursively (gross)

## Searching and sorting

--------------

# Small projects
* **Go Fish** - Use arrays to track a deck, each user has their hand, implement an automated game of Go Fish. Output the steps to a text file for review.

--------------

# CS 235 level
## Exceptions with try, catch, and throw

## Templates

## Testing

## Smart pointers

## STL Structures - vector, map, list, stack, queue
* **Word counter in text doc** - Use a map to keep track of every word in a document and how many times each word shows up. (Or every character).
* **Dictionary** - Use a map to store words and their definitions. Allow user to type in word to look up.

## Operator overloading

## Default parameters

## Friends

## Static members

## Interfaces and polymorphism

## Lambda expressions / anonymous functions

## Recursion 2
* **Maze traversal** - Navigate through maze, backtrack as needed when hitting a dead end.

## Searching and sorting 2

## Linking third party libraries


--------------

# CS 250 level
## Smart fixed-length array structure

## Smart dynamic-length array structure (vector)

## Linked list

## Stacks

## Queues
* Multi-queue queuing system

## Algorithm efficiency

## Recursion 3

## Trees
* **Morse Code Converter** - Convert a string into morse code

## AVL Trees

## Heaps

## Binary Search Trees

## Hash Tables

## Searching and sorting 3


--------------

# Large projects

* UberEats style program (server / driver / customer sides)
* Online store (inventory / purchases / customer accounts / store owner accounts)
* Venmo (users / transactions / activity feed)
* Spotify (users / musicians / subscription and billing / playlists / songs)
* Event ticketing website (event / ticket / customer / schedule and calendar)


--------------

# Other ideas

## Simulations

* **Library** - Keeping a list of books available in the system, checking in and out books, patrons can have an array of books checked out at a time.
* **Train station** - Trains arriving and leaving the station, a queue of people lining up to board certain trains.
* **Online shop** - User can select items to buy, program keeps track of the array of items and the total, prints out a receipt once done.
* **Election** - Generate an array of candidates and issues (randomly generated issues that are silly are best...). Each candidate gets randomly assigned issues, and then Citizens are created with their own stance on issues. An election occurs, and each citizen votes based on closest issue agreement. Show winner. 
* **Mars Colony** - Given some amount of food grown per day, some amount of waste generated per day, and some amount of air generated per day, calculate the survival of a crew for a mission. 
* **Bank ATM** - Implement a bank ATM system where each user has an account balance and can deposit or withdraw money. Have error checking. 
* **Train ticketing system and train scheduling** - popular destinations and starting locations, times of day, etc.
* **Movie theater** - Limited theaters, seats, and have a certain amount of movies, popular times, and set up the movie showtimes.
* **Herd migration** - Due to predators, weather, season, etc.?
* **Virus/outbreak simulation**
* **Waste/recycling/space/processing management**
* **Simulation of productivity** - set the amount of time in meetings and watch employee morale and productivity go down.
* **Airplane loading / unloading optimization**
* **Food** - For a given amount of people, with a certain amount with dietary restrictions, estimate the amount of food needed, types, and the ingredients. Run simulation to make sure enough was prepared?

## Computer Utilities

* **File searcher** - Write a program to search a text file for specific words, returns information like line number and the line it was found on.
* **File system mapper** - Use a recursive function to go through all the folders in a directory and build a text file with a list of those directories.
* **Text editor or text reader** - Write a program that reads text files and displays them to the screen. Implement pagination to make it a more accessible text reader. Could implement bookmarks and such.
* **Picture filters** - Use the PPM image format to write programs that will apply "filters" to images. 
* **Blogger app** - Create a blogging system, including Users, Posts, and Likes. Save/load blog posts and user data in text files.
* **Instant Messenger** - maybe wrap some basic sockets in an easy class for students to use.
* **Searching and modifying hex files** - maybe have a program that edits an .exe's hex file, replacing certain text (like dialogue text in some games).
* **Text comparison utility** - Write a program that shows the user two programs at one time, for comparison. Also analyzes simularities.
* **Chat bot** - try to have students analyze sentences for certain things, try to generate responses.
* **Heightmaps** - Given a set of data, have students output RGB values into a bitmap / image file to generate a height map.

## Apps

* **Book reading backlog** - List a series of books that the user wants, or owns, as well as the current reading status (read / reading / not started).
* **To-Do list** - Have user keep a to-do list on their computer with a schedule, allow exporting the schedule to CSV file.
* **Recipe** - Allow user to store recipes, can type in a batch (ratio) to adjust the ingredient list.
* **Card game** - Implement a simple card game with boring-person cards (whatever the normal 52 card thing is called), or make a new card game (like MTG or Pokemon),
and write a program to implement the turns so two people can play.
* **Quizzer** - Use Polymorphism to store an array of Questions of different types (multiple choice, true-false, fill in the blank, etc.),
allow user to edit and save a question deck, and then run the quiz.
* **Calorie counter** - Create a program that stores a list of food items and their calories, then allow user to build their meals (example, tacos or sandwiches or pizza) and come up with the total calories.
* **Translation dictionary** - Keep a file of English - (other language) terms and allow the user to do searches to find translations for certain words.
* **Contact book**
* **Fridge recipes** - User enters all ingredients they have, and the program does a search for recipes that contain those ingredients.
* **Workout organizer** - Store types of workouts based on some criteria, be able to view by type,  or randomly select something.

## Games

* **Text adventure like Zork** - Create an array of rooms, each room can have a neighbor to the North, South, East, or West. You can implement this via an array or via pointers. Can also implement an inventory system, or a door-and-key system.
* **RPG Battle** - Implement a turn-based RPG battle type system. Each turn, you can attack, heal, use an item, etc. 
* **ASCII Maze** - Draw a room to the screen using text symbols. Allow the user to move around to navigate to an exit.
* **Word Search generator**
* **Checkers AI** - Give students pre-built checkers game, have them write the AI.
* **Flip card game**
* **Gameshows**
  * **Wheel of Fortune**
  * **Jeopardy**
  * **The Price is Right**

## Calculations

* **Quadratic Formula** - Calculate roots of a polynomial ([CS 134 Lab](https://github.com/Rachels-Courses/CS134-Programming-Fundamentals/blob/master/Assignments/Java%20Labs/Java%20Lab%201%20-%20Math%20Programs.md))
* **Square feet** - Calculate the total square footage of a building, adding each room's dimensions
* **Physics** - For some acceleration, initial velocity, and initial position, calculate an item's position over time.
* **Fractions** - Write a Fraction class and overload math operators to handle fraction math.
* **Distance** - Calculate the distance between two objects.
* **Moving at an angle** - Use the trig functions to decide how much to modify an (x, y) coordinate pair based on the character's speed and the angle they're currently heading.
* **Binary Math**
* **Derivatives and Integrals** (note to self: find my old numerical analysis textbook)
* **Road trip calculator**

### Discrete Math

* **Generate truth tables**
* **Generate proposition results**
* **Generate power sets**
* **Solve summations**
* **Solving the Josephus Game**


## Language/Conversions

* **Text <-> Morse Code converter**
* **Text <-> Braile converter**
* **Caesar ciphers, other ciphers**
* **Text <-> Binary**
* **Cryptanalysis** - analyzing which characters show up most often to try to guess at the conversion.

## Other

* **Programming language creation** - Write a programming language that has variables and can perform simple math and echos
* **Digital clock LED** - Have an array of LEDs that make up a clock (how it's laid out like an "8" and only certain ones light up for specific numbers.)

## Silly

* **Random poetry generator** - Based on some form ("Roses are red, violets are blue..."), generate random poetry with a list of nouns and adjectives.
* **Astrology generator** - Randomly generate astrology forecasts like "You will have horrible luck today."



--------------

# Data

* https://data.gov/
* https://www.tableau.com/learn/articles/free-public-data-sets
* https://github.com/nytimes/covid-19-data
* https://data.kcmo.org/
* https://www.ssa.gov/open/data/

