2 price=0
3 tax=0.091

9 rem mainmenu
10 for x=1 to 20: print " ": next x
11 poke 36879, 122
12 print "- taco bell -"
13 print "price: ";price
14 print "1. favorites"
15 print "2. new"
16 print "3. combos"
17 print "4. value menu"
18 print "5. order and quit"

20 input choice
21 if choice=1 then 100
22 if choice=2 then 200
23 if choice=3 then 300
24 if choice=4 then 400
25 if choice=5 then 500

100 poke 36879, 204
101 for x=1 to 20: print " ": next x
102 print "- favorites -"
103 print "1. chk quesadilla"
104 print "2. med baja blast"
105 print "3. crunchy taco"
106 input choice
107 if choice=1 then price=price+5.49
108 if choice=2 then price=price+1.00
109 if choice=3 then price=price+1.89
110 goto 10

200 for x=1 to 20: print " ": next x
201 poke 36879, 187
202 print "- new -"
203 print "1. cravings box"
204 print "2. chzy gordita box"
205 print "3. strawberry freeze"
206 input choice
207 if choice=1 then price=price+5.00
208 if choice=2 then price=price+9.99
209 if choice=3 then price=price+1.00
210 goto 10

300 for x=1 to 20: print " ": next x
301 poke 36879, 255
302 print "- combos -"
303 print "1. nacho bg combo"
304 print "2. 2 chk chalupa combo"
305 print "3. crunchwrap combo"
306 input choice
307 if choice=1 then price=price+9.29
308 if choice=2 then price=price+11.49
309 if choice=3 then price=price+8.79
310 goto 10
 
400 for x=1 to 20: print " ": next x
401 poke 36879, 157
402 print "- value menu -"
403 print "1. potato taco"
404 print "2. bean burrito"
405 print "3. cinnamon twists"
406 input choice
407 if choice=1 then price=price+1.00
408 if choice=2 then price=price+1.00
409 if choice=3 then price=price+1.00
410 goto 10

500 for x=1 to 20: print " ": next x
501 poke 36879,221
502 print "- checkout -"
503 print "total $";price
504 print "tax: ";tax*100;"%"
505 final = price + price * tax
506 print "final: $";final
507 print ""
508 print "thank you for"
509 print "choosing taco bell!"
