#include <iostream>
using namespace std;

int main()
{
  cout << "Enter # of kids: ";
  int kid_count;
  cin >> kid_count;

  cout << "Enter # of buses: ";
  int bus_count;
  cin >> bus_count;

  int buses_needed = kid_count / bus_count;

  cout << "You need " << buses_needed << " buses" << endl;
  cout << "Note: this does not round up. ;)" << endl;
  
  return 0;
}
