#include <iostream>
using namespace std;

int main()
{
  cout << "Enter number to count until: ";
  int max;
  cin >> max;

  int counter = 1;

  while ( counter <= max )
  {
    cout << counter << endl;
    counter = counter + 1;
  }
  
  return 0;
}
